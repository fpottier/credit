
 
(** Binary search, in an array [t], looking for the value [v] *)

let rec bsearch t v i j = 
  if i > j then -1 else begin
  let m = i + (j - i) / 2 in
  if v = t.(m) then 
    m
  else if v < t.(m) then 
    bsearch t v i (m-1)
  else
    bsearch t v (i+1) j
  end























(** Unit testing of the binary search implementation *)

let _ =
  let n = 1000000 in
  let t = Array.init n (fun i -> i) in
  let test v =
    let r = bsearch t v 0 (n-1) in
    let s = if r <> -1 then "found at " ^ string_of_int r else "not found" in
    Printf.printf "Searching for %d \t %s\n" v s;
    in
  test (-2);
  test (-1);
  test 0;
  test 1;
  test 2;
  test (n-1);
  test n; 
  test (n+1)









(** Exhaustive testing of the binary search implementation *)

let _ =
  let n = 100000 in
  let t = Array.init n (fun i -> i) in
  for i = 0 to n-1 do 
    assert (bsearch t i 0 (n-1) = i);
   (* if i mod 100 = 0 then
    Printf.printf "ok pour %d\n" i;  *)
  done




