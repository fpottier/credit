\nonstopmode
\input{prologue-amphis}
\usepackage{mathpartir}
\input{macros}
\input{local}
% Change the default blue.
\definecolor{myblue}{rgb}{0.137,0.466,0.741}
\setbeamercolor{structure}{fg=myblue}
% Hoare triples with colored pre- and postcondition.
\newcommand{\Jhoarecolor}[3]{\insister{\{#1\}}\;#2\;\insister{\{#3\}}}
\newcommand{\Jhoarevertcolor}[3]{\begin{linest}\insister{\{#1\}}\vspace{1pt}\\#2\vspace{1pt}\\ \insister{\{#3\}}\end{linest}}
\newcommand{\Hcommentcolor}[1]{&\text{\insister{-- #1}}}
\renewcommand{\Hassert}[1]{\insister{\{ #1 \}}}

% TEMPORARY pour ESOP:
% repenser aux règles de Derek
% parler un peu du modèle, interprétation sur des paires de heaps
% parallèle avec les shared borrows de Rust,
%   expliquer pourquoi nous n'avons pas besoin de lifetimes

% ----------------------------------------------------------------------------
% Page de titre.

\pgfdeclareimage[height=7.5mm]{logo}{images/INRIA-SCIENTIFIQUE-UK-CMJN}
\institute{\pgfuseimage{logo}}
\title{Temporary Read-Only Permissions for Separation Logic}
\newcommand{\nl}{\texorpdfstring{\\}{}}
\subtitle{Making Separation Logic's \nl Small Axioms \nl Smaller}
\author{Arthur Chargu\'eraud \and Fran\c cois Pottier}
\date{LTP meeting\\ Saclay, November 28, 2016}
\begin{document}
\frame{\titlepage}

% ----------------------------------------------------------------------------

\section{Motivation}

% ----------------------------------------------------------------------------

\begin{frame}{Separation and ownership}

  Separation logic (Reynolds, 2002) is about \emph{disjointness} of heap
  fragments.
  \begin{itemize}
  \item what ``we'' own, versus what ``others'' own.
  \end{itemize}

  Therefore, it is about \emph{unique ownership}.
  \begin{itemize}
  \item if we don't own a memory cell, we cannot write it, or even read it.
  \item if we own it, we can read \emph{and} write it.
  \end{itemize}

  We have either \emph{no permission} or \emph{read-write permission}.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The read and write axioms}

  The reasoning rule for writing requires and returns a unique permission:
  \[
  \inferrule[set]
  {}
  { \Jhoarecolor
    { \HPsingle{l}{v'} }
    { \LSetof{l}{v} }
    { \HPabs{y}\, \HPsingle{l}{v} }
  }
  \]
  So does the reasoning rule for reading:
  \[
  \inferrule*[lab=traditional read axiom]
  {}
  { \Jhoarecolor
    { \HPsingle{l}{v} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} \Hstar \HPsingle{l}{v} }
  }
  \]
  They are known as \emph{``small axioms''}.

  But \emph{are they as small as they could be?} ...

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Consequences}

  From memory cells and arrays, \\
  the dichotomy extends to \emph{user-defined data structures}.

    For every data structure, we have either \emph{no permission} or
    \emph{read-write permission}.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Consequences}

  Here a specification of an array concatenation function:

  \[
    \Jhoarevertcolor
      {\Hdataarray{a_1}{L_1} \Sc\Hstar \Hdataarray{a_2}{L_2} }
      { (\arrayconcat\,a_1\,a_2) }
      { \HPabs{a_3} \;
        \Hdataarray{a_3}{\OLAppof{L_1}{L_2}}
        \Sc\Hstar \Hdataarray{a_1}{L_1}
        \Sc\Hstar \Hdataarray{a_2}{L_2}
      }
  \]

  It is a bit \emph{noisy}.

  It also has several deeper drawbacks (see next slide).

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Our goal}

  We would like the specification to look like this instead:

  \[
    \Jhoarevertcolor
      { \HRO{\Hdataarray{a_1}{L_1}} \Sc\Hstar \HRO{\Hdataarray{a_2}{L_2}} }
      { (\arrayconcat\,a_1\,a_2) }
      { \HPabs{a_3} \; \Hdataarray{a_3}{\OLAppof{L_1}{L_2}}   }
  \]

  This would be \emph{more concise},

  require \emph{less bookkeeping},

  make it clear that \emph{the arrays are unmodified},

  and in fact \emph{would not require the arrays to be distinct}.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Our means}

  \twocolumns{5cm}{5cm}{

  For this purpose, we introduce \emph{temporary read-only permissions}.

  % $\HRO{H}$ describes the same heap fragment as $H$,
  % but gives \emph{read access} only.

  \vspace{33mm}

  Thank you for your attention.

  }{

  \includegraphics[height=.5\textheight]{images/access.jpg}

  }

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Remboursez!}

  What!?

  \pause
  Couldn't one view $\HRO\cdot$ as \emph{syntactic sugar}?
  \begin{itemize}
  \item No.
  \end{itemize}
  \pause
  Couldn't one express this using \emph{fractional permissions}?
  \begin{itemize}
  \item Yes. More heavily.
  \end{itemize}
  \pause
  Isn't the metatheory of $\HRO\cdot$ \emph{very simple}?
  \begin{itemize}
  \item Yes, it is. If and once you get it right. That's the point!
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\section{More Motivation}

% ----------------------------------------------------------------------------

\begin{frame}{The sugar hypothesis}

  \begin{center}
  % \includegraphics[height=.35\textheight]{images/sugarbadfeatured.png}
  \includegraphics[height=.75\textheight]{images/sugarbadpinterest.png}
  \end{center}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The sugar hypothesis}

  Could the Hoare triple:
  \[\Jhoarecolor{\HRO{H_1} \Hstar H_2}tQ\]
  be syntactic sugar for:
  \[\Jhoarecolor{H_1 \Hstar H_2}t{H_1 \Hstar Q}\]
  \begin{center}
  \LARGE?
  \end{center}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Sugar does not reduce work} % pressure

  Sugar reduces \emph{apparent} redundancy in specifications,

  but has no effect on the proof obligations,

  so \emph{does not reduce} redundancy and bookkeeping in proofs.

  If we must prove this:

  \[\Jhoarecolor{H_1 \Hstar H_2}t{H_1 \Hstar Q}\]

  then we must work to ensure and argue that the permission $H_1$ is returned.

  If ``RO'' was native,
  proving $\Jhoarecolor{\HRO{H_1} \Hstar H_2}tQ$
  would require no such work.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Sugar does not allow aliasing}

  If ``RO'' is sugar, then this specification requires $a_1$ and $a_2$ to be \emph{disjoint arrays}:
  \[\begin{array}{l}
    \Jhoarevertcolor
      { \HRO{\Hdataarray{a_1}{L_1}} \Sc\Hstar \HRO{\Hdataarray{a_2}{L_2}} }
      { (\arrayconcat\,a_1\,a_2) }
      { \HPabs{a_3} \; \Hdataarray{a_3}{\OLAppof{L_1}{L_2}}   }
  \end{array}\]
  As a result, we must prove \emph{another specification} to allow aliasing:
  \[\begin{array}{@{\kern8mm}l}
    \Jhoarevertcolor
      {\Hdataarray{a}{L} }
      { (\arrayconcat\,a\,a) }
      { \HPabs{a_3} \;
        \Hdataarray{a_3}{\OLAppof{L}{L}}
        \Sc\Hstar \Hdataarray{a}{L}
      }
  \end{array}\]
  \emph{Duplicate work} for us; \emph{increased complication} for the user.

  If ``RO'' was native and duplicable,
  the first spec above would allow aliasing.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Sugar is deceptive}

  A read-only function admits an ``RO'' specification. % Good, it seems.
  % Actually, this spec is subject to amnesia; see later.
  \[
    \Jhoarecolor
      {\HRO{\Htable{h}{M}}}
      { (\mathit{population}\;h) }
      { \HPabs{y} \, \HPprop{y = \F{card}\,M} }
  \]

  If ``RO'' is sugar,
  a function that \emph{can} have an effect
  \emph{also} admits an ``RO'' spec. % Surprising!
  \[
    \Jhoarecolor
      {\HRO{\Htable{h}{M}}}
      { (\mathit{resize}\;h) }
      { \HPabs{()} \, \HPprop{} }
  \]

  An ``RO'' specification, interpreted as sugar, does \emph{not} mean ``read-only''.

  Such sugar, if adopted, should use another keyword, e.g., \textbf{preserves}.

  If ``RO'' was native, $\mathit{resize}$ would not admit the second spec above.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Sugar causes amnesia and weakness}

  Suppose $\mathit{population}$ has this ``RO'' specification:
  \[
    \Jhoarecolor
      {\HRO{\Htable{h}{M}}}
      { (\mathit{population}\;h) }
      { \HPabs{y} \, \HPprop{y = \F{card}\,M} }
  \]

  Suppose a hash table is a mutable record
  whose $\mathit{data}$ field points to an array:
  \[
  \begin{array}[t]{l}
  \Htable{h}{M} \eqdef \nlq
  \HPexinameof{a} { \HPexinameof{L} {(
    \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots
  )}}
  \end{array}
  \]

  Suppose there is an operation $\mathit{foo}$ on hash tables:

  \[\begin{array}{l@{\qquad}l}
    \Llettoplevel{\mathit{foo}\;h}{              \nlq
      \Llet{d}{h.\mathit{data}}
        \Hcomment{read the address of the array} \nlq
      \Llet{p}{\mathit{population}\;h}
        \Hcomment{call $\mathit{population}$}    \nlq
      \ldots
    }
  \end{array}\]

  If ``RO'' is sugar, then the proof of $\mathit{foo}$ runs into a problem...

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Sugar causes amnesia and weakness}

  Reasoning about $\mathit{foo}$ might go like this:
  \[\begin{array}{l@{\quad}l@{\quad}l}
  \numm1 \Llettoplevel{\mathit{foo}\;h}{                   \\
  \numq2   \Hassert{ \Htable{h}{M} }
           \Hcomment{$\mathit{foo}$'s precondition}        \\
      % \forall a.\forall L.
  \numq3    \Hassert{ \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots }
        \Hcomment{by unfolding}                       \\
  %      \Hcomment{$a, L$ fresh auxiliary variables}   \\
  \numq4    \Llet{d}{h.\mathit{data}}                       \\
  \numq5    \Hassert{ \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots \Hstar \HPprop{d=a} }
        \Hcomment{by reading}                         \\
  \numq6    \Hassert{ \Htable{h}{M} \Hstar \HPprop{d=a} }
        \Hcomment{by folding}                         \\
  \numq7    \Llet{p}{\mathit{population}\;h}
        \Hcomment{we \insister{have} to fold}         \\
  \numq8    \Hassert{ \Htable{h}{M} \Hstar \HPprop{d=a} \Hstar \HPprop{p = \#\!M} }
                                                      \\
  \numq9    \ldots
  }
  \end{array}\]
  At line~8, the equation $d = a$ is useless.

  We have \emph{forgotten} what $d$ represents, and \emph{lost the benefit} of the read at line~4.

  With ``RO'' as sugar,
  the specification of $\mathit{population}$ is \emph{weaker} than it seems.

  If ``RO'' was native, there would be a way around this problem. (Details omitted.)

\end{frame}

% ----------------------------------------------------------------------------

\section{Separation Logic with Read-Only Permissions}

% ----------------------------------------------------------------------------

\begin{frame}{Permissions}

  Permissions are as follows:
  \[\begin{array}{rrl}
  H & := &
  \HPprop{P} \mid
  \HPsingle{l}{v} \mid
  \Hstarof{H_1}{H_2} \mid
  \HPorof{H_1}{H_2} \mid
  %\HPandof{H_1}{H_2} \mid
  \HPexinameof{x}{H} \mid
  \insister{\HRO{H}}
  \end{array}\]

  \emph{Every} permission~$H$ has a read-only form $\HRO{H}$.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Properties of RO}

  RO is well-behaved:
  \input{ro}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{A new read axiom}

  The traditional read axiom:
  \[
  \inferrule*[lab=traditional read axiom]{}
  { \Jhoarecolor
    { \HPsingle{l}{v} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} \Hstar \HPsingle{l}{v} } }
  \]

  is replaced with a ``smaller'' axiom:
  \[
  \inferrule*[lab=new read axiom]{}
  { \Jhoarecolor
    { \HRO{\HPsingle{l}{v}} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} } }
  \]

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{A new frame rule}

  The traditional frame rule
  is subsumed by a new ``\rofr'':
  \begin{mathpar}
  \inferrule*[lab=frame rule]
    { \Jhoare{H}{t}{Q} \\ \HNormal{H'} }
    { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }

  \inferrule*[lab=\rofr]
    {  \Jhoare{H \insister{{} \Hstar \HRO{H'}}  }{t}{Q}  \\ \HNormal{H'} }
    { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }
  \end{mathpar}

  Upon entry into a block,
  $H'$ is \emph{temporarily replaced} with $\HRO{H'}$, \\
  and upon exit, \emph{magically re-appears}.

  The side condition ``$\HNormal{H'}$'' means roughly ``$H'$ has no RO components'',
  so \emph{$\HRO{H'}$ cannot escape} through $Q$.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{That's all, folks!}

  \begin{center}
    That's all there is to it!
  \end{center}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{That's all, folks!}

  The paper gives a simple \emph{model} that explains why the logic is sound.

  The proof is machine-checked.

  We believe that temporary read-only permissions sometimes help state \\ more
  \emph{concise}, \emph{accurate}, \emph{useful} specifications, and lead to
  simpler proofs.

  Possible future work: an implementation in CFML.

\end{frame}

\end{document}
