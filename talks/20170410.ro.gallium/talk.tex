\nonstopmode
\input{prologue-amphis}
\usepackage{mathpartir}
\input{macros}
\input{local}
\renewcommand{\HStarable}[2]{#1+#2\text{ is defined}}
\newcommand{\seqconcat}{\textit{append}}
\newcommand{\Hseq}[2] {\Hdata{#1}{\F{Seq}}{#2}}
\newcommand{\norm}[1]{\mathord\mid#1\mathord\mid}
\renewcommand{\mydoublevee}{\mathbin{{\vee}\mkern-10mu{\vee}}}
% Change the default blue.
\definecolor{myblue}{rgb}{0.137,0.466,0.741}
\setbeamercolor{structure}{fg=myblue}
% Hoare triples with colored pre- and postcondition.
\newcommand{\Jhoarecolor}[3]{\insister{\{#1\}}\;#2\;\insister{\{#3\}}}
\newcommand{\Jhoarevertcolor}[3]{\begin{linest}\insister{\{#1\}}\vspace{1pt}\\#2\vspace{1pt}\\ \insister{\{#3\}}\end{linest}}
\newcommand{\Hcommentcolor}[1]{&\text{\insister{-- #1}}}
\renewcommand{\Hassert}[1]{\insister{\{ #1 \}}}
% tikz
\usepackage{tikz}
\usetikzlibrary{shapes,positioning}
\tikzstyle{local}=[
  remember picture,
  every edge/.append style = {->, very thick, >=stealth, RedViolet},
  % every path/.append style = {->, very thick, >=stealth, RedViolet},
  every node/.append style = {text badly centered, minimum height=8mm, font=\bfseries, fill=RedViolet!15, thick, draw, rounded corners=.5mm},
]
\newcommand{\here}[1]{\tikz[remember picture] \node [] (#1) {};} % \coordinate
% \tikzbox{label}{content} creates a TikZ box
\newcommand{\tikzbox}[2]{\tikz[baseline,remember picture]{\node[anchor=base, inner sep=0, outer sep=0] (#1) {#2};}}

% TEMPORARY:

% parallèle avec les shared borrows de Rust,
%   expliquer pourquoi nous n'avons pas besoin de lifetimes, ou plutôt pourquoi eux en ont besoin
%   expliquer que nos permissions sont duplicables mais non pas persistantes (once true, forever true)
%     et en particulier ne peuvent pas être capturées par une clôture
%                       ni transmises à un autre thread
% noter dès le début que le cadre est séquentiel
% mentionner l'extension à la concurrence
%   notre RO reste sûr avec concurrence structurée, mais la preuve ne passe plus
%   et RO ne passe pas à la concurrence non structurée
% mentionner l'idée de "const" plus tôt
%   parler de "const" en Mezzo dans le future work?
% éviter "h" versus "H" qui est difficile à prononcer
% comment prononcer "RO"?

% TEMPORARY remarques d'Arthur

% TEMPORARY add more images, if possible

% ----------------------------------------------------------------------------
% Page de titre.

\pgfdeclareimage[height=7.5mm]{logo}{images/INRIA-SCIENTIFIQUE-UK-CMJN}
\institute{\pgfuseimage{logo}}
\title{Temporary Read-Only Permissions for Separation Logic}
\newcommand{\nl}{\texorpdfstring{\\}{}}
\subtitle{Making Separation Logic's \nl Small Axioms \nl Smaller}
\author{Arthur Chargu\'eraud \and Fran\c cois Pottier}
\date{Gallium seminar\\ Paris, April 10, 2017}
\begin{document}
\frame{\titlepage}

% ----------------------------------------------------------------------------

% \section{Motivation}

% ----------------------------------------------------------------------------

\begin{frame}{Separation Logic: to own, or not to own}

  % SL is a discipline for reasoning about programs.

  % \begin{flushright}
  % \textit{--- To own, or not to own, that is the question.}
  % \end{flushright}

  Separation Logic (Reynolds, 2002) is about \emph{disjointness} of heap
  fragments.
  \begin{itemize}
  \item what ``we'' own, versus what ``others'' own.
  \end{itemize}

  Therefore, it is about \emph{unique ownership}. A dichotomy arises:
  \begin{itemize}
  \item Of every memory cell, \emph{either} we have ownership, \emph{or} we don't.
        % we have either \emph{no permission} or \emph{full permission}.
  \item If we do, then we can \emph{read and write} this cell.
  \item If we don't, then we can \emph{neither write nor even read} this cell.
  \end{itemize}

  From memory cells (and arrays), this dichotomy extends to data structures:
  \begin{quote}
    To a (user-defined) data structure, \\
    either we have \emph{no access at all},
    or we have \emph{full read-write access}.
  \end{quote}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Separation Logic's read and write axioms}

  % The reasoning rules for reading and writing both require (and return) a
  % unique permission, represented by a ``points-to'' assertion.

  The reasoning rule for writing a cell requires (and returns) a full permission:
  \[
  \inferrule[set]
  {}
  { \Jhoarecolor
    { \HPsingle{l}{v'} }
    { \LSetof{l}{v} }
    { \HPabs{y}\, \HPsingle{l}{v} }
  }
  \]
  So does the reasoning rule for reading a cell:\here{perm}
  \[
  \inferrule*[lab=traditional read axiom]
  {}
  { \Jhoarecolor
    { \HPsingle{l}{v} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} \Hstar \HPsingle{l}{v} }
  }
  \]
  They are known as \emph{``small axioms''}, because they require minimum permission.
  % because they say nothing about the rest of the heap;
  % that is the job of the frame rule.
  \begin{tikzpicture}[overlay, style = local]
  \only<2>{%
    \node [right = 3mm of perm, text width = 45mm] { terminology: \hbox{``permission'' = ``assertion''} } ;
  }
  \end{tikzpicture}
  %
  \onslide<3>{%

  But are they as small as they could be?

  Isn't it excessive for reading to require a full permission?

  Are there adverse consequences of working with such coarse permissions?

  }

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The problem}

  Suppose we are implementing an abstract data type of (mutable) sequences.

  Here is a typical specification of sequence concatenation:
  % Noter que \Hseq{s}{L} est juste une notation agréable
  % pour l'application d'un prédicat abstrait à deux arguments.
  \[
    \Jhoarevertcolor
      {\Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2} }
      { (\seqconcat\,s_1\,s_2) }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L_1}{L_2}}
        \Sc\Hstar \Hseq{s_1}{L_1}
        \Sc\Hstar \Hseq{s_2}{L_2}
      }
  \]
  \pause
  Although correct, this style of specification can be criticized on several grounds:
  \begin{itemize}
  \item
  It is a bit \emph{noisy}.
  \item
  It requires the permissions
  $\Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2}$
  to be \\ \emph{threaded throughout the proof}
  of \seqconcat{}.
  % so as to prove that $s_1$ and $s_2$ are indeed left unmodified.
  \item
  It actually \emph{does not guarantee that $s_1$ and $s_2$ are unmodified}. \textcolor{RedViolet}{--- (next slide)}
  \item
  It requires $s_1$ and $s_2$ to be \emph{distinct} data structures. \textcolor{RedViolet}{--- (slide after next)}
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The problem, facet 3: temporary modifications are not forbidden}

  Repeating ``$\Hseq{s}{L}$'' in the pre- and postcondition can be deceiving.

  This does \emph{not} forbid changes to the \emph{concrete} data structure in memory.

  % upon completion $s$ still represents the abstract sequence $L$,

  Here is a function that really just reads the data structure:
  \[
    \Jhoarecolor
      {\Hseq{s}{L}}
      { (\mathit{length}\;s) }
      { \HPabs{y} \, \Hseq{s}{L} \Sc\Hstar \HPprop{\, y = \norm{L} \,} }
  \]
  \pause
  And a function that actually modifies the data structure:
  \[
    \Jhoarecolor
      {\Hseq{s}{L} \Sc\Hstar \HPprop{\, \norm{L} \leq n \,} }
      { (\mathit{resize}\;s\,n) }
      { \HPabs{()} \, \Hseq{s}{L} }
  \]

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The problem, facet 4: sharing is not permitted}

  The specification of \seqconcat{} requires $s_1$ and $s_2$ to be \emph{distinct} data structures:
  \[
    \Jhoarevertcolor
      {\Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2} }
      { (\seqconcat\,s_1\,s_2) }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L_1}{L_2}}
        \Sc\Hstar \Hseq{s_1}{L_1}
        \Sc\Hstar \Hseq{s_2}{L_2}
      }
  \]
  Indeed, $\Hseq{s}{L} \Sc\Hstar \Hseq{s}{L}$ is equivalent to $\mathit{false}$.

  \pause
  As a result, to allow sharing, we must establish \emph{another specification}:
  \[\begin{array}{@{\kern8mm}l}
    \Jhoarevertcolor
      {\Hseq{s}{L} }
      { (\seqconcat\,s\,s) }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L}{L}}
        \Sc\Hstar \Hseq{s}{L}
      }
  \end{array}\]
  \emph{Duplicate work} for us. \emph{Increased complication} and/or duplicate work for the user.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Fractional permissions to the rescue...?}

  Could sequence concatenation be specified as follows in Concurrent SL?
  \[
    \forall \pi_1,\pi_2.\;
    \Jhoarevertcolor
      { \pi_1\cdot(\Hseq{s_1}{L_1}) \Sc\Hstar \pi_2\cdot(\Hseq{s_2}{L_2}) }
      { (\seqconcat\,s_1\,s_2) }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L_1}{L_2}}
        \Sc\Hstar \pi_1\cdot(\Hseq{s_1}{L_1})
        \Sc\Hstar \pi_2\cdot(\Hseq{s_2}{L_2})
      }
  \]
  Yes, if the logic allows \emph{scaling}, $\pi\cdot H$.
  This requires existential quantification to be restricted so as to be precise (\href{http://dx.doi.org/10.1145/1749608.1749611}{Boyland, 2010}).

  Without scaling, one must define $\Hseq{s}{\pi\;L}$.
  % Plan ahead and parameterize Seq with a fraction.

  \pause
  This addresses problem facets 3 and 4,
  \begin{itemize}
  \item but is still \emph{noisy},
  \item and still requires careful \emph{splitting, threading, and joining} of permissions.
  \end{itemize}

  ``Hiding the fractions'' (\href{http://research.microsoft.com/en-us/um/people/leino/papers/krml225.pdf}{Heule et al, 2013})
  is cool but requires yet more machinery.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{In this paper}

  We propose a solution that is:
  \begin{itemize}
  \item \emph{not as powerful} as fractional permissions (or other share algebras),
  \item but significantly \emph{simpler}.
  \end{itemize}
  Our contributions:
  \begin{itemize}
  \item introducing a \emph{read-only modality}, $\HROname$. \\
  $\HRO{H}$ represents \emph{temporary} read-only access to the memory governed by $H$.
  \item finding \emph{simple and sound reasoning rules}
  for $\HROname$.
  \item proposing \emph{a model} that justifies these rules.
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\section{Some Intuition}

% ----------------------------------------------------------------------------

\begin{frame}{Our solution}

  We would like the specification of \seqconcat{} to look like this:
  \[
    \Jhoarevertcolor
      { \HRO{\Hseq{s_1}{L_1}} \Sc\Hstar \HRO{\Hseq{s_2}{L_2}} }
      { (\seqconcat\,s_1\,s_2) }
      { \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L_1}{L_2}}   }
  \]
  \pause
  Compared with the earlier specification based on unique read-write permissions,
  \begin{itemize}
  \item
  this specification is \emph{more concise},
  \item
  imposes \emph{fewer proof obligations},
  \item
  makes it clear that \emph{the data structures cannot be modified} by \seqconcat,
  \item
  and \emph{does not require $s_1$ and $s_2$ to be distinct}. \textcolor{RedViolet}{--- (next slide)}
  \item
  Furthermore, this spec \emph{implies} the earlier spec. \textcolor{RedViolet}{--- (slide after next)}
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Our solution, facet 4: sharing is permitted}

  The top Hoare triple is the new spec of \seqconcat, \\
  where $s_1$ and $s_2$ are instantiated with~$s$.
  \[
  \inferrule*[right=consequence]{
    \Jhoarevertcolor
      { \here{rodup} \HRO{\Hseq{s}{L}} \Sc\Hstar \HRO{\Hseq{s}{L}} \here{} }
      { (\seqconcat\,s\,s) }
      { \here{} \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L}{L}} \here{} }
  }{
    \Jhoarevertcolor
      { \here{entry} \HRO{\Hseq{s}{L}} \here{} }
      { (\seqconcat\,s\,s) }
      { \here{} \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L}{L}} \here{} }
  }\]
  The bottom triple states
  that, with read-only access to $s$,
  $\seqconcat\,s\,s$ is \emph{permitted}.
  \begin{tikzpicture}[overlay, style = local]
  \only<2>{%
    \draw (entry) edge[bend left=20] (rodup) ;
    \node [left = 0mm of entry, text width = 30mm, xshift=-2mm, yshift=-6mm] { read-only permissions are duplicable };
  }
  \end{tikzpicture}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Our solution, facet 5: the earlier specification can be derived}

  The Hoare triple at the top is the new spec of \seqconcat.
  \[
  \inferrule*[right=consequence]{
    \Jhoarevertcolor
      { \here{ro2} \HRO{\Hseq{s_1}{L_1}} \Sc\Hstar \HRO{\Hseq{s_2}{L_2}} \here{exit0} }
      { (\seqconcat\,s_1\,s_2) }
      { \here{} \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L_1}{L_2}} \here{exit1} }
  }{
  \inferrule*[Right=read-only frame]{
    \Jhoarevertcolor
      { \here{ro1} \HRO{\Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2}} \here{} }
      { (\seqconcat\,s_1\,s_2) }
      { \here{} \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L_1}{L_2}} \here{exit2} }
  }{
    \Jhoarevertcolor
      { \here{entry} \Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2} \here{} }
      { (\seqconcat\,s_1\,s_2) }
      { \here{} \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L_1}{L_2}}
        \Sc\Hstar \Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2} \here{exit3} }
  }}
  \]
  The triple at the bottom is the earlier spec of \seqconcat.
  \begin{tikzpicture}[overlay, style = local]
  \only<2>{%
    \draw (entry) edge[bend left=20] (ro1) ;
    \node [above left = 10mm and -10mm of entry, text width = 20mm] { permission becomes read-only };
  }
  \only<3>{%
    \draw (ro1) edge[bend left=20] (ro2) ;
    \node [above left = 10mm and -10mm of entry, text width = 20mm] { read-only permission is weakened };
  }
  \only<4>{%
    \draw (exit0) edge[bend left=20] (exit1) ;
    \draw (exit1) edge[bend left=20] (exit2) ;
    \node [above left = 10mm and -85mm of entry, text width = 20mm] { \seqconcat{} is called };
  }
  \only<5>{%
    \draw (exit2) edge[bend left=20] (exit3) ;
    \node [above left = 5mm and -85mm of entry, text width = 20mm] { read-write permission magically re-appears };
  }
  \end{tikzpicture}

\end{frame}

% ----------------------------------------------------------------------------

\section{Reasoning Rules}

% ----------------------------------------------------------------------------

\begin{frame}{Permissions}

  The syntax of permissions is as follows:
  \[\begin{array}{rrl}
  H & := &
  \HPprop{P} \mid
  \HPsingle{l}{v} \mid
  \Hstarof{H_1}{H_2} \mid
  \HPorof{H_1}{H_2} \mid
  %\HPandof{H_1}{H_2} \mid
  \HPexinameof{x}{H} \mid
  \insister{\HRO{H}}
  \end{array}\]

  \emph{Every} permission~$H$ has a read-only form $\HRO{H}$.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Properties of RO}

  Read-only access to a data structure entails read-only access to its parts:
  \[\begin{array}{rcl@{\qquad}l}
  \HRO{\Hstarof{H_1}{H_2}} & \Himpl & \HRO{H_1} \Hstar \HRO{H_2}
  & \text{(the reverse is false)}
  \end{array}\]
%
  Read-only permissions are duplicable (therefore, no need to count them!):
  \[\begin{array}{rcl@{\qquad}l}
  % Duplicability.
  \HRO{H} & = & \HRO{H} \Hstar \HRO{H} \hbox to 24mm{}
  \end{array}\]
%
  Read-only permissions are generally well-behaved:
  \[\begin{array}{rcl@{\qquad}l}
  \HRO{\HPprop{P}} & = & \HPprop{P} \\

  \HRO{\HPorof{H_1}{H_2}} & = & \HPorof{\HRO{H_1}}{\HRO{H_2}} \\

  % \HRO{\HPandof{H_1}{H_2}} & = & \HPandof{\HRO{H_1}}{\HRO{H_2}} \\

  \HRO{\HPexists{x}{H}} & = & \HPexists{x}{\HRO{H}} \\

  % Idempotence.
  \HRO{\HRO{H}} & = & \HRO{H} \\

  % Covariance.
  \HRO{H} & \Himpl & \HRO{H'} & \text{if $H \Sc\Himpl H'$ \hbox to 15mm{}} \\

  \end{array}\]

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{A new read axiom}

  The traditional read axiom:
  \[
  \inferrule*[lab=traditional read axiom]{}
  { \Jhoarecolor
    { \HPsingle{l}{v} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} \Hstar \HPsingle{l}{v} } }
  \]
  is replaced with a ``smaller'' axiom:
  \[
  \inferrule*[lab=new read axiom]{}
  { \Jhoarecolor
    { \HRO{\HPsingle{l}{v}} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} } }
  \]
  The traditional axiom can be derived from the new axiom.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{A new frame rule}

  The traditional frame rule
  is subsumed by a new ``\rofr'':
  \begin{mathpar}
  \inferrule*[lab=frame rule]
    { \Jhoare{H}{t}{Q} \\ \HNormal{H'} }
    { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }

  \inferrule*[lab=\rofr]
    {  \Jhoare{H \insister{{} \Hstar \HRO{H'}}  }{t}{Q}  \\ \HNormal{H'} }
    { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }
  \end{mathpar}

  This says:
  upon entry into a block,
  $H'$ is \emph{temporarily replaced} with $\HRO{H'}$, \\
  and upon exit, \emph{magically re-appears}.

  The side condition $\HNormal{H'}$ means roughly that $H'$ has no RO components.

  This means that \emph{read-only permissions cannot be framed out}.% (by either rule above).

  If they could, the \rofr would clearly be unsound. (Exercise!)
  % TEMPORARY prepare a backup slide with the solution
  % take t to be skip and Q to be RO(H')
  % then we get RO(H') * H' in the conclusion of the \rofr

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{How read-only permissions are used}

  No reasoning rule involves a triple whose postcondition contains $\HROname$.

  Read-only permissions always appear in \emph{preconditions}, never in postconditions.

  They are always \emph{passed down}, never returned.

  In fact, in the model, we will see that, in a triple $\Jhoare HtQ$:
  \begin{itemize}
  \item the postcondition applies only to \emph{some read-write fragment} of the final heap;
  \item the read-only part of the heap \emph{must be preserved} anyway, \\
        so there is no need for the postcondition to describe it.
        % It is as if every read-only permission was implicitly and automatically framed out.
        % Which is good, since we have disallowed explicit framing of read-only permissions.
  \end{itemize}

\end{frame}

% TEMPORARY prepare a backup slide with the frame-sequencing rule

% ----------------------------------------------------------------------------

\section{Model}

% ----------------------------------------------------------------------------

\begin{frame}{Memories \& heaps -- a simple model of access rights}

  % TEMPORARY possible drawing?

  A \emph{memory} is a finite map of locations to values.

  A \emph{heap}~$h$ is a pair of two \emph{disjoint} memories~$\Hf{h}$ and~$\Hr{h}$. % disjoint means domain-disjoint.
  \begin{itemize}
  \item $\Hf{h}$ represents the locations to which we have \emph{full access};
  \item $\Hr{h}$ represents the locations to which we have \emph{read-only access}.
  \end{itemize}

  An \emph{assertion}, or \emph{permission}, is a predicate over heaps (or: a
  set of heaps).

  % $\Hflatten{h}$ is the memory $\OMdisuniof{\Hf{h}\,}{\,\Hr{h}}$, where
  % access rights are forgotten.
  % -- hidden, as it is not central

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Heap composition \& separating conjunction}

  \begin{minipage}{0.45\linewidth}
  The combination of two heaps:
  \[
    \Hunionof{h_1}{h_2}
    =
    (\Hf{h_1} \mathbin{\tikzbox{uplus}{$\uplus$}} \Hf{h_2}
     \tikzbox{comma}{,}\;
     \Hr{h_1} \mathbin{\tikzbox{cup}{$\cup$}} \Hr{h_2})
  \]
  \end{minipage}
  \begin{minipage}{0.5\linewidth}
  \hfil\includegraphics[height=2cm]{images/composition.pdf}
  \end{minipage}

  \vspace{5mm}
  is defined only if:
  \begin{itemize}
  \item<2-> the read-write components $\Hf{h_1}$ and $\Hf{h_2}$ are \tikzbox{cond1}{\emph{disjoint}};
  \item<3-> the read-only components $\Hr{h_1}$ and $\Hr{h_2}$ \tikzbox{cond2}{\emph{agree} where they overlap;}
  \item<4-> the read-write component $\Hf{h_1}$ is \tikzbox{cond3}{\emph{disjoint} with the read-only component $\Hr{h_2}$,} and vice-versa.
  \end{itemize}
  \begin{tikzpicture}[overlay, style = local]
    \path[->, very thick, >=stealth, draw = RedViolet]<2>
    ([yshift=1mm]cond1.north) to [out = 45,in = -45, distance=10mm] ([yshift=-1mm]uplus.south) ;
    \path[->, very thick, >=stealth, draw = RedViolet]<3>
    ([yshift=1mm]cond2.north) to [out = 45,in = -90, distance=20mm] ([yshift=-1mm]cup.south) ;
    \path[->, very thick, >=stealth, draw = RedViolet]<4>
    ([yshift=1mm]cond3.east) to [out = 45,in = -45, distance=20mm] ([yshift=-1mm]comma.south) ;
  \end{tikzpicture}
  \onslide<5>{%
  With this in mind, \emph{separating conjunction} is interpreted as usual:
  \[\begin{array}{l}
  \Hstarof{H_1}{H_2} = \\ \qquad
  \Labs{h}{ \;
    \Texi{h_1 h_2} \;
    \; (\HStarable{h_1}{h_2}) \;\land \;
    h = \Hunionof{h_1}{h_2}
    \;\land \;H_1\,h_1
    \;\land \; H_2\,h_2
   }
  \end{array}\]}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The read-only modality}

  \begin{minipage}{0.45\linewidth}
  \
  \end{minipage}
  \begin{minipage}{.5\linewidth}
    \hfil\includegraphics[height=2cm]{images/ro.pdf}
  \end{minipage}

  $\HRO{H}$ is interpreted as follows:
  \[\begin{array}{l}
    \HRO{H} = \\ \qquad
    \Labs{h}{ \;
        \tikzbox{nothing}{$(\Hf{h} = \OMempty)$}
        \Sc\land
        \tikzbox{if}{$\Texi{h'}\; (\Hr{h} = \Hf{h'} \OMdisuni \Hr{h'}) \tikzbox{hold}{$\Sc\land H\,h'$}$}
    }
  \end{array}\]

  \onslide<2->{This means:}
  \begin{itemize}
  \item<2-> we have write access to \tikzbox{item1}{\emph{nothing}}.
  \item<3-> \tikzbox{item2}{\emph{if we had write access} to certain locations for which we have read access,} \\
            \tikzbox{item2b}{\emph{then} $H$ would hold.}
  \end{itemize}
  \begin{tikzpicture}[overlay, style = local]
    \path[->, very thick, >=stealth, draw = RedViolet]<2> ([yshift=1mm]item1.north) to [out = 90, in = -90, distance=10mm] (nothing.south) ;
    \path[->, very thick, >=stealth, draw = RedViolet]<3> ([yshift=1mm]item2.north) to [out = 90, in = -90, distance=10mm] (if.south) ;
    \path[->, very thick, >=stealth, draw = RedViolet]<3> ([xshift=1mm]item2b.east) to [out = 0, in = -90, distance=20mm] ([xshift=3mm]item2.east)
                                                                                    to [out = 90, in = -90, distance=10mm] ([yshift=-1mm]hold.south) ;
  \end{tikzpicture}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The rest of the connectives}

  \[\begin{array}{r@{\;\;}c@{\;\;}l}
  \HPprop{P} & = &
  \Labs{h}{ \; (\Hf{h} = \OMempty) \Sc\land (\Hr{h} = \OMempty) \Sc\land P }
  \nextrow{1}
  \HPsingle{l}{v} & = &
  \Labs{h}{ \; (\Hf{h} = \OMone{l}{v}) \Sc\land (\Hr{h} = \OMempty) }
  \nextrow{1}
  \HPorof{H_1}{H_2} & = &
  \Labs{h}{ \; H_1\,h \Sc\lor H_2\,h }
  \nextrow{1}
  %\HPandof{H_1}{H_2} & = &
  %\Labs{h}{ \; H_1\,h \Sc\land H_2\,h }
  %\nextrow{1}
  \HPexinameof{x}{H} & = &
  \Labs{h}{ \; \Texi{x}\; H\,h }
  \nextrow{5}
  \HNormalof{H} & = & \Tfor{h}{ \; H\,h \Sc\impl \Hr{h} = \OMempty }
  \end{array}\]

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Interpretation of triples}

  The meaning of the Hoare triple $\Jhoare{H}{t}{Q}$ is as follows:
  \[
  \Tfor{h_1 h_2}
  \left\{\begin{array}{l}
  \HStarable{h_1}{h_2} \\[1pt]
  H\,h_1
  \end{array}\right\}
  \impl\; \Texi{v h_1'}
  \left\{\begin{array}{l}
  \HStarable{h_1'}{h_2}
  \\[1pt]
  \Jredbigconfigof{ t }{ \,\Hflatten{h_1 \Hunion h_2} }{ v }{ \,\Hflatten{h_1' \Hunion h_2} }
  \\[1pt]
  \tikzbox{preservation}{$\Hr{h_1'} = \Hr{h_1}$}
  \\[1pt]
  \tikzbox{onrwsub}{$\Honrwsub{Q\,v}\,h_1'$}
  \end{array}\right\}
  \]
  %
  What's nonstandard?
  \begin{itemize}
  \item<2-> The read-only part of the heap must be \tikzbox{preserved}{\emph{preserved}}. % -- this is implicit.
  \item<3-> The postcondition describes only \tikzbox{only}{\emph{a read-write fragment of the final heap}}.
  \end{itemize}
  %
  \onslide<3>{%
  \[\begin{array}{l}
  \Honrwsub{H} = \\
  \qquad
  \lambda h.\,
  \exists h_1 h_2.\,
  (\HStarable{h_1}{h_2}) \Sc\land
           h = \Hunionof{h_1}{h_2}
  \Sc\land \Hr{h_1} = \OMempty
  \Sc\land H\,h_1
  \end{array}\]}
  \begin{tikzpicture}[overlay, style = local]
    \path[->, very thick, >=stealth, draw = RedViolet]<2> ([yshift=1mm]preserved.north) to [out = 135, in = 180, distance=10mm] ([xshift=-.5mm]preservation.west) ;
    % \path[->, very thick, >=stealth, draw = RedViolet]<3> ([yshift=1mm]only.north) to [out = 120, in = 270, distance=10mm] (onrwsub.south) ;
    \path[->, very thick, >=stealth, draw = RedViolet]<3> ([xshift=2mm]only.east) to [out = 0, in = 0, distance=10mm] ([xshift=1mm]onrwsub.east) ;
  \end{tikzpicture}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Soundness}

  \begin{theorem}
    With respect to this interpretation of triples,
    every reasoning rule is sound.
  \end{theorem}
  \begin{proof}
    ``Straightforward''. Machine-checked.
  \end{proof}

\end{frame}

% ----------------------------------------------------------------------------

% Conclusion.

\section{Conclusion}

\begin{frame}

  We propose:
  \begin{itemize}
  \item \emph{a simple extension} of Separation Logic with a read-only modality;
  \item \emph{a simple model} that explains why this is sound.
  \end{itemize}

  We believe that temporary read-only permissions sometimes help state \\ more
  \emph{concise}, \emph{accurate}, \emph{useful} specifications, and lead to
  simpler proofs.

  Possible future work: an implementation in CFML (Charguéraud).

\end{frame}

% Backup slides.

\input{amnesia}

\end{document}
