------------------------------------------------------------------------------

Q: since const permissions cannot be framed,
   when reasoning about a sequence (e1; e2),
   how can const permissions be transmitted from the entry point (before e1)
   to e2?

A: We have a modified sequencing rule with a built-in frame rule.
   (Backup slide.)
   [It is not restricted to "normal" permissions
   because the framed-out permission never appears in a postcondition.]

------------------------------------------------------------------------------

Q: what's the relationship with Rust's shared borrows?

A: Indeed, the type &T in Rust can be shared (it is "duplicable")
   and its lifetime is a lexical scope.
   So, there is a strong similarity, at least on the surface.

   However, the type &T is really sugar for &'a T
   That is, Rust's shared borrows are annotated with "lifetimes".
   Our system is simpler -- it doesn't need lifetimes.

   Rust is more expressive -- it allows many forms of concurrency --
   but also much more complicated.
   e.g. there are traits "Send" and "Sync" which indicate which types
        can safely be shared between threads
        but I couldn't figure out whether &T is Send / Sync

  I don't know what is the accepted (simplest) way of doing fork/join
  parallelism in Rust. Maybe "scoped threads":
    https://aturon.github.io/crossbeam-doc/crossbeam/struct.Scope.html
  I don't know why Rust needs lifetimes.

------------------------------------------------------------------------------

Q: Can you encode RO permissions into fractional permissions?
   Are fractional permissions more expressive than RO permissions?

A: For practical purposes, fractional permissions are more expressive.
   They work in the presence of unstructured concurrency
   (that is, threads and channels)
   whereas we believe that RO permissions are sound only for structured
   concurrency (that is, a parallel composition expression)
   and we have not even proved it.

   That said, from a formal point of view,
   encoding RO permissions into fractional permissions
   in a compositional manner
   does not seem entirely trivial.
   Apparently one needs to be able to *syntactically* split
   a precondition P into its read/write part and its read-only part,
   and that becomes complicated if (universal/existential) quantification
   over permissions is allowed in the logic
   (i.e., if the logic is higher-order).

------------------------------------------------------------------------------

Q: in a sequential setting, are RO permissions as expressive as
   fractional permissions?

A: roughly and informally, yes, it seems.

   Both allow temporary read-only shared state.

   One thing that fractional permissions allow,
   which temporary read-only permissions don't,
   is making a piece of state read-only forever.

------------------------------------------------------------------------------

Q: What is the point of RO permissions,
   considering that fractional permissions are more expressive?

A: RO permissions are less powerful, but simpler.
   - They can be used in proofs of sequential programs.
   - They can be used in teaching,
     as a step along the way, before
     teaching fractional permissions.
   - They might be useful in type systems inspired by separation logic;
     e.g., adding "const" to Mezzo

------------------------------------------------------------------------------

Q: Have you implemented RO permissions?
   How well do they work in practice?

A: Not yet implemented.
   We hope to implement them in CFML in the future,
   and we believe that they will be useful in proofs of sequential programs.
