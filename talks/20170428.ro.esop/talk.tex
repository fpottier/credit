\nonstopmode
\input{prologue-amphis}
\usepackage{mathpartir}
\input{macros}
\input{local}
\renewcommand{\HStarable}[2]{#1+#2\text{ is defined}}
\newcommand{\seqconcat}{\textit{append}}
\newcommand{\Hseq}[2] {\Hdata{#1}{\F{Seq}}{#2}}
\newcommand{\norm}[1]{\mathord\mid#1\mathord\mid}
\renewcommand{\mydoublevee}{\mathbin{{\vee}\mkern-10mu{\vee}}}
% Entailment:
\renewcommand{\Himpl}{\Vdash}
% Rename RO to const in the talk:
\renewcommand{\HROname}{const}
% Change the font of HNormal to italic, and use parentheses:
\renewcommand{\HNormal}[1]{\mathit{normal}(#1)}
% Change the default blue.
\definecolor{myblue}{rgb}{0.137,0.466,0.741}
\setbeamercolor{structure}{fg=myblue}
% Hoare triples with colored pre- and postcondition.
\newcommand{\Jhoarecolor}[3]{\insister{\{#1\}}\;#2\;\insister{\{#3\}}}
\newcommand{\Jhoarevertcolor}[3]{\begin{linest}\insister{\{#1\}}\vspace{1pt}\\#2\vspace{1pt}\\ \insister{\{#3\}}\end{linest}}
\newcommand{\Hcommentcolor}[1]{&\text{\insister{-- #1}}}
\renewcommand{\Hassert}[1]{\insister{\{ #1 \}}}
% tikz
\usepackage{tikz}
\usetikzlibrary{shapes,positioning}
\tikzstyle{local}=[
  remember picture,
  every edge/.append style = {->, very thick, >=stealth, RedViolet},
  % every path/.append style = {->, very thick, >=stealth, RedViolet},
  every node/.append style = {text badly centered, minimum height=8mm, font=\bfseries, fill=RedViolet!15, thick, draw, rounded corners=.5mm},
]
\newcommand{\here}[1]{\tikz[remember picture] \node [] (#1) {};} % \coordinate
% \tikzbox{label}{content} creates a TikZ box
\newcommand{\tikzbox}[2]{\tikz[baseline,remember picture]{\node[anchor=base, inner sep=0, outer sep=0] (#1) {#2};}}

% ----------------------------------------------------------------------------
% Page de titre.

\pgfdeclareimage[height=7.5mm]{logo}{images/INRIA-SCIENTIFIQUE-UK-CMJN}
\institute{\pgfuseimage{logo}}
\title{Temporary Read-Only Permissions for Separation Logic}
\newcommand{\nl}{\texorpdfstring{\\}{}}
% \subtitle{Making Separation Logic's \nl Small Axioms \nl Smaller}
\author{Arthur Chargu\'eraud \and Fran\c cois Pottier\\[2mm]
        (talk delivered by \textbf{Armaël Guéneau})}
\date{ESOP 2017\\ Uppsala, April 28, 2017}
\begin{document}
\frame{\titlepage}

% ----------------------------------------------------------------------------

% \section{Motivation}

% ----------------------------------------------------------------------------

\begin{frame}{Separation Logic: to own, or not to own}

  The setting is basic (sequential) Separation Logic.

  % SL (Reynolds, 2002) is a discipline for reasoning about programs.

  Separation Logic is about \emph{disjointness}, % of heap fragments,
  therefore about \emph{unique ownership}.

  A dichotomy arises:
  \begin{quote}
    To every memory cell, array, or user-defined data structure, \\
    either we have \emph{no access at all},
    or we have \emph{full read-write access}.
  \end{quote}

  \pause

  This is visible in the read and write axioms,
  which both need a \emph{full permission}:\here{perm}
  \begin{mathpar}
  \inferrule[set]
  {}
  { \Jhoarecolor
    { \HPsingle{l}{v'} }
    { \LSetof{l}{v} }
    { \HPabs{y}\, \HPsingle{l}{v} }
  }

  \inferrule*[lab=traditional read axiom]
  {}
  { \Jhoarecolor
    { \HPsingle{l}{v} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} \Hstar \HPsingle{l}{v} }
  }
  \end{mathpar}
  % \pause
  % \begin{tikzpicture}[overlay, style = local]
  % \node [left = 3mm of perm, text width = 45mm, yshift = -20mm] { terminology: \hbox{``permission'' = ``assertion''} } ;
  % \end{tikzpicture}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The problem}

  Suppose we are implementing an abstract data type of mutable sequences.

  An abstract predicate $\Hseq{s}{L}$ represents the \emph{unique ownership}
  of a sequence. % at address $s$, containing the list of elements $L$.

  Here is a typical specification of sequence concatenation:
  % Noter que \Hseq{s}{L} est juste une notation agréable
  % pour l'application d'un prédicat abstrait à deux arguments.
  \[
    \Jhoarevertcolor
      {\Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2} }
      { (\seqconcat\,s_1\,s_2) }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L_1}{L_2}}
        \Sc\Hstar \Hseq{s_1}{L_1}
        \Sc\Hstar \Hseq{s_2}{L_2}
      }
  \]
  \pause
  Although correct, this style of specification can be criticized on several grounds:
  \begin{itemize}
  \item
  It is a bit \emph{noisy}.
  \item
  It requires the permissions
  $\Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2}$
  to be \\ \emph{threaded through the proof}
  of \seqconcat{}.
  % so as to prove that $s_1$ and $s_2$ are indeed left unmodified.
  \item
  It actually \emph{does not guarantee that $s_1$ and $s_2$ are unmodified} in memory.
  \item
  It requires $s_1$ and $s_2$ to be \emph{distinct} data structures. \textcolor{RedViolet}{--- (next slide)}
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{The problem, focus: sharing is not permitted}

  This specification requires $s_1$ and $s_2$ to be \emph{distinct} (disjoint)
  data structures:
  \[
    \Jhoarevertcolor
      {\Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2} }
      { (\seqconcat\,s_1\,s_2) }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L_1}{L_2}}
        \Sc\Hstar \Hseq{s_1}{L_1}
        \Sc\Hstar \Hseq{s_2}{L_2}
      }
  \]
  $(\seqconcat\,s\,s)$ requires
  $\Hseq{s}{L} \Sc\Hstar \Hseq{s}{L}$,
  which the client cannot produce.

  % $\Hseq{s}{L}$ does not entail $\Hseq{s}{L} \Sc\Hstar \Hseq{s}{L}$.

  % This separating conjunction is likely equivalent to $\mathit{false}$.
  % Anyway, regardless of whether it is satisfiable or unsatisfiable,
  % the client does not have access to a duplication axiom for $\Hseq{s}{L}$.

  \pause
  To allow $(\seqconcat\,s\,s)$, we must establish \emph{another specification}:
  \[\begin{array}{@{\kern8mm}l}
    \Jhoarevertcolor
      {\Hseq{s}{L} }
      { (\seqconcat\,s\,s) }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L}{L}}
        \Sc\Hstar \Hseq{s}{L}
      }
  \end{array}\]
  \emph{Duplicate work} and \emph{increased complication} for us and for our
  clients.
  % possibly several levels down the line,
  % as each client might again need to publish multiple specs
  % of certain operations

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Fractional permissions to the rescue...?}

  In (some) Concurrent SLs,
  sequence concatenation can be specified as follows:
  \[
    \forall \pi_1,\pi_2.\;
    \Jhoarevertcolor
      { \pi_1\cdot(\Hseq{s_1}{L_1}) \Sc\Hstar \pi_2\cdot(\Hseq{s_2}{L_2}) }
      { (\seqconcat\,s_1\,s_2) }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L_1}{L_2}}
        \Sc\Hstar \pi_1\cdot(\Hseq{s_1}{L_1})
        \Sc\Hstar \pi_2\cdot(\Hseq{s_2}{L_2})
      }
  \]
  We \emph{scale} an assertion by a fraction: $\pi\cdot H$.
  % This requires existential quantification to be restricted so as to be precise
  % (\href{http://dx.doi.org/10.1145/1749608.1749611}{Boyland, 2010}).

  % Without scaling, one must define $\Hseq{s}{\pi\;L}$.
  % Plan ahead and parameterize Seq with a fraction.

  \pause
  This addresses the main aspects of the problem, % (points 3 and 4)
  \begin{itemize}
  \item but is still \emph{noisy},
  \item might seem a bit frightening to non-experts,
  \item and still requires careful \emph{splitting, threading, and joining} of
    permissions.
  \item ``Hiding'' fractions (Heule et al, 2013) adds another layer of sophistication.

  \end{itemize}

  % Fractions can be ``hidden'' via syntactic conventions and proof automation.
  % (\href{http://research.microsoft.com/en-us/um/people/leino/papers/krml225.pdf}{Heule et al, 2013})
  % This works very well in simple cases,
  % but you have to understand the sugar
  % in order to figure out what's wrong when it does not work.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{In this paper}

  We propose a solution that is:
  \begin{itemize}
  \item \emph{not as powerful} as fractional permissions (or other share algebras),
  \item but significantly \emph{simpler}. (A design ``sweet spot''?)
  \end{itemize}
  Our contributions:
  \begin{itemize}
  \item introducing a \emph{read-only modality}, $\HROname$ (in the paper: ``RO''). \\
  $\HRO{H}$ gives \emph{temporary} read-only access to the memory governed by $H$.
  \item finding \emph{simple and sound reasoning rules}
  for $\HROname$.
  \item proposing \emph{a model} that justifies these rules.
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\section{Some Intuition}

% ----------------------------------------------------------------------------

\begin{frame}{Our solution}

  We would like the specification of \seqconcat{} to look like this:
  \[
    \Jhoarevertcolor
      { \HRO{\Hseq{s_1}{L_1}} \Sc\Hstar \HRO{\Hseq{s_2}{L_2}} }
      { (\seqconcat\,s_1\,s_2) }
      { \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L_1}{L_2}}   }
  \]
  \pause
  Compared with the earlier specification based on unique read-write permissions,
  \begin{itemize}
  \item
  this specification is \emph{more concise},
  \item
  imposes \emph{fewer proof obligations},
  \item
  makes it clear that \emph{the data structures cannot be modified} by \seqconcat,
  \item
  and \emph{does not require $s_1$ and $s_2$ to be distinct}. \textcolor{RedViolet}{--- (next slide)}
  \item
  Furthermore, this spec \emph{implies} both earlier specs. \textcolor{RedViolet}{--- (next slide)}
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\newcommand{\NewSpec}{
    \Jhoarevert
      { \HRO{\Hseq{s_1}{L_1}} \Sc\Hstar \HRO{\Hseq{s_2}{L_2}} }
      { \mathbf{(\seqconcat\,s_1\,s_2)} }
      { \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L_1}{L_2}} }
}
\newcommand{\NewSpecShared}{
    \Jhoarevert
      { \HRO{\Hseq{s}{L}} }
      { \mathbf{(\seqconcat\,s\,s)} }
      { \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L}{L}} }
}
\newcommand{\OldSpec}{
    \Jhoarevert
      { \Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2} }
      { \mathbf{(\seqconcat\,s_1\,s_2)} }
      { \HPabs{s_3} \; \Hseq{s_3}{\OLAppof{L_1}{L_2}}
        \\ \Sc\Hstar \Hseq{s_1}{L_1} \Sc\Hstar \Hseq{s_2}{L_2} }
}
\newcommand{\OldSpecShared}{
    \Jhoarevert
      {\Hseq{s}{L} }
      { \mathbf{(\seqconcat\,s\,s)} }
      { \HPabs{s_3} \;
        \Hseq{s_3}{\OLAppof{L}{L}}
        \\ \Sc\Hstar \Hseq{s}{L}
      }
}

\begin{frame}[fragile]{The new spec subsumes both earlier specs}

  \begin{center}
  \let\oldHROname\HROname
  \renewcommand{\HROname}{\emph{\oldHROname}}
  \begin{tikzpicture}
    \node [rectangle] (newspec) {$\NewSpec$};
    \node [text badly centered, minimum height=10mm, font=\bfseries, fill=RedViolet!15,
           thick, draw, rounded corners=.5mm,
           below of=newspec, xshift=20mm,yshift=5mm] (newspecbubble) { new spec };

    \onslide<3->{
    \node [rectangle, below of=newspec, xshift=30mm, yshift=-15mm] (newspecshared) {$\NewSpecShared$};
    \node [text badly centered, minimum height=10mm, font=\bfseries, fill=RedViolet!15,
           thick, draw, rounded corners=.5mm, text width=22mm,
           below of=newspecshared, xshift=25mm,yshift=15mm] (newspecsharedbubble) { new spec (with sharing) };
    \path[->, very thick, >=stealth, draw = RedViolet, double distance=1pt] (newspecbubble.east)
    to [out = 0, in = 90, distance=10mm] (newspecsharedbubble.north) ;
    }

    \onslide<4->{
    \node [rectangle, below of = newspecshared, yshift=-15mm] (oldspecshared) {$\OldSpecShared$};
    \node [text badly centered, minimum height=10mm, font=\bfseries, fill=RedViolet!15,
           thick, draw, rounded corners=.5mm, text width=25mm,
           below of=oldspecshared, xshift=15mm,yshift=17mm] (earlierspecsharedbubble) { earlier spec (with sharing) };
    \path[->, very thick, >=stealth, draw = RedViolet, double distance=1pt] (newspecsharedbubble.south)
    to [out = -90, in = 45, distance=5mm] (earlierspecsharedbubble.north) ;
    }

    \onslide<2->{
    \node [rectangle, left of = oldspecshared, xshift=-40mm] (oldspec) {$\OldSpec$};
    \node [text badly centered, minimum height=10mm, font=\bfseries, fill=RedViolet!15,
           thick, draw, rounded corners=.5mm, text width=30mm,
           below of=oldspec, xshift=10mm,yshift=25mm] (earlierspecbubble) { earlier spec (without sharing) };
    \path[->, very thick, >=stealth, draw = RedViolet, double distance=1pt] (newspecbubble.west)
    to [out = 225, in = 90, distance=15mm] (earlierspecbubble.north) ;
    }
  \end{tikzpicture}
  \end{center}

\end{frame}

% ----------------------------------------------------------------------------

\section{Reasoning Rules}

% ----------------------------------------------------------------------------

\begin{frame}{Assertions}

  The syntax of assertions is extended:
  \[\begin{array}{rrl}
  H & := &
  \HPprop{P} \mid
  \HPsingle{l}{v} \mid
  \Hstarof{H_1}{H_2} \mid
  \HPorof{H_1}{H_2} \mid
  %\HPandof{H_1}{H_2} \mid
  \HPexinameof{x}{H} \mid
  \insister{\HRO{H}}
  \end{array}\]
%
  Read-only access to a data structure entails \emph{read-only access to its parts}:
  \[\begin{array}{rcl@{\qquad}l}
  \HRO{\Hstarof{H_1}{H_2}} & \Himpl & \HRO{H_1} \Hstar \HRO{H_2}
  & \text{(the reverse is false)}
  \end{array}\]
%
  Read-only access can be \emph{shared}:
  \[\begin{array}{rcl@{\qquad}l}
  % Duplicability.
  \HRO{H} & = & \HRO{H} \Hstar \HRO{H} \hbox to 24mm{}
  \end{array}\]
%
  (A few more axioms, not shown).

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{A new read axiom}

  The traditional read axiom:
  \[
  \inferrule*[lab=traditional read axiom]{}
  { \Jhoarecolor
    { \HPsingle{l}{v} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} \Hstar \HPsingle{l}{v} } }
  \]
  is replaced with a ``smaller'' axiom:
  \[
  \inferrule*[lab=new read axiom]{}
  { \Jhoarecolor
    { \HRO{\HPsingle{l}{v}} }
    { \LGetof{l} }
    { \HPabs{y} \, \HPprop{y = v} } }
  \]
  The traditional axiom can be derived from the new axiom.

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{A new frame rule}

  The traditional frame rule
  is subsumed by a new ``\rofr'':
  \begin{mathpar}
  \inferrule*[lab=frame rule]
    { \Jhoare{H}{t}{Q} \\ \HNormal{H'} }
    { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }

  \inferrule*[lab=\rofr]
    {  \Jhoare{H \insister{{} \Hstar \HRO{H'}}  }{t}{Q}  \\ \HNormal{H'} }
    { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }
  \end{mathpar}

  Upon entry into a code block,
  $H'$ can be \emph{temporarily replaced} with $\HRO{H'}$, \\
  and upon exit, $H'$ \emph{magically re-appears}.

\pause

  The side condition $\HNormal{H'}$ means roughly that $H'$ has no $\HROname$ components.

  This means that \emph{read-only permissions cannot be framed out}.% (by either rule above).

  % If they could, the \rofr would clearly be unsound.
  % Take t to be skip and Q to be RO(H')
  % then we get RO(H') * H' in the conclusion of the \rofr

  Not a problem, as they
  are always \emph{passed down}, never returned.

  They never appear in postconditions.

\end{frame}

% ----------------------------------------------------------------------------

\section{Model}

\newcommand{\RW}{RW\xspace}
\newcommand{\RO}{RO\xspace}

\begin{frame}{Interpretation of assertions}

  In a heap fragment, let every cell be colored \emph{\RW} or \emph{\RO}.
  % black cells are read-write, grey cells are read-only.
  % in the paper, a pair of two heaps is used

  Let separating conjunction require:
  \begin{itemize}
  \item \emph{disjointness} of the \RW areas;
  \item \emph{disjointness} of one side's \RW area with the other side's \RO area;
  \item \emph{agreement} on the content of the heap where the \RO areas overlap.
  \end{itemize}
  If an assertion $H$ describes a certain set of heaps, then:
  \begin{itemize}
  \item Let $\HRO{H}$ describe \emph{the same heaps, colored entirely \RO}.
  \item Let $\HNormal{H}$ mean that every heap in $H$ is colored entirely \RW.
  \end{itemize}

\end{frame}

\begin{frame}{Interpretation of triples}

  The meaning of the Hoare triple $\Jhoare{H}{t}{Q}$ is a variant of the usual:
  \[
  \Tfor{h_1 h_2}
  \left\{\begin{array}{l}
  \HStarable{h_1}{h_2} \\[1pt]
  H\,h_1
  \end{array}\right\}
  \impl\; \Texi{v h_1'}
  \left\{\begin{array}{l}
  \HStarable{h_1'}{h_2}
  \\[1pt]
  \Jredbigconfigof{ t }{ \,\Hflatten{h_1 \Hunion h_2} }{ v }{ \,\Hflatten{h_1' \Hunion h_2} }
  \\[1pt]
  \only<1>{(Q \Sc\Hstar \mathit{true}) \,v\,h_1'}
  \only<2->{\tikzbox{onrwsub}{$\Honrwsub{Q\,v}\,h_1'$}}
  \\[1pt]
  \onslide<3->{\tikzbox{preservation}{$\Hr{h_1'} = \Hr{h_1}$}}
  \end{array}\right\}
  \]
  Roughly, ``If part of the heap satisfies $H$, \\
  then $t$ runs safely and changes that part of the \\
  heap to satisfy $Q$, leaving the rest untouched.''

  %
  We make two changes:
  \begin{itemize}
  \item<2-> $Q$ describes \tikzbox{only}{\emph{a purely \RW
        part} of the final heap}.
  \item<3-> The \RO part of the heap is
    \tikzbox{preserved}{\emph{preserved}}, \\
    even though $Q$ says nothing about it.
  \end{itemize}
  %
  \begin{tikzpicture}[overlay, style = local]
    \path[->, very thick, >=stealth, draw = RedViolet]<2> ([xshift=2mm]only.east)
    to [out = 0, in = -75, distance=20mm] (onrwsub.south) ;
    \path[->, very thick, >=stealth, draw = RedViolet]<3> ([xshift=2mm]preserved.east)
    to [out = 0, in = -90, distance=15mm] ([xshift=10mm,yshift=-20mm]preservation.east)
    to [out = 90, in = -45, distance=10mm] ([xshift=1mm]preservation.east) ;
  \end{tikzpicture}

\end{frame}

% ----------------------------------------------------------------------------

\begin{frame}{Soundness}

  \begin{theorem}
    With respect to this interpretation of triples,
    every reasoning rule is sound.
  \end{theorem}
  \begin{proof}
    ``Straightforward''. Machine-checked.
  \end{proof}

\end{frame}

% ----------------------------------------------------------------------------

% Conclusion.

\section{Conclusion}

\begin{frame}{What about concurrency?}

  Our proof is carried out in a \emph{sequential} setting.
  \begin{itemize}
  \item The proof uses big-step operational semantics.
  \end{itemize}
  What about \emph{structured concurrency}, i.e., parallel composition $(e_1 \mathbin{\mid\mid} e_2)$?
  \begin{itemize}
  \item We believe that $\HROname$ permissions remain sound,
  \item but do not have a proof -- a different proof technique is required.
  \item They allow read-only state to be shared between threads.
  \end{itemize}
  % There cannot a big-step operational semantics of parallel composition,
  % it seems. At least, not one that allows for potential interference
  % (and therefore allows proving the absence of interference).
  What about \emph{unstructured concurrency}, i.e., threads and channels?
  \begin{itemize}
  \item One cannot allow $\HROname$ permissions to be sent along channels.
    % they would then exceed their lifetime
  \item More complex machinery is required: fractions, lifetimes, \ldots
  \end{itemize}

% Nos permissions RO sont duplicables mais non pas persistantes
% et en particulier ne peuvent pas être capturées par une clôture
% ni transmises à un autre thread.

\end{frame}

\begin{frame}{Conclusion}

  We propose:
  \begin{itemize}
  \item \emph{a simple extension} of Separation Logic with a read-only modality;
  \item \emph{a simple model} that explains why this is sound.
  \end{itemize}

  A possible design sweet spot?
  \begin{itemize}
  \item not so easy to find, worth knowing about;
  \item applicable to PL design? (e.g., adding $\HROname$ to Mezzo)
    % but Mezzo has concurrency, and "const" permissions would not be sendable
    % so not very useful?
  \end{itemize}

  Applications:
  \begin{itemize}
  \item more \emph{concise}, more \emph{accurate}, more \emph{general} specifications;
  \item \emph{simpler} proofs.
  \end{itemize}
  Pending implementation in CFML (Charguéraud).

\end{frame}

% Backup slides.

\section*{Backup}

\input{deception}
\input{amnesia}
\input{derivations}
\input{rules}
\input{model}

\end{document}
