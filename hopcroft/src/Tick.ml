(* -------------------------------------------------------------------------- *)

(* A global counter. We never reset it, but that is not a problem. *)

let c =
  ref 0

(* Consumption can be temporarily disabled -- see below. *)

let live =
  ref true

(* Incrementing the counter. *)

let step n =
  if !live then
    c := !c + n

(* Measuring the cost of a sub-computation. *)

let measure f =
  let initial = !c in
  let result = f() in
  let final = !c in
  final - initial, result

(* Asserting that the cost of a sub-computation is bounded by [n].
   The integer [n] is lazy, and the recording of execution steps is
   disabled while [n] is computed. This allows using physical code
   as part of a logical assertion. (In fact, if recording is
   disabled already, then we are already evaluating an assertion,
   and we do not need to evaluate this assertion at all.) *)

let bound n f =
  if !live then begin
    live := false;
    let n = Lazy.force n in  
    live := true;
    let cost, result = measure f in
    assert (cost <= n);
    result
  end
  else
    f()

(* -------------------------------------------------------------------------- *)

(* For every [n] greater than zero, we want [2^(log2 n) <= n < 2^(log2 n + 1)].
   We set [log2 0 = 0]. This coincides with Coq's [Nat.log2]. *)

(*
let rec log2 n =
  if n <= 1 then 0 else 1 + log2 (n / 2)
*)

let rec log2 n accu =
  if n <= 1 then accu else log2 (n / 2) (accu + 1)

let log2 n =
  log2 n 0

(* -------------------------------------------------------------------------- *)

(* A simple percentage computation. *)

let percentage a b =
  float_of_int (a * 100) /. float_of_int b

(* -------------------------------------------------------------------------- *)

(* Keeping track of time credits at runtime. *)

type account = {
  name: string;
  mutable credit: int;  (* always >= 0 *)
  mutable receipt: int; (* always >= 0 *)
}

let new_account name = {
  name;
  credit = 0;
  receipt = 0;
}

exception OutOfCredit of string

let pay account amount =
  assert (amount >= 0);
  account.credit <- account.credit - amount;
  if account.credit < 0 then
    raise (OutOfCredit account.name)

let receive account amount =
  assert (amount >= 0);
  account.credit <- account.credit + amount;
  account.receipt <- account.receipt + amount

let transfer src dst amount =
  pay src amount;
  receive dst amount

let new_account_from name src amount =
  let dst = new_account name in
  transfer src dst amount;
  dst

let balance account =
  account.credit

let receipt account =
  account.receipt

let status account =
  Printf.printf "%s: received $%d, currently $%d (%.2f%%)\n"
    account.name
    account.receipt
    account.credit
    (percentage account.credit account.receipt)
