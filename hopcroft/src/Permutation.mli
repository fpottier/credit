(* A mutable permutation of [0, n). *)

type permutation

(* A newly created permutation is the identity. *)

val make: int -> permutation

(* [size p] returns the size of the permutation [p]. That is, it returns
   the integer [n] that was provided as an argument to [make] when [p]
   was created. *)

val size: permutation -> int

(* The permutation and its inverse can be read in constant time. *)

val next: permutation -> int -> int
val prev: permutation -> int -> int

(* [transpose p w y] modifies the permutation [p] by exchanging the
   images (i.e., the successors) of [w] and [y] in the permutation.
   It runs in constant time. [w] and [y] must be distinct. *)

val transpose: permutation -> int -> int -> unit

(* [isolate p x] takes the point [x] out of whatever cycle it is in,
   and makes it isolated -- i.e., [x] becomes its own successor. It
   runs in constant time. *)

val isolate: permutation -> int -> unit

(* [is_isolated p x] tells whether [x] is currently its own successor.
   It runs in constant time. *)

val is_isolated: permutation -> int -> bool

(* [iter_next p x f] and [iter_prev p x f] apply the function [f] to
   every element of [x]'s cycle. [x] is processed first. The function
   [f] must be careful NOT to modify this cycle. Iteration is carried
   out in linear time. *)

val iter_next: permutation -> int -> (int -> unit) -> unit
val iter_prev: permutation -> int -> (int -> unit) -> unit

(* [cycle p x] returns the cycle of [x], represented as a list, in an
   unspecified order. The list is built in linear time. *)

val cycle: permutation -> int -> int list

(* [cardinal p x] returns the length of the cycle of [x]. It runs in
   linear time. *)

val cardinal: permutation -> int -> int

