(* This module offers an implementation of bags whose elements are integers in
   an interval [0, n). Here, a bag is a (mutable) set that supports constant
   time insertion, arbitrary element retrieval, and membership test. *)

type bag

(* [make n] creates a new empty bag which can contain elements in the interval
   [0, n). *)

val make: int -> bag

(* [add b x] inserts [x] into the bag [b], unless [x] already is a member of
   the bag. *)

val add: bag -> int -> unit

(* [add_new b x] inserts [x] into the bag [b]. [x] must not be a member of
   the bag already. *)

val add_new: bag -> int -> unit

(* [choose b] returns an arbitrary member of the bag [b], and takes it out of
   the bag. *)

val choose: bag -> int option

(* [mem b x] tells whether [x] is a member of the bag [b]. *)

val mem: bag -> int -> bool

(* [repeat b f] takes an arbitrary element out of the bag [b], then passes it
   to [f], which is allowed to insert new elements into the bag. This is
   repeated until the bag is exhausted. *)

val repeat: bag -> (int -> unit) -> unit

(* If the elements are not integers, but can be mapped to an integer interval
   of the form [0, n), then the following functor can be used. Applying the
   functor creates a new empty bag, which is an implicit parameter of every
   function produced by the functor. *)

module Make (E : sig
  type element
  val n: int
  val encode: element -> int
  val decode: int -> element
end) : sig
  open E
  val add: element -> unit
  val add_new: element -> unit
  val choose: unit -> element option
  val mem: element -> bool
  val repeat: (element -> unit) -> unit
end

