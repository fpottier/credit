(* A mutable partition of [0, n). *)

(* A partition contains a permutation of [0, n), in the sense of the
   module [Permutation]. It adds the idea that there is a mapping of
   points to blocks, where blocks are also numbered in [0, n). It
   allows querying this mapping, both ways, in constant time. It also
   keeps track of the size of each block, which can be looked up in
   constant time. *)

(* A point can be detached, which means that it is isolated in the
   sense of the underlying permutation, and is not part of any
   block. *)

type partition

type point =
  int (* in [0, n) *)

type block =
  int (* in [0, n) *)

(* [make n] creates a new partition of [n] points and [n] blocks.
   Every point is initially detached, and every block is initially
   empty. *)

val make: int -> partition

(* [size p b] returns the current size of the block [b]. *)

val size: partition -> block -> int

(* [element p b] returns an arbitrary element of the block [b].
   The block must be non-empty. *)

val element: partition -> block -> point

(* [block p x] returns the block in which the element [x] resides.
   [x] must not be currently detached. *)

val block: partition -> point -> block

(* [attach p b x] attaches the point [x], which must be currently
   detached, to the block [b]. *)

val attach: partition -> block -> point -> unit

(* [detach p x] detaches the point [x] from its current block.
   [x] must not be currently detached. *)

val detach: partition -> point -> unit

(* [move b x] detaches the point [x] from of its current block and
   attaches it to the block [b]. *)

val move: partition -> block -> point -> unit

(* [cycle p x] returns the cycle of [x], represented as a list, in an
   unspecified order. The list is built in linear time. *)

val cycle: partition -> point -> point list

