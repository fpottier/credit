(* A permutation of [0, n) is stored in two arrays of size [n].
   The array [next] contains the permutation; the array [prev]
   contains its inverse. *)

type permutation = {
  next: int array;
  prev: int array;
}

(* A newly created permutation is the identity. *)

let make n =
  {
    next = Array.init n (fun i -> i);
    prev = Array.init n (fun i -> i);
  }

let size p =
  Array.length p.next

(* The permutation and its inverse can be read in constant time. *)

let next p i =
  p.next.(i)

let prev p i =
  p.prev.(i)

(* [transpose p w y] modifies the permutation [p] by exchanging the
   images (i.e., the successors) of [w] and [y] in the permutation.
   It runs in constant time. [w] and [y] must be distinct. *)

let transpose p w y =
  (* We assume [w] and [y] are distinct. *)
  assert (w <> y);
  (* Let [x] and [z] be their successors. *)
  let x = next p w
  and z = next p y in
  (* [x] and [z] are distinct too. *)
  assert (x <> z);
  (* Exchange the successors of [w] and [y].
     It is clear that [next] remains bijective. *)
  p.next.(w) <- z;
  p.next.(y) <- x;
  (* Exchange the predecessors of [x] and [z].
     It is clear that [prev] remains the inverse of [next]. *)
  p.prev.(x) <- y;
  p.prev.(z) <- w

let isolate p y =
  let w = prev p y in
  if w <> y then
    (* If [y] is its own predecessor, then it is already isolated
       and there is nothing to do. Otherwise, we swap the successors
       of [w] and [y]. So, the new successor of [w] is the former
       successor of [y], and the new successor of [y] is [y] itself.
       Thus, [y] is isolated, as desired. *)
    transpose p w y

let is_isolated p y =
  let w = prev p y in
  w = y

let iter_next p x f =
  let rec loop y =
    (* Process [y]. *)
    f y;
    (* Stop if [y.next] is the starting point [x]. *)
    let y = next p y in
    if y <> x then 
      loop y
  in
  loop x

let iter_prev p x f =
  let rec loop y =
    (* Process [y]. *)
    f y;
    (* Stop if [y.prev] is the starting point [x]. *)
    let y = prev p y in
    if y <> x then 
      loop y
  in
  loop x

let cycle p x =
  let c = ref [] in
  iter_next p x (fun y -> c := y :: !c);
  !c

let cardinal p x =
  let c = ref 0 in
  iter_next p x (fun _ -> c := 1 + !c);
  !c

