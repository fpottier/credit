open Tick

module Run (G : sig

  (* We assume that user points are numbered from [0] to [n-1]. *)
  val n : int

  (* We assume symbols are numbered from [0] to [f-1]. *)
  val f : int

  (* We assume that the initial partition has [b] groups (some of which
     may be empty). In other words, it has at most [b] blocks. *)
  val b : int

  (* The initial partition is given by this function, which maps each point
     to its group number in the interval [0, b). *)
  val group: int -> int

  (* The graph is given by this function, which iterates over all edges.
     An edge is characterized by its source [x] (a point), its label [s]
     (a symbol), and its destination [y] (a point). *)
  val for_every_edge: (int -> int -> int -> unit) -> unit

end) = struct
open G

(* The global complexity of the algorithm. *)

let _global =
  new_account "global"
let () =
  receive _global (6 * n * f * (log2 n + 1))

(* Sanity checking. *)

let () =
  assert (n >= 0);
  assert (f >= 0);
  assert (b >= 0);
  assert (n == 0 || b > 0)

(* A point is an integer index in the interval [0, n). *)

type point =
    int

(* A symbol is an integer index in the interval [0, f). *)

type symbol =
    int

(* We need at most [n] blocks. Indeed, a block is non-empty. *)

let max_block =
  n

(* We represent a block as a (unique) integer index in the interval
   [0, max_block). This allows us to modularly allocate several arrays
   indexed by blocks. *)

type block =
    int

(* We maintain a partition of the points. The module [Partition] keeps
   track of the members of each block, of the size of each block, as
   well as of the correspondence between points and blocks. *)

let p =
  Partition.make n

(* Block allocation. Every block at and above [next] is empty/unused.
   Every allocated block is non-empty. *)

let alloc : unit -> block =
  let next = ref 0 in
  fun () ->
    let b = !next in
    assert (b < max_block);
    next := b + 1;
    b

(* Create and populate the blocks. The initial partition is made of groups,
   which may be empty. We are careful to allocate a block only when a group is
   non-empty. This allows us to work with at most [n] blocks. *)

let () =
  (* Place every point in a bucket indexed by its group. *)
  let initial_group : point list array = Array.make b [] in
  for x = 0 to n - 1 do
    let g = group x in
    assert (0 <= g && g < b); (* if this fails, the client goofed up *)
    initial_group.(g) <- x :: initial_group.(g)
  done;
  (* Iterate over the buckets. For every non-empty group, allocate a block. *)
  Array.iter (fun group ->
    match group with
    | [] -> ()
    | _  ->
        let b = alloc() in
        List.iter (Partition.attach p b) group
  ) initial_group
(* TEMPORARY could move this function to a separate library,
   if we just expect a list of non-empty lists? *)

(* For each point [y] and for each symbol [s], we need the ability to
   iterate over the predecessors of [y] along edges labeled [s]. *)

let predecessors =
  Array.init n (fun _ -> Array.init f (fun _ -> []))

(* Cost: 1 + #predecessors(y) *)
(* One would like to say that the cost is just 1, because if one
   charges one extra credit to every call to [action], then the
   term #predecessors(y) disappears. *)
let for_every_predecessor y s action =
  assert (0 <= y && y < n);
  assert (0 <= s && s < f);
  List.iter action predecessors.(y).(s)

(* 1 + #ys + #predecessors(ys) *)
let for_every_predecessor ys s action =
  List.iter (fun y -> for_every_predecessor y s action) ys

(* The bag is a set of pairs of a symbol and a block. *)

module Bag =
  IntegerBag.Make (struct
    type element = symbol * block
    let n = f * max_block
    let encode (s, b) = s + f * b
    let decode i = (i mod f, i / f)
  end)

(* Install all edges. *)

let edges =
  ref 0

let () =
  for_every_edge (fun x s y ->
    assert (0 <= x && x < n);
    assert (0 <= y && y < n);
    assert (0 <= s && s < f);
    (* Count edges. (For debugging.) *)
    incr edges;
    (* Initialize [predecessors]. *)
    predecessors.(y).(s) <- x :: predecessors.(y).(s);
    (* [y]'s block now has an incoming edge labeled [s], which means that
       the pair of [s] and this block may represent an instability. This
       pair should be inserted into the bag, unless it is there already. *)
    Bag.add (s, Partition.block p y)
  )

let edges =
  assert (!edges <= n * f);
  !edges

let leaving : point list array =
  Array.make max_block [] (* meaningful default value *)

(* Splitting. *)

(* According to Lemma 10 in Gries (1973), every call to [split] can be
   associated with a particular edge (which has been followed backwards,
   from its destination [y] to its source [x]), and the number of times
   this particular edge is involved in a potential split is bounded by [log2 n + 1],
   thanks to the halving scheme. (Every time this edge is involved, the
   size of [y]'s block has shrunk by a factor of two.) These remarks imply
   that the total number of calls to [split] is at most the number of edges,
   multiplied by [log2 n + 1], or in other words, O(fnlog n). *)

let _split =
  new_account_from "split" _global (3 * edges * (log2 n + 1))

(* The number of calls to [commit] is of course the same. *)

let _commit =
  new_account_from "commit" _global (edges * (log2 n + 1))

(* We use this account to amortize the cost of some operations in [commit].
   We charge these operations to [split], where elements are inserted in
   the list [leaving.(b)]. *)

let _leaving =
  new_account "leaving"

(* The number of actual splits is at most [n], because each actual split
   creates a new block and there are at most [n] blocks at the end. The
   (amortized) cost of each actual split is [O(f)], so the total cost is
   [O(nf)]. *)

let _actual_split =
  new_account_from "actual_split" _global (n * f)

let split x =
  pay _split 1;
  transfer _split _leaving 2; (* save two credits *)
  let b = Partition.block p x in
  (* Mark [x] as potentially leaving [b]. *)
  leaving.(b) <- x :: leaving.(b)

(* During one iteration of the main loop, [commit] is called at most once per
   edge. Several calls to [commit] may concern the same block [b], but the
   list [leaving.(b)] will be found non-empty at most once, because, after
   processing it, we set it to empty again. Because every element in a
   [leaving] list is processed exactly once, every cost that is linear in the
   length of this list can be charged to [split], where elements are inserted
   into the [leaving] list. Once this is done, the cost of [commit] can be
   viewed as follows: 1. O(1) for every call to [commit], hence O(fnlog n);
   2. the cost of splits, which is clearly O(fn), because there are at most
   [n] actual splits and the amortized cost of each actual split is O(f). *)

let commit x =
  pay _commit 1;
  let b = Partition.block p x in
  match leaving.(b) with
  | [] ->
      ()
  | xs ->
      leaving.(b) <- [];
      let k = List.length xs in
      (* Pay for measuring the list length. This has zero amortized cost;
         we charge [split], where the elements were inserted in the list. *)
      pay _leaving k;
      assert (k > 0);
      if k < Partition.size p b then begin
        (* Pay for the loop below. *)
        pay _actual_split f;
        let b' = alloc() in
        List.iter (Partition.move p b') xs;
        (* Pay for iterating the list. As above, zero amortized cost. *)
        pay _leaving k;
        let smaller = if Partition.size p b < Partition.size p b' then b else b' in
        for s = 0 to f-1 do
	  Bag.add_new (s, if Bag.mem (s, b) then b' else smaller)
        done
      end

(* According to Gries (1973), it is useful to think of the binary tree of the
   logical blocks that have existed at some point in the algorithm. When a
   block is split, we get two new logical blocks (even though we re-use a
   block number) which become the children of the original logical block in
   the tree. This tree has at most [n] leaves, which are the blocks at the
   end. Thus, it has at most [n] internal nodes. This implies that the total
   number of logical blocks that have existed at some point is at most [2 *
   n]. Since a pair [(s, b)] of a symbol and a logical block is never enqueued
   twice, the number of iterations of the main loop is at most [2 * n * f].
   See Lemma 8. *)

(* This bound is perhaps not important in itself. We check it, just to make
   sure it is valid. But the cost of [refine] is considered to be [#ys] --
   which is sound because the list [ys] is nonempty -- and we pay for it. *)

(* TEMPORARY experimental evidence suggests that [n * f] is also a valid
   bound (and is tight), but I do not see why it would be valid. *)

let _iterations =
  new_account "iterations"
let () =
  receive _iterations (2 * n * f)

(* The cumulated cost of *all* calls to [split] and [commit] has been assessed
   above and is at most O(fnlog n). Thus, in [refine], there remains to assess
   the cost of computing [ys] and iterating over every predecessor of [ys].
   This cost is linear in the size of [ys] plus the size of its pre-image.
   Actually, the second part (the size of the pre-image) can be charged to
   [split] and [commit], so we may count it as zero here. In that sense, the
   amortized complexity of [refine] is O(#ys), and thanks to the halving scheme,
   the cumulated size of the blocks [ys] that are processed is O(fnlog n).
   This seems intuitively clear, and seems to correspond to Lemma 9 in Gries'
   paper. I am not sure why the proof of Lemma 9 seems so complicated. *)

(* The cumulated size of all processed blocks is at most [f * n * (log2 n + 1)]. *)

let _refine =
  new_account_from "refine" _global (f * n * (log2 n + 1))

let refine (s, b) =
  pay _iterations 1;
  assert (Partition.size p b > 0);
  let ys = Partition.cycle p (Partition.element p b) in
  pay _refine (List.length ys);
  for_every_predecessor ys s split;
  for_every_predecessor ys s commit
    (* In the second loop, instead of iterating over all predecessors again,
       we could iterate only over the blocks [b] such that [leaving.(b)] is
       non-empty. *)

(* The main loop. Runs in time O(fnlog n). *)

let () =
  Bag.repeat refine

(* Publish a function that maps each point to a representative member of its
   block. *)

let representative x =
  Partition.element p (Partition.block p x)

(* Optionally, construct a list of all blocks, in time O(n). *)

let partition =
  lazy (
    let partition : int list list ref = ref [] in
    for x = 0 to n-1 do
      if x = representative x then
        partition := Partition.cycle p x :: !partition
    done;
    !partition
  )

end
