(* -------------------------------------------------------------------------- *)

(* Counting execution steps. *)

(* [step n] records [n] execution steps. *)

val step: int -> unit

(* [bound n f] executes the computation [f()], counts how many steps are taken
   during this computation, and asserts that this number is less than or equal
   to [n]. Calls to [step] during the computation of [n] are allowed and have
   no effect. *)

val bound: int Lazy.t -> (unit -> 'a) -> 'a

(* -------------------------------------------------------------------------- *)

(* A definition of [log2]. *)

val log2: int -> int

(* -------------------------------------------------------------------------- *)

(* Keeping track of time credits at runtime. *)

(* An account is a mutable container where a number of time credits is stored.
   It is safe to duplicate (i.e., share) an account; this means that several
   people draw from the same account, but does not lead to duplication of
   credits. It is also safe to discard an account. *)

type account

(* [new_account name] creates an account. A name is supplied for debugging
   purposes. The account is initially empty. *)

val new_account: string -> account

(* [pay account amount] consumes [amount] credits, taken from [account].
   [amount] must be nonnegative. This operation raises [OutOfCredit] if the
   account does not have sufficient balance. *)

exception OutOfCredit of string

val pay: account -> int -> unit

(* [transfer src dst amount] transfers [amount] credits from the account [src]
   to the account [dst]. [amount] must be nonnegative. This operation raises
   [OutOfCredit] if the source account does not have sufficient balance. *)

val transfer: account -> account -> int -> unit

(* [receive account amount] creates [amount] credits out of thin air and
   adds them to the account [account]. [amount] must be nonnegative. This
   operation is considered UNSAFE. Ideally, it should be called just once
   at the beginning of the code whose complexity one wishes to control. *)

val receive: account -> int -> unit

(* [new_account_from name src amount] creates a new account, transfers
   [amount] from the account [src] to the new account, and returns the
   new account. *)

val new_account_from: string -> account -> int -> account

(* [balance account] returns the current balance of [account]. *)

val balance: account -> int

(* [receipt account] tells how much credit this account has received
   since it was created. *)

val receipt: account -> int

(* [status account] prints a status message about [account]. *)

val status: account -> unit

