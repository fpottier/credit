module Run (G : sig

  (* We assume that user points are numbered from [0] to [n-1]. *)
  val n : int

  (* We assume symbols are numbered from [0] to [f-1]. *)
  val f : int

  (* We assume that the initial partition has [b] groups (some of which
     may be empty). In other words, it has at most [b] blocks. *)
  val b : int

  (* The initial partition is given by this function, which maps each point
     to its group number in the interval [0, b). *)
  val group: int -> int

  (* The graph is given by this function, which iterates over all edges.
     An edge is characterized by its source [x] (a point), its label [s]
     (a symbol), and its destination [y] (a point). *)
  val for_every_edge: (int -> int -> int -> unit) -> unit

end) : sig

  (* Applying the functor [Run] computes the coarsest partition that refines
     the initial partition and is compatible with all [f] edge relations. *)

  (* The function [representative] maps each point in the semi-open interval
     [0,n) to a representative member of its block. *)
  val representative: int -> int

  (* The partition can be explicitly computed, on demand. It is presented as
     a list of blocks, that is, a list of lists of points. *)
  val partition: int list list Lazy.t

end
