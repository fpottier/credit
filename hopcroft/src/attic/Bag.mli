(* This module offers an implementation of bags. Here, a bag is a (mutable)
   set that supports constant time insertion, arbitrary element retrieval,
   and membership test. *)

module Make (X : sig

  (* Items. *)

  type item

  (* Each item must carry a [mark] field. In a newly allocated item,
     this field should be set to [false]. *)

  val get_mark: item -> bool
  val set_mark: item -> bool -> unit

end) : sig

  open X

  (* The bag is implicit. It is initially empty. *)

  (* [add x] inserts item [x] into the bag, unless it already is a member of
     the bag. *)

  val add: item -> unit

  (* [add_new x] inserts item [x] into the bag. [x] must not be a member of
     the bag already. *)

  val add_new: item -> unit

  (* [choose()] returns an arbitrary member of the bag, and takes it out of
     the bag. *)

  val choose: unit -> item option

  (* [mem x] tells whether [x] is a member of the bag. *)

  val mem: item -> bool

  (* [repeat f] takes an arbitrary item out of the bag, then passes it to [f],
     which is allowed to insert new items into the bag. This is repeated until
     the bag is exhausted. *)

  val repeat: (item -> unit) -> unit

end

