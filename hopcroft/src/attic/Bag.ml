module Make (X : sig

  (* Items. *)

  type item

  (* Each item must carry a [mark] field. *)

  val get_mark: item -> bool
  val set_mark: item -> bool -> unit

end) = struct

  open X

  let queue : item list ref =
    ref []

  let add_new_safe x =
    queue := x :: !queue;
    set_mark x true

  let add_new x =
    assert (not (get_mark x));
    add_new_safe x

  let add x =
    if not (get_mark x) then
      add_new_safe x

  let choose () =
    match !queue with
    | [] ->
        None
    | x :: rest ->
        queue := rest;
        assert (get_mark x);
        set_mark x false;
        Some x

  let mem x =
    get_mark x

  let rec repeat f =
    match !queue with
    | [] ->
	()
    | x :: rest ->
        queue := rest;
        assert (get_mark x);
        set_mark x false;
	f x;
	repeat f

end

