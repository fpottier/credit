module ListBased (X : sig

  val n: int

end) = struct

  open X

  (* The free addresses are represented as a list. The constructor
     [Cons] has its usual meaning. The constructor [Geq head] means
     that every address from [head] (included) until [n] (excluded)
     is free. *)

  type free =
  | Geq of int
  | Cons of int * free

  let free =
    ref (Geq 0)

  let alloc () =
    match !free with
    | Geq head ->
        (* If this fails, we are wrong: *)
        assert (head <= n);
        (* If this fails, the client is wrong. There are too many live
           addresses. The client promises to never need more than [n]
           live addresses. *)
        assert (head < n);
        (* Allocate the address [head] and take it out of the free list. *)
        free := Geq (head + 1);
        head
    | Cons (head, tail) ->
        (* Allocate the address [head], which has been allocated and
           freed before. *)
        free := tail;
        head

  (* Linear search in the free list. Inefficient. Used only as a debugging
     help, as part of an assertion, below. *)

  let rec is_free i free =
    match free with
    | Geq head ->
        head <= i
    | Cons (head, tail) ->
        i = head || is_free i tail

  let free i =
    (* If this fails, the client is wrong: *)
    assert (0 <= i && i < n);
    (* Detect double frees. COSTLY. Should be disabled when shipping. *)
    assert (not (is_free i !free));
    match !free with
    | Geq head when head = i + 1 ->
        (* A feeble attempt at compressing the free list. *)
        free := Geq i
    | tail ->
        free := Cons (i, tail)

end

module ArrayBased (X : sig

  val n: int

end) = struct

  open X

  (* The free addresses are represented as a list, which is stored
     within an array of size [n]. This costs O(n) at initialization
     time. This approach is more compact than the list-based approach
     (if the free list is long, that is), reduces GC activity, and
     allows testing membership in the free list in constant time. *)

  (* If [free.(i)] is [j], where [j] lies between [0] (included) and
     [n] (included), then the address [i] is free and its successor
     in the free list is [j]. If [j] is [n], there is no successor.
     If [free.(i)] is [-1], then the address [i] is allocated. *)

  let free =
    Array.init n (fun i -> i + 1)

  let head =
    ref 0

  let alloc () =
    let i = !head in
    (* If this fails, we are wrong: *)
    assert (0 <= i && i <= n);
    (* If this fails, the client is wrong. There are too many live
       addresses. The client promises to never need more than [n]
       live addresses. *)
    assert (i < n);
    (* Mark [i] as allocated. Make its successor [j] the new head. *)
    let j = free.(i) in
    free.(i) <- -1;
    head := j;
    i

  let is_free i =
    free.(i) <> -1

  let free i =
    (* If this fails, the client is wrong: *)
    assert (0 <= i && i < n);
    (* Detect double frees: *)
    assert (not (is_free i));
    (* Mark [i] as free. Make the current head its successor. *)
    let j = !head in
    free.(i) <- j;
    head := i

end
