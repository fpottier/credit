open Store

module Make (S : STORE_UNINIT) = struct

  type 'a cell = {
    prev: 'a rref;
    next: 'a rref;
    content: 'a
  }

  and 'a rref =
    'a cell S.rref

  type 'a store =
      'a cell S.store

  let new_store =
    S.new_store

  let make (s : 'a store) (x : 'a) : 'a store * 'a rref =
    let s, r = S.make_uninitialized s in
    let s = S.set s r {
      prev = r;
      next = r;
      content = x
    } in
    s, r

  let get (s : 'a store) (r : 'a rref) : 'a store * 'a =
    let s, c = S.get s r in
    s, c.content

  let set (s : 'a store) (r : 'a rref) (x : 'a) : 'a store =
    let s, c = S.get s r in
    S.set s r { c with content = x }

  let eq =
    S.eq

  let next (s : 'a store) (r : 'a rref) : 'a store * 'a rref =
    let s, c = S.get s r in
    s, c.next

  let prev (s : 'a store) (r : 'a rref) : 'a store * 'a rref =
    let s, c = S.get s r in
    s, c.prev

  let transpose s w y =
    (* We assume [w] and [y] are distinct. *)
    assert (let _, b = S.eq s w y in not b);
    let s, cw = S.get s w in
    let s, cy = S.get s y in
    (* Let [x] and [z] be their successors. *)
    let x = cw.next
    and z = cy.next in
    (* [x] and [z] are distinct too. *)
    assert (let _, b = S.eq s x z in not b);
    (* We now perform four write instructions:
       w.next <- z;
       y.next <- x;
       x.prev <- y;
       z.prev <- w; *)
    let s = S.set s w { cw with next = z } in
    let s = S.set s y { cy with next = x } in
    let s, cx = S.get s x in
    let s = S.set s x { cx with prev = y } in
    let s, cz = S.get s z in
    let s = S.set s z { cz with prev = w } in
    s
    (* In the code above, in order to minimize the number of [S.get] and
       [S.set] instructions and the number of memory allocations, one could
       explicitly check whether [x] is: 1- equal to [w], 2- equal to [y], or
       3- equal to none of them, and in the first two cases, fuse two of the
       four write instructions. One would perform similar checks concerning
       [z]. However, that would lead to verbose code, so we avoid this idea
       for now. *)

  let isolate (s : 'a store) (y : 'a rref) : 'a store =
    let s, w = prev s y in
    let s, b = eq s w y in
    if b then
      (* [y] is its own predecessor. It is already isolated. *)
      s
    else
       (* Swap the successors of [w] and [y]. So, the new successor
          of [w] is the former successor of [y], and the new successor
          of [y] is [y] itself. Thus, [y] is isolated, as desired. *)
      transpose s w y

  let is_isolated (s : 'a store) (y : 'a rref) : 'a store * bool =
    let s, w = prev s y in
    eq s w y

  let iter_next (s : 'a store) (x : 'a rref) (f : 'a store -> 'a rref -> 'a store) : 'a store =
    let rec loop s y =
      (* Process [y]. *)
      let s = f s y in
      (* Stop if [y.next] is the starting point [x]. *)
      let s, y = next s y in
      let s, stop = eq s y x in
      if stop then 
        s
      else
        loop s y
    in
    loop s x

  let iter_prev (s : 'a store) (x : 'a rref) (f : 'a store -> 'a rref -> 'a store) : 'a store =
    let rec loop s y =
      (* Process [y]. *)
      let s = f s y in
      (* Stop if [y.prev] is the starting point [x]. *)
      let s, y = prev s y in
      let s, stop = eq s y x in
      if stop then 
        s
      else
        loop s y
    in
    loop s x

end
