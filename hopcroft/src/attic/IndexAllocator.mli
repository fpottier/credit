(* This module offers manual management of a fixed-size pool
   of addresses. Addresses are numbered from [0] (included)
   until [n] (excluded), where the integer [n] is fixed ahead
   of time. *)

module ListBased (X : sig

  val n: int

end) : sig

  (* [alloc()] allocates a new (i.e., currently unused) address.
     It is the client's responsibility to ensure that there
     remains at least one unused address, i.e., the set of live
     addresses currently has less than [n] elements. O(1). *)

  val alloc: unit -> int

  (* [free i] frees the address [i], which has been previously
     obtained via [alloc]. O(1). *)

  val free: int -> unit

end

module ArrayBased (X : sig

  val n: int

end) : sig

  (* [alloc()] allocates a new (i.e., currently unused) address.
     It is the client's responsibility to ensure that there
     remains at least one unused address, i.e., the set of live
     addresses currently has less than [n] elements. O(1). *)

  val alloc: unit -> int

  (* [free i] frees the address [i], which has been previously
     obtained via [alloc]. O(1). *)

  val free: int -> unit

end
