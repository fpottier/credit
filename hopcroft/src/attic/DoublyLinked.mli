open Store

module Make (S : STORE_UNINIT) : sig

  type 'a cell

  (* The new store is implemented on top of the store provided by the user.
     This means that any extra operations supported by the underlying store
     (such as, say, opening and committing a transaction) are applicable to
     this new store as well. *)

  include STORE
  with type 'a store = 'a cell S.store
   and type 'a rref = 'a cell S.rref

  val next: 'a store -> 'a rref -> 'a store * 'a rref
  val prev: 'a store -> 'a rref -> 'a store * 'a rref
  val transpose: 'a store -> 'a rref -> 'a rref -> 'a store (* = meld *)
  val isolate: 'a store -> 'a rref -> 'a store
  val is_isolated: 'a store -> 'a rref -> 'a store * bool
  val iter_next: 'a store -> 'a rref -> ('a store -> 'a rref -> 'a store) -> 'a store
  val iter_prev: 'a store -> 'a rref -> ('a store -> 'a rref -> 'a store) -> 'a store

end
