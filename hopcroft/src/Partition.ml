type point =
  int

let none : point =
  -1

type block =
  int

let detached : block =
  -1

type partition = {

  (* We maintain a permutation of the points. *)

  permutation: Permutation.permutation; (* of size [n] *)

  (* We keep track of the size of every block. *)

  size: int array; (* of size [max_block] *)

  (* We keep track of one (arbitrary) element of every block. The
     special value [none] is used if the block is empty. *)

  element: point array; (* of size [max_block] *)

  (* Conversely, we maintain a mapping of points to blocks. The
     special value [detached] is used if a point is not currently
     attached to any block. *)

  block: block array; (* of size [n] *)

}

let make n =
  (* We assume [max_block] is [n]. *)
  let max_block = n in
  {
    permutation = Permutation.make n;
      (* Every point initially is isolated. *)
    size = Array.make max_block 0;
      (* Every block initially has size 0. *)
    element = Array.make max_block none;
      (* Every block initially has no elements. *)
    block = Array.make n detached;
      (* Every point initially is detached. *)
  }

let attach p b x =
  (* Sanity checking. *)
  assert (
    let n = Permutation.size p.permutation in
    let max_block = n in
    0 <= b && b < max_block &&
    0 <= x && x < n &&
    Permutation.is_isolated p.permutation x &&
    p.block.(x) = detached
  );
  (* [x]'s block is now [b]. *)
  p.block.(x) <- b;
  let y = p.element.(b) in
  if y = none then begin
    (* The block [b] was empty. It now has just one element [x]. *)
    assert (p.size.(b) = 0);
    p.size.(b) <- 1;
    p.element.(b) <- x
  end
  else begin
    (* The block [b] was non-empty. It now has one more element.
       [x] must be inserted into the cycle that represents [b]. *)
    p.size.(b) <- p.size.(b) + 1;
    Permutation.transpose p.permutation x y
  end

let detach p x =
  (* Sanity checking. *)
  assert (
    let n = Permutation.size p.permutation in
    0 <= x && x < n &&
    p.block.(x) <> detached
  );
  let b = p.block.(x) in
  p.block.(x) <- detached;
  if p.size.(b) = 1 then begin
    (* The block [b] had just one element. It is now empty. *)
    assert (p.element.(b) = x);
    p.size.(b) <- 0;
    p.element.(b) <- none
  end
  else begin
    (* The block [b] had more than one element. It now has one
       fewer elements. If its distinguished element was [x],
       this must be changed. Furthermore, [x] must be taken
       out of the cycle that represents [b]. *)
    p.size.(b) <- p.size.(b) - 1;
    if p.element.(b) = x then
      p.element.(b) <- Permutation.next p.permutation x;
    Permutation.isolate p.permutation x
  end

let move p b x =
  detach p x;
  attach p b x

(* Read accessors. *)

let size p b =
  p.size.(b)

let element p b =
  let y = p.element.(b) in
  assert (y <> none);
  y

let block p x =
  let b = p.block.(x) in
  assert (b <> detached);
  b

let cycle p x =
  Permutation.cycle p.permutation x

