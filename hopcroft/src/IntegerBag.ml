(* A bag is represented as a linked list stored in an integer array.
   This low-level encoding seems attractive because it is compact. *)

(* [b.head] is always [nil] or a valid array index, which means [cons].
   An array entry can be [absent], [nil], or a valid array index, which
   means [cons]. A list is stored in the array: by beginning at [head]
   and following [cons] pointers, one eventually ends up on [nil]. *)

(* TEMPORARY could make things even more compact by storing [head] as
   an extra array cell *)

type bag = {
  mutable head: int;
  data: int array;
}

let nil =
  -1

let absent =
  -2

let make n = {
  head = nil;
  data = Array.make n absent;
}

let add_new_safe b x =
  b.data.(x) <- b.head;
  b.head <- x

let add_new b x =
  assert (b.data.(x) = absent);
  add_new_safe b x

let add b x =
  if b.data.(x) = absent then
    add_new_safe b x

let mem b x =
  b.data.(x) <> absent

let choose b =
  let x = b.head in
  if x = nil then
    None
  else begin
    assert (b.data.(x) <> absent);
    b.head <- b.data.(x);
    b.data.(x) <- absent;
    Some x
  end

let rec repeat b f =
  let x = b.head in
  if x = nil then
    ()
  else begin
    assert (b.data.(x) <> absent);
    b.head <- b.data.(x);
    b.data.(x) <- absent;
    f x;
    repeat b f
  end

module Make (E : sig
  type element
  val n: int
  val encode: element -> int
  val decode: int -> element
end) = struct
  open E
  let b = make n
  let add x = add b (encode x)
  let add_new x = add_new b (encode x)
  let choose () =
    match choose b with None -> None | Some x -> Some (decode x)
  let mem x = mem b (encode x)
  let repeat f = repeat b (fun x -> f (decode x))
end

