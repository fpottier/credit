(* List utilities. *)

let rec uniq1 cmp x ys =
  match ys with
  | [] ->
      []
  | y :: ys ->
      if cmp x y = 0 then
        uniq1 compare x ys
      else
        y :: uniq1 cmp y ys

let uniq cmp xs =
  match xs with
  | [] ->
      []
  | x :: xs ->
      x :: uniq1 cmp x xs

let weed cmp xs =
  uniq cmp (List.sort cmp xs)

let pairwise_distinct cmp xs =
  List.length xs = List.length (weed cmp xs)

(* Probability that a node has an outgoing transition. *)

let outgoing =
  0.5

(* Build an array of outgoing transitions. Every node has zero or
   one outgoing transition -- this is a deterministic automaton. *)

let transitions n =
  Array.init n (fun _ ->
    if Random.float 1.0 < outgoing then
      Random.int n
    else
      -1
  )

(* Probability that a node is "final". *)

let final =
  0.2

let final n =
  Array.init n (fun _ ->
    if Random.float 1.0 < final then 1 else 0
  )

(* A random automaton of [n] states and [f] symbols. *)

let automaton n f =
  let transitions = Array.init f (fun _ -> transitions n)
  and final = final n in
  transitions, final

(* Determining whether [y] simulates [x]. *)

let inclusion transitions representative s x y : bool =
  let sx = transitions.(s).(x)
  and sy = transitions.(s).(y) in
  sx = -1 ||
    (* If [x] has an outgoing edge along [s], then [y] should have one too. *)
    sy <> -1 &&
    (* And the two edges should lead into the same block. *)
    representative sx = representative sy

let rec for_every i j f =
  i = j || f i && for_every (i + 1) j f

let inclusion transitions representative x y : bool =
  let f = Array.length transitions in
  for_every 0 f (fun s -> inclusion transitions representative s x y)

let equivalent (transitions, final) representative x y : bool =
  final.(x) = final.(y) &&
  inclusion transitions representative x y &&
  inclusion transitions representative y x

let stable (transitions, final) representative : bool =
  let n = Array.length final in
  for_every 0 n (fun x ->
    let rx = representative x in
    equivalent (transitions, final) representative x rx
  )

(* Printing. *)

open Printf

let print_automaton (_transitions, final) =
  let n = Array.length final in
  printf "Automaton: %d states\n" n;
  printf "Final: ";
  Array.iter (fun b -> printf "%d; " b) final;
  printf "\n%!"

(* Testing. *)

let test n f =
  (* Contruct a random automaton. *)
  let transitions, final = automaton n f in
  let module G = struct
    let n = n
    let f = f
    let b = 2
    let group i = final.(i)
    let for_every_edge action =
      (* TEMPORARY not super efficient *)
      Array.iteri (fun s transitions ->
        Array.iteri (fun x y ->
          if y <> -1 then
            action x s y
        ) transitions
      ) transitions
  end in
  (* Run the algorithm. *)
  let module H = Hopcroft.Run(G) in
  let partition = Lazy.force H.partition in
  Printf.printf "n = %d, blocks = %d\n%!" n (List.length partition);
  (* Check that [partition] and [representative] are consistent with one another. *)
  (* 1. All elements of a block have the same representative. *)
  List.iter (fun block ->
    assert (block <> []);
    let r = H.representative (List.hd block) in
    List.iter (fun x ->
      assert (H.representative x = r)
    ) block
  ) partition;
  (* 2. Distinct blocks have distinct representatives. *)
  let representatives = List.map (fun block -> H.representative (List.hd block)) partition in
  assert (pairwise_distinct compare representatives);
  (* Check that the partition respects every relation. *)
  assert (stable (transitions, final) H.representative);
  (* Check that the partition is coarsest. For each pair of points [x]
     and [y] in distinct blocks, check that they cannot be identified. *)
  let representatives = Array.of_list representatives in
  let nb = Array.length representatives in
  for x = 0 to nb - 1 do
    for y = x + 1 to nb - 1 do
      let rx = representatives.(x)
      and ry = representatives.(y) in
      (* Define a [representative] function that merges the blocks of [rx] and [ry]. *)
      let representative z =
        let rz = H.representative z in
        if rz = ry then rx else rz
      in
      (* Check that it is NOT stable. *)
      (* assert (not (stable (transitions, final) representative)) *)
      (* It suffices to check that [rx] and [ry] are NOT equivalent. *)
      assert (not (equivalent (transitions, final) representative rx ry))
    done
  done

let () =
  for n = 0 to 100 do
    for f = 0 to 8 do
      test n f
    done
  done;
  for f = 1 to 10 do
    test 1000 (100 * f)
  done;
  for n = 1 to 100 do
    test (100 * n) 5
  done

(* TEMPORARY for fun, try Berstel and Carton's automaton *)

