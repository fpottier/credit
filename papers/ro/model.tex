\section{Model}
\label{sec:model}


In this section, we establish the soundness of our extension of \SL. We first
provide a concrete model of permissions, then give an interpretation of
triples with respect to which each of the reasoning rules is proved sound.

%------------------------------------------------------------------------------
\subsection{A model of permissions}
\label{sec:real}

In traditional \SL, a permission is interpreted as a predicate over heaps,
also known as heap fragments. There, a ``heap'' coincides with what we have
called a ``memory'', that is, a finite map of memory locations to values.
%
Then, famously, a separating conjunction $H_1 \Hstar H_2$ is satisfied by a
heap~$h$ if and only if $h$ is the disjoint union of two subheaps that
respectively satisfy $H_1$ and~$H_2$.

In the following, we need a slightly more complex model, where a ``heap'' is a
richer object than a ``memory''. Whereas a memory maps locations to values, a
heap must additionally map memory locations to access rights: that is, it must
keep track of which memory locations are considered accessible for reading and
writing and which memory locations are accessible only for reading.
A permission remains interpreted as a predicate over heaps.

\newcommand{\footnotedisjoint}{\footnote{Technically, in Coq, a heap is
defined as a pair $(f,r)$ accompanied with a proof that $f$ and $r$
have disjoint domains. Thus, whenever in the paper we assemble a heap $(f, r)$,
we have an implicit obligation to prove
$\OMdom{f} \OSinter \OMdom{r} = \OMempty$.}}
% In Coq, this proof obligation arises automatically, thanks to the use of
% the ``\F{Program}'' mechanism.

\begin{comment}
  In Coq, we define a heap as follows:
  \begin{coq}
    Definition heap := { h :
      (state*state)%type | let '(f,r) := h in state_disjoint f r }.
      Definition hprop := heap -> Prop. (* type of permissions *)
  \end{coq}
\end{comment}

We let a ``heap'' be a pair $(f, r)$ of two memories~$f$ and~$r$ whose domains
are disjoint\footnotedisjoint. (We let $f$ and $r$ range over memories.)
% The metavariable~$m$ was already used for this purpose; see \figref{fig:red}.
The memory~$f$ represents the memory cells that are \underline{f}ully
accessible, that is, accessible for reading and writing. The memory~$r$
represents the memory cells that are accessible only for \underline{r}eading.
We note that there exist other (isomorphic) concrete representations of heaps:
for instance, we could have defined a heap as a map of memory locations to
pairs of a value and an access right (either ``read-write'' or ``read-only'').

% or: two maps with the same domain:
% a pair of a memory~$m$ and a map~$a$ which associates a Boolean access right
% with every location~$l$ in the domain of~$m$.

We let $h$ range over heaps. We write $\Hf{h}$ for the first component of the
pair~$h$ (that is, the read-write memory) and $\Hr{h}$ for its second
component (the read-only memory).

\input{auxiliary}
\input{figure-model}

In traditional \SL, two heaps are compatible (that is, can be composed) if and
only if they have disjoint domains, and their composition is just their union.
Here, because heaps contain information about access rights, we need slightly
more complex notions of compatibility and composition of heaps. These notions
are defined in \figref{fig:auxiliary}.
%
We first introduce a few notations.
%
We write $m_1 \Hdis m_2$ when the memories~$m_1$ and~$m_2$ have disjoint domains.
By extension, we write $m_1 \Hdis m_2 \Hdis m_3$ when
the memories $m_1$, $m_2$ and~$m_3$ have pairwise disjoint domains.
%
We write $\Hcompat{r_1}{r_2}$ when the memories $r_1$ and $r_2$ agree where
their domains overlap.
%

%paragraph break, c'est fait exprès.

These notations allow us to succinctly state when two heaps~$h_1$ and~$h_2$
are compatible. First, the read-only components of $h_1$ and~$h_2$ must agree where
their domains overlap. Second, the read-write component of $h_1$, the read-write
component of $h_2$, and the combined read-only components of $h_1$ and $h_2$ must have
pairwise disjoint domains.
%
When two heaps~$h_1$ and~$h_2$ are compatible, they can be composed.
Composition is performed component-wise, that is, by taking the (disjoint)
union of the read-write components and the (compatible) union of the read-only
components.
% the proof obligation:
(The %compatibility hypothesis
hypothesis that~$h_1$ and~$h_2$ are compatible
is used to meet the proof
obligation that $\Hunionof{h_1}{h_2}$ is a well-formed heap
whose read-write and read-only components have disjoint domains.)
%
Composition, where defined, is associative and commutative.
% LATER this is analogous to composition of heaps in Mezzo's model
% there, the read-only part is permanently immutable
% the difference is that Mezzo does not have RO
% LATER it is also essentially identical to composition of ``fractional heaps''~\cite{boyland-nesting-10}
% in the special case where every permission is either ``epsilon'' or 1

The interpretation of permissions appears in \figref{fig:model}. The
interpretation of the standard permission forms is essentially standard:
superficial adaptations are required to deal with the fact that a heap is a
pair of a read-write memory and a read-only memory.
%
The permission $\HPprop{P}$ is satisfied by a heap whose read-write
and read-only components are both empty, provided the proposition $P$
holds.
%
The permission $\HPsingle{l}{v}$ is satisfied by a heap whose read-write component
is a singleton memory $\OMone{l}{v}$ and whose read-only component is empty.
%
The permission $\Hstarof{H_1}{H_2}$ is satisfied by a heap $h$ if and only if
$h$ is the composition of two (compatible) subheaps $h_1$ and $h_2$ which
respectively satisfy $H_1$ and $H_2$.
%
The interpretation of disjunction %, conjunction,
and existential quantification is standard.
%
\begin{comment}
% fp: coupé tout ça car orthogonal aux permissions RO et standard
The permission $\HPorof{H_1}{H_2}$ holds of a heap
$h$ if either $H_1$ holds of $h$ or $H_2$ holds of $h$.
%
%The permission $\HPandof{H_1}{H_2}$ holds of a heap
%$h$ if both $H_1$ holds of $h$ and $H_2$ holds of $h$.
%
The permission $\HPexinameof{x}{H}$ holds of a heap $h$
if there exists an entity $x$ such that the permission
$H$, instantiated on that entity $x$, holds of $h$.
The corresponding Coq definition, shown below, properly handles
the binder.
%
\begin{coq}
  Definition hprop_exists (A : Type) (H : A -> hprop) : hprop :=
    fun h => exists x, H x h.
  Notation "'Hexists' x , H" := (hprop_exists (fun x => H)).
\end{coq}
%
% fp: ça, si on voulait le dire, cele devrait être dit plus tôt,
%     quand on introduit la syntaxe des permissions.
Observe that the entity $x$ being quantified has an arbitrary type.
In particular, $x$ might denote a permission.
The possibility to existentially
quantify over permissions is essential for the definition
of abstract representation predicates in \SL.
\end{comment}

% fp:
% In words, the meaning of $\HRO{H}$ could be described as follows: ``$H$ would
% hold, if it weren't for the fact that we do not have write access to any
% memory locations''.

% arthur:
% "RO(H) covers the same set of locations as H, but with all of them tagged as read-only."
% ou, alternativement:
% "RO(H) covers the same set of locations as H, but with the cells
% tagged in H as read-write being now tagged as read-only".

A key aspect is the interpretation of $\HRO{H}$.
%
A human-readable, yet relatively accurate rendering of the formal meaning
of $\HRO{H}$ is as follows:
%
``we do not have write access to any memory locations, but if we did have
read-write (instead of read-only) access to certain locations, then $H$~would
hold''.
%
Technically, this is expressed as follows.
The permission $\HRO{H}$ is satisfied by a heap~$h$
if % and only if
(1)~the read-write component of~$h$ is empty
and
(2)~the read-only component of~$h$ is of the form $\Hf{h'} \OMdisuni \Hr{h'}$,
where $h'$ satisfies $H$.
%
Thus, ``$\HROname$'' is a modality: it changes the ``world'' (the heap) with
respect to which $H$~is interpreted. In the outside world~$h$, everything must
be marked as read-only, whereas in the inside world~$h'$, some locations may
be marked as read-write.

As explained earlier (\sref{sec:normal}), the meaning of $\HNormal{H}$ is
defined as follows: a permission $H$ is normal if and only if every heap~$h$
that satisfies it has an empty read-only component.

Entailment is defined in a standard way: $\Himplof{H_1}{H_2}$ holds if every
heap that satisfies $H_1$ also satisfies $H_2$.

\begin{lemma}
  The above definitions validate the laws listed in Figures~\ref{fig:ro}
  and~\ref{fig:normal}.
\end{lemma}

%------------------------------------------------------------------------------
\subsection{A model of triples} % and a statement of soundness

We now wish to assign an interpretation to a Hoare triple of the form
\W{\Jhoare{H}{t}{Q}}. Then, each of the reasoning rules of the logic can be
proved sound, independently, by checking that it is a valid lemma.
%
\begin{comment}
% Ancienne version.
For expository purposes, we first define a slightly simplified interpretation
of triples, for which we write \W{\Jhoareprelim{H}{t}{Q}}. This interpretation
is ``almost right'', but does not allow discarding permissions: that is, it
does not validate the rules \RULE{discard-pre} and \RULE{discard-post} of
\fref{fig:rules}. Then, we define the truly desired interpretation, for which
we write \W{\Jhoare{H}{t}{Q}}, as a simple variation on top of the previous
interpretation.
\end{comment}
%
Before giving this interpretation, a couple of auxiliary definitions are
needed.

First, if $h$ is a heap, let $\Hflatten{h}$ be the memory
$\OMdisuniof{\Hf{h}\,}{\,\Hr{h}}$. That is, $\Hflatten{h}$ is the memory
obtained by forgetting the distinction between read-write and read-only
locations in the heap $h$.
%
This definition serves as a link between memories, which exist at runtime
(they appear in the operational semantics: see \fref{fig:red}), and heaps,
which additionally contain access right information. This information does not
exist at runtime: it is ``ghost'' data.

Second, if $H$ is a permission,
\begin{comment}
% Ancienne version, on-rw:
let $\Honrw{H}$ be the permission
(that~is, the predicate over heaps)
that holds of a heap~$h$ if and only if
$H$ holds of the heap $(\Hf{h}, \OMempty)$.
% fp: inutile, en fait:
% Let us write $\Honrw{Q}$ for $\lambda y.\Honrw{Q\;y}$.
% fp: je crois que si H est normale,
% alors $\Honrw{H}$ est équivalente à
% $H \Hstar \HRO{\mathit{true}}$.
Thus, ``$\Honrw{H}$'' means, ``on the read-write component, $H$ holds''.
\end{comment}
let $\Honrwsub{H}$ stand for the permission
(that~is, the predicate over heaps)
that holds of a heap~$h$ if and only if
$h$ is the composition of two heaps $h_1$ and~$h_2$,
where $h_1$ has an empty read-only component
and satisfies~$H$.
In mathematical notation:
%
\[\begin{array}{l}
\Honrwsub{H} \Sc\equiv {} \\
\qquad
\lambda h.\,
\exists h_1 h_2.\, \HStarable{h_1}{h_2}
\Sc\land h = \Hunionof{h_1}{h_2}
\Sc\land \Hr{h_1} = \OMempty
\Sc\land H\,h_1
\end{array}\]
% fp: c'est bête, car ça répète beaucoup l'interprétation de \Hstar.
%     si on pouvait dire que on-rw-sub(H) = on-rw(H * true)
%     pour ma définition de on-rw,
%     ou bien on-rw(H) * true pour une définition appropriée de on-rw
%     (attention à ces subtilités)
%     ce serait pas mal. LATER
Intuitively, $\Honrwsub{H}\,h$ asserts that $H$ holds of a
fragment of $h$ whose read-only component is empty. This
definition is used in the following to precisely express the meaning of
postconditions, which in reality do not apply to the whole final heap, but to
some fragment of the final heap whose read-only component is empty,
% fp: the postcondition does not ``see'' the read-only locations,
%     even though they exist
or equivalently, to ``some read-write fragment'' of the final heap.

The interpretation of triples is now defined as follows:
%
\begin{definition}[Interpretation of triples]
\label{def:septriple}
A semantic Hoare triple $\Jhoare{H}{t}{Q}$
is a short-hand for the following statement:
\[
% \Jhoareprelim{H}{t}{Q}
% \Scs\equiv
\Tfor{h_1 h_2}
\begin{ands}
\HStarable{h_1}{h_2}
\\[1pt]
H\,h_1
\end{ands}
\impl\; \Texi{v h_1'}
\begin{ands}
\HStarable{h_1'}{h_2}
\\[1pt]
\Jredbigconfigof{ t }{ \,\Hflatten{h_1 \Hunion h_2} }{ v }{ \,\Hflatten{h_1' \Hunion h_2} }
\\[1pt]
\Hr{h_1'} = \Hr{h_1}
\\[1pt]
\Honrwsub{Q\,v}\,h_1'
\end{ands}
\]
\end{definition}

\newcommand{\footnoteunchanged}{\footnote{That is, no read-write locations
  become read-only, or vice-versa. This reflects the fact that ``$\HROname$''
  permissions appear and disappear following a lexical scope discipline.
  As a consequence, any newly-allocated memory cells must be marked
  read-write in $h'_1$.}}

\newcommand{\footnoteonrw}{\footnote{%
  The operator~$\Honrwsubname$ is used here for two reasons.
  First, in the presence of the rules \RULE{discard-pre} and \RULE{discard-post},
  which discard arbitrary permissions,
  one cannot expect the postcondition $Q\;v$ to be satisfied by the whole final heap~$h'_1$.
  Instead, one should expect $Q\;v$ to be satisfied by a fragment of $h'_1$.
  Second,
  $Q\;v$ is typically a normal permission,
  which can be satisfied only by a heap whose read-only component is empty.
  So, one may expect that $Q\;v$ is satisfied by a ``read-write'' fragment of $h'_1$.}}

% fp:
% ceci clarifie le fait que nos postconditions sont implicitement sujettes
% à une modalité on-rw-sub implicite. Bien qu'elles affirment (en apparence)
% que la zone read-only est vide à la fin, en réalité, elles ignorent la
% zone read-only, qui est en réalité préservée (ceci est vissé dans
% l'interprétation des triplets)

% fp: en fait l'explication était un peu différente quand on faisait on-rw
% d'abord puis _ * true dans un second temps. Ici les deux explications se
% téléscopent un peu.

In order to understand this definition, it is useful to first read the special
case where the heap~$h_2$ is empty:
\[
% \Jhoareprelim{H}{t}{Q}
% \Scs\equiv
\Tfor{h_1} \;
H\,h_1
\impl\, \Texi{v h_1'}
\begin{ands}
\Jredbigconfigof{ t }{ \Hflatten{h_1} }{ v }{ \Hflatten{h_1'} }
\\[1pt]
\Hr{h_1'} = \Hr{h_1}
\\[1pt]
\Honrwsub{Q\,v}\,h_1'
\end{ands}
\]

This may be read as follows. Let~$h_1$ be an arbitrary initial heap that
satisfies the permission~$H$. Then, the term~$t$, placed in the
memory~$\Hflatten{h_1}$, runs safely and terminates, returning a value~$v$ in
a final memory that can be described as~$\Hflatten{h'_1}$, for some heap
$h'_1$. The heap~$h'_1$ obeys the constraint $\Hr{h_1'} = \Hr{h_1}$, which
means that the set of all read-only locations is unchanged\footnoteunchanged{}
and that the content of these locations is unchanged as well. Last, the
permission $Q\;v$ is satisfied by some read-write fragment of the heap~$h'_1$\footnoteonrw.

In the general case, where $h_2$ is an arbitrary heap, the definition of
\W{\Jhoare{H}{t}{Q}} states that the execution of the term~$t$ cannot
affect a subheap~$h_2$ which $t$ ``does not know about''. Thus, running~$t$
in an initial heap $\Hflatten{h_1 \Hunion h_2}$
% (where $h_1$ and $h_2$ are compatible)
must yield a final heap of the form $\Hflatten{h'_1 \Hunion h_2}$.
% (where $h'_1$ and $h_2$ are compatible, too).
This requirement has the effect of ``building the frame rule into the
interpretation of triples''. It is standard
\cite[Definition~11]{dinsdale-young-views-13}.
% LATER autres citations? Iris. Un article classique d'O'Hearn, Calcagno, ou autre.
% LATER un peu bizarre de citer Views ici et nulle part ailleurs
We note that the memories $\Hflatten{h_1}$ and $\Hflatten{h_2}$ are not
necessarily disjoint: indeed, the read-only components of the heaps $h_1$ and~$h_2$
may have overlapping domains.
% It is also the intersection of the read-only components of the heaps $h'_1$ and~$h_2$.

\begin{comment}
% Ancienne version:
This preliminary interpretation of triples does not validate the rules
\RULE{discard-pre} and \RULE{discard-post}. For instance, the (semantic)
triple $\Jhoareprelim{\HPsingle{l}{v}}{\Ltt}{\lambda\Ltt.\HPempty}$ is
invalid%
\footnote{
We may prove that the triple $\Jhoareprelim{\HPsingle{l}{v}}{\Ltt}{\lambda\Ltt.\HPempty}$
is invalid as follows.
The singleton heap $\OMone{l}{v}$ satisfies its precondition.
If this triple was valid, the final heap, which is also~$\OMone{l}{v}$, would
satisfy the postcondition~$\HPempty$. However, only the empty heap satisfies
$\HPempty$; contradiction.}.
%
In order to validate the rules \RULE{discard-pre} and \RULE{discard-post}, it
suffices to allow the postcondition $Q$ to apply to part of the heap, as
opposed to the entire heap. This can be concisely expressed as shown below,
where $\HGC$ denotes the permission that is satisfied by every heap
(recall from \secref{sec:reasoning-rules} that $\HGC$ is defined as $\HPexists{H}{H}$).
\end{comment}

% Based on this definition, we are able to establish soundness.

\begin{theorem}[Soundness]
\label{th:soundness-basic}
The above definition of triples validates all of the reasoning rules
of Figures~\ref{fig:rules} and~\ref{fig:term-rules}.
\end{theorem}
\begin{proof}
We refer the reader to our Coq formalization~\cite{online}. \qed
\end{proof}

\begin{comment}
  % LONG VERSION
  %------------------------------------------------------------------------------
  \subsection{Discussion}

  gc-pre is needed, it is not derivable by frame

  normal does not distribute on star:
    normal(H1) and normal(H2) imply normal(H1 * H2),
    but the converse is false.
    Just choose H1 and H2 so that H1 * H2 = false.
    e.g. H1 = RO(l pointsto 1) and H2 = RO(l pointsto 2)
    Then normal(H1 * H2) holds
    but normal(H1) and normal(H2) do not.

  equivalence with direct definition of read-only singleton heap.
\end{comment}

%  LocalWords:  Hstar subheaps newcommand footnotedisjoint OMdom OSinter ully
%  LocalWords:  OMdom OMempty hprop eading figref Hdis Hcompat Hunionof OMone
%  LocalWords:  HPprop HPsingle Hstarof HPorof HPexinameof Hexists OMdisuni
%  LocalWords:  HROname sref HNormal Himplof Jhoare Jhoareprelim fref Honrw
%  LocalWords:  Hflatten OMdisuniof Honrwsub equiv qquad HStarable septriple
%  LocalWords:  Tfor impl Texi Jredbigconfigof Hunion Hunion footnoteonrw Ltt
%  LocalWords:  footnoteunchanged subheap dinsdale-young-views-13 Ltt HPempty
%  LocalWords:  secref HPexists th qed gc-pre pointsto pointsto
