% ------------------------------------------------------------------------------

\section{Overview}
\label{sec:overview}

In this section, we describe our additions to \SL, with which we assume a
basic level of familiarity. The following sections describe our logic
(\sref{sec:logic}) and its model (\sref{sec:model}) in full, and may serve as
a reference.

% ------------------------------------------------------------------------------

\subsection{A ``read-only'' modality}
\label{sec:overview:ro}

\begin{figure}[t]
\input{ro}
\hrule
\caption{Properties of \HROname}
\label{fig:ro}
\end{figure}

To begin with, we introduce read-only permissions in the syntax of
permissions. Informally, the permission $\HRO{H}$ controls the same heap
fragment as the permission~$H$, but can be used only for reading.
%
A more precise understanding of the meaning of ``$\HROname$'' is given by the
semantic model (\sref{sec:real}).

The ``$\HROname$'' modality enjoys several important properties, shown in
\fref{fig:ro},
%
where the symbol~$\Himpl$ denotes entailment.
% Pure:
When applied to a pure assertion~$\HPprop{P}$, ``$\HROname$'' vanishes.
% Separating conjunction:
It can be pushed into a separating conjunction:
$\HRO{\HPstarof{H_1}{H_2}}$ entails $\HPstarof{\HRO{H_1}}{\HRO{H_2}}$.
The reverse entailment is false\footnote{If it were true, then we would have
the following chain of equalities:
$\HRO{\HPsingle{l}{v}} =
 \Hstarof{\HRO{\HPsingle{l}{v}}}{\HRO{\HPsingle{l}{v}}} =
 \HRO{\Hstarof{\HPsingle{l}{v}}{\HPsingle{l}{v}}} =
 \HRO{\HPprop{\F{False}}} =
 \HPprop{\F{False}}$.
}.
Because of this, one might worry that exploiting the entailment
$\HRO{\HPstarof{H_1}{H_2}} \Sc\Himpl \HPstarof{\HRO{H_1}}{\HRO{H_2}}$
causes a loss of information. This is true, but if that is a problem,
then one can exploit the equality
$\HRO{\HPstarof{H_1}{H_2}} =
 \HRO{\HPstarof{H_1}{H_2}} \Sc\Hstar \HPstarof{\HRO{H_1}}{\HRO{H_2}}$
instead.
%
% Disjunction and existential quantification.
``$\HROname$'' commutes with disjunction and existential quantification.
% (fractional permissions do not have this property)
%
% Idempotence.
``$\HROname$'' is idempotent\footnote{In practice, this property should not be
  useful, because permissions of the form $\HRO{\HRO{H}}$ never appear: the
  \rofr (\sref{sec:rofr}) is formulated in such a way that it cannot give rise
  to such a permission.}.
%
A read-only permission for a single cell of memory,
$\HRO{\HPsingle{l}{v}}$,
cannot be rewritten into a simpler form,
which is why there is no equation for it in \fref{fig:ro}.
It can be exploited via the new read axiom (\sref{sec:overview:read}).
%
The last two lines of \fref{fig:ro} respectively state that ``$\HROname$'' is
covariant and that read-only permissions are duplicable.

Together, these rules allow pushing ``$\HROname$'' into composite permissions.
For instance, if $\Htable{h}{M}$ is defined as before (\sref{sec:intro},
(\ref{def:Htable})), then the read-only permission
$ \HRO{\Htable{h}{M}} $
entails:
\[
% \HRO{\Htable{h}{M}}
% \Sc\Hstar
\HPexinameof{a} { \HPexinameof{L} {\left(
  \HRO{\Hrecord{h}{a}} \Hstar \HRO{\Hdataarray{a}{L}} \Hstar \HRO{\ldots}
\right)}}
\]
In other words, ``read-only access to a hash table'' implies read-only access
to the component objects of the table, as expected.

% ------------------------------------------------------------------------------

\subsection{A \rofr}
\label{sec:rofr}
\label{sec:normal}

% fp: on aimerait écrire que l'invariant est:
%   a permission~$H$ must never coexist with its read-only form $\HRO{H}$
% mais en fait c'est trop fort,
% car si H est une permission RO, il n'y a pas de danger à autoriser cette cohabitation.
%   RO(RO(H)) * RO(H) = RO(H) * RO(H) = RO(H)
% On pourrait alors écrire:
%   a ``normal'' permission~$H$ must never coexist with its read-only form $\HRO{H}$
% mais à ce point de l'article,
% on n'a pas encore défini le mot ``normal'', et ça vient un peu tôt.
%
% Je propose donc de dire qu'une location ne doit pas être accessible
% à la fois via une permission read-write
% et via une permission read-only.
% C'est d'ailleurs bien ça le vrai invariant sémantique,
% puisque dans les heaps que tu manipules ensuite,
% tu exiges que la partie read-write et la partie read-only
% soient de domaines disjoints.

To guarantee the soundness of our extension of \SL with read-only permissions,
we must enforce one key metatheoretical invariant, namely:
%
a~memory location is never governed at the same time
by a read-write permission and
by a read permission.
%
Indeed, if two such permissions were allowed to coexist, the read-write
permission by itself would allow writing a new value to this memory location.
The read permission would not be updated
(it could be framed out, hence invisible, during the write)
and would therefore become stale (that is, carry out-of-date information about the value
stored at this location).
That would be unsound.

In order to forbid the co-existence of read-write and read-only permissions
for a single location, we propose enforcing the following informal rules:
% fp: on pourrait fusionner 3-4 si on voulait.
%     on pourrait aussi sortir 1 de l'énumération,
%     en l'annonçant à l'avance.
%     on aurait alors seulement 2 règles, entrée-du-bloc et sortie-du-bloc.
%     mais ça ne change pas grand-chose je pense, c'est informel.
\begin{enumerate}
\item \label{rule:lexical}
      Read-only permissions obey a lexical scope (or ``block'') discipline.
\item \label{rule:entry}
      Upon entry into a block,
      a permission~$H$ \\ can be replaced
      with its read-only counterpart $\HRO{H}$.
\item \label{rule:exit}
      Upon exit of this block,
      the permission~$H$ reappears.
\item \label{rule:exit:ro}
      No read-only permissions are allowed to exit the block.
\end{enumerate}
Roughly speaking, there is no danger of co-existence
between read-write and read permissions
when a block is entered,
because $H$ is removed at the same time $\HRO{H}$ is introduced.
There is no danger either when this block is exited,
even though $H$ reappears,
because no read-only permissions are allowed to exit.

Technically, all four informal rules above take the form of a single reasoning
rule, the ``\rofr'', which subsumes the frame rule of \SL. The frame rule
allows an assertion~$H'$ to become hidden inside a block: $H'$ disappears upon
entry, and reappears upon exit of the block. The \rofr does this as well, and
in addition, makes the read-only permission $\HRO{H'}$ available within the
block. In our system, the two rules are as follows:
%
\begin{mathpar}
\inferrule*[lab=frame rule]
  { \Jhoare{H}{t}{Q} \\ \HNormal{H'} }
  { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }

\inferrule*[lab=\rofr]
  {  \Jhoare{H \Hstar \HRO{H'}  }{t}{Q}  \\ \HNormal{H'} }
  { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }
\end{mathpar}

The frame rule (above, left) is in fact just a special case of
the \rofr (above, right). Indeed,
a read-only permission can be discarded at any time
(using rule \RULE{discard-pre} from \fref{fig:rules}).
% the rule \RULE{discard-pre} (\fref{fig:rules})
%states that any permission can be discarded at any time.
Thus, the frame rule can be derived
by applying the \rofr and immediately discarding $\HRO{H'}$.

\input{normal}

Both rules above have the side condition $\HNormal{H'}$, which requires $H'$
to be ``normal''. Its role is to ensure that ``no read-only permissions are
allowed to exit the block''. ``Normality'' can be understood in several ways:
\begin{enumerate}
\item
A~syntactic understanding is that a permission is ``normal'' if ``$\HROname$''
does not occur in it. This view is supported by the rules in
\fref{fig:normal}. The one thing to remark about these rules
is that there is no rule whose conclusion is $\HNormalof{\HRO{H}}$.
%
% fp: faut-il préciser que:
% - quand on a une variable de type hprop, elle peut être normale ou non,
%   cela dépend des hypothèses que l'on a sur elle
% - quand on a un prédicat abstrait comme HashTable, il est normal si et
%   seulement si son expansion est normal
%   (en fait cela se ramène à la remarque précédente)
%
\item
A semantic understanding is given when we set up a model of our logic
(\sref{sec:real}). There, we define what it means for a heap, where
each memory location is marked either as read-write or as read-only, to
satisfy a permission. Then, a permission is ``normal'' if a
heap that satisfies it cannot contain any read-only memory locations.
\end{enumerate}
% fp: j'ai suivi ta remarque et formulé la définition de normal
% de façon plus positive.

\newcommand{\footnotenormality}{\footnote{If we restricted the
rule of \RULE{consequence} in \fref{fig:rules} by adding the
side condition $\HNormal{Q}$,
then we would be able to prove that every triple $\Jhoare{H}{t}{Q}$
that can be established using the reasoning rules
has a normal postcondition~$Q$.
We did not restrict the rule of consequence in this way because
this is technically not necessary.}}

Because of the normality condition in the \rofr, a Hoare triple
$\Jhoare{H}{t}{Q}$ typically\footnotenormality{} has a normal
postcondition~$Q$.
% implicite: il faut définir ``normal Q'' par ``forall x, normal (Q x)''.
This means that read-only permissions can only be ``passed down'', from caller
to callee. They cannot be ``passed back up'', from callee to caller.

% So, not only is not necessary/useful to return RO permissions;
%   (which is a strength; cf. our shortcoming 1)
% but it is actually forbidden.

\begin{comment}
As the traditional frame rule of \SL does not have a ``normality'' side
condition, one might wonder if our frame rule is less powerful than the
traditional rule. That is not the case. In traditional \SL, there are
no read-only permissions, so every permission is ``normal''.
% LATER fp: c'est faux! la permission true, qui existe en \SL traditionnelle,
% n'est pas ``normale'' chez nous. Donc on ne peut pas appliquer la règle frame
% à la permission true.
% LATER fp: je note d'ailleurs que nous pourrions ajouter la permission
% ``true'' à notre système, car elle n'est pas codable dans l'une des autres
% formes. À moins de la définir comme $\exists H.H$, mais je note d'ailleurs
% qu'il nous manque les variables dans la syntaxe des H pour pouvoir écrire
% cette formule. Enfin je note que ``true = GC''.
Our logic is a conservative extension of \SL: every proof that can be carried
out in \SL can also be carried out in our logic.
% LATER cf. discussion par email: on pourrait indiquer comment lifter une
% assertion de memory -> Prop vers heap -> Prop,
% et en déduire un codage systématique de \SL vers SLRO.
\end{comment}

% ------------------------------------------------------------------------------

\subsection{A framed sequencing rule}
\label{sec:overview:seq}

The sequencing rule of \SL (below, left) remains sound in our logic. However,
in a setting where postconditions must be normal (or are typically normal),
this rule is weaker than desired: it does not allow read-only permissions to
be distributed into its second premise.
%
Indeed, suppose~$Q'$ is normal, as it is the postcondition of the first
premise. Unfortunately, $Q'$ is also the precondition of the second premise.
This means that no read-only permissions are available in the proof of~$t_2$.
%
Yet, in practice, it is useful and desirable to be able to thread one or more
read-only permissions through a sequence of instructions.
%
We remedy the problem by giving a slightly generalized sequencing rule (below,
right), which allows an arbitrary permission~$H'$ to be framed out of~$t_1$
and therefore passed directly to~$t_2$.
%
\begin{mathpar}
\inferrule*[lab=traditional sequencing rule]
{ \Jhoare{H}{t_1}{Q'} \\ \Jhoare{Q'}{t_2}{Q} }
{ \Jhoare{H}{ \LSeqof{t_1}{t_2} }{Q} }

\inferrule*[lab=framed sequencing rule]
{ \Jhoare{H}{t_1}{Q'} \\ \Jhoare{Q' \Hstar H'}{t_2}{Q} }
{ \Jhoare{H \Hstar H'}{ \LSeqof{t_1}{t_2} }{Q} }
\end{mathpar}
%

In \SL, the ``framed sequencing rule'' can be derived from the sequencing rule
and the frame rule. Here, conversely, it is viewed as a primitive reasoning
rule; the traditional sequencing rule can be derived from it, if desired.
% Just take $H'$ to be empty.

When a read-only permission is available at the beginning of a sequence of
instructions, it is in fact available to every instruction in the
sequence.
This is expressed by the following rule,
which can be derived from the framed sequencing rule,
the rule of consequence, and from
the fact that read-only permissions are duplicable.
\begin{mathpar}
\inferrule[read-only sequencing rule]
{ \Jhoare{H \Hstar \HRO{H'}}{t_1}{Q'} \\ \Jhoare{Q' \Hstar \HRO{H'}}{t_2}{Q} }
{ \Jhoare{H \Hstar \HRO{H'}}{ \LSeqof{t_1}{t_2} }{Q} }
\end{mathpar}

% ------------------------------------------------------------------------------

\subsection{A new read axiom}
\label{sec:overview:read}

The axiom for reading a memory cell must be generalized so as to accept a
read-only permission (instead of a read-write permission) as proof that
reading is permitted.
%
% If it was not generalized, then RO permissions would be unusable!
%
The traditional axiom (below, left) requires a read-write permission
$\HPsingle{l}{v}$, which it returns. The new axiom (below, right) requires a
read-only permission $\HRO{\HPsingle{l}{v}}$, which it discards.
%
\begin{mathpar}
\inferrule*[lab=traditional read axiom]
{}
{ \Jhoare{ \HPsingle{l}{v} }{ \LGetof{l} }{ \HPabs{y} \, \HPprop{y = v} \Hstar \HPsingle{l}{v} } }

\inferrule*[lab=new read axiom]
{}
{ \Jhoare{ \HRO{\HPsingle{l}{v}} }{ \LGetof{l} }{ \HPabs{y} \, \HPprop{y = v} } }
\end{mathpar}
%
The traditional read axiom remains sound, and can in fact be derived from the
new read axiom and the \rofr.

% LONG VERSION
%instantiating it
%with $H'$ set to $\HPsingle{x}{v}$ and with $H$ set
%to the empty permission.
%
%\begin{mathpar}
%\inferrule*[right=\rofr]
%{ \inferrule*[Right=new read axiom]
%  {  }
%  { \Jhoare{ \HPempty \Hstar \HRO{\HPsingle{x}{v}} }{ \LGetof{x} }{ \HPabs{y} \, %\HPprop{y = v} } } }
%{ \Jhoare{ \HPempty \Hstar  \HPsingle{x}{v} }{ \LGetof{x} }{ \HPabs{y} \, %\HPprop{y = v} \Hstar \HPsingle{x}{v} } }
%\end{mathpar}

%------------------------------------------------------------------------------
\subsection{Illustration}
\label{sec:population:explained}

Recall the hypothetical operation $\mathit{foo}$ that was used earlier
(\sref{sec:shortcomings}) to illustrate our claim that faking read-only
permissions by macro-expansion is unsatisfactory. Let us carry out this proof
again, this time with our read-only permissions.
%
As before, we assume that $\mathit{population}$ requires read access to
the hash table. This is expressed by specification~(\ref{spec:population}),
which we repeat here:
%
\begin{equation*}
  \Jhoare
    {\HRO{\Htable{h}{M}}}
    { (\mathit{population}\;h) }
    { \HPabs{y} \, \HPprop{y = \#\!M} }
\end{equation*}
%

\newcommand{\rofrconseq}{read-only frame rule (with consequence)}
We will use the following derived rule, which follows from the \rofr, the rule
of consequence, and the fact that ``$\HROname$'' is covariant:
%
\begin{mathpar}
\inferrule*[lab=\rofrconseq]
  { H' \Himpl H'' \\
    \Jhoare{H \Hstar \HRO{H''}  }{t}{Q} \\
    \HNormal{H'}
  }
  { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }
\end{mathpar}
%
This rule differs from the \rofr in that,
instead of introducing $\HRO{H'}$, this rule introduces $\HRO{H''}$, where
$H''$ is logically weaker than $H'$. Nevertheless, upon exit, the
permission~$H'$ is recovered.

We can now give a new proof outline for $\mathit{foo}$:
\newcommand{\Hcommenthidden}[1]{}
\renewcommand{\Hcomment}[1]{\\&\quad\text{-- #1}}
%
\[\begin{array}{l@{\quad}l@{\quad}l}
\numm1 \Llettoplevel{\mathit{foo}\;h}{                   \\
\numq2   \Hassert{ \Htable{h}{M} }
         \Hcommenthidden{$\mathit{foo}$'s precondition}        \\
    % \forall a.\forall L.
\numq3    \Hassert{ \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots }
      \Hcommenthidden{by unfolding}                       \\
%      \Hcomment{$a, L$ fresh auxiliary variables}   \\
\numq4    \Llet{d}{h.\mathit{data}}                       \\
\numq5    \Hassert{ \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots \Hstar \HPprop{d=a} }
      \Hcommenthidden{by reading}                         \\
\numq6  \Hassert{ \HRO{\Htable{h}{M}} \Hstar \HPprop{d=a} }
      \Hcomment{by the \rofrconseq}                       \\
\numq7    \Llet{p}{\mathit{population}\;h}                \\
\numq8    \Hassert{ \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots \Hstar \HPprop{d=a} \Hstar \HPprop{p = \#\!M} }
                                                    \\
\numq9    \ldots
}
\end{array}\]

% arthur: Il manque l'explication de sur quel H' on applique la règle frame avec conséquence.
% fp: non, le texte dit bien ``we exploit the entailment ...'' ce qui indique comment les
%     méta-variables sont instanciées.

Up to and including line~5, the outline is the same as in our previous
attempt. At this point, in order to justify the call to $\mathit{population}$,
we wish to obtain the read-only permission $\HRO{\Htable{h}{M}}$. This is done
by applying the \rofrconseq{} around the call.
We exploit the entailment
$(\Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots)
 \Sc\Himpl
 (\Htable{h}{M})$,
which corresponds to folding the \F{HashTable} predicate.
Thus, for the duration of the call, we obtain the
read-only permission $\HRO{\Htable{h}{M}}$.
After the call,
the more precise, unfolded, read-write permission
$\Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots$
reappears.
The loss of information in our first proof
outline (\sref{sec:shortcomings}) no longer occurs here.

% fp: on pourrait imaginer des variantes: par exemple ici,
% puisque ``let d = h.data'' est une lecture, on pourrait
% appliquer une seule fois \rofr autour des deux
% instructions: lecture de h.data et appel de fonction.

% fp: toutefois, pour appliquer \rofr autour des k premières
% instructions d'une séquence de n instructions, on a un
% problème: il faudrait pouvoir ré-associer la séquence
% pour considérer les k premières instructions comme une
% seule instruction, et lui appliquer \rofr.
% Il faudra tenir compte de ce problème dans
% l'implémentation, si on veut implémenter?

%  LocalWords:  sref sref HROname fref Himpl HPprop HPstarof HPstarof Hstarof
%  LocalWords:  HPsingle Hstar rofr rofr Htable HPexinameof HPexinameof ldots
%  LocalWords:  Hrecord Hdataarray metatheoretical mathpar inferrule Jhoare
%  LocalWords:  HNormal Qstar HNormalof newcommand footnotenormality LSeqof
%  LocalWords:  LGetof HPabs mathit rofrconseq Hcommenthidden renewcommand
%  LocalWords:  Hcomment numm1 Llettoplevel numq2 Hassert numq3 numq4 Llet
%  LocalWords:  numq5 numq6 numq7 numq8 numq9
