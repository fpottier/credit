\section{Related work}
\label{sec:related}

% TEMPORARY
% aspinall-hofmann-02 presents a type system with notions of read-only access

%------------------------------------------------------------------------------
%------------------------------------------------------------------------------

% fp: le "const" de C/C++ me semble un peu anecdotique, et on pourrait se
% contenter de pointer vers des gens qui le critiquent, comme coblenz-16.
% Ça rentre dans l'énorme morceau de littérature sur "reference immutability",
% ou "comment annother les types traditionnels avec des modificateurs simples".

\subsection{The C/C++ ``const'' modifier}

The C and C++ languages provide a type qualifier, \C{const}, which, when
applied to the type of a pointer, makes this pointer usable only for reading.
For example, a pointer of type \C{const int*} offers read access to an integer
memory cell, but does not allow mutating this cell.
%
A pointer of type \C{int*} can be implicitly converted to a pointer of type
\C{const int*}.
% (which allows fewer operations) (so this is safe)
%
\begin{comment}
  Thus, to some extent, the \C{const} mechanism is quite similar to our
  read-only permissions: a function that expects a read-only permission may be
  invoked either from a context where the same read-only permission is
  available, or from a context where the corresponding read-write permission
  is available.
% fp: oui, mais c'est quand même très différent
% fp: et chez nous il n'y a pas de conversion de H vers RO(H), bien sûr
\end{comment}

The \C{const} qualifier arguably suffers from at least two important defects.
%
First, as aliasing is not restricted, the above conversion rule immediately
implies that a single pointer can perfectly well be stored at the same time in
two distinct variables of types \C{const int*} and \C{int*}. Thus, although
\C{const} prevents writing (through this pointer), it does not guarantee that
the memory cell cannot be written (through an alias).
%
% Unlike our read-only permissions, the \C{const} qualifier imposes a
% restriction, but offers no guarantee.
% fp: je fais référence à la remarque faite plus haut dans la discussion
%     à propos de Jensen \etal.
%
Second, \C{const} does not take effect ``in depth''. If \C{t} has type
\C{const tree*}, for instance, then \C{t->left} has type \C{tree*}, as
opposed to \C{const tree*}. Thus, the \C{const} qualifier, applied to
the type~\C{tree*}, does not forbid modifications to the tree.
%
% It only forbids modifications to the root node. And even that is not
% certain; if the tree has parent pointers, then the root is reachable
% from itself via a cycle, so a non-const pointer to it can be obtained.

For these reasons, when a function expects a \C{const} pointer to a mutable
data structure, one cannot be certain that this data structure is unaffected
by a call to this function.
%
In contrast, our read-only permissions do offer such a guarantee.

\begin{comment}
That said, there is a fundamental difference between the \C{const}
mechanism and our read-only permission, namely the depth at which
the read-only restriction applies in the case of recursive structure.
Consider for example
a tree data structure. A const pointer to the root of the tree
prevents modification to the root cell. However, with such a pointer,
one may read the address of, say, the left branch, and thereby obtain
a \E{non-const} pointer to the left subtree. This pointer may
in turn be used to modify any data inside the left subtree.
Even worse, if the nodes in the trees happens to store parent pointers,
these pointers may be used to recover a \E{non-const} pointer
to the root, and subsequently modify the root, although it was
originally only accessible via a \E{const} pointer.
The C++ code below illustrates these possibilities.

\begin{coq}
  struct tree {
    tree* left;
    tree* right;
    tree* parent;
  };

  int main() {
    // building a tree t, with both branches storing the empty tree u.
    tree u = { NULL, NULL, NULL };
    tree t = { &u, &u, NULL };
    u.parent = &t;

    const tree* p = &t; // create a const pointer on the root
 // p->left = &u;       // write rejected, because p is const
    p->left->left = &u; // write on the subtree accepted
    p->left->parent->left = &u; // write through alias accepted
  }
\end{coq}
\end{comment}

% Const may also be used for restricting a method, limiting it to a read-only
% access to the fields of the object on which the method is called.
% A const method may only call other const methods, and it cannot modify fields
% (except from those tagged as mutable...).

%------------------------------------------------------------------------------
%------------------------------------------------------------------------------

\subsection{The ``read-only frame'' connective}

Jensen \etal.~\citeyear{jensen-13} present a \SL for ``low-level'' code.
It is ``high-level'' in the sense that, even though machine
code does not have a built-in notion of function,
% or: code fragment with well-defined entry point, exit point, and calling
% convention
the logic offers structured reasoning rules, including first- and higher-order
frame rules.
%
The logic is stratified: assertions and specifications form two distinct levels.
%
At the specification level, one finds a ``frame'' connective~$\otimes$ and a
``read-only frame'' connective~$\oslash$.

When applied to the specification of a first-order function, the ``frame''
connective has analogous effect to the ``macro-expansion'' scheme that was
discussed earlier (\sref{sec:macro}): framing such a specification~$S$ with an
assertion~$R$ amounts to applying~$\_ \Hstar R$ to the pre- and postcondition.
%
Thus, if $R$ is (say) $\Htable{h}{M}$, then the specification $S \otimes R$
states that $h$ must remain a valid hash table that represents the
dictionary~$M$, but does not require that the table be unchanged: it could,
for instance, be resized.

The ``read-only frame'' connective~$\oslash$ is stronger: the specification $S
\oslash R$ requires not just that the assertion~$R$ be preserved, but that the
concrete heap fragment that satisfies~$R$ be left in its initial state.
%
It is defined on top of the ``frame'' connective by bounded quantification.
%
A typical use is to indicate that the code of a function (which, in this
machine model, is stored in memory) must be accessible for reading, and is not
modified when the function is called.
%
Jensen \etal.'s ``read-only frame'' connective allows ``read-only'' memory to
be temporarily modified, as long as its initial state is restored upon exit.
Therefore, it is quite different from our read-only permissions, which at the
same time impose a restriction and offer a guarantee: they prevent the current
function from modifying the ``read-only'' memory, and they guarantee that a
callee cannot modify it either.
% fp: je souligne au passage cet aspect rely-guarantee de notre système,
%     ça pourrait être fait ailleurs.
%
Furthermore, our read-only permissions can be duplicated and discarded,
% which we have argued is convenient,
whereas Jensen \etal.'s ``read-only frame'' connective exists at the
specification level: they have no read-only assertions.
% fp: pour établir une spec chez eux, il faut déplier la définition de
%     read-only frame, donc accepter un sigma abstrait qui satisfait R,
%     et le trimballer tout le long pour le rendre à la fin.
%     En plus, ça ne doit pas être particulièrement facile à utiliser si
%     R est une permission composite et qu'on a envie de la déplier pour
%     aller des faire des lectures à l'intérieur.

% The rule Frame-RO has nothing to do with our \rofr.
% It simply states that if a piece of code does not know about R,
% then it preserves R.

% fp: extraits de mon journal:
\begin{comment}
  Il y a une version read-only de l'opérateur frame: ``S / R'' est interprété
  sémantiquement comme (en gros) ``pour tout sigma qui satisfait R, (S tenseur
  sigma)''. Donc, il existe un état (inconnu) qui satisfait R, et on le préserve.
  C'est plus fort que de dire simplement qu'on préserve R. Et c'est un besoin
  qu'on a parfois remarqué en Mezzo: dire qu'on préserve r @ ref int, c'est bien,
  mais parfois on voudrait dire qu'on ne modifie pas le contenu de la référence,
  et cela se code de façon lourde en disant que l'on préserve r @ ref (=i) *
  i: int, pour un certain i.

  Apparemment leurs triplets `en lecture seule' autorisent tout de même à
  modifier temporairement l'état, à condition de le rendre à la fin dans l'état
  où on l'a trouvé. Chez nous, on pourrait croire que c'est pareil, sauf que
  tout de même, nous avons cette permission RO P qui est *duplicable*, ce qui
  implique qu'une modification même temporaire de l'état n'est pas sound (elle
  invaliderait les autres permissions RO qui peuvent exister). Ce n'est donc
  probablement pas le même concept, mais on a du mal à comparer.
\end{comment}

%------------------------------------------------------------------------------
%------------------------------------------------------------------------------

% Lexical scope approaches.

\subsection{Thoughts about lexical scope}

\newcommand{\footnoteescape}{\footnote{For instance,
we do not have concurrency, so a read-only permission cannot be transmitted to
another thread via a synchronization operation. Furthermore, unlike
Mezzo~\cite{balabonski-pottier-protzenko-mezzo-journal-16}, we do not allow a
closure to capture a duplicable permission, so a read-only permission cannot
escape by becoming hidden in a closure.}}

% fp: en fait, la règle de Mezzo ``une permission duplicable peut être capturée
%     silencieusement par une clôture'' s'appuie sur le fait que les permissions
%     duplicables sont vraies pour toujours. Ce qui est faux des permissions
%     read-only temporaires.

The \rofr takes advantage of lexical scope: it applies to a code block with
well-defined entry and exit points, and governs how permissions are
transformed when control enters and exits this block. Upon entry, a read-write
permission is transformed to a read-only permission; upon exit, no read-only
permissions are allowed to go through, and the original read-write permission
reappears. The soundness of this rule relies on the fact that read-only
permissions cannot escape through side channels\footnoteescape.

There are several type systems and program logics in the literature which rely
on lexical scope in a similar manner, sometimes for the same purpose (that is,
to temporarily allow shared read-only access to a mutable data structure),
sometimes for other purposes.

% Wadler (1990).

Wadler's ``let!'' construct~\cite[\S4]{wadler-linear-90}, for instance, is
explicitly designed to allow temporary shared read-only access to a ``linear
value'', that is, in our terminology, a uniquely-owned, mutable data
structure. The ``let!' rule changes the type of a variable~$x$ from $T$
outside the block to $!T$ inside the block, which means that~$x$ temporarily
becomes shareable and accessible only for reading.
%
% the side conditions required by Wadler are:
% - hyperstrict evaluation
%   we do not have this problem because we use call-by-value
% - U safe for T
%   a conservative test which guarantees that no component of x
%   can escape in the result; requires, in particular, that there
%   are no function types in U
%   we do not have this problem because we do not care if *values* escape
%   we only care if *permissions* escape, and this is easy to rule out
%   via our side condition ``normal H''
%   the fact that RO only has meaning as a hypothesis, not a conclusion,
%   is important; in Wadler's system, the ! connective is used on both
%   sides, and that is a source of confusion.
%   the fact that permissions cannot be hidden in closures also plays a role.
In order to ensure that no component of~$x$ is accessed for reading after the
block is exited, Wadler requires a stronger property, namely that no component
of~$x$ is accessible through the result value. Furthermore, in order to
enforce this property, he imposes an even more conservative condition, namely
that the result type~$U$ be ``safe for~$T$''.
%
In comparison, things are simpler for us. \SL distinguishes values and
permissions: thus, we do not care if a value (the address of $x$, or of a
component of~$x$) escapes, as long as no read permission escapes. Furthermore,
in our setting, it is easy to enforce the latter condition. Technically, the
side condition ``$\HNormal{H'}$'' in the \RULE{read-only frame rule} plays
this role. At a high level, this side condition implies that read-only
permissions appear only in preconditions, never in postconditions. In Wadler's
system, in contrast, the ``!'' modality describes both inputs and outputs, and
describes both permanently-immutable and temporarily-read-only data, so things
are less clear-cut.

\begin{comment}
Boyland~\cite{boyland-fractions-03} writes:

  Wadler's let! construct~\cite{wadler-linear-90} permits a linear variable to
  be used nonlinearly by code that only needs read access. However, let! forbids
  the code using the variable nonlinearly from (among other things) returning a
  function, since a reference could be hidden in the closure.

Boyland also writes:

  SCIR~\cite{ohearn-scir-99} permits a linear key to be moved into a non-linear
  section while type checking a statement that only needs read permission. In
  SCIR, a writeable variable may be given a read-only type temporarily.

  SCIR split the context into two parts: an active part (writable); and a
  passive part (read only). The passive part can be duplicated in two branches
  of the proof (unlike the active part) enabling the sharing of read-only
  state. An interesting rule called ``passification'' enabled a write of a
  variable to be ignored if the result was a passive type. A monadic-like
  structure ensures that (visible) state mutations cannot be hidden in a
  passively-type result.

  % Boyland seems to understand SCIR much better than I do.
  % I do not see the connection between passive versus active in SCIR
  % and read-only versus read-write.
  % I do see that Passification allows an active variable to be
  % temporarily viewed as passive (therefore duplicable)
  % provided the type of the expression is itself passive.
\end{comment}

% Focus.

In Vault~\cite{faehndrich-deline-02}, the ``focus'' mechanism temporarily
yields a unique read-write permission for an object that inhabits a region
(therefore, can be aliased, so normally would be accessible only for reading).
Meanwhile, the permission to access this region is removed. This is
essentially the dual of the problem that we are addressing! In the case of
``focus'', it is comparatively easy to ensure that the temporary read-write
permission does not escape: as this is a unique permission, it suffices to
require it upon exit. For the same reason, it is possible to relax the lexical
scope restriction by using an explicit linear
implication~\cite[\S6]{faehndrich-deline-02}.
%
Boyland and Retert's explanation of ``borrowing''~\cite{boyland-retert-05} is
also in terms of ``focus''.

% Gordon et al.

Gordon \etal.~\cite{gordon-12} describe a variant of C\# where four kinds of
references are distinguished, namely: ordinary writable references; readable
references, which come with no write permission and no guarantee; immutable
references, which come with a guarantee that nobody has (or will ever have) write permission;
and isolated references, which form a unique entry point into a cluster of
objects. Quite strikingly, the system does not require any accounting.
Lexical scope is exploited in several interesting ways.
% T-RecovIso
% T-RecovImm
% T-Par requires that no write permissions be in scope.
In particular, the ``isolation recovery'' rule states that,
if, upon entry into a block,
only isolated and immutable references are available,
and if, upon exit of that block,
a single writable reference is available,
then this reference can safely be viewed as isolated.
(The soundness of this rule relies on the fact that there are no
mutable global variables.)
This rule may seem superficially analogous to the \rofr, in that a
unique permission is lost and recovered.
It is unclear to us whether there is a deeper connection.
% Ils peuvent partager temporairement une structure entre deux
% threads (cf. discussion email), même si ce n'est pas du tout
% évident quand on lit l'article. La réponse de Matt Parkinson
% utilise un trick qui consiste à copier la variable isolated
% dans une variable locale déclarée "readable". Ensuite on
% cache la variable isolated (frame). Du coup la règle T-Par
% s'applique, seule la variable readable est visible. Et à la
% sortie, on arrive à récupérer l'isolation semble-t-il...
The system has a typing rule for structured parallelism and allows
a mutable data structure to be temporarily shared (for reading) by
several threads.
%
\begin{comment}
% fp: je doute fort (à propos de la deeper connection)
%     chez nous on a caché une permission unique, et à la sortie on la récupère,
%     c'est tout simple.
%     chez eux la permission unique est vraiment perdue au début,
%     et une autre permission unique (portant possiblement sur un autre objet)
%     est retrouvée à la sortie par un argument d'accessibilité.
%     chez nous il n'y a aucun argument d'accessibilité

% The system has been implemented and used in industrial applications.
% A ``low annotation burden'' is reportedly required.

% They have four kinds of references:
% writable  -- ordinary
% readable  -- read-only through this pointer, and hereditary,
%              but possibly writable through aliases
% immutable -- not writable through any alias
% isolated  -- a uniquely-owned entry point into a cluster of objects
%              (with possible outgoing edges to immutable objects)

% isolated objects cannot be used directly, but can be converted
%   to writable or to immutable, as desired.
% isolation is apparently lost but can be recovered upon exit:
%   ``if the input type context for an expression contains only isolated and
%   immutable objects, then if the output context contains a single writable
%   reference, we can convert that reference back to isolated''
%   -- fp: why does it have to be writable?
%          maybe because otherwise it could be reachable from another ``isolated'' variable
%   -- fp: can we convert isolated to readable, and back to isolated upon exit?
%          I do not see how
% analogous to what we do:
%   the read-only frame rule also ``forgets'' that we have unique ownership
%   and recovers it upon exit.

% isolated can be converted directly (and permanently) to immutable.
% There is also a more complicated and slightly more general (?) rule,
% in section 2.3.

% ``any object reachable from an immutable object is also immutable'':
% same with RO

% Gordon et al. also have a rule for structured parallelism (T-Par):

% ``while all writable references in a context are temporarily framed away, it
% becomes safe to share all read-only or immutable references among multiple
% threads''

% And they have a rule for spawn (``asymmetric parallelism''),
% which basically says that immutable and unique data can be
% transferred to the new thread.
% -- fp: I suppose this must be restricted to *permanently immutable* data

% no mutable global variables
\end{comment}

% Reference immutability in Java, etc.

Gordon \etal.'s work is part of a line of research on ``reference
immutability'', where type qualifiers are used to control object mutation.
We refer to the reader to Gordon \etal.'s paper~\citeyear{gordon-12} and to
Potanin \etal.'s survey~\citeyear{potanin-13}. Coblenz \etal.'s recent study
of language support for immutability~\citeyear{coblenz-16} is also of
interest.

% Rust.

Rust~\cite{rust} has lexically-scoped ``borrows'', including ``immutable
borrows'', during which multiple temporary read-only pointers into a
uniquely-owned mutable data structure can be created. The borrowing discipline
does not require any counting, but involves ``lifetimes'', a form of region
variables. Lifetimes can often be hidden in the surface syntax, but must
sometimes be exposed to the programmer. In contrast, our read-only permissions
require neither counting nor region variables.
Reed~\citeyear{reed-15} offers a tentative formal description of Rust's
borrowing discipline. % and borrow checker

% voir si la description du borrow-checker existe en ligne
% sinon elle doit être dans la distribution source de Rust.

%------------------------------------------------------------------------------

% Views, and other frameworks.

% Can we work in the setting of an existing framework, such as Views?
% or Calcagno et al.'s abstract separation logic?

% fp: I believe not, because we have an ad hoc interpretation of triples,
%     where the input and output heaps are related
%     (they must have the same read-only area)
% fp: that said, a closer look would be required.

%------------------------------------------------------------------------------

% Mezzo.

% Could one add read-only permissions to Mezzo along the lines of the paper?

% One would need to introduce a constraint ``normal p''
% and constrain lock invariants to be normal.
% (this would in turn forbid sending RO permissions along channels,
% since channels are defined in terms of locks)
%
% note however that Mezzo has a small-step semantics, and in that
% setting defining the meaning of RO might be nontrivial. The
% big-step semantics in this paper is there for a reason!
%
% also note that Mezzo allows a duplicable permission to become hidden
% in a closure; one would have to restrict this to normal permissions.

%------------------------------------------------------------------------------

% Misc.

\begin{comment}
Joe_3:
https://people.dsv.su.se/~tobias/iwaco.pdf
seems overly complicated -- they have fractional permissions *plus* a bunch of other stuff (owners, etc.)

boyland-noble-retert-01
  presque entièrement informel
  tout au plus on trouve une ``sémantique'' des capabilities
  aucune règle de typage

borrowing = allowing temporary aliases of a unique reference
  boyland-retert-05 % essentiellement identique à adoption & focus
  haller-odersky-10 % ensure race safety in Scala's actors
                    % no concerns about read-only access
                    % the motivating example could be dealt with easily with \SL, except the list is doubly-linked
  -- both require tracking or invalidation of specific references or capabilities, according to Gordon et al.
\end{comment}

%------------------------------------------------------------------------------
%------------------------------------------------------------------------------

\subsection{Fractional permissions}
\label{sec:fractional}

% On a déjà parlé des fractional permissions dans l'intro, donc on va faire
% plutôt court ici.

Fractional permissions were introduced by
Boyland~\citeyear{boyland-fractions-03} with the specific purpose of enabling
temporary shared read-only access to a data structure.
% in a ``simple imperative language with structured parallelism''.
% He proves that ``the permission system enables parallelism to proceed with deterministic results''.
%
They have been integrated into several variants of Concurrent
\SL~\cite{bornat-permission-accounting-05,gotsman-storable-07,hobor-oracle-08}
and generalized in several ways, e.g., by replacing fractions with
more abstract ``shares''~\cite{dockins-algebras-09}.
%
They are available in several program verification tools, including
VeriFast~\cite{verifast},
Chalice~\cite{leino-mueller-chalice-09,heule-13},
and
the Verified Software Toolchain~\cite{appel-vst-11}.

\begin{comment}
zhao-07
  % has both scaling and existential quantification (!). Also, nesting, linear implication, etc.
  % not clear whether it is proven sound. Boyland's later paper~\citeyear{boyland-nesting-10}
  % says: ``It remains further work to prove the soundness of a permission type system [Zhao 2007]
  % for a concurrent language.''

Il y a un modèle de permissions dans la thèse de Parkinson,
qui gère à la fois les fractions et le comptage entier, comme Bornat et al.
Dockins et al. le critiquent.
\end{comment}

%------------------------------------------------------------------------------

% Simple fractional permissions, built into points-to assertions:

In its simplest incarnation, a fractional permission takes the form
$\HPsinglefrac{l}{\Ga}{v}$, where $\Ga$ is a rational number in the range
$(0,1]$. If $\Ga$ is~1, then this permission grants unique read-write access to the
memory location~$l$; if $\Ga$ is less than~1, then it grants shared read
access.
%
The following conversion rule allows splitting and joining fractional
permissions:
$$\HPSinglefrac{l}{\Ga + \Gb}{v} \Sc{=}
\HPSinglefrac{l}{\Ga}{v} \Sc\Hstar
\HPSinglefrac{l}{\Gb}{v}
\qquad \X{ when } \Ga, \Gb, (\Ga + \Gb) \in (0,1]$$
%

Thanks to this rule, one can transition from a regime where a single thread
has read-write access to a regime where several threads have read-only access,
and back.
%
Fractional permissions are not duplicable, and must not be carelessly
discarded: indeed, in order to move back from read-only regime to read-write
regime, one must prove that ``no share has been lost'' and that the fraction~1
has been recovered. This requires ``accounting'', that is, arithmetic reasoning,
as well as returning fractional permissions in postconditions.
%
% Thus, fractional permissions do not address the problem that we have
% identified as ``shortcoming~\ref{shortcoming:work}'' in the introduction
% (\sref{sec:intro}).
%
In contrast, our proposal is less expressive in some ways (for instance, it
does not support unstructured concurrency; see \sref{sec:parallelism}) but
does not require accounting: our read-only permissions can be freely
duplicated and discarded.
% arthur: n'aime pas ``less expressive''
%         préférerait incomparable, car on peut faire des choses que les
%         fractions ne font pas
% fp: je rajoute "in some ways"
%     y a-t-il des choses que nous pouvons faire et les permissions
%     fractionnelles non? ça m'étonnerait. mais ça peut être plus
%     lourd avec des permissions fractionnelles qu'avec RO, et cette
%     idée est renforcée par le fait que (apparemment) il n'y a pas
%     de codage compositionnel de RO vers les permissions fractionnelles.
%     Difficile à argumenter, toutefois.

%------------------------------------------------------------------------------

% Fractional permissions can express permanent read-only permissions:

Fractional permissions also allow expressing a form of irrevocable (as opposed
to temporary) read-only permissions. Define $\HPSinglefrac{l}{\F{ro}}{v}$ as
follows:
%
\[ \HPSinglefrac{l}{\F{ro}}{v}
   =
   \HPexists{\Ga\in (0,1)} \; \HPSinglefrac{l}{\Ga}{v}\]
%
From this definition, it follows that $\HPSinglefrac{l}{\F{ro}}{v}$ is
duplicable. It also follows that $\HPSinglefrac{l}{1}{v}$ can be converted to
$\HPSinglefrac{l}{\F{ro}}{v}$, but not the other way around: the transition
from read-write to read-only mode, in this case, is permanent.

% Boyland also makes this remark, but notes that $\exists z.z\Pi$ is an
% illegal form of existential quantification. Anyway, there are some
% systems where it is legal, e.g. heule-13, where quantification over
% fractions plays a key role.

%------------------------------------------------------------------------------

% Fractional permissions with scaling:

In the systems of fractional permissions mentioned above, the fraction~$\Ga$
is built into the points-to assertion $\HPsinglefrac{l}{\Ga}{v}$.
Boyland~\citeyear{boyland-nesting-10} studies a more general idea,
``scaling'', where any permission $H$ can be scaled by a fraction: that is, $H
::= \alpha.H$ is part of the syntax of permissions. Scaling seems a desirable
feature, as it allows expressing read-only access to an abstract data
structure, as in, say, $\frac12(\Htable{h}{M})$, which is impossible when
scaling is built into points-to assertions.
%
% One could simulate this effect by parameterizing the HashTable predicate
% with a fraction, but this requires planning ahead, and (I suppose) this
% works well only when the predicate is precise.
%
However, scaling exhibits a problematic interaction
with disjunction and existential quantification.
Boyland shows that ``reasoning with a fractional unrestricted existential is
unsound''~\cite[\S5.4]{boyland-nesting-10}.
In short, it seems difficult to find a model that validates both of the laws
$(\alpha.H) \Hstar (\beta.H) = (\alpha + \beta).H$
and
$\alpha.(\HPexists xH) = \HPexists x{(\alpha.H)}$.
% et la covariance du scaling, aussi.
Indeed, under these laws,
$\frac12.(\HPsingle{l_1}{v}) \Sc\Hstar \frac12.(\HPsingle{l_2}{v})$
entails
$\HPexists{l}(1.(\HPsingle{l}{v}))$,
which does not make intuitive sense.
Boyland escapes this problem by restricting the existential quantifier so that
it is precise: the construct $\HPexists xH$ is
replaced with $\HPexists y{(\HPsingle{x}{y} \Hstar H)}$.
% fp: en fait ce n'est plus vraiment un existentiel, c'est une
%     définition locale: ``let y = *x in H''.
In contrast, our read-only permissions do not require any such restriction,
yet do support ``scaling'', in the sense that ``$\HROname$'' can be applied
to an arbitrary permission:
$\HRO{H}$ is well-formed and has well-defined meaning for every~$H$.

% fp: on pourrait discuter pour savoir si la restriction aux existentiels
% précis est problématique ou non. Un exemple de prédicat abstrait non
% précis est UF (dans notre article union-find).

% Boyland notes that nesting requires scaling.

%------------------------------------------------------------------------------

% Hidden fractional permissions:

Chalice offers ``abstract read permissions''~\cite{heule-13}, an elaborate
layer of syntactic sugar above fractional permissions. An abstract read
permission, which could be written $\HPsinglefrac{l}{\F{rd}}{v}$, is
translated to a fractional permission $\HPsinglefrac{l}{\epsilon}{v}$, where
the variable~$\epsilon$ stands for an unknown fraction. The
variable~$\epsilon$ is suitably quantified: for instance, if this abstract
read permission appears in a method specification, then~$\epsilon$ is
universally quantified in front of this specification~\cite[\S4.1]{heule-13}.
The system is powerful enough to automatically introduce and instantiate
quantifiers and automatically split and join fractional permissions where
needed.
%
Unfortunately, because abstract read permissions are just fractional
permissions, they are not duplicable, and they must not be carelessly
discarded: they must be returned (or transferred to some other thread)
so that the fraction~1 can eventually be recovered.
%
Also, it is not known to us whether abstract read permissions can be
explained to the programmer in a direct manner, without reference to
fractional permissions.

\begin{comment}
% LATER Arthur notes:
  A propos de Chalice, le fait que la quantification du epsilon est toujours
  implicite me semble impossible à réaliser pour des spécifications de
  fonctions d'ordre supérieur, où il faudrait quantifier des epsilon à
  l'intérieur de parenthèses. Je suppose qu'ils s'en sortent car ils ne font
  que des fonctions du premier ordre. Ca vaut peut-être le coup d'être dit,
  car c'est un vrai argument pour l'avantage des RO par rapport à notre
  principal concurrent (les fractions avec automatisation des quantifications
  et de l'arithmétique).
% fp:
  Ce n'est pas certain qu'ils soient incapables de traiter les fonctions
  d'ordre supérieur. D'abord je ne vois pas ce qui empêcherait de mettre
  les \forall en tête de chaque type flèche. Ensuite je note que dans un
  langage objet il n'y a pas de ``fonctions d'ordre supérieur'': il y a
  seulement des méthodes qui prennent des objets en argument. Vraiment
  difficile de critiquer Chalice sans aller réellement voir comment ça
  marche.
\end{comment}

% still requires accounting (and returning fractions in postconditions)
% but the exact amounts and the required quantifications are inferred

% fractions apply only to specific locations/fields (x.f): no scaling

% what happens when something cannot be proved? one may fear that the
% underlying fractions are shown in the error message.

% enfin, la notion de rd est la plus simple, mais il y a aussi
% rd*    (un epsilon indépendant des autres)
% rd(tk) (un epsilon porté par un token -- nécessaire pour fork/join)
% addition, soustraction, multiplication, fractions concrètes

% aldrich-borrowing-12

In a somewhat related vein, Aldrich \etal.~\citeyear{aldrich-borrowing-12}
propose a type system where (among other features) out of a ``unique''
reference, any number of ``local immutable'' references can be temporarily
``borrowed''. The type system internally relies on integer accounting, but
this is hidden from the user.

  % ``[We] support [...] borrowing multiple local permissions from a unique
  % reference and recovering the unique reference when the local permissions
  % go out of scope, without any explicit management of fractions in the
  % source language. All accounting of fractional permissions is done by the
  % type system under the hood.''

% fp: je le cite ici parce qu'il y a du comptage caché sous le tapis,
%     mais on pourrait aussi le citer dans la section ``lexical scope''

% fp: It sounds as if their ``local immutable'' should be closely related to
% our RO. But I am not sure why they need counting at all!

%  LocalWords:  etal citeyear otimes oslash sref Hstar Htable newcommand rofr
%  LocalWords:  footnoteescape balabonski-pottier-protzenko-mezzo-journal-16
%  LocalWords:  Wadler's wadler-linear-90 Wadler HNormal Boyland Retert's
%  LocalWords:  boyland-fractions-03 ohearn-scir-99 passification Potanin
%  LocalWords:  faehndrich-deline-02 boyland-retert-05 potanin-13 Coblenz
%  LocalWords:  boyland-noble-retert-01 typage gotsman-storable-07 verifast
%  LocalWords:  bornat-permission-accounting-05 hobor-oracle-08 appel-vst-11
%  LocalWords:  dockins-algebras-09 leino-mueller-chalice-09 zhao-07 Bornat
%  LocalWords:  Dockins HPsinglefrac qquad HPexists HPsingle HPsingle HROname
%  LocalWords:  forall aldrich-borrowing-12
