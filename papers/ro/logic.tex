\section{Logic}
\label{sec:logic}

In this section, we give a formal presentation of \SL with read-only
permissions. We present the syntax of programs, the syntax of assertions, and
the reasoning rules. This is all a user of the logic needs to know. The
semantic model and the proof of soundness come in the next section
(\sref{sec:model}).

%------------------------------------------------------------------------------
\subsection{Calculus} % Syntax and semantics

Our programming language is
a $\lambda$-calculus with references.
% but our approach would equally well be
% applied to a richer programming language.
Its syntax is as follows:
%
$$\begin{array}{rrl}
v & := & x \mid \Ltt \mid n \mid l \mid \Lfix{f}{x}{t}  \nextrow{2}
t & := & v \mid \Lifthenelse{v}{t_1}{t_2} \mid  \Lletof{x}{t_1}{t_2}
             \mid \LApp{v}{v}
             \mid \Lrefof{v} \mid \Lgetof{v} \mid \Lsetof{v}{v} \\
\end{array}$$
%
A value~$v$ is either a variable~$x$, the unit value~$\Ltt$,
an integer constant~$n$,
a memory location~$l$,
or a recursive function $\Lfix{f}{x}{t}$.
%
A term~$t$ is either a value,
a conditional construct,
a sequencing construct,
a function call,
or a primitive instruction for allocating, reading, or writing a reference.
%
% The language is presented in A-normal form, to improve factorization
% of the evaluation and reasoning rules.

% arthur: Est-il utile de dire qu'on ne détaille pas les opérateurs de calcul?
% peut être pas, après tout c'est du lambda-calcul.
% fp: tu veux dire les opérations primitives sur les entiers?

% arthur: Dans la figure, tu n'aimais pas mes "m" en subscript ? ok, mais du
% coup j'ai du ajouter des parenthèses sur les heaps, c'est pas hyper joli.
% fp: oui, les subscripts me font souffrir...

\input{red}

The big-step evaluation judgement (\figref{fig:red}) takes the form
$\Jredbigconfigof{ t }{ m }{ v }{ m' }$, and asserts that the evaluation of the
term~$t$ in the memory~$m$ terminates and produces the value $v$ in the memory $m'$.
A memory is a finite map of locations to values.


%------------------------------------------------------------------------------
\subsection{Permissions}

% LATER the Coq formalisation defines the type of assertions directly as
% heap -> Prop, so there is no explicit syntax of assertions.
% Maybe this could be mentioned in a footnote?
% It does not really make a difference. The only problem I can see is
% when we define GC as ``exists H.H'' : this is apparently outside of
% the syntax of assertions that we have given.

The syntax of permissions, also known as assertions, is as follows:
%
\[\begin{array}{rrl}
H & := &
\HPprop{P} \mid
\HPsingle{l}{v} \mid
\Hstarof{H_1}{H_2} \mid
\HPorof{H_1}{H_2} \mid
%\HPandof{H_1}{H_2} \mid
\HPexinameof{x}{H} \mid
\HRO{H}
\end{array}\]
%
All of these constructs are standard, except for $\HRO{H}$, which represents a
read-only form of the permission~$H$.
%
The pure assertion $\HPprop{P}$ is true of an empty heap, provided the
proposition~$P$ holds. $P$ is expressed in the metalanguage; in our Coq
formalisation, it is an arbitrary proposition of type \F{Prop}.
In particular,
% thanks to the impredicativity of \F{Prop}.
a Hoare triple $\Jhoare{H}{t}{Q}$ (defined later on) is a proposition:
this is important, as it allows reasoning about first-class functions.
%
The empty permission $\HPempty$ can be viewed
as syntactic sugar for $\HPprop{\Ttrue}$.
%
The permission $\HPsingle{l}{v}$ grants unique read-write access to the
reference cell at address~$l$, and asserts that this cell currently contains
the value~$v$.
%
Separating conjunction $\Hstar$, %ordinary conjunction $\HPand$ and
disjunction $\HPor$, and existential quantification are standard.
% LATER why not universal quantification too?
%
We omit ordinary conjunction~$\HPand$, partly because we do not use it
in practice when carrying out proofs in CFML, partly because we did not
have time to study whether the rule of conjunction holds in our logic.
% LATER rétablir la conjonction non séparante,
%       et déterminer si la règle de conjonction est vraie ou non
% on aura potentiellement un problème du fait que la sémantique de
% l'allocation mémoire n'est pas déterministe

From a syntactic standpoint, a ``normal'' permission is one that does not
contain any occurrences of ``$\HROname$''. (Recall \fref{fig:normal}.)
% fp: un peu trompeur car cela ne tient pas compte de définitions
%     de prédicats que l'on peut faire en Coq, mais tant pis.

As usual in \SL, permissions are equipped with an entailment relation, written
$\Himplof{H_1}{H_2}$ (``$H_1$ entails $H_2$''). It is a partial order.
% (reflexive, transitive, antisymmetric)
(In particular, it is antisymmetric: we view two propositions that entail each
other as equal.) The standard connectives of \SL enjoy their usual properties,
which, for the sake of brevity, we do not repeat.
%
(For instance, separating conjunction is associative, commutative, and admits
$\HPempty$ as a unit.)
% Autre exemple:
\begin{comment}
The existential quantifier commutes with separating conjunction, as follows:
$(\HPexinameof{x}{H_1}) \Hstar H_2 = \HPexinameof{x}{(H_1 \Hstar H_2)}$,
provided $x$ does not occur free in $H_2$.
\end{comment}
%
In addition, read-only permissions satisfy the laws of \fref{fig:ro}, which
have been explained earlier (\sref{sec:overview:ro}).

% LONG VERSION
%For completeness, we show these properties formally stated
%in \figref{fig:himpl}, but note that they are not needed
%for reading the rest of the paper.

% LONG VERSION
\begin{comment}

  \begin{figure}[t]
  TODO: il manque les règles pour la disjonction.

  \begin{mathpar}
  \inferrule[reflexivity]{ \mathstrut }{H \Himpl H}

  \inferrule[transitivity]{ H_1 \Himpl H_2  \\ H_2 \Himpl H_3 }{ H_1 \Himpl H_3  }

  \inferrule[antisymmetry]{ H_1 \Himpl H_2 \\ H_2 \Himpl H_1 }{ H_1 = H_2 }

  \inferrule[regularity]{ H_1 \Himpl H_1' \\ H_2 \Himpl H_2' }{ \Hstarof{H_1}{H_2} \Himpl \Hstarof{H_1'}{H_2'} }

  \inferrule[exists-left]{ \Tfor{x}\;\, (H_1 \Himpl H_2) }{ \HPExinameof{x}{H_1} \Himpl H_2 }

  \inferrule[exists-right]{ H_1 \Himpl \OSubst{x}{v}{H_2} }{ H_1 \Himpl \HPExinameof{x}{H_2} }

  \inferrule[prop-left]{ P \Sc\impl (H_1 \Himpl H_2) }{ \HStarof{\HPprop{P}}{H_1} \Himpl H_2 }

  \inferrule[prop-right]{ (H_1 \Himpl H_2) \\ P }{ H_1 \Himpl \HStarof{H_2}{\HPprop{P}} }
  \end{mathpar}

  \hrule
  \caption{Properties of the entailment relation (standard).}
  \label{fig:himpl}
  \end{figure}
\end{comment}


%------------------------------------------------------------------------------
\subsection{Reasoning rules}
\label{sec:reasoning-rules}

As usual in \SL, a Hoare triple takes the form
%
\W{\Jhoare{H}{t}{\HPabs{y}{H'}}}
%
where $H$ and $H'$ are permissions and $t$ is a term.
%
The precondition~$H$ expresses requirements on the initial state; the
postcondition~$\HPabs{y}{H'}$ offers guarantees about the result~$y$ of the
computation and about the final state.
%
We write~$Q$ for a postcondition~$\HPabs{y}{H'}$.
%
For greater readability, we write
$Q\Hstar H'$ for $\Labs{y}(Q\,y\Hstar H')$.
We write $\HPorof{Q}{Q'}$
as a shorthand for $\Labs{y}\,(Q\,y)\HPor (Q'\,y)$.
We write $\Himplof{Q}{Q'}$
as a shorthand for $\Tfor{y}\,(Q\,y)\Himpl (Q'\,y)$.

We adopt a total correctness interpretation, whereby a triple
\W{\Jhoare{H}{t}{\HPabs{y}{H'}}} guarantees that (under the precondition~$H$)
the evaluation of~$t$ terminates. This is arbitrary: our read-only permissions
would work equally well in a partial correctness setting.

\input{rules}

\begin{comment}
  % arthur: J'ai ajouté un truc à améliorer:
  % fp: je ne vois pas ce que ça apporte ici
  Note that these rules may also be read as lemmas, which are proved
  correct with respect to a concrete definition of triples
  (see \secref{sec:model}). For example, the \rofr rule
  would take the form:
  $$\Tfor{t H H' Q}\quad
  \HNormal{H'} \Scs\land
  \Jhoare{H \Hstar \HRO{H'}  }{t}{Q}  \Scs\impl
  \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}}.$$
\end{comment}

The reasoning rules, by which Hoare triples can be established, are divided in
two groups: structural (or non-syntax-directed) rules (\figref{fig:rules}) and
syntax-directed rules (\figref{fig:term-rules}).

Among the structural rules,
the only nonstandard rule is the \RULE{read-only frame rule},
which
% subsumes the traditional frame rule and
has been explained earlier (\secref{sec:rofr}).
%
The rule of \RULE{consequence} allows exploiting entailment
to strengthen the precondition and weaken the postcondition.
%
The rules \RULE{discard-pre} and \RULE{discard-post}
allow discarding part of the pre- or postcondition.
(The rule of consequence cannot be used for this purpose:
for instance, $\HPsingle{x}{v}$ does not entail~$\HPempty$.)
The permission~$\HGC$ controls what permissions can be discarded.
Here, we let $\HGC$ stand for $\HPexists{H}{H}$:
% which is usually written ``true'' in \SL
this means that any permission can be discarded%
\footnote{%
  $\HPexists{H}{H}$ is equivalent to \textsf{true}.
  If the programming language had explicit deallocation instead of
  garbage collection, one might wish to define $\HGC$ as
  $\HPexists{H}{\HRO{H}}$, which means that read-only permissions can be
  implicitly discarded, but read-write permissions cannot. This restriction would be
  necessary in order to enforce ``complete collection'', that is, to ensure
  that every reference cell is eventually deallocated.}.
% fp: cela dit, cela ne résoudrait pas le problème des clôtures,
% pour lesquelles on aurait toujours besoin de GC.
%
% arthur: Technical remark:
% In the garbage collection rules, the existential quantification
% of the collected permission, involved in the definition of $\HGC$,
% is required in the rule \D{gc-post} because the permission may
% depend on the return value produced by $t$.
% fp: je suppose que tu veux dire que si la règle disait: on peut
% abandonner un H quelconque, ce serait trop faible. En effet, il
% faut autoriser à abandonner un Q quelconque.
%
The last three rules in \figref{fig:rules} are elimination rules for pure
assertions, % conjunction,
disjunction, and existential quantification.
Note that the standard symmetric rule of disjunction, shown below,
follows directly from the rules \D{extract-or} and \D{consequence}.
%
$$\inferrule[disjunction]
{ \Jhoare{H_1}{t}{Q_1} \\ \Jhoare{H_2}{t}{Q_2} }
{ \Jhoare{\HPorof{H_1}{H_2}}{t}{Q_1 \HPor Q_2} }$$

% fp: je pensais qu'on pouvait définir toute la logique sans les règles
% discard-pre et discard-post (qui permettent de jeter une permission)
% et que dans un second temps, si on voulait ces règles, on pouvait
% définir un nouveau triplet <H>t<Q> qui est un sucre pour {H}t{Q * true}.
%
% arthur: on est obligés de se donner les règles discard, car sinon il n'y a
% aucun moyen de jeter des permissions RO, et vu que tu n'as pas le droit de
% les retourner dans les post, tu serais complèment coincé.

The syntax-directed reasoning rules appear in \figref{fig:term-rules}. They
are standard, except for the \RULE{framed sequencing rule}
and the \RULE{new read axiom}, which have been explained
earlier (\sref{sec:overview:seq}, \sref{sec:overview:read}).
\begin{comment}
% fp: j'ai coupé car les choses me semblent relativement claires sans
%     expliquer cela, et puis il est tard et on manque un peu de place.
In the \RULE{ref} axiom, the postcondition
$\HPexists{l}\, \HPprop{y = l} \Sc\Hstar \HPsingle{l}{v}$ means that the
value~$y$ returned by $\Lrefof{v}$ must be a memory location $l$, and
that this memory cell currently stores the value~$v$.
(If the injection \F{val\_loc} of memory locations into values was explicit,
as it is in Coq, then the equation $y = l$ would be $y = \F{val\_loc}\;l$.)
\end{comment}

The implications in the premises of \RULE{extract-prop} and \RULE{if} and the
universal quantifiers in the premises of \RULE{extract-exists} and
\RULE{framed sequencing rule} are part of the metalanguage (which, in our
formalization, is Coq).
Thus, in reality, we work with assertions of the form $\forall\Gamma.\; \Jhoare HtQ$,
where $\Gamma$ represents a metalevel hypothesis.

%------------------------------------------------------------------------------
\subsection{Treatment of variables and functions}

Our treatment of variables, as well as the manner in which we reason about
functions, may seem somewhat mysterious or unusual. We briefly explain them
here. This material is entirely independent of the issue of read-only
permissions, so this section may safely be skipped by a reader who wishes to
focus on read-only permissions.

In the paper presentation, we identify program variables with the variables of
the metalanguage. For example, in the \RULE{framed sequencing rule}, the
name~$x$ that occurs in the conclusion $ \Lletof{x}{t_1}{t_2} $ denotes a
program variable, while the name~$x$ that is universally quantified in the
second premise denotes a variable of the metalanguage.
% fp: au passage, ça veut dire que le statut de x dans la seconde prémisse
% n'est pas très clair...

In our Coq formalization, we clearly distinguish between program variables and
metavariables.
%
On the one hand,
program variables are explicitly represented as identifiers (which may be implemented as
integers or as strings). They are explicitly embedded in the syntax of values.
% in the sense that if $x$ is an identifier, then $\F{val\_var}\,x$ is a value
% of type $\Tval$.
The type of values, $\Tval$, is inductively defined (this is a ``deep embedding'').
%
On the other hand,
metavariables are not ``represented as'' anything. They \emph{are} just Coq
variables of type $\Tval$, that is, they stand for an unknown value.
%
To bridge the gap between program variables and metavariables, a substitution
is necessary.
%
For example, in Coq, the \RULE{framed sequencing rule} is formalized
as follows:
%
$$
\begin{array}{rcl}
\Tfor{xt_1 t_2HH'QQ'}
& &
\Jhoare{H}{t_1}{Q'} \nextrow{2}
& \land &
(\Tfor{X} \;\;\Jhoare{Q'\,X \Hstar H'}{ \OSubst{x}{X}{t_2} }{Q}) \nextrow{2}
% fp: caché \F{val\_var}
%     de toute façon c'est standard pour une substitution de faire disparaître le constructeur Var
& \impl &
\Jhoare{H \Hstar H'}{ \LLetof{x}{t_1}{t_2} }{ Q }
\end{array}
$$

Note how, in the second premise, a metavariable~$X$ (of type $\Tval$) is
substituted for the program variable~$x$ in the term $t_2$. The
metavariable~$X$ denotes the runtime value of the program variable~$x$.
%
% fp: on peut se dire que, normalement, X devrait être une valeur *close*.
%     on arrive à s'en sortir sans cette hypothèse?

As one descends into the syntax of a term, metavariables are substituted for
program variables, as explained above. Thus, one can never reach a leaf that
is an occurrence of a program variable~$x$!
% fp: assuming that initially the term was closed, i.e., every program
%     variable was bound.
If a leaf labeled~$x$ originally existed in the term, it must be replaced
with a value~$X$ when the binder for $x$ is entered.

This explains a surprising feature of our reasoning rules,
which is that they do not allow
reasoning about terms that have free program variables.
% fp: donc en fait, elles sont prévues pour raisonner sur des termes clos.
%     je ne sais pas si ce serait mieux de le dire comme ça.
The rule~\RULE{if}, for instance, allows reasoning about a term of the form
$\LIfthenelse{n}{t_1}{t_2}$, where $n$~is a literal integer value.
It does not allow reasoning about $\LIfthenelse{x}{t_1}{t_2}$, where $x$ is
a program variable. As argued above, this is not a problem. Similarly,
\RULE{app} expects the value~$v_1$ to be a $\lambda$-abstraction; it
does not allow $v_1$ to be a program variable. \RULE{new read axiom}
and \RULE{set} expect the first argument of \F{get} and \F{set} to be
a literal memory location; it cannot be a program variable.

Another possibly mysterious aspect of our presentation is the treatment of
functions. In apparence, our only means of reasoning about functions is the
rule \RULE{app}, which states that, in order to reason about a call to a
literal function (a~$\lambda$-abstraction), one should substitute the actual
arguments for the formal parameters in the function's body, and reason about
the term thus obtained. At first, this may not seem modular: in Hoare logic,
one expects to first assign a specification to each function, then check that
each function body satisfies its specification, under the assumption that each
function call satisfies its specification.
% ce que je viens de dire là, c'est un peu ``partial correctness'';
% en correction totale, il faut raisonner par induction.
% Je rajoute donc la parenthèse:
In a total correctness setting, one must also establish termination.

It turns out that, by virtue of the power of the metalanguage, this style of
reasoning is in fact possible, based on the rules that we have given. We
cannot explain everything here, but present one rule for reasoning about the
definition and uses of a (possibly recursive) function. This rule can be
derived from the rules that we have given. It is as follows:
%
$$
\begin{linest}
  \left(
  \Tfor{S}\Tfor{F}\;
  \begin{lines}
  \phantom{\land\;}
    \left(\begin{lines}
      \left(\begin{array}{rcl}
        \Tfor{XH'Q'} &&
        \Jhoare{H'}{ \OSubst{f}{F}{\Osubst{x}{X}{t_1}} }{Q'} \nextrow{2}
        & \impl & \Jhoare{H'}{ \LApp{F}{X} }{ Q' }
        \end{array}\right)
      \impl
        S\,F %\right)
    \end{lines}\right)
  \nextrow{2}
  \land\; \left( S\,F \Sc\impl \Jhoare{H}{ \OSubst{f}{F}{t_2} }{ Q } \right)
  \end{lines}
  \!\!\right) \nextrow{2}
  \Sc\impl
 \Jhoare{H}{ \LLetof{f}{ \Lfix{f}{x}{t_1} }{t_2} }{ Q }
\end{linest}
$$
%

This rule provides a way of establishing a Hoare triple about a term of the
form $\Lletof{f}{ \Lfix{f}{x}{t_1} }{t_2}$. In order to establish such a triple,
it suffices to:
\begin{enumerate}
\item Pick a specification~$S$, whose type is $\Tval\rightarrow\Tprop$.
      This is typically a Hoare triple, which represents a specification of
      % that describes the behavior of
      the function $\Lfix{f}{x}{t_1}$.
\item Establish the desired triple about the term $t_2$, under the assumption
      that the function satisfies the specification~$S$. (This is the third line above.)
      In doing so, one does not have access to the code of the function:
      it is represented by a metavariable~$F$ about which nothing is known
      but the hypothesis $S\;F$.
      % so, the method is modular.
\item Prove that the function satisfies the specification~$S$.
      (These are the first two lines above.)
      The function is still represented by a metavariable~$F$.
      This time, one has access to the hypothesis that invoking~$F$
      is equivalent to executing the function body~$t_1$,
      under a substitution of actual arguments for formal parameters.
      If the function $\Lfix{f}{x}{t_1}$ is recursive,
      then this proof must involve induction
      (which is carried out in the metalanguage).
\end{enumerate}

% fp: si j'ai bien compris, la règle ci-dessus est suffisante,
%     même dans le cas où la fonction est récursive. (LATER à vérifier)
% Note that a generalized version of this reasoning rule may be derived
% for recursive function, so as to provide with an induction hypothesis
% for recursive calls.

\begin{comment}
  If a function is recursive, then its specification may be established
  by means of performing an induction in the host logic (Coq).
  For example, consider the recursive factorial function
  $\Lfix{\F{facto}}{n}{\LIfthenelse{n=0}{1}{n\times \F{facto}\,(n-1)}}$.
  The logical variable, called \F{Facto}, which represents this
  function is specified as follows:%
  % \footnote{The specification of the factorial function may also
  % be written in the following form:
  % $\Tfor{n}\; \Jhoare{\HPprop{n\geq0}}{(\F{facto}\,n)}{\HPabs{x}\; \HPprop{x=n!}}.$}
  %
  $$S\,\F{Facto} \Sq\equiv
  \Tfor{n}\;n\geq 0 \Sc\impl \Jhoare{\HPempty}{(\F{Facto}\,n)}{\HPabs{x}\; \HPprop{x=n!}}.$$
  %
  We may prove this statement by (well-founded) induction on $n$.
  Given a fixed non-negative value for $n$,
  we apply the \D{app} rule to unfold the definition of
  $\F{facto}\,n$. We are left to prove the following triple:
  %
  $$\Jhoare{\HPempty}{\LIfthenelse{n=0}{1}{n\times \F{Facto}\,(n-1)}}{\HPabs{x}\; \HPprop{x=n!}}.$$
  %
  The then-branch is trivial. For the else-branch, we
  need to invoke the induction hypothesis, which is as follows:
  $$\Tfor{n'<n}\;\;n'\geq 0 \Sc\impl \Jhoare{\HPempty}{(\F{Facto}\,n')}{\HPabs{x}\; \HPprop{x=n'!}}.$$
  %
  We check that the inequalities $n-1 <n $ and $n-1\geq 0$ are satisfied,
  and we conclude using the standard equality $n \times (n-1)! = n!$.
\end{comment}

%\begin{mathpar}
%\inferrule
%{ \GG \Jthesis \Jhoare{H}{t_1}{Q'}  \\
%  \GG,\,X \Jthesis \Jhoare{Q'\,X}{ \OSubst{x}{X}{t_2} }{Q} }
%{ \GG \Jthesis \Jhoare{H}{ \LLetof{x}{t_1}{t_2} }{ Q } }
%\end{mathpar}


\begin{comment}
  Our presentation does not provide an induction
  principle for reasoning about a recursive function.
  Rather than adding a rule to our logic, we here assume
  for simplicity that such a principle is already available
  at the metalevel. In other words, we establish a
  specification triple for recursive functions by conducting a
  proof by induction that this triple is derivable for the function.

\end{comment}

%Remark: in traditional \SL, the generalized rule
%shown to the right is derivable from the rule shown to the left
%using the frame rule; however, this is not the case here due
%to the restriction of the frame rule to apply only over
%normal permissions.

%\inferrule[val]
%{ \Himplof{H}{(Q\,v)} \\ \HNormal{H} }
%{ \Jhoare{H}{v}{Q} }
%The rule for values receives a side condition to ensure
%that no read-only permission may leak to the postcondition.

%  LocalWords:  sref rrl Ltt Lfix nextrow Lifthenelse Lletof LApp Lrefof HPor
%  LocalWords:  Lgetof Lsetof Jredbigconfigof figref HPprop HPsingle Hstarof
%  LocalWords:  HPorof HPexinameof Hoare Jhoare HPempty Ttrue Hstar HPand Scs
%  LocalWords:  HROname fref Himplof mathpar inferrule Himpl Tfor OSubst impl
%  LocalWords:  hrule HPabs secref rofr HNormal Qstar HPexists forall Tval
%  LocalWords:  emph rcl xt_1 rightarrow Tprop equiv geq
