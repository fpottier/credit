\section{Introduction}
\label{sec:intro}

%------------------------------------------------------------------------------
% Separation Logic

\SL~\cite{reynolds-02} offers a natural and effective framework for proving
the correctness of imperative programs that manipulate the heap. It is
exploited in many implemented program verification systems, ranging from
%
fully automated systems, such as Infer~\cite{calcagno-11},
% also: calcagno-15
% http://fbinfer.com/
through semi-interactive systems,
% or: mostly-automated
such as
Smallfoot~\cite{smallfoot-05},
jStar~\cite{distefano-parkinson-08}, and
VeriFast~\cite{verifast},
to fully interactive systems (embedded within a proof assistant), such as
the Verified Software Toolchain~\cite{appel-vst-11} and
Charge!~\cite{bengtson-12},
to cite just a few.
% also: IPM~\cite{krebbers-17}.
% also: FCSL.
% also: dynamic frames (Kassios 2006, Smans et al. 2008).
% also: Dafny (2008);
% also: Chalice (implicit dynamic frames; expressive permission system)
%
The CFML system, developed by the first
author~\cite{chargueraud-10,chargueraud-cfml},
can be viewed as a member of the latter category.
% C'est un peu approximatif, car il n'est pas entièrement embarqué dans
% Coq, mais ça ira?
We have used it to verify many
sequential
% nontrivial
data structures and algorithms,
representing several thousand lines of OCaml code.

% arthur: il manque une mini intro avant de partir dans des sous-sections.
% fp: pas sûr, il n'y avait rien avant. Pourquoi ne pas enchaîner?

%------------------------------------------------------------------------------
% Motivation: redundancy of traditional pre/post pairs.
% Beginning with the small axiom for dereferencing.

\subsection{Redundancy in specifications}

Our experience with \SL at scale in CFML leads us to observe that many
specifications suffer from a somewhat unpleasant degree of verbosity, which
results from a frequent need to repeat part of the precondition in the
postcondition. This repetition is evident already in the \SL axiom for
dereferencing a pointer:
\begin{mathpar}
\inferrule*[lab=traditional read axiom]
{}
{ \Jhoare{ \HPsingle{l}{v} }{ \LGetof{l} }{ \HPabs{y} \, \HPprop{y = v} \Hstar \HPsingle{l}{v} } }
\end{mathpar}
This axiom states that ``if initially the memory location~$l$ stores the
value~$v$, then dereferencing~$l$ yields the value~$v$ \emph{and, after this
operation, $l$ still stores~$v$.}'' Arguably, to a human reader, the last part
of this statement may seem obvious, even though it is not formally redundant.

\begin{comment}
The frame rule of \SL cannot be exploited to suppress this redundancy. The
knowledge that $l$ stores $v$ is necessary for the operation $\LGetof{l}$ to
be permitted, and for its result to be predicted. Therefore, this information
must explicitly appear in the precondition.
\end{comment}

%------------------------------------------------------------------------------
% The phenomenon is viral. Example of array_concat.

Beginning with this axiom, this redundancy contaminates the entire system. It
arises not only when a single memory cell is read, but, more generally, every
time a data structure is accessed for reading. To illustrate this,
% let us consider a concrete example, which is representative of many
% specifications that we have seen in practice.
consider a function $\mathit{array\_concat}$ that expects (pointers to) two arrays~$a_1$ and~$a_2$
and returns (a pointer to) a new array $a_3$ whose content is
the concatenation of the contents of $a_1$ and~$a_2$.
Its specification in \SL would be as follows:
%
\begin{equation}
\label{spec:arrayconcat}
 \Jhoarevert
   {\Hdataarray{a_1}{L_1} \Sc\Hstar \Hdataarray{a_2}{L_2} }
   { (\mathit{array\_concat}\,a_1\,a_2) }
   { \HPabs{a_3} \;
     \Hdataarray{a_3}{\OLAppof{L_1}{L_2}}
     \Sc\Hstar \Hdataarray{a_1}{L_1}
     \Sc\Hstar \Hdataarray{a_2}{L_2}
   }
\end{equation}
% LATER \Hstar should be \Sc\Hstar everywhere? or just let \Hstar do the right thing
We assume that $\Hdataarray{a}{L}$ asserts the existence (and unique
ownership) of an array at address~$a$ whose content is given by the list~$L$.
A separating conjunction~$\Hstar$ is used in the precondition to require that
$a_1$ and $a_2$ be disjoint arrays. Its use in the postcondition guarantees
that $a_3$ is disjoint with $a_1$ and~$a_2$.
In this specification, again, the fact that the arrays $a_1$ and~$a_2$ are
unaffected must be explicitly stated as part of the postcondition,
making the specification seem verbose.

\begin{comment}
The above specification consists of 30 tokens
(excluding parentheses and brackets). Among them, 10 tokens
are used for reproducing the precondition inside the
postcondition. If we were able to avoid this cumbersome duplication,
we would only need to write 20 tokens instead of 30 tokens.
In other words, the above specification
is 50\% longer than we would like it to be.
\end{comment}

%------------------------------------------------------------------------------
% The specification that we would like.

Ideally, we would like to write a more succinct specification, which directly
expresses the idea that the arrays $a_1$ and~$a_2$ are only read by
$\mathit{array\_concat}$, even though they are mutable arrays.
Such a specification could be as follows, where ``\HROname'' is a read-only
modality, whose exact meaning remains to be explained:
%
\begin{equation}
\label{spec:arrayconcat:ro}
\Jhoarevert
  { \HRO{\Hdataarray{a_1}{L_1}} \Sc\Hstar \HRO{\Hdataarray{a_2}{L_2}} }
  { (\mathit{array\_concat}\,a_1\,a_2) }
  { \HPabs{a_3} \; \Hdataarray{a_3}{\OLAppof{L_1}{L_2}}   }
\end{equation}
The idea is, because only read access to $a_1$ and~$a_2$ is granted,
% it is clear that
these arrays cannot be modified or deallocated by the call $\mathit{array\_concat}\,a_1\,a_2$.
Therefore, the postcondition need not say anything about these arrays:
that would be redundant.
% the assertion $\Hdataarray{a_1}{L_1} \Sc\Hstar \Hdataarray{a_2}{L_2}$
% (or its read-only form) need not appear in the postcondition.

%------------------------------------------------------------------------------
% The simplest macro interpretation, and its shortcomings.

\subsection{Can ``\HROname'' be interpreted by macro-expansion?}
\label{sec:macro}
\label{sec:shortcomings}

% Note: je me sens obligé d'avoir une sous-section ici,
% car j'ai ensuite un découpage en \paragraph,
% et sans cette sous-section pour les emballer,
% on n'arrivera pas à distinguer où commence la suite.

At this point, the reader may wonder whether the meaning of ``\HROname'' could
be explained by a simple macro-expansion process. That is, assuming that
``\HROname'' is allowed to appear only at the top level of the precondition,
the Hoare triple $\Jhoare{\HRO{H_1} \Hstar H_2}tQ$ could be viewed as
syntactic sugar for $\Jhoare{H_1 \Hstar H_2}t{H_1 \Hstar Q}$.
%
% In general, we might also wish to allow RO under existential quantifiers.
% But this can be simulated by lifting them to the outside as universal quantifiers.
%
Such sugar is easy to implement; in fact, CFML offers it, under the notation
``\F{INV}'', for ``invariant''.
%
% Comme ça prend de la place de développer les shortcomings,
% je crois que je vais d'abord les énumérer, puis les développer
% dans des paragraphes séparés.
%
However, this naïve interpretation suffers from several shortcomings,
which can be summarized as follows:
\begin{enumerate}
\item \label{shortcoming:work}
      It reduces apparent redundancy in specifications, \\
      but does not eliminate the corresponding redundancy in proofs.
\item \label{shortcoming:aliasing}
      It does not allow read-only state to be aliased. % and shared between threads
      % or: multiple read-only borrows
\item \label{shortcoming:weak:deceptive}
      It leads to deceptively weak specifications.
\item \label{shortcoming:weak:unusable}
      It can lead to unusably weak specifications.
\end{enumerate}
In the following, we expand on each of these points.

%------------------------------------------------------------------------------
% Shortcoming 1.

\paragraph{Shortcoming~\ref{shortcoming:work}: does not reduce proof effort}

% arthur: Je vais développer avec l'exemple des listes.
% fp: est-ce nécessaire?

% 1. Not satisfactory, as it may help reduce verbosity,
%    but does not reduce the WORK of establishing the postcondition.

Under the naïve interpretation
of ``\HROname'' as macro-expansion,
the Hoare triple $\Jhoare{\HRO{H_1} \Hstar H_2}tQ$
is just syntactic sugar. The presence or absence of this sugar has no effect
on the proof obligations that the user must fulfill. Even though the sugar
hides the presence of the conjunct~$H_1$ in the postcondition, it really is
still there. So, the user must prove that $H_1$ holds upon termination
of the command~$t$. This might take several proof steps: for example, several
predicate definitions might need to be folded. In other words, the issue that
we would like to address is not just undesired verbosity; it is also undesired
work.

% How RO permissions address this shortcoming:

In this paper, we intend to give direct semantic meaning to ``$\HROname$''. In
this approach, $\Jhoare{\HRO{H_1} \Hstar H_2}tQ$ is an ordinary Hoare triple,
whose postcondition does not mention $H_1$. Thus, there is no need for the
user to argue that $H_1$ holds upon termination. The proof effort is therefore
reduced.

\begin{comment}
% This corresponds to \item #1 above.
Indeed, while a macro can only help making specifications more concise,
an enhanced logic would also make verification proofs shorter.
Typically, when the read-only permissions
are not mentioned at all in the postconditions, it becomes simpler
to validate the postconditions. Such simplification is particularly
beneficial when the proof involves representation predicates whose
definition needs to be unfolded for performing read operations.
Representation predicates need, in traditional \SL,
to be folded back in order to establish the postcondition.
On the contrary, with a \SL equipped with read-only permissions,
the unfolded permissions are simply thrown away---their definition
needs not be folded back.
Our proof scripts reveal that, in many situations, the fold back
operations account for roughly 25\% to 30\% of the length of
the proof script. Thus, read-only permissions would bring a
significant improvement.
% fp: I am not really sure about those figures.
% In my experience, folding back is very easy/concise once the
% right tactics are available, but it takes time to set up these
% tactics.
\end{comment}

%------------------------------------------------------------------------------
% Shortcoming 2.

\paragraph{Shortcoming~\ref{shortcoming:aliasing}: does not allow aliasing read-only state}

Under the naïve interpretation
of ``\HROname'',
our proposed specification of
$\mathit{array\_concat}$ (\ref{spec:arrayconcat:ro})
is just sugar for the obvious specification (\ref{spec:arrayconcat}),
therefore means that $\mathit{array\_concat}$ must be
applied to two disjoint arrays. Yet, in reality, a call of the
form \Q{\mathit{array\_concat}\,a\,a} is safe and makes sense. To allow
it, one could prove another specification for
$\mathit{array\_concat}$, dealing specifically with the case where an
array is concatenated with itself:
%
\begin{equation}
\label{spec:arrayconcat:self}
 \Jhoarevert
   {\Hdataarray{a}{L} }
   { (\mathit{array\_concat}\,a\,a) }
   { \HPabs{a_3} \;
     \Hdataarray{a_3}{\OLAppof{L}{L}}
     \Sc\Hstar \Hdataarray{a}{L}
   }
\end{equation}
%
However, that would imply extra work:
firstly, when $\mathit{array\_concat}$ is defined,
as its code must be verified twice;
secondly, when it is invoked,
as the user may need to indicate which of
the two specifications (\ref{spec:arrayconcat}) and~(\ref{spec:arrayconcat:self}) should be used.

% How RO permissions address this shortcoming:

In this paper, we define the meaning of ``$\HROname$'' in such a way that
every read-only assertion is duplicable: that is, $\HRO{H}$ entails
$\HRO{H} \Hstar \HRO{H}$. Thanks to this property, our proposed specification
of $\mathit{array\_concat}$ (\ref{spec:arrayconcat:ro}) allows justifying the
call \Q{\mathit{array\_concat}\,a\,a}. In fact, under our reasoning rules,
specification~(\ref{spec:arrayconcat:ro}) subsumes both of the specifications
(\ref{spec:arrayconcat}) and~(\ref{spec:arrayconcat:self})
that one would need in the absence of read-only assertions.

\begin{comment}
The example of $\mathit{array\_concat}$ illustrates a situation where, thanks
to read-only assertions, one can give a single, ``principal'' specification,
whereas two distinct specifications may otherwise seem necessary.
% fp: je mets ``seem'' parce qu'on peut toujours s'en sortir avec une seule
% spec, qui dit qu'on attend deux arguments qui doivent tous deux appartenir
% à une région groupe pour tous les éléments de laquelle on a le droit d'accès
% complet.
\end{comment}
% fp: J'allais continuer ici en disant qu'un autre exemple, c'est la
% fonction de tri, qui doit exiger que son argument ``compare'' soit capable
% de comparer deux éléments, aliasés ou non.
% Mais je me rends compte que, pour que la fonction de tri soit applicable
% à des éléments aliasés ou non, il faut supposer que tous les éléments
% sont dans une région groupe sur le côté. Et dans ce cas, comme je l'écris
% ci-dessus, on peut toujours passer cette région groupe à compare, en lui
% garantissant que les deux éléments à comparer sont dedans. Ça donne une
% spec principale, un peu lourde mais pas vraiment améliorable,
% me semble-t-il.

%------------------------------------------------------------------------------
% Shortcoming 3.

% Weakness.
% In two items: first, it is deceptively weak (= easily intuitively mis-understood);
% second, it is sometimes unusably weak (= so weak that another specification must
% be written, without using the RO sugar).

\paragraph{Shortcoming~\ref{shortcoming:weak:deceptive}: deceptively weak}

% 2. Weak / deceptive
Under the naïve interpretation
of ``\HROname'' as macro-expansion,
the Hoare triple $\Jhoare{\HRO{H_1} \Hstar H_2}tQ$
does not guarantee that the memory covered by~$H_1$ is
unaffected by the execution of the command~$t$.
Instead,
it means only that $H_1$ still holds upon termination of~$t$.
To see this,
imagine that the assertion $\Htable{h}{M}$
means ``the hash table at address $h$
currently represents the dictionary~$M$''.
A~function $\mathit{population}$, which returns the
number of entries in a hash table, could have
the following specification:
\begin{equation}
\label{spec:population}
  \Jhoare
    {\HRO{\Htable{h}{M}}}
    { (\mathit{population}\;h) }
    { \HPabs{y} \, \HPprop{y = \F{card}\,M} }
\end{equation}
Under the macro-expansion interpretation,
this specification
% (although valid and provable)
guarantees that $\Htable{h}{M}$ is preserved,
so, after a call to $\mathit{population}$, the table~$h$ still represents the dictionary~$M$.
Somewhat subtly, this does not guarantee that the concrete data structure is unchanged.
In fact, a~function $\mathit{resize}$, which
% re-allocates the hash table's data array
doubles the physical size of the table and
profoundly affects its organization in memory, would admit a similar specification:
\begin{equation}
\label{spec:resize}
  \Jhoare
    {\HRO{\Htable{h}{M}}}
    { (\mathit{resize}\;h) }
    { \HPabs{()} \, \HPprop{} }
\end{equation}

% How RO permissions address this shortcoming:

In this paper, we define the meaning of ``$\HROname$'' in such a way
that it really means ``read-only''. Therefore, the above specification
of $\mathit{population}$~(\ref{spec:population}) acquires stronger meaning,
and guarantees that
$\mathit{population}$ does not modify the hash table. The specification
of $\mathit{resize}$~(\ref{spec:resize}) similarly acquires stronger meaning,
and can no longer be established,
since $\mathit{resize}$ does modify the hash table.
A valid specification of $\mathit{resize}$ is
$\Jhoare{\Htable{h}{M}}{ (\mathit{resize}\;h) } { \HPabs{()} \, \Htable{h}{M} }$.
% for which sugar can still be provided

%------------------------------------------------------------------------------
% Shortcoming 4.

% arthur: là il faudrait développer, je n'ai pas compris l'argument au final;
% tu ne dis pas en quoi c'est un problème de ne pas garantir l'invariance de l'implem.
% Ah, ça vient après, dans le point 3; mais alors pkoi ne pas merger les deux ?

% fp: Oui. Il m'a semblé plus clair de distinguer les deux points.
% Je pense que c'est important que RO signifie vraiment read-only,
% au moins comme documentation. C'est trompeur sinon.
% Dans un second temps, vient l'argument plus technique comme quoi
% la spec avec RO est trop faible si RO est interprété naïvement.

\paragraph{Shortcoming~\ref{shortcoming:weak:unusable}: unusably weak}

% arthur: On devrait être capable d'expliquer toute l'intuition avant de
% montrer la moindre formule, je pense.
% fp: C'est vraiment pas facile.

% 2b. So weak that it cannot be used / unfolding is required instead.

The weakness of the above specifications is not only somewhat unexpected and
deceptive: there are in fact situations where it is problematic.

% Continuing the example of hash tables,
Imagine that a hash table is internally represented as a record of several
fields, among which is a $\mathit{data}$ field, holding a~pointer to an array.
The abstract predicate $\Htable{h}{M}$ might then be defined as follows:
%
\begin{equation}
\label{def:Htable}
\begin{array}[t]{l}
\Htable{h}{M} \eqdef \nlq
\HPexinameof{a} { \HPexinameof{L} {(
  \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots
)}}
\end{array}
\end{equation}

Suppose we wish to verify an operation $\mathit{foo}$, inside the ``hash
table'' module, whose code begins as follows:
%
\[\begin{array}{l@{\qquad}l}
  \Llettoplevel{\mathit{foo}\;h}{              \nlq
    \Llet{d}{h.\mathit{data}}
      \Hcomment{read the address of the array} \nlq
    \Llet{p}{\mathit{population}\;h}
      \Hcomment{call $\mathit{population}$}    \nlq
    \ldots
  }
\end{array}\]

A proof outline for this function must begin as follows:
%
\[\begin{array}{l@{\quad}l@{\quad}l}
\numm1 \Llettoplevel{\mathit{foo}\;h}{                   \\
\numq2   \Hassert{ \Htable{h}{M} }
         \Hcomment{$\mathit{foo}$'s precondition}        \\
    % \forall a.\forall L.
\numq3    \Hassert{ \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots }
      \Hcomment{by unfolding}                       \\
%      \Hcomment{$a, L$ fresh auxiliary variables}   \\
\numq4    \Llet{d}{h.\mathit{data}}                       \\
\numq5    \Hassert{ \Hrecord{h}{a} \Hstar \Hdataarray{a}{L} \Hstar \ldots \Hstar \HPprop{d=a} }
      \Hcomment{by reading}                         \\
\numq6    \Hassert{ \Htable{h}{M} \Hstar \HPprop{d=a} }
      \Hcomment{by folding}                         \\
\numq7    \Llet{p}{\mathit{population}\;h}                \\
\numq8    \Hassert{ \Htable{h}{M} \Hstar \HPprop{d=a} \Hstar \HPprop{p = \#\!M} }
                                                    \\
\numq9    \ldots
}
\end{array}\]

At line~3, we unfold $\Htable{h}{M}$. Two auxiliary variables, $a$~and~$L$,
are introduced at this point; their scope extends to the end of the proof
outline. This unfolding step is mandatory:
% in its absence, the read instruction at line~4 could not be justified
indeed, the read instruction at line~4 requires
$\Hrecord{h}{a}$. % for some $a$
This instruction produces the pure assertion $\HPprop{d=a}$,
which together with the assertion $\Hrecord{h}{a}$ means that
% the value of the local variable
$d$~is the current value of the field $\mathit{h.data}$.

At line~6, we fold $\Htable{h}{M}$. This is mandatory:
indeed, under the naïve interpretation of ``\HROname'',
the precondition of the call ``$\mathit{population}\;h$'' is $\Htable{h}{M}$. % for some M
Unfortunately, this folding step is harmful:
it causes us to lose $\Hrecord{h}{a}$ and
thereby to forget that $d$~is the current value of the
field $\mathit{h.data}$. (The equation $d=a$ remains true, but becomes
useless.)
% since there is no other occurrence of $a$
Yet, in reality, this fact is preserved through the call, which does not
modify the hash table.

In summary, because the specification of $\mathit{population}$
(\ref{spec:population}) is too weak,
calling $\mathit{population}$ at line~7 causes us to lose the benefit of the read
instruction at line~4. In this particular example, one could work around the
problem by exchanging the two instructions. In general, though, it might not
be possible or desirable to modify the code so as to facilitate the proof.
Another work-around is to equip $\mathit{population}$ with a lower-level
specification, where the predicate \F{HashTable} is manually unfolded.
% (With sharing between the pre and postconditions, via universal quantifiers.)

We have demonstrated that, under the naïve interpretation, the
specification of $\mathit{population}$ (\ref{spec:population}) can be
unsuitable for use inside the ``hash table'' abstraction.
% (Outside the abstraction barrier, it is fine.)

%   * one could think of a more complex macro-expansion scheme, based on bounded quantification,
%     along the lines of:
%       forall H,   H entails H1 => { H * H2 } e { H * Q }
%     but that does not work: H needs to be weakened to H1 in order to be useable,
%     and thereafter H cannot be recovered.

% How RO permissions address this shortcoming:

In this paper, we define the meaning of ``$\HROname$'' in such a way that all
of the information that is available at line~5 is preserved through the call
to $\mathit{population}$.
% even though the specification of $\mathit{population}$ is still (\ref{spec:population})
This is explained later on (\sref{sec:population:explained}).

%------------------------------------------------------------------------------

% I would like to add another shortcoming:
% - 4. the naïve interpretation does not allow deep read-only state
%      (i.e. using RO inside a container, e.g. a list of read-only elements)
% But it is not clear that our system supports this.

%------------------------------------------------------------------------------
%\subsection{State of the art}

% Terminologie: assertion = permission; à la première occurrence
% du mot ``permission''. Pour le moment, c'est ici.

\newcommand\terminology{\footnote{%
Following Boyland~\citeyear{boyland-fractions-03},
Balabonski \etal.~\citeyear{balabonski-pottier-protzenko-mezzo-journal-16},
and others,
we use the words ``assertion'' and ``permission'' interchangeably.}}

\subsection{Towards true read-only permissions}

The question that we wish to address is: what is a simple extension of
sequential \SL with duplicable temporary read-only permissions\terminology{}
for mutable data?

We should stress that we are primarily interested in a logic of sequential
programs. We do discuss structured parallelism and shared-memory concurrency
near the end of the paper (\sref{sec:parallelism}).

% On pourrait vouloir affirmer ici que cette question n'a pas (encore)
% de réponse dans la littérature, mais cela risquerait de nous entraîner
% dans une discussion du related work (Rust, etc.).

We should also emphasize that we are not interested in read permissions for
permanently immutable data, which are a different concept.
Such permissions can be found, for instance, in
Mezzo~\cite{balabonski-pottier-protzenko-mezzo-journal-16}, and could
be introduced in \SL, if desired. They, too, grant read access only, and are
duplicable.
Mezzo allows converting a unique read-write permission to a
duplicable read permission, but not the other way around: the transition from
mutable to immutable state is irrevocable.
% permanent
Mezzo has no mechanism for obtaining a temporary read-only view of a mutable
data structure.

% The technical reason why it must be permanent is that there is no accounting
% of read permissions. If one allowed a read permission to be converted back
% to a read-write permission, one would not be able to guarantee that a
% read-write permission and a read permission cannot coexist.

Finally, we should say a word of fractional permissions
(which are discussed in greater depth in \sref{sec:fractional}).
Fractional permissions~\cite{boyland-fractions-03} can be used to obtain
temporary read-only views of mutable data. A fraction that is strictly less
than~1 grants read-only access, and, by joining all shares so as to recover
the fraction~1, a unique read-write access permission can be recovered.
Nevertheless, fractional permissions are not what we seek. They do not address
our shortcoming~\ref{shortcoming:work}: their use requires work that we wish
to avoid, namely ``accounting'' (arithmetic reasoning) as well as (universal
and existential) quantification over fraction variables.
%
% fp: un peu douteux, car il faut voir ce qu'on veut dire par là exactement:
% Neither do they address our shortcoming~\ref{shortcoming:weak:unusable}.
% Je vais plutôt mettre l'accent sur les problèmes du scaling.
Furthermore, whereas $\HRO{\Htable{h}{M}}$ is a well-formed permission
in our logic, in most systems of fractional permissions,
$\frac 12(\Htable{h}{M})$ is not well-formed. The systems that do
allow this kind of ``scaling'', such as Boyland's~\citeyear{boyland-nesting-10},
do so at the cost of restricting disjunction and existential quantification so
that they are ``precise''.

% If population requires a fraction of h ~> HashTable M,
% then we must still fold prior to calling population,
% so we still lose knowledge.
% We could try a hack based on folding one half of h ~> HashTable M
% and keeping the other half unfolded, so we have both views at the
% same time. But this is complicated.
% Furthermore, h ~> HashTable M is defined by an existential quantification,
% and I am not sure that fractions commute with existential quantifiers; in
% principle, they do not.

% Fractional permissions do not address shortcoming 1: verbosity and work.
% They do address shortcoming 2: aliasing.
% They do address shortcoming 3: they really mean read-only.
% They do not address shortcoming 4: if a fractional permission is weakened
% (via entailment) then the original permission can never be recovered.

%------------------------------------------------------------------------------
%\subsection{Contribution}

In this paper, we answer the above question. We introduce a generic assertion
transformer, ``$\HROname$''. For any assertion~$H$, it is permitted to form
the assertion $\HRO{H}$, which offers read-only access to the memory covered
by~$H$. For instance, $\HRO{\HPsingle{x}{v}}$ offers read-only access to the
memory cell at address~$x$.
% fp: on avait un exemple avec un tableau, mais j'aime mieux une référence
% car les tableaux ne sont pas dans le système formalisé.
%
% arthur: du coup, est-ce qu'on veut montrer la notation
% $\Hdataarrayro{a}{L}$ qui est quand meme beaucoup plus jolie
% et facile à lire, où bien on laisse tomber?
% fp: pour moi, on laisse tomber. C'est évident que si on veut
% une notation plus concise dans un cas particulier, on peut se
% la donner.
%
The temporary conversion from a permission $H$ to its read-only
counterpart $\HRO{H}$ is performed within a lexically-delimited scope,
via a ``\rofr''.
% fp: je propose cette terminologie, plutôt que generalized frame rule
Upon entry into the scope, $H$ is replaced with $\HRO{H}$.
Within the scope, $\HRO{H}$ can be duplicated if desired,
and some copies can be discarded; there is no need to
keep track of all shares and recombine them so as to regain a full
permission.
Upon exit of the scope, the permission~$H$
% magically
re-appears.

The rest of the paper is organized as follows. First, we review our additions
to \SL (\secref{sec:overview}). Then, we give a formal, self-contained
presentation of our logic~(\secref{sec:logic}) and of its model, which we use
to establish the soundness of the logic~(\secref{sec:model}).
The soundness proof is formalized in Coq and can be found online~\cite{online}.
Then, we review some of the related work (\secref{sec:related}), discuss some
potential applications and extensions of our logic (\sref{sec:potential}), and
conclude (\secref{sec:conclusion}).

%  LocalWords:  Smallfoot smallfoot-05 distefano-parkinson-08 verifast Jhoare
%  LocalWords:  appel-vst-11 bengtson-12 chargueraud-10 chargueraud-cfml emph
%  LocalWords:  mathpar inferrule HPsingle LGetof HPabs HPprop Hstar HPsingle
%  LocalWords:  mathit arrayconcat Hdataarray Hdataarray OLAppof HROname nlq
%  LocalWords:  Hoare Htable newcommand Hrecord Hdatasep ldots eqdef Hassert
%  LocalWords:  Hcomment HPexinameof HPexinameof qquad Llettoplevel Llet numm
%  LocalWords:  numq sref Boyland citeyear boyland-fractions-03 etal rofr
%  LocalWords:  balabonski-pottier-protzenko-mezzo-journal-16 secref
