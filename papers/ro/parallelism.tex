\subsection{Parallelism and concurrency}
\label{sec:parallelism}

We believe that our read-only permissions remain sound when the calculus is
extended with ``structured parallelism'', that is, with a term construct $t
::= \Lpar{t}{t}$ for evaluating two terms in parallel. The parallel
composition rule of Concurrent \SL~\cite{ohearn-07} can be used:
%
\begin{mathpar}
\inferrule[parallel composition]
{ \Jhoare{H_1}{t_1}{Q_1} \\ \Jhoare{H_2}{t_2}{Q_2} }
{ \Jhoare{ H_1 \Hstar H_2 }{ \Lpar{t_1}{t_2} }{ \Qstarof{Q_1}{Q_2} } }
\end{mathpar}
%
Because read-only permissions are duplicable, this rule, combined with the
rule of consequence, allows read access to be shared between the threads~$t_1$
and~$t_2$. That is, the following rule is derivable:
\begin{mathpar}
\inferrule[parallel composition with shared read]
{ \Jhoare{H_1 \Hstar \HRO{H'}}{t_1}{Q_1} \\ \Jhoare{H_2 \Hstar \HRO{H'}}{t_2}{Q_2} }
{ \Jhoare{H_1 \Hstar H_2 \Hstar \HRO{H'} }{ \Lpar{t_1}{t_2} }{ \Qstarof{Q_1}{Q_2} } }
\end{mathpar}
%
This rule can be used to share read access between any number of threads. By
combining it with the \rofr, one obtains the following rule, which allows a
mutable data structure (represented by the permission~$H'$) to temporarily be
made accessible for reading to several threads and to become again accessible
for reading and writing once these threads are finished:
%
\begin{mathpar}
\inferrule[parallel composition with temporary shared read]
{ \Jhoare{H_1 \Hstar \HRO{H'}}{t_1}{Q_1} \\ \Jhoare{H_2 \Hstar \HRO{H'}}{t_2}{Q_2} \\ \HNormal{H'} }
{ \Jhoare{H_1 \Hstar H_2 \Hstar H' }{ \Lpar{t_1}{t_2} }{ \Qstarof{Q_1}{Q_2} \Hstar H' } }
\end{mathpar}
%

% arthur: illustrate with read over array

At present, we do not have a proof that the \RULE{parallel composition} rule
is sound. Our current proof technique apparently cannot easily accommodate it,
primarily because it is based on a big-step operational semantics
(\fref{fig:red}), which (to the best of our knowledge) cannot be easily
extended to support parallel composition $\Lpar{t_1}{t_2}$.

These remarks lead to several questions. Could \SL with read-only permissions
be proved sound, based on a small-step operational semantics? Could the logic
and its proof then be extended with support for structured parallelism? There
seems to be no reason why they could not, but this requires further research.

Another question arises: could \SL with read-only permissions be extended so
as to support unstructured parallelism, that is, shared-memory concurrency
with explicit threads and synchronization facilities, such as locks and
channels? We do not have an answer. We know that naïvely transmitting a
read-only permission from one thread to another would be unsound. So,
probably, the logic would have to be made more complex, perhaps by explicitly
annotating read-only permissions with ``lifetime'' information. Whether this
can be done while preserving the simplicity of the approach is an open
question. After all, the whole approach is worthwhile only as long as it
remains significantly simpler than fractional permissions
(\sref{sec:fractional}), which offer a well-understood solution to this
problem.

%  LocalWords:  Lpar ohearn-07 mathpar inferrule Jhoare Jhoare Hstar Qstarof
%  LocalWords:  rofr HNormal fref sref
