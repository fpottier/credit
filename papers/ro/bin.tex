\newcommand{\HStarablename}{\F{starable}}
\newcommand{\HStarable}[4]{\HStarablename\,{#1}\,{#2}\,{#3}\,{#4}}
\newcommand{\HStarableof}[4]{\HStarablename({#1}{#2}{#3}{#4})}



The third one, $\HStarable{f_1}{g_1}{f_2}{g_2}$ asserts that the heaps
described by the pairs $(f_1,g_1)$
and $(f_2,g_2)$ are valid candidate for composition by the separating
conjunction $(\Hstar)$.
As explained in the overview, it requires $g_1$ and $g_2$ to be compatible
and it requires $f_1$ and $f_2$ and $(\OMuniof{g_1}{g_2})$ to have disjoint
domains.




%--------------
\begin{figure}[t]

$$\begin{array}{l@{\;\;}l@{\;\;}l}
% \HPempty & \Labs{h}{ \; h = \OMempty } \nextrow{1} % viewed as sugar, to save a line
\HPprop{P} & \equiv & \Labs{(f,g,\Gd)}{ \; f = \OMempty \Sc\land g = \OMempty \Sc\land P } \nextrow{2}
\HPsingle{l}{v} & \equiv & \Labs{(f,g,\Gd)}{ \; f = \OMone{l}{v} \Sc\land g = \OMempty } \nextrow{2}
\Hstarof{H_1}{H_2} &
\equiv & \Labs{(f,g,\Gd)}{ } \;
  \begin{array}[t]{l}
    \Texi{(f_1,g_1,\Gd_1)} \; \Texi{(f_2,g_2,\Gd_2)} \nextrow{1}
    \phantom{\land} \; \HStarable{f_1}{g_1}{f_2}{g_2} \nextrow{0}
    \land \; f = \OMdisuniof{f_1}{f_2}  \nextrow{0}
    \land \;g = \OMuniof{g_1}{g_2}\nextrow{0}
    \land \;H_1\,(f_1,g_1,\Gd_1) \nextrow{0}
    \land \; H_2\,(f_2,g_2,\Gd_2)
   \end{array}  \nextrow{2}
\HPexinameof{x}{H} & \equiv & \Labs{(f,g,\Gd)}{ \; \Texi{x}\; H\,(f,g,\Gd) }   \nextrow{2}
\HRO{H} & \equiv &  \Labs{(f,g,\Gd)}{ \; \Texi{(f',g',\Gd')}\; H\,(f',g',\Gd') \Sc\land  f = \OMempty \Sc\land g = \OMdisuniof{f'}{g'}  }
\nextrow{15}
\HNormalof{H} & \equiv & \Tfor{(f,g,\Gd)}{ \; H\,(f,g,\Gd) \Sc\impl g = \OMempty }
\nextrow{2}
\Himplof{H_1}{H_2}  & \equiv &
  \Tfor{(f,g,\Gd)}\; H_1\,(f,g,\Gd) \Sc\impl H_2\,(f,g,\Gd)
\end{array}$$

\hrule
\caption{Model for permissions.}
\label{fig:model}
\end{figure}
%--------------


The realization of core permissions appear in \figref{fig:model}.
The permission $\HPprop{P}$ is a predicate that holds of
a heap of the form $(\OMempty,\OMempty,\Gd)$ when the proposition
$P$ is true. (Here, $\Gd$ is a proof of $\OMempty\Hdis\OMempty$.)
The permission $\HPsingle{l}{v}$ is a predicate that holds of
a heap of the form $(f,\OMempty,\Gd)$, where $f$ is a singleton
state that binds the location $l$ to the value $v$.
%
The permission $\Hstarof{H_1}{H_2}$ is a predicate that holds of
a heap of the form $(\OMdisuniof{f_1}{f_2}, \OMuniof{g_1}{g_2},\Gd)$,
where $(f_1,g_1,\Gd_1)$ satisfies $H_1$ (for some proof $\Gd_1$),
and $(f_2,g_2,\Gd_2)$ satisfies $H_2$ (for some proof $\Gd_2$),
and the states $g_1$ and $g_2$ are compatible, and
the states $f_1$ and $f_2$ and $(\OMuniof{g_1}{g_2})$ are disjoint.

The permission $\HPexinameof{x}{H}$ holds if there exists an
entity $x$ such that the permission $H$ is available.
The corresponding Coq definition, shown below, properly handes binders.
Observe that the entity $x$ may have any type. In particular,
$x$ might denote a permission---the possibility to existentially
quantify over permissions is essential for abstraction.
%
\begin{coq}
  Definition hprop_exists (A:Type) (Hof : A -> hprop) : hprop :=
    fun h => exists x, Hof x h.
  Notation "'Hexists' x , H" := (hprop_exists (fun x => H)).
\end{coq}

The definition of $\HRO{H}$ characterizes a heap of the form
$(\OMempty, \OMdisuniof{f'}{g'} ,\Gd)$ such that
$(f',g',\Gd')$ satisfies $H$, for some proof $\Gd'$
ensuring that $f'$ and $g'$ are disjoint. Effectively,
it transfers the normal ownership of $f'$ into the
read-only part, merging it with $g'$.
%
The predicate $\HNormal{H}$ is true if for any heap $(f,g,\Gd)$ that
may satisfy the permission $H$, it is the case that the read-only
component $g$ is empty. This definition ensures that $H$ cannot
contain any $\HROname$ permission inside it.

The entailment relation is defined in the standard way:
$\Himplof{H_1}{H_2}$ holds if any heap satisfying $H_1$
also satisfies $H_2$.



%------------------------------------------------------------------------------
\subsection{Proof of soundness}

To establish the soundness, we first need to assign an
interpretation to triples of the form \W{\Jhoare{H}{t}{Q}}.
Then, we may prove the derivation rules of our logic
by stating them in the form of lemmas.

For the purpose of presentation,
we begin by presenting a slightly simplified interpretation of triple
that does not account for garbage collection, i.e.
that does not allow throwing away permissions.
The definition shows below, involves two heaps. The first one
corresponds to the heap described by the pre-condition $H$,
while the second one corresponds to the part currently being framed.
The evaluation of the term $t$ should terminate and produce a value $v$.
The memory state may only be modified on $f_1$,
the fully-owned part of the heap that was covered by the pre-condition.
The updated part of memory (possibly also including freshly allocated
data), is called $f_1'$, and it is the piece of state specified by
the post-condition $Q$. This part $f_1'$ should remain separated
from the other pieces of state in the same way as $f_1$ was initially.
Observe that the post-condition cannot describe any read-only piece
of state, hence the $\OMempty$ provided as second argument of $Q$.
%
$$\begin{linest}
\Jhoare{H}{t}{Q}
\Sq\equiv \nextrow{7}
\quad
\Tfor{(f_1,g_1,\Gd_1)} \; \Tfor{(f_2,g_2,\Gd_2)}\;\,  \nextrow{1}
\quad\quad
\begin{ands}
H\,(f_1,g_1,\Gd_1)
\nextrow{1}
\HStarable{f_1}{g_1}{f_2}{g_2}
\end{ands}
\Scs\impl \Texi{v f_1' \Gd_1' } \;
\begin{ands}
\Jredbigconfigof{ t }{ f_1 \OMdisuni f_2 \OMdisuni (g_1 \OMuni g2) }{ v }{ f_1' \OMdisuni f_2 \OMdisuni (g_1 \OMuni g2) }
& %\hypertarget{c:seprun}{(1)}
\nextrow{1}
\HStarable{f_1'}{g_1}{f_2}{g_2}
& %\hypertarget{c:sepdisof}{(2)}
\nextrow{1}
Q\,v\,(f_1',\OMempty,\Gd_1')
& %\hypertarget{c:seppost}{(3)}
\end{ands}

\end{linest}$$

The actual definition refines the one above, by allowing
for a subset of the state $f_1'$, named $f_1''$, to
be thrown away, that is, to be discarded from the post-condition.
This refinement is required to establish the soundness of the
garbage collection rules (\D{gc-pre} and \D{gc-post}).
%
\begin{definition}[Interpretation of \SL triples with read-only permissions]
\label{def:septriple}
%
$$\begin{linest}
\Jhoare{H}{t}{Q}
\Sq\equiv \nextrow{7}
\quad
\Tfor{(f_1,g_1,\Gd_1)} \; \Tfor{(f_2,g_2,\Gd_2)}\;\,  \nextrow{1}
\quad\quad
\begin{ands}
H\,(f_1,g_1,\Gd_1)
\nextrow{1}
\HStarable{f_1}{g_1}{f_2}{g_2}
\end{ands}
\Scs\impl \Texi{f_1' f_1'' \Gd_1' v } \;
\begin{ands}
\Jredbigconfigof{ t }{ f_1 \OMdisuni f_2 \OMdisuni (g_1 \OMuni g2) }{ v }{   f_1' \OMdisuni f_1'' \OMdisuni f_2 \OMdisuni (g_1 \OMuni g2) }
& %\hypertarget{c:seprun}{(1)}
\nextrow{1}
\Hdisof{f_1'}{f_1''}
\nextrow{1}
\HStarable{(f_1' \OMdisuni f_1'')}{g_1}{f_2}{g_2}
& %\hypertarget{c:sepdisof}{(2)}
\nextrow{1}
Q\,v\,(f_1',\OMempty,\Gd_1')
& %\hypertarget{c:seppost}{(3)}
\end{ands}

\end{linest}$$
\end{definition}

Based on this definition, we are able to establish soundness.

\begin{theorem}[Soundness of \SL with read-only permissions]
\label{th:soundness-basic}
Each of the reasoning rules shown in \figref{fig:seplogic} is sound
with respect to the definition of triples from \ref{def:septriple}.
\end{theorem}
\begin{proof}
We refer the reader to the Coq proof script. \qed
\end{proof}
