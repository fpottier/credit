type rank = int
type elem = content ref
and content = Link of elem | Root of rank 

let make () = ref (Root 0)
 
let rec find x =
  match !x with
  | Root _ -> x
  | Link y ->
      let z = find y in
      x := Link z;
      z

let link x y =
  if x == y then x else
  match !x, !y with
  | Root rx, Root ry ->
      if rx < ry then begin
        x := Link y; y
      end else if rx > ry then begin
        y := Link x; x
      end else begin
        y := Link x; x := Root (rx+1); x
      end
  | _, _ ->
      assert false

let union x y = link (find x) (find y)
let equiv x y = find x == find y

