\documentclass{llncs}
\pagestyle{headings} % Show page numbers.
\usepackage[breaklinks,colorlinks=true,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{makeidx}  % allows for indexgeneration
\input{macros}
\input{local}
\input{remarks}


%******************************************************************************

\begin{document}

\subtitle{---Technical Appendix---}
\newcommand{\nl}{\texorpdfstring{\\}{}}
\title{Machine-Checked Verification\nl of the Correctness and Amortized Complexity\nl of an Efficient Union-Find Implementation}

\author{Arthur Charguéraud\inst{1} \and François Pottier\inst{2}}

\institute{Inria \& LRI, Université Paris Sud, CNRS
\and Inria}

\maketitle

%******************************************************************************
%******************************************************************************


In this technical appendix, we establish the soundness theorem for
characteristic formulae. To that end, we introduce a number of auxiliary
definitions and establish a couple of lemmas. Note that we omit the
treatment of polymorphism, which is completely orthogonal to time credits,
yet requires explicitly keeping track of types and would add an extra 
layer of complexity to the proof. Note also that we only consider a core 
language here, but that all the ideas can be easily generalized to 
other programming constructs.
We refer to the first
author's PhD thesis~\cite{chargueraud-10} for details on polymorphism
and other language constructs.


We begin by definining the semantics of the source language, with a judgment
that keeps track of the number of beta-reductions.

%%------------------------------------------
\begin{definition}[Cost semantics]
Definition of $\Jtredbignconfigof{ \Ut }{ \Um }{ n }{ \Uv }{ \Um' }$.

\begin{mathpar}
\inferrule*[]{ }{ \Jtredbignconfigof{ \Uv }{ \Um }{ 0 }{ \Uv }{ \Um } }

\inferrule*[]
{ \Jtredbignconfigof{ \Ut_1 }{ \Um }{ n }{ \Uv_1 }{ \Um' }  \\
  \Jtredbignconfigof{ \OSubst{x}{\Uv_1}{\Ut_2} }{ \Um' }{ n' }{ \Uv }{ \Um'' }  }
{ \Jtredbignconfigof{ \LLetof{x}{\Ut_1}{\Ut_2} }{ \Um }{ n+n' }{ \Uv }{ \Um'' } }

\inferrule*[]
{ \Jtredbignconfigof{ \OSubst{f}{ \Lfix{f}{x}{\Ut_1} }{ \Ut_2 } }{ \Um }{ n }{ \Uv }{ \Um' }  }
{ \Jtredbignconfigof{ \LFunof{f}{x}{\Ut_1}{\Ut_2} }{ \Um }{ n }{ \Uv }{ \Um' } }

\inferrule*[]
{ 
\Jtredbignconfigof{\OSubst{f}{\LFix{f}{x}{\Ut_1}}{\Osubst{x}{\Uv_2}{\Ut_1}} }{ \Um }{ n }{ \Uv }{ \Um' } }
{ \Jtredbignconfigof{ \LApp{ \LFix{f}{x}{\Ut_1} }{\Uv_2} }{ \Um }{ n+1 }{ \Uv }{ \Um' } }
\quad
\inferrule*[]
{ {l} = 1+\F{max}({\OMdom{\Um}}) }
{ \Jtredbignconfigof{ \LRefof{\Uv} }{ \Um }{ 0 }{ l }{ \OMadd{\Um}{l}{\Uv} } }

\inferrule*[]
{ l \in \OMdom{\Um} \\ \Uv = \OMget{\Um}{l} \\  }
{ \Jtredbignconfigof{ \LGetof{l} }{ \Um }{ 0 }{ \Uv }{ \Um } }

\inferrule*[]
{ l \in \OMdom{\Um} }
{ \Jtredbignconfigof{ (\Lset\,({l},{\Uv})) }{ \Um }{ 0 }{ \Ltt }{ (\OMset{\Um}{l}{\Uv}) } }
\end{mathpar}
\end{definition}
%%------------------------------------------

We then define a translation of types and values from the source language
into the logical language, and establish a one to one correspondance between
program values and logical values with certain types.
Below, $C$ denotes a type constructor and $D$
denotes a data constructor (used for describing algebraic data types).

%%------------------------------------------
\begin{definition}[Translation of program types into logical types]
Given a program type $T$, we define the logical type $\Freflect{T}$ as follows.
$$\begin{array}[t]{l@{\Sq\equiv}ll} 
\Freflect{\Tint} & \mathbb{Z} \vspace{2pt}\\
%\Freflect{\Tprodof{\Tt_1}{\Tt_2}} & \Tprodof{\Freflect{\Tt_1}}{\Freflect{\Tt_2}}\vspace{2pt} \\
%\Freflect{\Tsumof{\Tt_1}{\Tt_2}} & \Tsumof{\Freflect{\Tt_1}}{\Freflect{\Tt_2}} \vspace{2pt}\\
\Freflect{\Ldataof{C}{\Tt_1}{\Tt_2}} & \Ldataof{C}{\Freflect{\Tt_1}}{\Freflect{\Tt_2}} \vspace{2pt}\\
\Freflect{\Ttoof{\Tt_1}{\Tt_2}} & \Tfunc \vspace{2pt} \\
\Freflect{\Tref{\Tt}} & \Tloc
\end{array}$$
\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{definition}[Translation of program values into logical values]
Given a program value $v$ of type $T$, we define the logical value $\Fdecode{v}$ of type $\Freflect{T}$ as follows.
$$\begin{array}[t]{l@{\quad}l@{\quad}lll} 
\Fdecode{x} &\equiv& x \vspace{2pt}\\ 
\Fdecode{n} &\equiv& n \vspace{2pt} \\
\Fdecode{\Ldataof{D}{\Uv_1}{\Uv_k}} &\equiv& \Ldataof{D}{\Fdecode{\Uv_1}}{\Fdecode{\Uv_k}}  \vspace{4pt}\\ 
\X{Only for proofs:} \vspace{2pt}\\ 
\Fdecode{l} &\equiv& l \qquad \X{(translation of concrete locations)} \vspace{2pt} \\
\Fdecode{ \Lfix{f}{x}{t} } &\equiv& \begin{linest}  
  (\Lfix{f}{x}{\Ut}, \;\R{H}) \Sc{:} \Tfunc  \qquad \X{(translation of closures)}
 \vspace{1pt}\\
  \X{where $\R{H}$ is a proof of \Q{\Jiswtcof{\Uv}}} \vspace{1pt}\\
\end{linest}
\end{array}$$
\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{lemma}[Bijectivity of the translation of values]
For any program type~$T$ and for any logical value~$V$ of type $\Freflect{T}$, 
there exists a unique program value $v$ such that $\Fdecode{v} = V$.
In other words, $\Fdecode{\cdot}$ is injective, and surjective on
the set of logical values with a type of the form $\Freflect{\cdot}$.
\end{lemma}
\begin{proof}
Straightforward.
%, except for closures.
%, for which we exploit the fact that,
%technically $\Fdecode{f}$ is a Coq value that carries the {\em typed} closure.
We refer to Charguéraud's PhD thesis for details. \qed
\end{proof}
%%------------------------------------------


We next introduce a couple of useful pieces of notation.

%%------------------------------------------
\begin{definition}[Notation for the type heap predicates]
We let $\Thprop$ be a shorthand for $\Theap\Tto\Tprop$.
\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{definition}[Notation for evaluations]
We define $\Jtredbignconfigof{ \Ut }{ h }{ n }{ \Uv }{ h' }$ to mean
$\Jtredbignconfigof{ \Ut }{ \Um }{ n }{ \Uv }{ \Um' }$ where $\Um$ 
and $\Um'$ are the state components (i.e. first projection)
of $h$ and $h'$, respectively.
\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{definition}[Notation for credit components]
We write $\Fcreds{h}$ the credits components (i.e. second projection)
of $h$. Note that $\Fcreds{\OSdisuniof{h_1}{h_2}} =\Fcreds{h_1} + \Fcreds{h_2}$.
\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{definition}[Implication between post-conditions]
We extend the definition of $H \Hstar H'$ to $Q \Qstar H'$ as follows:
$$\Qstarof{Q}{H'} \Sq\equiv \Labs{x\,}{\Hstarof{(Q\,x)}{H'}} $$
%where the variable $x$ denotes the output value and \W{Q_1\,x} describes the output heap.
\end{definition}
%%------------------------------------------

We are then ready to give the formal definition of the predicate
$\Jlocal$ (used for implementing structural rules in characteristic formulae), 
the definition of characteristic formulae, and the specification of
primitive operations on references.

%%------------------------------------------
\begin{definition}[Predicate $\Jlocal$]
  $$\begin{array}{@{}l@{\;\;}ll}
  \Jlocalof{\R{F}} \;\equiv & \Labsrun{H\,Q} \; &
   \Tfor{h}\; H\,h \Sc\impl \Texi{H_1 H_2 H_3 Q_1} \\
  & & \;\; \HStarof{H_1}{H_2}\,h \Scs\land
  \R{F}\,H_1\,Q_1 \Scs\land
  \Dpar{\Tfor{x}\;\Himplof{\Hstarof{(Q_1\,x)}{H_2}}{\Hstarof{(Q\,x)}{H_3}}}
  \end{array}$$
\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{definition}[Characteristic formulae]
Given a term $t$ of type $T$, its characteristic formula $\Fcfgi{t}$ has type 
\Q{\Thprop\Tto(\Freflect{T}\Tto\Thprop)\Tto\Tprop} and is defined recursively
as follows:
%
$$\begin{array}{lll}
\Fcfgi{v} &\equiv&
  \JlocalOf{\HPabs{HQ}{\; \Himplof{H}{Q\,\Fdecode{v}} }}
\\
% \Fcfgi{\Lseqof{t_1}{t_2}} &\equiv&
%   \JlocalOf{\HPabs{HQ}{\; \Texi{Q'}\;\, \Fcfgi{t_1}\,H\,Q' \Sc\land  \Fcfgi{t_2}\,(Q'\,\Ltt)\,Q }}
% \nextrow{1}
\Fcfgi{\Lletof{x}{t_1}{t_2}} &\equiv&
  \JlocalOf{\HPabs{HQ}\; \Texi{Q'}\;\, \Fcfgi{t_1}\,H\,Q' \Sc\land \Tfor{x}\;\, \Fcfgi{t_2}\,(Q'\,x)\,Q }
\\
\Fcfgi{\Lapp{f}{v}} &\equiv&
  \JlocalOf{\HPabs{HQ}{\; \Jiappreturnsof{\Fdecode{f}}{\Fdecode{v}}{H}{Q} }}
\label{Appelim}
\\
\Fcfgi{\Lfunnorec{f}{x}{t_1}{t_2}} &\equiv& \JlocalOf{\HPabs{HQ}\; \Tfor{f}\, P \Sc\impl \Fcfgi{t_2}\,H\,Q }
\label{Appintro}
\nextrow{3}
% & \phantom{\equiv{}}&
%\X{ where } P \equiv \Dpar{\Tfor{xH'Q'}\; \Fcfgi{t_1}\,H'\,Q' \Sc\impl \Jiappreturnsof{f}{x}{H'}{Q'}} \nonumber
 \multicolumn{3}{l}{ \quad \X{where } P \equiv \Dpar{\Tfor{xH'H''Q'}\; H' \Himpl \HPcreds{1}\!\Hstar H'' \Sc\land  \Fcfgi{t_1}\,H''\,Q' \Sc\impl \Jiappreturnsof{f}{x}{H'}{Q'} } }
\end{array}$$

\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{definition}[Specification of primitive operations on references]
%
$$\begin{array}{lll@{\,}l@{\,}l@{}l}
\Tfor{v} & \Jiappreturnsof{\Lref}{v}{\HPempty}{ (\HPabs{l}\; \HPsingle{l}{v} ) } \nextrow{2}
\Tfor{lv} & \Jiappreturnsof{\Lget}{l}{ \HPSingle{l}{v} }{ (\HPabs{x} \HPprop{x = v} \Sc\Hstar \HPSingle{l}{v} ) } \nextrow{2}
\Tfor{lvv'} & \Jiappreturnsof{\Lset}{\,(l,v)}{ \HPSingle{l}{v'} }{ (\HPabsunit\; \HPsingle{l}{v} ) }
\end{array}$$
\end{definition}
%%------------------------------------------



%%------------------------------------------
\begin{definition}[Soundness of a triple] 
Let $\Ut$ be a well-typed term of type $T$, let $H$ be a precondition
of type $\Thprop$ and $Q$ be a postcondition of type $\Freflect{T}\Tto\Thprop$.
We define:
%
$$\Jsoundtripleof{\Ut}{H}{Q} \Scs\equiv \Tfor{h_ih_k}\, 
\begin{ands}
H\;h_i  \\
\Hdisof{h_i}{h_k}
\end{ands}
\!\!\!\!\impl \Texi{n\Uv\,h_f\,h_g}
\begin{ands}
\Jtredbignconfigof{\Ut}{\Huniof{h_i}{h_k}}{n}{\Uv}{\Hunithree{h_f}{h_k}{h_g}} \vspace{2pt}\\
Q\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f \\
\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g} \\
\Hdisthree{h_f}{h_k}{h_g}
\end{ands}$$
\end{definition}
%%------------------------------------------

Remark: above, $h_i$ and $h_f$ correspond to the initial and final heap which are
involved in the evaluation of the term $\Ut$, while the heap $h_k$ corresponds
to the pieces of heap that have been framed out, and $h_g$ corresponds
to the piece of heap being discarded during the reasoning on~$\Ut$.
%
Thereafter, we usually leave the last conclusion
implicit, following the convention that we  
allow ourselves to write $\Huniof{h}{h'}$ only 
when the assumption $\Hdisof{h}{h'}$ is available.

To conduct the proof, we need to assign concrete definitions for 
the types $\Tloc$ and $\Tfunc$, and for the predicate
$\Jiappreturns$.

%%------------------------------------------
\begin{definition}[Interpretation of $\Tloc$ and $\Tfunc$]
We assign the following interpretation to the abstract type $\Tloc$ and $\Tfunc$.
%(Only needed for the proofs.)
$$\begin{array}{l@{\quad}l@{\quad}l}
\Tloc &\equiv & \mathbb{N} \vspace{2pt}\\
\Tfunc &\equiv & \GS_{\Uv} \JIswtcof{\Uv}  \vspace{2pt}\\
\end{array}$$ % \Scs\land \Jtyping{v}{T}
\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{definition}[Soundness of a formula] 
Let $\Ut$ be a well-typed term of type $T$, and
let $\R{F}$ be a formula of type \Q{\Thprop\Tto(\Freflect{T}\Tto\Thprop)\Tto\Tprop}.
We let:
%$\Tcompl{\Freflect{T}}$, i.e., of type
$$\Jsoundof{\Ut}{\R{F}} \Scs\equiv 
\Tfor{H\,Q}\; \R{F}\,H\,Q \Sc\impl \Jsoundtripleof{\Ut}{H}{Q}$$
\end{definition}
%%------------------------------------------

%%------------------------------------------
\begin{definition}[Interpretation of $\Jiappreturns$]
We assign the following concrete definition to the abstract predicate $\Jiappreturns$. %(Only needed for the proofs.)
$$\Jiappreturnsof{F}{V}{H}{Q} \Scs\equiv
\Texi{fv}\; F = \Fdecode{f} \Sc\land V = \Fdecode{v} \Sc\land \Jsoundtripleof{\LApp{f}{v}}{H}{Q}$$
\end{definition}
%%------------------------------------------

In the definition above, note that $f$ and $v$ are uniquely defined, 
because $\Fdecode{\cdot}$ is bijective. Thus,
\Q{\Jiappreturnsof{\Fdecode{f}}{\Fdecode{v}}{H}{Q}} implies 
\Q{\Jsoundtripleof{\LApp{f}{v}}{H}{Q}}.

We are ready to conduct the proofs. We being with substitution lemmas
for translation of values and for characteristic formulae.
We then establish the soundness of the specification of primitive
operations, the soundness of the predicate $\Jlocal$, and then the
soundness of the core of characteristic formulae, giving first a
general form for the induction, and then the soundness theorem for
complete executions as it appears in the paper.

%%------------------------------------------
\begin{lemma}[Substitution lemmas] 
Let $\Uv'$ (resp. $\Ut$) be a well-typed value (resp. term) with a free variable $x$, and let $\Uv$ be a typed value with the same type as $x$. Then,
$$\Fdecode{\,\Osubst{x}{\Uv}{\Uv'}\,} \Sc{=}
\Osubst{x}{ \Fdecode{\Uv} }{ \Fdecode{ \Uv' }}  
\SQ{\X{and}}
\Fcfgi{\,\Osubst{x}{\Uv}{\Ut}\,} \Sc{=} \Osubst{x}{\Fdecode{\Uv}}{\Fcfgi{\Ut}} $$
\end{lemma}
\begin{proof}
Straightforward by induction. \qed
\end{proof}
%%------------------------------------------

%%------------------------------------------
\begin{lemma}[Soundness of the specification of primitive operations]
The specifications given for primitive operations on references
are sound with respect to the interpretation of $\Jiappreturns$.
\end{lemma}
\begin{proof}
We assume that each primitive operation is reflected by an abstract
definition of type $\Tfunc$ in the logic. We write, for example,
$\Lref = \Fdecode{\Lref}$, where the left-hand side is the Coq value $\Lref$
whereas the right-hand side is the translation of the Caml value $\Lref$.

\Pcase{\Lref}. The specification is 
$\Jiappreturnsof{\Lref}{V}{\HPempty}{ (\HPabs{l}\, \HPsingle{l}{V} ) }$.
Let $v$ be such that $V = \Fdecode{v}$.
Our goal is to show 
$\Jsoundtripleof{\LApp{\Lref}{v}}{\HPempty}{(\HPabs{l}\, \HPsingle{l}{V} )}$.
Consider $h_i$ and $h_k$ such that $\HDisof{h_i}{h_k}$ and $\HPempty\,h_i$ hold.
The goal is to exhibit $n$, $l$, $h_f$ and $h_g$ such that
$\Jtredbignconfigof{\LRefof{\Uv}}{\Huniof{h_i}{h_k}}{n}{l}{\Hunithree{h_f}{h_k}{h_g}} $
and $(\HPabs{l}\, \HPsingle{l}{V} )\;\Fdecodetyp{l}{\Freflect{T}}\;h_f$
and $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$.

By definition of $\HPempty$, we have $h_i = \OSempty$.
We instantiate $n$ as $0$,
$l$ as $1+\F{max}({\OMdom{\Um}})$, where $\Um$ is the state component
associated with $h_k$,
and $h_f$ as $(\OMone{l}{\Uv}, 0)$,
and $h_g$ as $\OMempty$.

The evaluation rule gives us
${ \Jtredbignconfigof{ \LRefof{\Uv} }{ \Um }{ 0 }{ l }{ \OMadd{\Um}{l}{v} } }$
which can be rewritten as
${ \Jtredbignconfigof{ \LRefof{\Uv} }{ h_k }{ 0 }{ l }{ \HUnithree{h_f}{h_k}{h_g} } }$.
The conclusion $(\HPabs{l}\, \HPsingle{l}{V} )\;\Fdecodetyp{l}{\Freflect{T}}\;h_f$
simplifies to $(\HPsingle{l}{\Fdecode{v}})\;h_f$, which holds by 
definition of $h_f$ as $(\OMone{l}{\Uv}, 0)$.
The conclusion $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$ simplifies
to $\Fcreds{\OSempty} = 0 + 0 + \Fcreds{\OSempty}$, which is trivially true.


\Pcase{\Lget}. 
The specification is 
$\Jiappreturnsof{\Lget}{l}{ \HPSingle{l}{V} }{ (\HPabs{X} \HPprop{X = V} \Sc\Hstar \HPSingle{l}{V} ) } $
Let $v$ be such that $V = \Fdecode{v}$.
Our goal is to show 
$\Jsoundtripleof{\LApp{\Lget}{l}}{ \HPSingle{l}{V} }{(\HPabs{X} \HPprop{X = V} \Sc\Hstar \HPSingle{l}{V} )}$.
Consider $h_i$ and $h_k$ such that $\HDisof{h_i}{h_k}$ and $\HPSingle{l}{V}\,h_i$ hold.
The goal is to exhibit $n$, $v'$, $h_f$ and $h_g$ such that
$\Jtredbignconfigof{\LGetof{l}}{\Huniof{h_i}{h_k}}{n}{v'}{\Hunithree{h_f}{h_k}{h_g}} $
and $(\HPabs{X} \HPprop{X = V} \Sc\Hstar \HPSingle{l}{V} )\;\Fdecode{v'}\;h_f$
and $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$.

By definition of $\HPsingle{l}{V}$, we have $h_i = (\OMone{l}{\Uv}, 0)$.
We instantiate $n$ as $0$,
$v'$ as $v$,
and $h_f$ as $h_i$,
and $h_g$ as $\OMempty$.

Let $\Um$ be the state component of $\Huniof{h_i}{h_k}$.
We have $l \in \OMdom{\Um}$ and $\Uv = \OMget{\Um}{l}$.
The evaluation rule thus gives us
$\Jtredbignconfigof{ \LGetof{l} }{ \Um }{ 0 }{ \Uv }{ \Um }$,
which can be rewritten as
$\Jtredbignconfigof{ \LGetof{l} }{ \Huniof{h_i}{h_k} }{ 0 }{ \Uv }{ \HUnithree{h_f}{h_k}{h_g} }$.
The conclusion $(\HPabs{X} \HPprop{X = V} \Sc\Hstar \HPSingle{l}{V} )\;\Fdecode{v'}\;h_f$
simplifies to $\Fdecode{v'} = V$, which holds because $v' = v$ and $V = \Fdecode{v}$, and $(\HPsingle{l}{V})\;h_f$, which holds because $h_f = h_i = (\OMone{l}{\Uv}, 0)$.
The conclusion $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$ simplifies
to $0 = 0 + 0 + \Fcreds{\OSempty}$, which is trivially true.

\Pcase{\Lset}.  
The specification is 
$\Jiappreturnsof{\Lset}{\,(l,V)}{ \HPSingle{l}{V'} }{ (\HPabsunit\; \HPsingle{l}{V} ) }$.
Let $v$ be such that $V = \Fdecode{v}$,
and let $v'$ be such that $V' = \Fdecode{v'}$.
Our goal is to show 
$\Jsoundtripleof{\LApp{\Lset}{(l,V)}}{ \HPSingle{l}{V'} }{(\HPabsunit\; \HPsingle{l}{V} )}$.
Consider $h_i$ and $h_k$ such that $\HDisof{h_i}{h_k}$ and $\HPSingle{l}{V'}\,h_i$ hold.
The goal is to exhibit $n$, $h_f$ and $h_g$ such that
$\Jtredbignconfigof{(\Lset\,({l},{\Uv}))}{\Huniof{h_i}{h_k}}{n}{\Ltt}{\Hunithree{h_f}{h_k}{h_g}} $
and $(\HPabsunit\; \HPsingle{l}{V} )\;\Fdecode{\Ltt}\;h_f$
and $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$.

By definition of $\HPsingle{l}{V'}$, we have $h_i = (\OMone{l}{\Uv'}, 0)$.
We instantiate $n$ as $0$,
and $h_f$ as $(\OMone{l}{\Uv}, 0)$,
and $h_g$ as $\OMempty$.

Let $\Um$ be the state component of $\Huniof{h_i}{h_k}$.
We have $l \in \OMdom{\Um}$.
The evaluation rule thus gives us
$\Jtredbignconfigof{ (\Lset\,({l},{\Uv})) }{ \Um }{ 0 }{ \Ltt }{ (\OMset{\Um}{l}{\Uv}) }$,
which can be rewritten as
$\Jtredbignconfigof{ (\Lset\,({l},{\Uv})) }{ \Huniof{h_i}{h_k}}{ 0 }{ \Ltt }{ \HUnithree{h_f}{h_k}{h_g} }$
The conclusion $(\HPabsunit\; \HPsingle{l}{V} )\;\Fdecode{\Ltt}\;h_f$
simplifies to $(\HPsingle{l}{V})\;h_f$, which holds because $h_f = (\OMone{l}{\Uv}, 0)$.
The conclusion $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$ simplifies
to $0 = 0 + 0 + \Fcreds{\OSempty}$, which is trivially true.

\end{proof}
%%------------------------------------------

In the soundness proof, we need to eliminate the predicate $\Jlocal$ which
appears at the head of every characteristic formula. We do so using the
following lemma.

%%------------------------------------------
\begin{lemma}[Elimination of $\Jlocal$]
Let $\Ut$ be a well-typed term of type $T$, and let $\R{F}$ be a formula
of type \Q{\Thprop\Tto(\Freflect{T}\Tto\Thprop)\Tto\Tprop}. Then,
$$\Jsoundof{\Ut}{\R{F}} \Scs\impl \Jsoundof{\Ut}{\JLocalof{\R{F}}}$$
\end{lemma}
\begin{proof}
Assume \W{\Jlocal\,\R{F}\,H\,Q} and $H\;h_i$ and $\Hdisof{h_i}{h_k}$.
The goal is to exhibit $n$, $\Uv$, $h_f$ and $h_g$ such that
$\Jtredbignconfigof{\Ut}{\Huniof{h_i}{h_k}}{n}{\Uv}{\Hunithree{h_f}{h_k}{h_g}}$
and $Q\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f$
and $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$
and $\Hdisthree{h_f}{h_k}{h_g}$ hold.

We unfold the definition of $\Jlocal$ in the assumption \W{\Jlocal\,\R{F}\,H\,Q},
and we apply this assumption to the heap $h_i$, which satisfies $H$.
We obtain predicates $H_i$, $H_k$, $H_g$ and $Q_f$ such that
$\HStarof{H_i}{H_k}\,h_i$ and
\W{\R{F}\,H_i\,Q_f} and \W{\Qimplof{\Qstarof{Q_f}{H_k}}{\Qstarof{Q}{H_g}}} hold.

By definition of the separating conjunction applied to $\HStarof{H_i}{H_k}\,h_i$, 
the heap $h_i$ can be decomposed into two 
heaps $h_i'$ and $h_k'$ such that $H_i\,h_i'$ and
$H_k\,h_k'$ and $\Hdisof{h_i'}{h_k'}$ and $h_i = \Huniof{h_i'}{h_k'}$ hold.

We now apply the hypothesis \W{\Jsoundof{\Ut}{\R{F}}} to the heaps $h_i'$ and
$\HUniof{h_k}{h_k'}$. We can check that \W{\R{F}\,H_i\,Q_f} and
$H_i\,h_i'$ and $\Hdisof{h_i'}{\HUniof{h_k}{h_k'}}$ hold.
We obtain the existence of $n$, $\Uv$, $h_f'$ and $h_g'$ such that
$\Jtredbignconfigof{\Ut}{\Hunithree{h_i'}{h_k}{h_k'}}{n}{\Uv}{\Hunifour{h_f'}{h_k}{h_k'}{h_g'}}$
and $Q_f\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f'$
and $\Fcreds{h_i'} = n + \Fcreds{h_f'} + \Fcreds{h_g'}$
and $\Hdisthree{h_f'}{\HUniof{h_k}{h_k'}}{h_g'}$ hold.

From  $Q_f\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f'$ and $H_k\,h_k'$, by exploiting 
the definition of the star operator, we deduce
$\HStarof{Q_f\,\Fdecodetyp{\Uv}{\Freflect{T}}}{H_k}\,\HUniof{h_f'}{h_k'}$.
Exploiting the fact \W{\Qimplof{\Qstarof{Q_f}{H_k}}{\Qstarof{Q}{H_g}}} on the later
gives $\HStarof{Q\,\Fdecodetyp{\Uv}{\Freflect{T}}}{H_g}\,\HUniof{h_f'}{h_k'}$,
By definition of the star, there exist two heaps $h_f$ and $h_g''$
such that \W{\Huniof{h_f'}{h_k'} = \Huniof{h_f}{h_g''}} and $\Hdisof{h_f}{h_g''}$ and
\W{Q\;\Fdecodetyp{\Uv}{\Freflect{T}}\,h_f} and \W{H_g\,h_g''} hold.

To conclude, we instantiate $n$ as $n$, $v$ as $v$, $h_f$ as $h_f$
and $h_g$ as $\Huniof{h_g'}{h_g''}$. We check that
$\Jtredbignconfigof{\Ut}{\Huniof{h_i}{h_k}}{n}{\Uv}{\Hunithree{h_f}{h_k}{h_g}}$
and $Q\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f$
and $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$
and $\Hdisthree{h_f}{h_k}{h_g}$ hold, by exploiting
the equalities
and $h_i = \Huniof{h_i'}{h_k'}$ and \W{\Hunithree{h_f'}{h_k'}{h_g'} = \Huniof{h_f}{h_g}}
and $\Fcreds{h_i'} = n + \Fcreds{h_f'} + \Fcreds{h_g'}$.
In particular, $\Hdisof{h_f}{h_k}$ follows from \W{h_f \OSinc \Huniof{h_f'}{h_k'}};
indeed, we show $\Hdisof{h_f'}{h_k}$ using $\Hdisof{h_f'}{\HUniof{h_k}{h_k'}}$
and we show $\Hdisof{h_k'}{h_k}$ using  $\Hdisof{h_i}{h_k}$  and $h_i = \Huniof{h_i'}{h_k'}$.
\qed
\end{proof}
%%------------------------------------------


%%------------------------------------------
\begin{lemma}[Soundness of characteristic formulae with time credits]
Let $\Ut$ be a well-typed term of type $T$. Then,
\Q{\Jsoundof{\Ut}{\Fcfgi{\Ut}}} holds.
\end{lemma}
\begin{proof}

The proof goes by induction on the size of the term $\Ut$, under
the convention that all values occuring in $\Ut$ have size one.

Given a typed term $\Ut$, its characteristic formula takes the form
$\Jlocalof{\R{F}}$. Thus, the goal is of the form
\Q{\Jsoundof{\Ut}{\JLocalof{\R{F}}}}.
By exploiting the previous lemma, it suffices to
establish \Q{\Jsoundof{\Ut}{\R{F}}}, for the formula $\R{F}$
that corresponds to the term $\Ut$. In each cases, the assumptions is
\W{\R{F}\,H\,Q} and the goal is to establish $\Jsoundtripleof{\Ut}{H}{Q}$.

Concretely, we assume $H\;h_i$ and $\Hdisof{h_i}{h_k}$ for some
$h_i$ and $h_k$, and the goal
is to exhibit $n$, $\Uv$, $h_f$ and $h_g$ such that
$\Jtredbignconfigof{\Ut}{\Huniof{h_i}{h_k}}{n}{\Uv}{\Hunithree{h_f}{h_k}{h_g}}$
and $Q\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f$
and $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$
and $\Hdisthree{h_f}{h_k}{h_g}$ hold.

%-----
\Pcase \W{\Ut = \Uv}. The assumption \W{\R{F}\,H\,Q}
is $\Himplof{H}{Q\,\Fdecodetyp{\Uv}{T}}$.
Instantiate $n$ as $0$, $\Uv$ as $\Uv$, $h_f$ as $h_i$ and $h_g$ as $\OSempty$.
We check
$\Jtredbignconfigof{\Uv}{\Huniof{h_i}{h_k}}{0}{\Uv}{\Huniof{h_i}{h_k}}$
and  $\Fcreds{h_i} = 0 + \Fcreds{h_i} + \Fcreds{\OSempty}$.
To prove $Q\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_i$, we exploit
$\Himplof{H}{Q\,\Fdecodetyp{\Uv}{T}}$ on the assumption \Q{H\,h_i}.

%-----
\Pcase \W{\Ut = \LApp{\Uf}{\Uv'}}. 
The assumption \W{\R{F}\,H\,Q} is 
$\Jiappreturnsof{\Fdecode{\Uf}}{\Fdecodetyp{\Uv'}{\Freflect{T'}}}{H}{Q}$.
By definition of $\Jiappreturns$, this is equivalent to
$\Jsoundtripleof{\LApp{f}{v'}}{H}{Q}$, which is exactly the goal that
we have to prove.

%-----
\Pcase \W{\Ut = \LLetof{x}{\Ut_1}{\Ut_2}}. 
The assumption \W{\R{F}\,H\,Q} gives us
$ \Fcfgi{\Ut_1}\,H\,Q'$ and  $\Tfor{X}\; \Fcfgi{\Ut_2}\,(Q'\,X)\,Q $,
for some $Q'$.

We apply the induction hypothesis to $\Ut_1$
and $H\;h_i$ and $\Hdisof{h_i}{h_k}$. We obtain the existence of
$n_1$, $\Uv_1$, $h_f'$ and $h_g'$ such that
$\Jtredbignconfigof{\Ut_1}{\Huniof{h_i}{h_k}}{n}{\Uv_1}{\Hunithree{h_f'}{h_k}{h_g'}}$
and $Q\;\Fdecodetyp{\Uv_1}{\Freflect{T_1}}\;h_f'$
and $\Fcreds{h_i} = n_1 + \Fcreds{h_f'} + \Fcreds{h_g'}$.

By instantiating $X$ as $\Fdecode{\Uv_1}$, we obtain
$\Osubst{x}{\Fdecodetyp{v_1}{\Freflect{T_1}}}{\Fcfgi{\Ut_2}}\,(Q'\,\Fdecodetyp{v_1}{\Freflect{T_1}})\,Q$.
By a substitution lemma for characteristic formulae (trivial, thus not developed here), the latter
is equivalent to 
$\Fcfgi{\Osubst{x}{v_1}{\Ut_2}}\,(Q'\,\Fdecodetyp{v_1}{\Freflect{T_1}})\,Q$.

We apply the induction hypothesis to $\Osubst{x}{v_1}{\Ut_2}$, which has
the same size as $\Ut_2$ (recall that values have size one), 
and $H\;h_f'$ and $\Hdisof{h_f'}{\HUniof{h_k}{h_g'}}$. We obtain the existence of 
$n_2$, $\Uv$, $h_f$ and $h_g''$ such that
$\Jtredbignconfigof{\OSubst{x}{v_1}{\Ut_2}}{\Hunithree{h_f'}{h_k}{h_g'}}{n_2}{\Uv}{\Hunifour{h_f}{h_k}{h_g'}{h_g''}}$
and $Q\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f$
and $\Fcreds{h_f'} = n_2 + \Fcreds{h_f} + \Fcreds{h_g''}$.

We instantiate $n$ as $n_1 + n_2$ and $v$ as $v$ and $h_f$ as $h_f$ and $h_g$ as $\Huniof{h_g'}{h_g''}$.
We check 
$\Jtredbignconfigof{\LLetof{x}{\Ut_1}{\Ut_2}}{\Huniof{h_i}{h_k}}{n_1+n_2}{\Uv}{\Hunithree{h_f}{h_k}{h_g}}$
and $Q\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f$
and $\Fcreds{h_i} = n_1 + n_2 + \Fcreds{h_f} + \Fcreds{h_g}$.

%-----
\Pcase \W{\Ut = \LFunof{f}{x}{\Ut_1}{\Ut_2}}. 
The assumption  \W{\R{F}\,H\,Q} gives us
$ \Tfor{f}\, P \impl \Fcfgi{t_2}\,H\,Q$,
where $P \equiv \Dpar{\Tfor{xH'H''Q'}\; H' \Himpl \HPcreds{1}\!\Hstar H'' \Sc\land  \Fcfgi{t_1}\,H''\,Q' \Sc\impl \Jiappreturnsof{f}{x}{H'}{Q'} }$.
In the assumption $ \Tfor{f}\, P \impl \Fcfgi{t_2}\,H\,Q$,
we specialize $f$ to \W{\Fdecodetyp{\Lfix{f}{x}{\Ut_1}}{\Tfunc}}. 
If we prove $\Osubst{f}{\Fdecodetyp{\Lfix{f}{x}{\Ut_1}}{\Tfunc}}{\R{H}}$,
then we obtain $\Osubst{f}{\Fdecode{\Lfix{f}{x}{\Ut_1}})}{\Fcfgi{\Ut_2}}\,H\,Q$,
which by the substitution lemma gives
$\Fcfgi{\Osubst{f}{\Lfix{f}{x}{\Ut_1}}{\Ut_2}}\,H\,Q$.
From there, we can easily conclude, using the induction hypothesis.

It remains to establish $\Osubst{f}{\Fdecodetyp{\Lfix{f}{x}{\Ut_1}}{\Tfunc}}{\R{H}}$.
Consider the parameters $VH'H''Q'$ given, and assume 
$H' \Himpl \HPcreds{1}\!\Hstar H''$ and 
$\Osubst{x}{V}{ \Osubst{f}{\Fdecode{\Lfix{f}{x}{\Ut_1}} } }{\Fcfgi{t_1}}\,H''\,Q'$ to hold.
Our goal is to prove $\Jiappreturnsof{\Fdecode{\Lfix{f}{x}{\Ut_1}}}{V}{H'}{Q'} $.
The logical value $V$ has a type of the form $\Freflect{T}$, thus there exists
a value $\Uv_1$ such that $V = \Fdecode{\Uv_1}$. 
Thus, the goal is to prove $\Jiappreturnsof{\Fdecode{\Lfix{f}{x}{\Ut_1}}}{\Fdecode{\Uv_1}}{H'}{Q'} $,
which by definition of $\Jiappreturns$ is the same as 
$\Jsoundtripleof{\LApp{\LFix{f}{x}{\Ut_1}}{\Uv_1}}{H'}{Q'}$.

To prove the latter,
consider $h_i$ and $h_k$ such that $\HDisof{h_i}{h_k}$ and $H'\,h_i$ hold.
The goal is to exhibit $n$, $v$, $h_f$ and $h_g$ such that
$\Jtredbignconfigof{\LApp{\LFix{f}{x}{\Ut_1}}{v}}{\Huniof{h_i}{h_k}}{n}{v}{\Hunithree{h_f}{h_k}{h_g}} $
and $Q'\;\Fdecode{v}\;h_f$
and $\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$.

From $H'\,h_i$ and $H' \Himpl \HPcreds{1}\!\Hstar H''$,
we deduce the existence of $h_i'$ such that $h_i = \Huniof{(\OSempty,1)}{h_i'}$
and $H''\,h_i'$. In particular, we have $\Fcreds{h_i} = 1 + \Fcreds{h_i'}$.

Let $t'$ denote the term $\Osubst{f}{\Lfix{f}{x}{\Ut_1}}{\Osubst{x}{\Uv_1}{t_1}}$.
By the substitution lemma, and using $V = \Fdecode{\Uv_1}$,
the assumption $\Osubst{x}{V}{ \Osubst{f}{ \Fdecode{\Lfix{f}{x}{\Ut_1}} }{ \Fcfgi{t_1} } }\,H''\,Q'$,  is equivalent to $\Fcfgi{t'}\,H''\,Q'$.
By induction hypothesis applied to $t'$, which has size less than $t$, 
we know that \Q{\Jsoundof{t'}{\Fcfgi{t'}}} holds, so we can exploit it
on $\Fcfgi{t'}\,H''\,Q'$ and $H''\,h_i'$ and $\HDisof{h_i'}{h_k}$.
We obtain the existence of $n'$ and $v$ and $h_f$ and $h_g$ such that
$\Jtredbignconfigof{\Ut'}{\Huniof{h_i'}{h_k}}{n}{\Uv}{\Hunithree{h_f}{h_k}{h_g}}$
and $Q'\;\Fdecodetyp{\Uv}{\Freflect{T}}\;h_f$
and $\Fcreds{h_i'} = n' + \Fcreds{h_f} + \Fcreds{h_g}$.

We instantiate $n$ as $n'+1$, $v$ as $v$, $h_f$ as $h_f$ and $h_g$ as $h_g$.
The first conclusion
$\Jtredbignconfigof{\LApp{\LFix{f}{x}{\Ut_1}}{v}}{\Huniof{h_i}{h_k}}{n'+1}{v}{\Hunithree{h_f}{h_k}{h_g}}$.
follows from 
$\Jtredbignconfigof{\Ut'}{\Huniof{h_i'}{h_k}}{n}{\Uv}{\Hunithree{h_f}{h_k}{h_g}}$.
The second conclusion $Q'\;\Fdecode{v}\;h_f$ comes from above.
The third conclusion $\Fcreds{h_i} = n'+1 + \Fcreds{h_f} + \Fcreds{h_g}$
follows from  $\Fcreds{h_i} = 1 + \Fcreds{h_i'}$
and $\Fcreds{h_i'} = n' + \Fcreds{h_f} + \Fcreds{h_g}$.

\qed
\end{proof}
%%------------------------------------------




%%------------------------------------------
\begin{theorem}[Soundness for complete executions]
$$\Tfor{m c}\;\,
\begin{ands}
\Fcfgi{t}\,H\,Q  \nextrow{2}
H\,(m, c)
\end{ands}
\Scs\impl \Texi{n v m' m'' c' c''} \;
\begin{ands}
\Jredbignconfigof{ t }{ m }{n}{ v }{ \OMdisuniof{m'}{m''} }
\nextrow{1} 
\Hdisof{m'}{m''}
\nextrow{1}
Q\,v\,(m', c')
\nextrow{1}
c = n + c' + c'' % fp: j'ai remplacé \geq par = en ajoutant c''
\end{ands}$$
\end{theorem}
\begin{proof}
Apply the above lemma with $h_i = (m, c)$ and $h_k = \OSempty$.
Then decompose $h_f$ as $(m',c')$
and $h_g$ as $(m'',c'')$, and simplify
$\Fcreds{h_i} = n + \Fcreds{h_f} + \Fcreds{h_g}$
into $c = n + c' + c''$.
\qed
\end{proof}
%%------------------------------------------











%*****************************************************************************

\bibliographystyle{splncs03}
\bibliography{english,extra}

\end{document}
