# Set TEXINPUTS.

export TEXINPUTS=.:

# Set DVICOPY.
# Using dvicopy provides support for virtual fonts, which advi lacks.

mvdvicopy () { dvicopy "$1" "$2"; }
DVICOPY=mvdvicopy

