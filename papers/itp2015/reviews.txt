

Dear Arthur,

I am pleased to inform you that your paper 

   Paper 10
   Machine-Checked Verification of the Correctness and Amortized Complexity of an Efficient Union-Find Implementation

has been accepted for inclusion in the proceedings of and 
presentation at ITP 2015. Congratulations!

We attach the reviews below. The program committee members 
and external reviewers have put in a significant effort in reviewing 
submissions. Please address their issues when revising your paper.

Please note that we need to receive the camera-ready
version by 5 June the latest, and also send us the copyright
form as soon as possible (detailed instructions for this
will follow soon).

Please also note that we expect that at least one author will present 
the paper at the conference in Nanjing (details about conference 
registration will also follow soon).

Best wishes,
Xingyuan Zhang and Christian Urban (ITP 2015 chairs)


----------------------- REVIEW 1 ---------------------
PAPER: 10
TITLE: Machine-Checked Verification of the Correctness and Amortized Complexity of an Efficient Union-Find Implementation
AUTHORS: Arthur Charguéraud and François Pottier

OVERALL EVALUATION: 2 (accept)
REVIEWER'S CONFIDENCE: 4 (high)

----------- REVIEW -----------
This is a very nice paper, and I think it certainly deserves to appear at ITP this year.

In summary, the paper describes how a somewhat simplified but still realistic implementation of the Union-Find data structure can be verified.  More, the verification is of both the correctness of the operations on the structure and of the operations’ complexity.  Earlier work (cited here) did the former, but for a version of the code that assumed functional arrays, rather than with a heap and references as done here.  So, just in this respect, the work is an advance.  In addition, the work verifies a non-trivial amortized cost complexity analysis.  This clearly demonstrates a significant contribution.

I found the paper very easy to read, with a great level of detail in terms of presenting definitions and theorems from the Coq mechanization.

Though the authors admit that the approach to amortized complexity using separation logic (with cost credits available as part of the “heap”) is not novel to them, it does seem as if they are the first to do anything mechanically in this vein.  The paper explains how this machinery works extremely well.

Trivia:

It seems that it would be fair to say that the semantics is a “shallow embedding” of the language (an OCaml subset(?)), mediated via an external tool which translates from the OCaml syntax to the semantic domain (h.o.l. predicates).

p5: the separation logic triple that is the semantics of ⟦t⟧ uses braces, which suggests partial correctness to my mind.  However, the prose is explicit that the notion here is actually total correctness.

p8: In “The parameter $N$ is an upper bound on the cardinal of $D$” the word used should be “cardinality” rather than “cardinal”.


----------------------- REVIEW 2 ---------------------
PAPER: 10
TITLE: Machine-Checked Verification of the Correctness and Amortized Complexity of an Efficient Union-Find Implementation
AUTHORS: Arthur Charguéraud and François Pottier

OVERALL EVALUATION: 1 (weak accept)
REVIEWER'S CONFIDENCE: 4 (high)

----------- REVIEW -----------
Summary:

In 'Machine-Checked Verification of the Correctness and Amortized
Complexity of an Efficient Union-Find Implementation' the authors
present a Coq verification of an OCaml Union-Find implementation's
correctness and non-trivial amortized analysis, which the authors
claim is the first such that has a fully machine-checked proof. After
describing the Union-Find interface the authors explain their approach
to combining time-credits with characteristic formulas in a separation
logic. To verify the implementation the authors first give a
mathematical analysis of Union-Find in Coq. Hereafter they use their
CFML tool to transform an OCaml implementation of Union-Find into
characteristic formulas and prove this implementation satisfies the
specification and corresponds to the mathematical analysis of
Union-Find from which the amortized cost bound follows.


Judgment:

Due to the forthcoming explanation I recommend the 'Machine-Checked
Verification of the Correctness and Amortized Complexity of an
Efficient Union-Find Implementation' by Arthur Charguéraud and
François Pottier for weak accept.

The first contribution is, according to the authors, the first
practical verification framework with support for heap allocation,
mutation and amortized complexity analysis. It is not the first to
have the first two features and utilizing separation logic with a
refined heap seems straight forward, since the new refined heap is
also monoidal in its structure. Therefore, it is not clear how big the
contribution is, especially when this is already known to work outside
Coq [12,1,15]. However, in the setting of this conference I regard
this as a significant contribution.

The second contribution is a reformulation of a mathematical known
fact in Coq, which is necessary in order to prove the implementation
correct with respect to the specification including the amortized
analysis. This is perhaps a minor contribution in the setting of this
conference. However, this maybe gives a recipe of how to prove
amortized bounds on existing algorithms, because the correspondence
between the proof in [18] and the Coq-stated version are very similar.

The third contribution is the actual verification of a Union-Find
implementation in OCaml, which I find to be a very nice example for
interactive theorem proving, since it is a basic building block of
unification. With respect to this proof I have a few worries:

I assume you do not handle any concurrency features of OCaml, but if
you do support concurrency, you should be aware that Okasaki [I]
shows, that the potential method for amortized analysis only works for
lazy implementations with memoization. Such an assumption is not made
so you could either implement laziness with thunks or just add this
requirement, but I think this would be required. As a side note, you
could give a better description of the source language you actually
support.

The authors argue that one should just count function calls as the
unit of cost. In the transformation a call to an abstract pay function
is prepended to every function call. Since this is itself a function
call, the call having no cost feels problematic. I assume the
transformation should be a semantics preserving transformation with
respect to the heap, and one could argue that no longer is the
case. It would be cleaner if the authors did what they propose and
altered the definition of the equation (4) to impose the consumption
of one credit.

The authors require law of excluded middle for the following:

If R w = R x Or R w = R y then z else R w 

R is just a relation mapping an element to its root (or it is a
root) so the equals seems decidable in this setting, so could the
proof be done without this axiom?

Also, the Coq-representation of any OCaml program is not very readable
therefore it is very difficult to verify the transformation. Since you
already give some kind of semantics to (a subset) OCaml, it seems like
you could have written the transformation in Coq, proved it correct
and then extracted an OCaml program. I would have more trust in this
approach than your current approach.

Comments:

In the article the authors present Theorem link_spec, but I could not
find it in Union-Find_proof.v. There is a Lemma with the same name but
it is not stated in the exact same way. It would be nice if the two
things were aligned.

It would also be nice if the proofs were done in version 8.4 since 8.5
is currently in beta.

References:

[I] OKASAKI, Chris. Purely functional data structures. Cambridge
University Press, 1999.


----------------------- REVIEW 3 ---------------------
PAPER: 10
TITLE: Machine-Checked Verification of the Correctness and Amortized Complexity of an Efficient Union-Find Implementation
AUTHORS: Arthur Charguéraud and François Pottier

OVERALL EVALUATION: 2 (accept)
REVIEWER'S CONFIDENCE: 4 (high)

----------- REVIEW -----------
The paper introduces a technique to simultaneously prove in Coq the functional correctness and the bound on the use of resources of an OCaml program. The kind of non functional analysis performed is amortized cost analysis. The authors apply the technique to a Union-Find Implementation based on path compression and union-by-rank.

Heap cells and cost units are just two instances of a more abstract concept --- not spelled out --- of atomic elements of a discrete set that can be collected into unions that can be later split into non overlapping sets. Separation logic, the tool the paper is based on, has traditionally been presented as a logic to reason on properties of heap and programs using the heap, but it is actually more general and it allows to reason on the non spelled out notion. The technique introduced in the paper is just that: the "divisible monoid" representing collection of heap cells is replaced by its product with a cost monoid, and the definitions of the Separation Logic connectives in Coq is updated accordingly. Even if the idea is very simple and quite obvious, the authors claim to be the first one to apply it this way, which seems true to our knowledge with maybe the
exception of some experiments done by Francois Bobot in the CerCo's project (references
below).


The above idea is combined with other tools and methodology that are already well established in the literature, like: 1) instrumenting each function call and each body
of a cycle with an instruction that pays the cost of the function body/cycle body
(used again in CerCo); 2) proving the correctness of an (imperative) implementation by
establishing a simulation between the actual code and an abstract/mathematical 
description of the implementation; 3) embedding Separation Logic into a more expressive
logic like the one of Coq.


While the previous ideas are not novel, it is nice to see them combined and put to work in/with the CMFL library, and applied to a non trivial example. Another strong point of
the paper is that it is very well written, self contained, and that it does a good job of explaining both the specification and what the trusted code base is.


The paper also has a few weak points that could be addressed in the camera ready:
1. the authors took a number of decisions in the design space, but fail to tell the reader why a certain solution was picked among the alternatives. Even just telling that it was the first one tried and that it worked would be more informative. By reading the paper, I should learn how to improve my skills when faced with a similar task, and that is not currently happening. Some examples: why do you rely on Hilbert's operator to define the if-then-else predicate in place of assuming a decidable equality over canonical elements? (that could be pointer equality in OCaml) Why do you introduce an artificial upper bound to the number of elements that is not required by functional correctness and that could be dispensed with for complexity analysis? Why do you avoid generalizing the elem type to carry a datum since it seems a trivial change? Why do you pick the mathematical definition of forest in place of defining forests as an inductive data type (i.e. a list of n-ary trees, that!
  are nodes carrying a list of n-ary trees)?

2. the authors claim very briefly that the number of machine instructions executed is accurately measured by the number of function calls, once garbage collection is ignored. The same assumption is done in many papers in the litterature. However, in the context of formal verification, I would like something more precise. What you formally prove is just a bound on the number of function call invocations, and a formal proof that a compiled OCaml program really has a computational cost linear in the number of function calls is lacking and can only be established w.r.t RAM-like machine models. Moreover, since garbage collection accounts for a significant percentage of execution time, it would be interesting to perform at once an analysis of the maximal amount of memory used by the OCaml code, applying the methodology to triples heap-time tokens-space tokens or something like that. I would like to hear the opinion of the authors on the feasibility of such an approach.

Minor remarks:
- what has been the role of automation in the formalization? Are there parts that you expect or you would have liked to be proved automatically, but were not?
- the work done in the European Project CerCo consisted in instrumenting source programs written in C, in a core ML language or in a syncrhonous language with instructions, similar to pay, to record the amount of resources used by the body of a function, loop or if-then-else branch. Later the user could specify at once the functional and non functional properties of the code using for example Hoare logic, and prove them semi-automatically with the Frama-C tool. Francois Bobot extended the Hoare logic of Frama-C with operators from Separation Logic, and he also applied the modified tool to the simultaneous verification of functional and non functional properties of C code. The approach presented here by the authors seem more integrated and clean to me, but Bobot's work is maybe worth citing in the related works. The authors also claim that they are interested in extending their framework to replace exact arithmetical bounds with big-O notation. Also the other way around, prop!
 osed by CerCo, is of interest. In CerCo the bounds were kept exact, and the non compositional cost 1 assigned to function calls was replaced by a number computed with abstract interpretation on the object code produced by a complexity preserving compiler. Possible references:
​* Separation Predicates: A Taste of Separation Logic in First‐Order Logic, F. Bobot, J.‐C. Filliatre, in Proc. IFCEM, Springer LNCS 7635:167‐181, 2012 
* Certified Complexity, R. Armadio et alt., Procedia Computer Science (7) 2011, 175-177.


