> The authors require law of excluded middle [...] but the equals seems
> decidable in this setting.

When verifying programs, we find it more convenient to use classical logic in
several places. However, it seems that indeed all the proofs could be done
constructively --- at some cost, though.

> [The presentation of the theorems differ in the paper and the Coq proofs]

Indeed, for the sake of presentation, the paper uses the simple "App" notation
to specify functions, whereas the Coq proofs use the more powerful "Spec"
notation, which correctly specifies the behavior of partial applications of
the functions (contrary to the "App" notation, which does not). It is trivial
to derive an "App" instance from a "Spec" instance, and indeed we plan to add
it in the formalization.

> "Why do you introduce an artificial upper bound to the number of elements
> that is not required by functional correctness and that could be dispensed
> with for complexity analysis?"

The proof which we have followed (which is found in Tarjan's course notes and
in the textbook by Cormen et al.) has this artificial upper bound.

Kaplan, Shafrir and Tarjan ("Union-find with deletions") offer a refined
analysis which apparently does not require this artificial upper bound. This
analysis has been simplified by Alstrup et al. ("Union-Find with Constant Time
Deletions") to a point where it seems that we could formalize it. We leave
this to future work, and we will cite these works in the final version of our
paper.

> Why do you avoid generalizing the elem type to carry a datum since it seems
> a trivial change?

We decided not to generalize in order to keep the presentation as simple as
possible. Preliminary experiments suggest that it is indeed straightforward
to generalize the code/specification/proof to accommodate the extra datum.
We have planned to do so in the future.

> Why do you pick the mathematical definition of forest in place of defining
> forests as an inductive data type?

In principle, this is purely a matter of taste, as this definition is not
exposed in the final specification.

Our view of a forest as a directed graph, where children point to parents,
seems very convenient. A view of forests as an inductive data type would mean
that parents point to children, which does not seem appropriate here.

> What you formally prove is just a bound on the number of function call
> invocations, and a formal proof that a compiled OCaml program really has a
> computational cost linear in the number of function calls is lacking and can
> only be established w.r.t RAM-like machine models.

Indeed, it is true that we only bound the number of function calls. There
remains to argue: 1- that an OCaml program *can* be compiled in a manner that
introduces a constant factor of overhead (where the "constant" may depend on
the program's text, but not on the program's input) with respect to this
measure; 2- that the OCaml compiler *actually* does this.

Concerning 1, we claim that this result is not difficult to understand
intuitively, and that it has been proved several times already in the
literature. We plan to expand this argument in the final version of the paper.

Concerning 2, this cannot really be done at present; it would require a formal
definition of the OCaml compiler.

We are not sure why the reviewer claims that this result "can only be
established w.r.t RAM-like machine models". It seems to us that even on a
pointer machine (i.e. using only records, no arrays) this result can be
achieved. Either of the two well-known representations of closures (flat
closures or linked closures) achieves this result, since in either case,
environment construction and environment access require time bounded by the
number of free variables in the closure (which is a "constant", i.e., it
depends only on the program text).

> what has been the role of automation in the formalization? Are there parts
> that you expect or you would have liked to be proved automatically, but were
> not?

We essentially relied on the "omega" arithmetic decision procedure and "eauto"
prolog-style proof search features. Of course, one can always hope to benefit
of a higher degree of automation in proofs.

