% \nonstopmode
\def\macrosUseLlncs{}
\let\citeyear\cite
\input{arthur}
\input{macros}
\input{local}

% for submission: make sure no \remarks remain
\renewcommand{\myremark}[3]{\error}

%\renewcommand{\section}[1]{\Bf{#1}}



\newcommand{\nl}{\texorpdfstring{\\}{}}
\title{Temporary Read-Only Permissions \nl for Separation Logic \nl (Extended Abstract)}
\author{Arthur Chargu\'eraud \inst{1,2} \and Fran\c cois Pottier \inst{1}}
\institute{
  Inria, Paris, France%
   \thanks{This research was partly supported by the French National Research
           Agency (ANR) under the grant ANR-15-CE25-0008.}%
  \and
  ICube -- CNRS, Université de Strasbourg, France
}

%\institute{
%  Inria, Paris, France%
%   \thanks{This research was partly supported by the French National Research
%           Agency (ANR) under the grant ANR-15-CE25-0008.}%
%  Inria, Nancy, France%
%  \and
%  ICube – CNRS, Université de Strasbourg, France
%}

\begin{document}
\maketitle

\vspace{-1em}
\begin{abstract}
\input{abstract}
\end{abstract}

% LATER: je trouve que "normal(H)" c'est quand même pas terrible
% comme nom, on devrait pouvoir trouver mieux.

% LATER: je ne suis pas hyper fan d'avoir le nom de toutes les règles en bleu

% LATER a better symbol than + for composition of heaps?
% could be \cup with a \star in it (as suggested on the phone)
% or could be a \star in a circle

%***********************************************************************



%------------------------------------------------------------------------------
%\section{Motivation}
\vspace{-5pt}
\SL~\cite{reynolds-02} offers a natural and effective framework for proving
the correctness of imperative programs that manipulate the heap.
However, practice shows some limitations of the specification language.
Consider the following example, which describes the specification of
the concatenation of two arrays.
%
\vspace{-1pt}
$$\Jhoarevert
   {\Hdataarray{a_1}{L_1} \Sc\Hstar \Hdataarray{a_2}{L_2} }
   { (\ca{concat}\,a_1\,a_2) }
   { \HPabs{a_3} \;
     \Hdataarray{a_3}{\OLAppof{L_1}{L_2}}
     \Sc\Hstar \Hdataarray{a_1}{L_1}
     \Sc\Hstar \Hdataarray{a_2}{L_2}
   }$$
%
Above, $\Hdataarray{a}{L}$ asserts the existence (and unique
ownership) of an array at address~$a$ whose content is given by the list~$L$.
A separating conjunction~$\Hstar$ is used in the precondition to require that
$a_1$ and $a_2$ be disjoint arrays. Its use in the postcondition guarantees
that $a_3$ is disjoint with $a_1$ and~$a_2$.
In this specification, the fact that the arrays $a_1$ and~$a_2$ are
unaffected must be explicitly stated as part of the postcondition,
making the specification seem verbose.

We wish to extend the specification language of Separation Logic
so as to be able to specify the same function more concisely, as follows.
%
\vspace{-1pt}
$$\Jhoarevert
  { {\Hdataarrayro{a_1}{L_1}} \Sc\Hstar {\Hdataarrayro{a_2}{L_2}} }
  { (\ca{concat}\,a_1\,a_2) }
  { \HPabs{a_3} \; \Hdataarray{a_3}{\OLAppof{L_1}{L_2}}   }$$
\vspace{-1pt}

At first sight, it may seem that the notation ``$\HROname$'',
which stands for ``read-only'', could
be interpreted as syntactic sugar for repeating part
of the precondition in the postcondition. However, we wish
to assign $\HROname$ a much stronger interpretation.
%\begin{itemize}
%\item
First, we
wish an assertion of the form $\HRO{H}$ to prevent
writing the heap fragment described by $H$, whereas the syntactic
sugar interpretation only guarantees that this heap fragment
satisfies~$H$ upon entry and upon exit.
%\item
Second, we wish to allow aliasing of read-only arguments,
whereas traditional Separation Logic requires a separate specification
to handle the case where the two arguments are aliased.
%\item
Third, we wish to save the user the need the trouble of proving that
$H$ is still satisfied upon exit: since a read-only heap fragment
cannot be mutated, its properties cannot be falsified.
%The latter is especially relevant when
%representation predicates need to be unfolded in order to access the data.
%\end{itemize}


%------------------------------------------------------------------------------
%\section{Fractional permissions}

Fractional permissions~\citeyear{boyland-fractions-03} offer a partial solution. % to the problem.
Let $\Hdataarrayfrac{a}{\Ga}{L}$ denote a fraction $\Ga$ of the ownership
of the array. When $\Ga$ is $1$, this assertion is equivalent to
$\Hdataarray{a}{L}$. When $\Ga$ is less than $1$, this assertion grants
read-only access. Fractional permissions can be split or merged using the following rule:
\vspace{-1pt}
$$\Hdataarrayfrac{a}{\Ga+\Gb}{L} \Sc{=} \Hdataarrayfrac{a}{\Ga}{L} \Sc\Hstar \Hdataarrayfrac{a}{\Gb}{L} \qquad\X{ with } 0 < \Ga,\Gb \leq 1.$$
\vspace{-1pt}
%
With fractional permissions, the concatenation function may be specified as:
%
\vspace{-1pt}
$$\Tfor{\Ga\Gb}\;\; \Jhoarevert
  { {\Hdataarrayfrac{a_1}{\Ga}{L_1}} \Sc\Hstar {\Hdataarrayfrac{a_2}{\Gb}{L_2}} }
  { (\ca{concat}\,a_1\,a_2) }
  { \HPabs{a_3} \; {\Hdataarrayfrac{a_1}{\Ga}{L_1}} \Sc\Hstar {\Hdataarrayfrac{a_2}{\Gb}{L_2}} \Hstar \Hdataarrayfrac{a_3}{1}{\OLAppof{L_1}{L_2}}   }$$
%
This specification allows $a_1$ and $a_2$ to be aliases.
% With some syntactic sugar, we could also avoid the copy-pasting between the pre- and the postcondition.
Nevertheless, fractional permissions suffer from several limitations.
First, they require explicit quantification over the fractions $\Ga$ and $\Gb$,
and may involve arithmetic operations when splitting and merging permissions.
Second,
% they require the user to establish postconditions even for read-only
% permissions.
they require careful accounting: if a single nonzero fraction is lost,
a full read/write permission can never be recovered.
%
Third, this approach lacks generality because scaling $\frac{1}{2} H$
cannot be defined for an arbitrary assertion $H$.

%------------------------------------------------------------------------------
%\section{Generic read-only modifier}

We propose an extension of the logic with a modality ``$\HROname$'' that applies to any
assertion $H$. The assertion $\HRO{H}$ is duplicable, allowing
read-only arguments to be aliased.
Moreover, an assertion $\HRO{H}$ appears only in preconditions, never
in postconditions. Thus, compared with traditional Separation Logic,
our specifications are more concise, and the number of proof obligations
for establishing postconditions is reduced.

Read-only permissions are introduced by a generalization of the frame rule:
%
\begin{mathpar}
\inferrule*[Right=frame-ro]
  {  \Jhoare{H \Hstar \HRO{H'}  }{t}{Q}  \\ \HNormal{H'} }
  { \Jhoare{H \Hstar {H'}}{t}{Q\Qstar {H'}} }
\end{mathpar}
%
where the side condition $\HNormal{H'}$ %intuitively
means that $H'$ does not contain any read-only assertions.
The intuition is as follows. With the traditional frame rule,
the assertion $H'$ that is ``framed out'' disappears within
a certain scope and reappears when that scope is exited.
With the read-only frame rule (above),
instead of disappearing altogether,
the assertion $H'$ is turned into $\HRO{H'}$ within the scope,
and reappears when that scope is exited.
The soundness of this extension of Separation Logic has been
entirely formalized in Coq.



%By design, our extension of Separation Logic ensures that a read-only
%permission only grants read access




%\begin{mathpar}
%\inferrule*[right=parallel-ro]
%{ \Jhoare{H_1 \Hstar \HRO{H_3}}{t_1}{Q_1} \\ \Jhoare{H_2 \Hstar \HRO{H_3}}{t_2}{Q_2} }
%{ \Jhoare{ H_1 \Hstar H_2 \Hstar \HRO{H_3} }{ \Lpar{t_1}{t_2} }{ \Qstarof{Q_1}{Q_2} } }
%\end{mathpar}


%\Skiph
%\begin{mathpar}
%\inferrule*[Right=dup-ro]
%{ }
%{ \HRO{H} \Scs\Himpl \HRO{H} \Sc\Hstar \HRO{H} }
%\\
%\inferrule*[Right=get-ro]
%{ }
%{ \Jhoare{ \HRO{\HPsingle{l}{v}} }{ \LGetof{l} }{ \HPabs{x} \, \HPprop{x = v} } }
%\end{mathpar}




%------------------------------------------------------------------------------

\begin{comment}
\begin{mathpar}
\inferrule*[Right=seq]
{ \Jhoare{H}{t_1}{Q'} \\ \Jhoare{Q'\Ltt}{t_2}{Q} }
{ \Jhoare{H}{ \LSeqof{t_1}{t_2} }{Q} }
\\
\inferrule*[Right=seq-ro]
{ \Jhoare{H\Hstar \Highlight{\HRO{H'}}}{t_1}{Q'} \\ \Jhoare{Q'\Ltt \Hstar \Highlight{\HRO{H'}}}{t_2}{Q} }
{ \Jhoare{H \Hstar \Highlight{\HRO{H'}}}{ \LSeqof{t_1}{t_2} }{Q} }
\\
\inferrule*[Right=seq-frame]
{ \Jhoare{H}{t_1}{Q'} \\ \Jhoare{Q'\Ltt \Hstar \Highlight{H'}}{t_2}{Q} }
{ \Jhoare{H \Hstar \Highlight{H'}}{ \LSeqof{t_1}{t_2} }{Q} }
\end{mathpar}
\end{comment}



%------------------------------------------------------------------------------



%***********************************************************************

\clearpage
\bibliographystyle{splncs03}
\bibliography{english,extra}

\end{document}
