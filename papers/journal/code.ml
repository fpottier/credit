type rank = int

type 'a elem =
  'a content ref
and 'a content =
  | Link of 'a elem
  | Root of rank  * 'a

let make v =
  ref (Root (0, v))

let rec find x =
  match !x with
  | Root (_, _) ->
      x
  | Link y ->
      let z = find y in
      x := Link z;
      z

let get x =
  let x = find x in
  match !x with
  | Root (_, v) ->
      v
  | Link _ ->
      assert false

let set x v =
  let x = find x in
  match !x with
  | Root (r, _) ->
      x := Root (r, v)
  | Link _ ->
      assert false

let link x y =
  if x == y then x else
  match !x, !y with
  | Root (rx, vx), Root (ry, _) ->
      if rx < ry then begin
        x := Link y; y
      end else if rx > ry then begin
        y := Link x; x
      end else begin
        y := Link x;
        x := Root (rx+1, vx); x
      end
  | _, _ ->
      assert false

let union x y = link (find x) (find y)

let eq    x y = (find x == find y)
