Require Import LibTactics.
Require Import LibNatExtra.
Require Import Filter.
Require Import FilterTowardsInfinity. (* default filter for [nat] *)
Require Import Norm.
Require Import Dominated.
Require Import Big.

Lemma sub_leq:
  forall a b c,
  a <= b ->
  a - c <= b - c.
Proof using.
  intros. omega.
Qed.

Require Import List.
Lemma exploit_in_big_max:
  forall j : nat, forall range f,
  In j range ->
  f j <= \big[max]_(i <- range) f i.
Proof using.
Admitted. (* OK *) (* TEMPORARY *)

Lemma two_halves:
  forall n,
  n / 2 + (n + 1) / 2 = n.
Proof using.
Admitted. (* OK *) (* TEMPORARY *)

Definition master1_witness (base : nat -> nat) (threshold : nat) (h : 3 <= threshold) : nat -> nat.
refine (Fix lt_wf (fun _ => nat) (fun (n : nat) (self : forall m : nat, m < n -> nat) =>
  if le_lt_dec threshold n then
    self (n/2) _ + self ((n+1)/2) _ + 1
  else
    base n
)).
Proof using.
(* n/2 < n *)
eauto with div2 omega.
(* (n+1)/2 < n *)
eauto with div2 omega.
Defined.

Lemma master1_witness_eq (base : nat -> nat) (threshold : nat) (h : 3 <= threshold) :
  forall n,
  master1_witness base threshold h n =
  if le_lt_dec threshold n then
    master1_witness base threshold h (n/2) + master1_witness base threshold h ((n+1)/2) + 1
  else
    base n.
Proof using.
  intro. apply (Fix_eq lt_wf (fun _ => nat)); intros.
  match goal with
    | [ |- context[match ?E with left _ => _ | right _ => _ end] ] => destruct E
  end; simpl; f_equal; auto.
Qed.

Lemma master1_existence:
  forall base : nat -> nat,
  forall threshold,
  3 <= threshold ->
  exists f,
  (forall n, n <  threshold -> f n = base n) /\
  (forall n, n >= threshold -> f n = f (n/2) + f ((n+1)/2) + 1).
Proof using.
  introv h.
  exists (master1_witness base threshold h).
  split; intros.
  rewrite master1_witness_eq at 1. destruct (le_lt_dec threshold); [ false; omega | ]. reflexivity.
  rewrite master1_witness_eq at 1. destruct (le_lt_dec threshold); [ | false; omega ]. reflexivity.
Qed.

Lemma prove_in_interval:
  forall i j k,
  i <= j < k ->
  In j (interval i k).
Proof using.
Admitted. (* OK *) (* TEMPORARY *)

Lemma master1:
  forall f : nat -> nat,
  towards_infinity (fun n => f n <= f (n/2) + f ((n+1)/2) + 1) ->
  dominated towards_infinity f (fun n => n).
Proof using.
  introv [ rank req ].
  set (threshold := max 2 rank).
  set (a := \big[max]_(i <- interval 0 (threshold + 1)) f i + 1).
  set (b := 1).
  assert (a > 0). unfold a. omega.
  assert (fact: forall n, threshold <= n -> f n <= a * n - b).
  intro n. induction_wf ih: lt_wf n. intros.
  assert (2 <= n). unfold threshold in *; max_ub; omega.
  assert (1 <= n/2). { eauto with div2 omega. }
  assert (1 <= (n+1)/2). { eauto with div2 omega. }
  rewrite req by (unfold threshold in *; max_ub; omega).
  destruct (le_dec (2 * threshold) n).
  (* Case: [2 * threshold <= n]. *)
  forwards ih1: ih (n/2); eauto 2 with div2 omega.
  forwards ih2: ih ((n+1)/2); eauto 2 with div2 omega.
  rewrite ih1. rewrite ih2. clear ih1. clear ih2.
  assert (0 < a * (n/2)). { eauto with positive omega. }
  assert (0 < a * ((n+1)/2)). { eauto with positive omega. }
  replace (a * (n / 2) - b + (a * ((n + 1) / 2) - b) + 1)
     with (a * (n / 2) + a * ((n + 1) / 2) + 1 - b - b) by (unfold b; omega).
  eapply sub_leq.
  replace (a * (n / 2) + a * ((n + 1) / 2))
     with (a * (n/2 + (n+1)/2)) by ring.
  rewrite two_halves.
  unfold b. generalize (a * n). intros. omega.
  (* Case: [n < 2 * threshold]. *)
  clear ih.
  assert (h1: f(n/2) <= a - 1).
    unfold a.
    match goal with |- ?foo <= ?bar + 1 - 1 =>
      replace (bar + 1 - 1) with bar by omega
    end.
    eapply exploit_in_big_max.
    eapply prove_in_interval.
    eauto with div2 omega.
  assert (h2: f((n+1)/2) <= a - 1).
    unfold a.
    match goal with |- ?foo <= ?bar + 1 - 1 =>
      replace (bar + 1 - 1) with bar by omega
    end.
    eapply exploit_in_big_max.
    eapply prove_in_interval.
    eauto with div2 omega.
  rewrite h1. rewrite h2. clear h1. clear h2.
  assert (h3: 2 <= n).
    unfold threshold in *. max_ub. omega.
  rewrite <- h3. clear h3.
  unfold b.
  omega.
  (* Conclusion. *)
  exists a. exists threshold. simpl. intros. rewrite fact by assumption. generalize (a * n). intros. omega.
Qed.

Lemma master2:
  forall f g : nat -> nat,
  towards_infinity (fun n => f n = f (n/2) + f ((n+1)/2) + g n) ->
  dominated towards_infinity g (fun n => n) ->
  dominated towards_infinity f (fun n => n * log2 n).
Proof using.
Abort. (* OK *) (* TEMPORARY *)

(*
prove by induction on k that if 2^k <= n < 2^(k+1) then
f n <= a * n * k

f n = f (n/2) + f ((n+1)/2) + g n
   <= a * n/2 * (k-1) + a * ((n+1)/2) * (k - 1) (* WISH *) + b * n
    = a * n * (k - 1) + b * n
   <= a * n * k as soon as a >= b

F n = 2 ^ (f n)
IH: F n <= n^n
F n = 2 ^ (f (n/2) + f ((n+1)/2) + g n)
    = F(n/2) * F((n+1)/2) * 2^(g n)
   <= (n/2)^(n/2) * ((n+1)/2)^((n+1)/2) * 2^(b * n)
    = 
*)

