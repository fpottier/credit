Set Implicit Arguments.
Generalizable All Variables.
Require Import LibTactics.
Require Import LibLogic.
Require Import Filter.

Section Everywhere.

(* If the type [A] is inhabited, then the singleton set that contains just the
   set [A] is a filter. We call this modality [everywhere]. *)

Context `{IA:Inhab A}.

Definition everywhere (P : A -> Prop) :=
  forall a, P a.

(* Make this a definition, not an instance, because we do not wish it to be
   used during the automated search for instances. *)

Definition filter_everywhere : Filter everywhere.
Proof using IA.
  econstructor; unfold everywhere.
  (* There exists an element in this filter, namely the universe. *)
  exists (fun (a : A) => True). eauto.
  (* The universe is nonempty. *)
  intros. exists arbitrary. eauto.
  (* Closure by intersection and subset. *)
  eauto.
Qed.

End Everywhere.

