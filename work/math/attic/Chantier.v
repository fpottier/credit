Require Import Unpack.
Require Import LibTactics.
Require Import LibNatExtra.
Require Import LibRewrite.
Require Import Filter.
Require Import Dominated. (* TEMPORARY this causes [FilterAtAndAbove] to become visible -- not cool *)
Require Import FilterWithin.
Require Import FilterTowardsInfinity.
Require Import DominatedFacts.

Definition fplus {A} (f g : A -> nat) : A -> nat :=
  fun x => f x + g x.

Lemma def_fplus:
  forall {A} (f g : A -> nat),
  pw eq (fun x => f x + g x) (fplus f g).
Proof.
  reflexivity.
Qed.

Lemma fplus_eq:
  forall {A} (f1 g1 f2 g2 : A -> nat),
  pw eq f1 f2 ->
  pw eq g1 g2 ->
  pw eq (fplus f1 g1) (fplus f2 g2).
Proof.
  unfold fplus. introv h1 h2. repeat intro. rewrite h1, h2. reflexivity.
Qed.

Obligation Tactic := unfold Proper, respectful.
Program Instance Fplus_eq: forall A, Proper (pw eq ++> pw eq ++> pw eq) (@fplus A).
Next Obligation.
  eauto using fplus_eq.
Qed.

Goal
  forall {A} (f g h : A -> nat) (x : A),
  pw le (fun x => f x + g x + h x) (fplus (fplus f g) h).
Proof.
  intros. rewrite def_fplus. rewrite def_fplus. reflexivity.
Qed.

Program Instance Dominated_fplus
  {A} (ultimately : filter A) `{Filter A ultimately}
  : Proper (dominated _ ++> dominated _ ++> dominated _) (@fplus A).
Next Obligation.
  intros. eauto using dominated_sum.
Qed.

Goal
  dominated _ (fun x => 3 * x + 1) (fun x => x).
Proof.
  dominated.
  eapply dominated_one_n.
Qed.

(* The diagonal constraint, [i <= n], is non-blocking. *)

Instance nonblocking_diagonal:
  NonBlocking _ (fun p : nat * nat => let (i, n) := p in i <= n).
Proof.
  econstructor.
  unfold product, towards_infinity. intros. unpack.
  match goal with x1: nat, x2: nat |- _ => exists (x1, max x1 x2) end.
  max_ub. eauto.
Qed.

(* Hence [within _ (i <= n)] is a filter. *)

Goal Filter (within
               (product towards_infinity towards_infinity)
               (fun p : nat * nat => let (i, n) := p in i <= n)).
Proof.
  eauto with typeclass_instances.
Qed.

(* And under this filter, [i + n] is dominated by [n], as one would expect. *)

Goal dominated
  (within _
  (fun p : nat * nat => let (i, n) := p in i <= n))
  (fun p : nat * nat => let (i, n) := p in i + n)
  (fun p : nat * nat => let (i, n) := p in n).
Proof.
  unfold dominated. exists 2.
  unfold within, product. simpl.
  exists (le 0) (le 0). repeat split; eauto with filter.
  eauto with omega.
Qed.

(*

F = somme i = 1 a n des f(i,n)
avec f(i,n)=O(i+n)
           =O(n) si on sait que i <= n
peut-on prouver que F est O(n^2)?

*)