Set Implicit Arguments.
Require Import CFLib Demo_ml.
Generalizable Variables A B.

Implicit Types n m : int.

Ltac auto_tilde ::= eauto with maths.


(*************************************************************************)

(** --
    Grosse question : des crédits sur int ou sur nat ?
    sur int, ça oblige à demander que les arguments soit positifs
    à plusieurs endroits; sur nat, ça oblige l'utilisateur à faire
    des conversions très moches entre les arguments de ses fonctions
    caml (qui sont sur int) et les expressions de complexités (sur nat).
    Intuitivement, j'ai bcp souffert par le passé de ces conversions
    lorsque je travaillais sur des listes, et j'ai envie de tout faire
    sur int. Après, il faut voir si ça oblige à écrire des prédicats
    en plus dans les specs lorsqu'on quantifie sur des fonctions de coûts,
    disant que la fonction retourne que des valeurs positives... 
    -- *)

(* FP: c'est quoi ce type int? Un sous-ensemble non spécifié de Z?
   Comment on fait pour faire fonctionner les tactiques omega ou ring
   sur ce type? *)

(** Credits as a heap predicate ('$' seems to be already used) *)

Parameter credits : int -> hprop.
Notation "'$$' n" := (credits n) (at level 80).
  (* FP: je mettrais bien level 20, ou quelque chose comme ça,
     pour pouvoir écrire $$n \* ... sans parenthéser.
     Apparemment tu as choisi l'inverse! *)

(** FP: I suggest [$$n] should be equivalent to [True] when [n] is negative.
    This should lead to fewer side conditions. *)

(* FP: removed because of new interpretation of negative credits.
Parameter credits_pos : forall n,
  ($$n) ==> ($$n) \* [n >= 0].
*)

Parameter credits_zero : 
  ($$0) ==> [].
  (* FP: cet axiome semble inutile, non?
     peut-être l'autre implication, [] ==> credits 0, pourrait-elle
     éventuellement servir. *)

(** Credits can be joined and split. *)

Parameter credits_add : forall n m, 
  n >= 0 -> m >= 0 ->
  ($$ n) \* ($$ m) = $$ (n + m).

Implicit Arguments credits_add [ ].

(* FP: because of new interpretation of negative credits, one direction
   of [credits_add] is valid without side conditions. *)

Parameter credits_join : forall n m,
  ($$ n) \* ($$ m) ==> ($$ n + m).

(** Reformulation of the rule with a subtraction *)

Lemma credits_sub : forall m n,
  m >= 0 -> m <= n ->
  ($$ n) = ($$ m) \* ($$ n - m).
Proof.
  introv Pm Le. math_rewrite (n - m = n + (-m)).
  rewrite~ credits_add. fequals. math.
Qed.

Implicit Arguments credits_sub [ ].

(** Reformulation as a simplification lemma *)

Lemma credits_simpl : forall m n H,
  m >= 0 -> m <= n ->
  ($$ n-m) ==> H ->
  ($$ n) ==> ($$ m) \* H.
Proof.
  introv Pm Le M. rewrite* (credits_sub m). hsimpl*.
Qed.

Lemma credits_simpl' : forall m n H,
  m >= 0 -> m <= n ->
  ($$ n-m) ==> H ->
  ($$ n) ==> H \* ($$ m).
Proof.
  intros. hchange* credits_simpl. hsimpl. hsimpl*.
Qed.




(*************************************************************************)

Axiom mult_ge0 : forall a b, a >= 0 -> b >= 0 -> a * b >= 0.

Axiom mult_succ : forall a b, a > 0 -> a*b = b + (a-1)*b.


(*************************************************************************)

Open Scope I.
(* FP: I still get [%I] in Coq's output.
   Any way of suppressing it? *)

Parameter pay_spec : 
  App pay tt; ($$ 1) (# []).

Lemma iter_spec : 
  forall (f:func) (fc:int),
  fc >= 0 ->
  (forall (i:int), App f i; ($$ fc) (# [])) ->
  forall (n:int),
  n >= 0 ->
  App iter f n; ($$ n+n*fc) (# []).
Proof.
  introv Pfc Hf.
  intro n. induction_wf IH: (int_downto_wf 0) n. intros.
  xcf_app.
  xif.
  (* call to pay *)
  xseq. xapply pay_spec.
    apply~ credits_simpl. skip. (* use mult_ge0 *)
    intros x. hsimpl.
  (* call to f *)
  xseq. xapply Hf.
    hsimpl. rewrite~ mult_succ. apply~ credits_simpl'. 
      skip. (* use mult_ge0 *)
    intros x. hsimpl.
  (* call to iter *)
  xapply IH. 
    hnf. math.
    math.
    hsimpl. apply credits_simpl'.
      skip. 
      math. (* c'est là que tout se passe *)
      hsimpl.
    intros x. hsimpl.
      (* là on aurait fini, si on avait appliqué la règle de garbage collection.
         à la place, on peut vérifier si on veut qu'il reste effectivement zéro. *)
     math_rewrite ((((n + (fc + ((n - 1)%I * fc)%I)%I)%I - 1)%I - fc)%I -
 ((n - 1)%I + ((n - 1)%I * fc)%I)%I = 0).
     apply credits_zero. 
  (* base case *)
  xret. hsimpl. 
  (* on aurait aussi pu utiliser le fait que n = 0 *)
Qed.

(*************************************************************************)

Require Import Filter.
Require Import FilterTowardsInfinity.
Require Import Norm.
Require Import Dominated.

Definition O := (dominated towards_infinity : (nat -> nat) -> (nat -> nat) -> Prop).

(*************************************************************************)

(* [OSpec f ac] asserts that [f] is a function (of one argument [i] and no
   result) that consumes [O(ac(s))] credits, where [s] is [abs i]. *)

Definition OSpec (f : func) (ac : nat -> nat) :=
  exists (fc : nat -> nat),
  O fc ac /\
  forall i : int,
  forall s : nat,
  s = abs i ->
  i >= 0 ->
  App f i; ($$(fc s)) (# []).

(* Test. If [f] consumes [1 + 2i] credits, for instance, then it consumes
   [O(s)] credits, where [s] is [abs i]. *)

Goal
  forall f : func,
  (forall i : int, i >= 0 -> App f i; ($$ 1 + 2 * i) (# [])) ->
  OSpec f (fun s => s).
Proof.
  introv hf.
  exists (fun s => 1 + 2 * s)%nat. split.
  (* Show [1 + 2i = O(i)]. *)
  exists 3%nat 1%nat. intros. simpl. omega.
  (* *)
  intros.
  assert (i = (s : int)). skip.
  skip.
Qed.

(*************************************************************************)

(*************************************************************************)
(* FUTURE WORK 


(** Setting up of lemmas for tactics *)

Lemma hsimpl_credits_1 : forall n m HA HR HT,
  m >= 0 -> m <= n ->
  credits (n - m) \* HT ==> HA \* HR ->
  credits n \* HT ==> HA \* (credits m \* HR).
Proof.
  intros. rewrite* (credits_extract m).
 subst. apply~ hsimpl_cancel_1. Qed.

Lemma hsimpl_cancel_eq_2 : forall H H' HA HR H1 HT,
  H = H' -> 
  H1 \* HT ==> HA \* HR -> 
  H1 \* H \* HT ==> HA \* (H' \* HR).
Proof. intros. subst. apply~ hsimpl_cancel_2. Qed.


Ltac hsimpl_find_credits l HL cont :=
  match HL with
  | credits _ \* _ => apply hsimpl_cancel_eq_1
  | _ \* credits _ \* _ => apply hsimpl_cancel_eq_2
  | _ \* _ \* credits _ \* _ => apply hsimpl_cancel_eq_3
  | _ \* _ \* _ \* credits _ \* _ => apply hsimpl_cancel_eq_4
  | _ \* _ \* _ \* _ \* credits _ \* _ => apply hsimpl_cancel_eq_5
  | _ \* _ \* _ \* _ \* _ \* credits _ \* _ => apply hsimpl_cancel_eq_6
  | _ \* _ \* _ \* _ \* _ \* _ \* credits _ \* _ => apply hsimpl_cancel_eq_7
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* credits _ \* _ => apply hsimpl_cancel_eq_8
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* credits _ \* _ => apply hsimpl_cancel_eq_9
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* credits _ \* _ => apply hsimpl_cancel_eq_10
  end; [ cont tt | ].

*)

