Set Implicit Arguments.
Generalizable All Variables.
Require Import LibTactics.
Require Import MySets.
Require Import Filter.

(* ---------------------------------------------------------------------------- *)

(* If [A] is infinite, then the set of all cofinite subsets of [A]
   is a filter, known as the Fre'chet filter. *)

(* TEMPORARY maybe infinite (universe A) should be a type class too *)

Instance filter_cofinite (A : Type) (h : infinite (@universe A)) : Filter (@cofinite A).
Proof.
  econstructor.
  (* There exists a cofinite set. *)
  eauto using cofinite_universe.
  (* A cofinite set is nonempty. (This exploits the hypothesis that the
     universe is infinite.) *)
  eauto using cofinite_nonempty.
  (* The intersection of two cofinite sets is cofinite. *)
  eauto using cofinite_intersection, cofinite_subset.
Qed.

