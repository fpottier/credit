(* Two vertices are equivalent iff they have a common representative.
   This is a reformulation of the definition of [is_equiv]. *)

Lemma is_equiv_iff_same_repr:
  forall x y rx ry,
  is_repr x rx ->
  is_repr y ry ->
  (rx = ry) = is_equiv x y.
Proof.
  unfold is_equiv.
  intros. extens. iff ? (r & ? & ?).
    { subst. eauto. }
    { repeat exploit_functional. eauto. }
Qed.

(* If a vertex has rank 0, then if it is a leaf, i.e., it has no children.
   The converse is false, due to path compression. *)

Lemma rank_zero:
  forall y,
  R y = 0 ->
  forall x, ~ F x y.
Proof.
  intro y.
  (* Assume [y] has rank 0, yet is not a leaf. It has a descendant [x].
     Then, the rank of [y] must be greater than the rank of [x]. Contradiction. *)
  intros ? x ?.
  assert (R x < R y). { eapply is_rdsf_F. eauto. }
  omega.
Qed.

(* Section Link. *)

(* If the representative after the link is [y], then the representative before
   the link was [x] or [y]. *)

Lemma is_repr_link_1_converse:
  forall w,
  is_repr link w y ->
  is_repr F w x \/ is_repr F w y.
Proof.
  unfold is_repr. intros; unpack.
  forwards: path_link_1_converse; eauto. branches; eauto.
Qed.

(* Every vertex whose representative after the link is not [y] already
   admitted this representative before the link. *)

Lemma is_repr_link_2_converse:
  forall w r,
  is_repr link w r ->
  r <> y ->
  is_repr F w r.
Proof.
  unfold is_repr.
  intuition eauto using path_link_2_converse, is_root_link_converse.
Qed.

(* Installing the new link preserves all existing equivalences. *)

Lemma is_equiv_link_1:
  forall w z,
  is_equiv F w z ->
  is_equiv link w z.
Proof.
  introv [ r [ ? ? ]].
  destruct (classic (r = x)).
    { exists y. subst r. eauto using is_repr_link_1. }
    { exists r. eauto using is_repr_link_2. }
Qed.

(* Installing the new link creates a new equivalence between [x] and [y]. *)

Lemma is_equiv_link_2:
  is_equiv link x y.
Proof.
  intros. exists y. eauto 6 using is_repr_link_1, is_repr_link_2 with uf.
Qed.

(* Conversely, if [w] and [z] are equivalent after the linking, then
   either they were equivalent before the linking already, or one of
   them had representative [x] and the other had representative [y]
   before the linking. *)

Lemma is_equiv_link_converse:
  forall w z,
  is_equiv link w z ->
  is_equiv F w z \/
  is_repr F w x /\ is_repr F z y \/
  is_repr F w y /\ is_repr F z x.
Proof.
  introv [ r [ ? ? ]].
  destruct (classic (r = y)).
  { subst r.
    forwards: is_repr_link_1_converse w; eauto.
    forwards: is_repr_link_1_converse z; eauto.
    repeat branches; eauto with uf. }
  { forwards: is_repr_link_2_converse w; eauto.
    forwards: is_repr_link_2_converse z; eauto.
    eauto with uf. }
Qed.

(* Thus, the effect of the new link on [is_equiv] is to fuse the equivalence
   classes of [x] and [y]. *)

Lemma is_equiv_per_add_edge:
  is_equiv link =
  per_add_edge (is_equiv F) x y.
Proof.
  (* This proof feels manual and duplicates some of the arguments in
     the proof of [is_equiv_rstclosure]. It has been replaced by a
     proof based on [is_equiv_rstclosure]. *)
  intros. extens. intros w z.
  split.
  { intros.
    (* Apply [is_equiv_link_converse] and argue that in each of
       the three cases, we have a path in the symmetric-transitive
       closure of the union of [F] and the link [x]--[y]. *)
    forwards: is_equiv_link_converse; eauto. branches; unpack.
      eapply per_add_edge_le. assumption.
      eapply stclosure_trans; [ | eapply stclosure_trans ].
        1: eauto using stclosure_sym, stclosure_step with relations uf.
        2: eauto using stclosure_sym, stclosure_step with relations uf.
        1: eauto using stclosure_sym, stclosure_step with relations uf.
      eapply stclosure_trans; [ | eapply stclosure_trans ].
        1: eauto using stclosure_sym, stclosure_step with relations uf.
        2: eauto using stclosure_sym, stclosure_step with relations uf.
        1: eauto using stclosure_sym, stclosure_step with relations uf.
  }
  { (* Induction on [stclosure]. *)
    induction 1.
      { unfold union, per_single in *. branches; unpack; subst;
        eauto using is_equiv_link_1, is_equiv_link_2. }
      { eapply is_equiv_sym; eauto. }
      { eapply is_equiv_trans; eauto using is_dsf_link. }
  }

(* Linking preserves [finite_descendants] -- the property that every vertex
   has a finite set of descendants. This property is no longer useful, as
   [finite_descendants] is now a lemma, which we prove just once, by
   exploiting the fact that [D] is finite. *)

Lemma finite_descendants_link:
  forall D F x y,
  is_dsf D F ->
  finite_descendants F ->
  x \in D ->
  y \in D ->
  is_root F x ->
  is_root F y ->
  x <> y ->
  finite_descendants (link F x y).
Proof.
  intros. intro v.
  tests : (v = y).
  { (* Case: [v = y]. *)
    erewrite descendants_link_1 by eauto.
    eauto with finite. }
  { (* Case: [v <> y]. *)
    erewrite descendants_link_2 by eauto.
    eauto. }
Qed.

Qed.

(* Section PathCompression. *)

(* A path to [x] is replaced with a path to [z]. *)

Lemma compress_transforms_paths_to_x:
  forall v w,
  path F v w ->
  w = x ->
  path compress v z.
Proof.
  induction 1 as [| w v ? ]; intros; subst.
    (* If the path is empty, then [v] is [x]. The new edge forms a path
       from [v] to [z]. *)
    { eauto using compress_x_z with rtclosure. }
    (* If the path is nonempty, then [v] cannot be [x], as otherwise we
       would have a cycle. Thus, we can follow the old edge out of [v],
       which is preserved. *)
    { assert (v <> x).
        { eauto using paths_have_distinct_endpoints, tclosure_intro. }
      eauto using rtclosure_step, compress_preserves_other_edges. }
Qed.

(* No vertex has new descendants. *)

Lemma compress_creates_no_new_descendants:
  forall v,
  descendants compress v \c descendants F v.
Proof.
  intro. eapply incl_prove. intro u.
  repeat rewrite descendant.
  eauto using compress_preserves_paths_converse.
Qed.

