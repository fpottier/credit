
let pay () = ()

let rec iter f n =
   if n > 0 then begin
      pay();
      f (n-1);
      iter f (n-1)
   end
