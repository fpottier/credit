Require Import LibTactics.
Require Import LibNatExtra.
Require Import LibRewrite.
Require Import Filter.
Require Import FilterAtAndAbove.      (* non-standard filter for [nat] *)
Require Import FilterTowardsInfinity. (* default filter for [nat] *)
Require Import Norm.
Require Import Big.

(* ---------------------------------------------------------------------------- *)

(* If we have a filter on [A] and a norm on [B], then we can define a
   domination relation between two functions of type [A -> B]. *)

(* [dominated f g] holds if and only if, for some constant [c], [f x] is
   ultimately bounded (in norm) by [c * g x]. *)

(* This definition is implicitly parameterized over the filter [ultimately]
   and by the norm [norm]. *)

Definition dominated {A} ultimately `{@Filter A ultimately} {B} `{@Norm B} (f g : A -> B) :=
  exists c,
  ultimately (fun x => norm (f x) <= c * norm (g x)).

(* For simplicity, we have fixed the codomain of [norm] to be [nat], but it
   could perhaps be any ordered ring. *)

(* Note that if this property holds of a certain constant [c], then it holds
   of all larger [c]s as well. Thus, the quantification [exists c, ...] could
   be replaced with [towards_infinity (fun c => ...)]. I don't know if that
   would be useful, though. *)

(* This notion is analogous to [is_domin] in Coquelicot. *)

(* If one writes [dominated _ f g], then the default filter for the domain
   of the functions [f] and [g] is chosen by the type class system. One
   can also write [dominated ultimately f g] in order to choose an explicit
   filter. *)

Goal (dominated _                (fun x => 2 * x + 1) (fun x => x)). Proof. Abort. (* OK *)
Goal (dominated (at_and_above 1) (fun x => 2 * x + 1) (fun x => x)). Proof. Abort. (* OK *)

(* ---------------------------------------------------------------------------- *)

Section DominationProperties.

Context {A} {ultimately} `{@Filter A ultimately} {B} `{@Norm B}.

(* Pointwise inequality implies domination. *)

Lemma subrelation_norm_le_dominated:
  forall f g : A -> B,
  (forall a, norm (f a) <= norm (g a)) ->
  dominated _ f g.
Proof.
  intros. exists 1. eapply filter_universe. intros. ring_simplify. eauto.
Qed.

(* Domination is reflexive. *)

Lemma dominated_reflexive:
  forall f : A -> B,
  dominated _ f f.
Proof.
  intros. eapply subrelation_norm_le_dominated. eauto.
Qed.

(* Domination is transitive. *)

Lemma dominated_transitive:
  forall f g h : A -> B,
  dominated _ f g ->
  dominated _ g h ->
  dominated _ f h.
Proof.
  introv [ c1 u1 ] [ c2 u2 ].
  exists (c1 * c2).
  eapply (filter_closed_under_intersection _ _ _ u1 u2).
  introv h1 h2.
  rewrite h1.
  rewrite <- mult_assoc. eapply mult_le_compat_l.
  rewrite h2.
  eauto.
Qed.

(* This allows rewriting. *)

Obligation Tactic := idtac.
Global Program Instance Subrelation_eq_dominated : subrelation (@pointwise_relation A B eq) (dominated _).
Next Obligation.
  introv h. eapply subrelation_norm_le_dominated. intro. rewrite h. reflexivity.
Qed.

Obligation Tactic := eauto using dominated_reflexive, dominated_transitive.
Global Program Instance Reflexive_dominated : Reflexive (dominated _).
Global Program Instance Transitive_dominated : Transitive (dominated _).

(* Proving a domination goal often requires re-arranging the left- and right-hand
   sides. This can be done as follows. *)

Lemma dominated_upto:
  forall f1 f2 g1 g2,
  dominated _ f1 g1 ->
  (forall a, norm (f2 a) <= norm (f1 a)) ->
  (forall a, norm (g1 a) <= norm (g2 a)) ->
  dominated _ f2 g2.
Proof.
  intros.
  eapply dominated_transitive; [ eapply subrelation_norm_le_dominated | ].
    1: eauto.
  eapply dominated_transitive; [ | eapply subrelation_norm_le_dominated ].
    2: eauto.
  eauto.
Qed.

End DominationProperties.

Ltac prove_pw_le :=
  intros;
  repeat match goal with
  | x: (_ * _)%type |- _ =>
      destruct x
  end;
  try reflexivity.

Ltac dominated_upto tactic :=
  eapply dominated_upto; [ tactic tt | prove_pw_le | prove_pw_le ].

(* ---------------------------------------------------------------------------- *)

(* The following lemmas are specialized to the case where [B] is [nat],
   although some of them could perhaps be generalized to an arbitrary ring [B],
   provided the [norm] function ultimately commutes with the ring operation. *)

Section DominationPropertiesNat.

Context {A} {ultimately} `{@Filter A ultimately}.

(* Pointwise inequality implies domination. *)

Lemma subrelation_le_dominated:
  forall f g : A -> nat,
  pw le f g ->
  dominated _ f g.
Proof.
  intros. eapply subrelation_norm_le_dominated. eauto.
Qed.

Obligation Tactic := repeat intro; eauto using subrelation_le_dominated.
Global Program Instance Subrelation_le_dominated: subrelation (pw le) (dominated _).

(* TEMPORARY prove that [dominated ultimately1] implies [dominated ultimately2]
   if [ultimately1] is coarser. *)

(* A maximum is dominated by a sum. *)

Lemma dominated_max_sum:
  forall f g : A -> nat,
  dominated _ (fun a => max (f a) (g a)) (fun a => f a + g a).
Proof.
  intros. exists 1. eapply filter_universe. intros. simpl. max_case; omega.
Qed.

(* Conversely, a sum is dominated by a maximum. [max] and [+] are asymptotically
   equivalent. *)

Lemma dominated_sum_max:
  forall f g : A -> nat,
  dominated _ (fun a => f a + g a) (fun a => max (f a) (g a)).
Proof.
  intros. exists 2. eapply filter_universe. intros. simpl. max_case; omega.
Qed.

(* Domination is compatible with sum. *)

Lemma dominated_sum:
  forall f1 f2 g1 g2 : A -> nat,
  dominated _ f1 g1 ->
  dominated _ f2 g2 ->
  dominated _ (fun a => f1 a + f2 a) (fun a => g1 a + g2 a).
Proof.
  introv [ c1 u1 ] [ c2 u2 ].
  exists (max c1 c2).
  eapply filter_closed_under_intersection.
    eexact u1.
    eexact u2.
  simpl. introv h1 h2. ring_simplify. max_ub_as h3 h4.
  (* At this point, it is just a matter of rewriting with inequalities. *)
  rewrite h1. rewrite h2. rewrite h3. rewrite h4. reflexivity.
Qed.

(* Domination is compatible with product. *)

Lemma dominated_product:
  forall f1 f2 g1 g2 : A -> nat,
  dominated _ f1 g1 ->
  dominated _ f2 g2 ->
  dominated _ (fun a => f1 a * f2 a) (fun a => g1 a * g2 a).
Proof.
  introv [ c1 u1 ] [ c2 u2 ].
  exists (c1 * c2).
  eapply filter_closed_under_intersection.
    eexact u1.
    eexact u2.
  simpl. introv h1 h2. ring_simplify.
  (* At this point, it is just a matter of rewriting with inequalities. *)
  rewrite h1. rewrite h2. ring_simplify. reflexivity.
Qed.

(* [c * f] is dominated by [f]. *)

Lemma dominated_scalar_product:
  forall f : A -> nat,
  forall c,
  dominated _ (fun a => c * f a) f.
Proof.
  intros.
  exists c.
  eapply filter_universe.
  eauto.
Qed.

(* Domination is compatible with maximum. *)

Lemma dominated_max:
  forall f1 f2 g1 g2 : A -> nat,
  dominated _ f1 g1 ->
  dominated _ f2 g2 ->
  dominated _ (fun a => max (f1 a) (f2 a)) (fun a => max (g1 a) (g2 a)).
Proof.
  introv [ c1 u1 ] [ c2 u2 ].
  exists (max c1 c2).
  eapply filter_closed_under_intersection.
    eexact u1.
    eexact u2.
  simpl. introv h1 h2. rewrite h1. rewrite h2.
  max_case_m_n_as c1 c2 h; rewrite h;
  rewrite Nat.mul_max_distr_l; reflexivity.
Qed.

(* ---------------------------------------------------------------------------- *)

(* If [f] is dominated by [g], then [f + g] is dominated by [g]. *)

Lemma dominated_right:
  forall f g : A -> nat,
  dominated _ f g ->
  dominated _ (fun a => f a + g a) g.
Proof.
  intros.
  (* TEMPORARY could use rewriting if set up *)
  eapply dominated_transitive.
  eapply dominated_sum.
    eauto.
    eapply dominated_reflexive.
  dominated_upto ltac:(fun _ => eapply dominated_scalar_product with (c := 2)).
  simpl. omega.
Qed.

Lemma dominated_left:
  forall f g : A -> nat,
  dominated _ f g ->
  dominated _ (fun a => g a + f a) g.
Proof.
  intros.
  dominated_upto ltac:(fun _ => eapply dominated_right; eauto).
  simpl. omega.
Qed.

(* If [f] and [g] are dominated by [h], then [f + g] is dominated by [h]. *)

Lemma dominated_sum_split:
  forall f g h : A -> nat,
  dominated _ f h ->
  dominated _ g h ->
  dominated _ (fun a => f a + g a) h.
Proof.
  intros.
  (* TEMPORARY could use rewriting if set up *)
  eapply dominated_transitive.
    eapply dominated_sum; eauto.
  eapply dominated_right.
  eapply dominated_reflexive.
Qed.

(* ---------------------------------------------------------------------------- *)

(* If [f] is a function of two parameters [a] and [i] into [nat], then
   [cumul lo f] is a function of two parameters [a] and [hi] into [nat].
   It is the sum, for [i] varying in [interval lo hi], of [f (a, i)]. *)

Definition cumul {A : Type} (lo : nat) (f : A * nat -> nat) : A * nat -> nat :=
  fun ahi => let (a, hi) := ahi in
  \big[plus]_(i <- interval lo hi) (f (a, i)).

(* Domination is compatible with cumulated sums. *)

(* This lemma is NOT proved under the default filter for [A * nat], which
   would be the product of [ultimately] and [towards_infinity]. That would
   mean that [f] and [g] are required to be comparable only for sufficiently
   large values of the index [i]. In fact, it seems that we need this
   hypothesis to hold for every value of [i] at and above [lo]. Perhaps this
   makes the lemma too weak; we shall see. *)

Lemma dominated_big_sum:
  forall f g : A * nat -> nat,
  forall lo : nat,
  dominated (product ultimately (at_and_above lo))           f            g ->
  dominated (product ultimately (at_and_above lo)) (cumul lo f) (cumul lo g).
Proof.
  unfold cumul.
  (* For a certain multiplicative constant [c], for every sufficiently large [a],
     and for every [i] at and above [lo], the inequality between [f] and [g] holds. *)
  introv [ c [ largeA [ largeI [ ? [ ? ? ]]]]]. intros.
  unfold at_and_above in *.
  simpl in *.
  (* The multiplicative constant is still [c]. *)
  exists c. exists largeA. exists (le lo).
  repeat split; eauto with filter.
  intros a hi. intros. simpl.
  (* Let's prove this inequality. Things work well because we have requested
     the inequality between [f] and [g] to hold for every [i] at and above [lo],
     as opposed to for sufficiently large [i]. Otherwise, we would have to split
     the range of [i] in two intervals, and we would be in trouble in the lower
     interval, because we would have no relationship between [f] and [g], and
     no constant bound, except in the special case where the parameter [a] has
     type [unit]. *)
  rewrite big_map_distributive by eauto with typeclass_instances.
  eapply big_covariant; eauto using in_interval_lo with omega typeclass_instances.
Qed.

(* A test. (TEMPORARY) *)

Goal dominated (product _ (at_and_above 0)) (fun ai => let (a, i) := ai in i) (fun ai => let (a, i) := ai in a).
Proof.
  exists 1. exists (le 0). exists (le 0). repeat split; eauto with filter.
  intros. ring_simplify. simpl.
  (* PROBLEM: this is false, of course. We lack the hypothesis that [i] is
     less than [i], so cannot prove i = O(a). *)
Abort. (* OK *) (* TEMPORARY *)

End DominationPropertiesNat.

(* ---------------------------------------------------------------------------- *)

(* The beginning of a tactic that helps prove domination goals. *)

Ltac dominated :=
  repeat first [
    rewrite dominated_scalar_product
  | eapply dominated_sum_split
  | eapply dominated_reflexive
  ].

(* ---------------------------------------------------------------------------- *)

Section ParameterTransformation.

Context {I} {ultimatelyI} `{Filter I ultimatelyI}.
Context {J} {ultimatelyJ} `{Filter J ultimatelyJ}.
Context {B} `{Norm B}.

(* This lemma offers a general mechanism for transforming the parameters
   of the asymptotic analysis. *)

(* Let [f] and [g] be functions of a parameter [j]. Assume [f] is dominated
   by [g]. Now, let [p] be a function of [I] into [J], which defines [j] in
   terms of [i]. Assume that [p i] becomes arbitrarily large as [i] grows. 
   Then, [f . p] is dominated by [g . p]. These are functions of [i]. *)

(* The converse implication is false, as the image of the function [p] could
   lie in a subset of well-chosen values of [j] outside of which [f] is not
   dominated by [g]. *)

(* This lemma is analogous to [domin_comp] in Coquelicot. *)

Lemma parameter_transformation:
  forall f g : J -> B,
  dominated _ f g ->
  forall p : I -> J,
  limit _ _ p ->
  dominated _ (fun i => f (p i)) (fun i => g (p i)).
Proof.
  (* The statement is really quite obvious, since [dominated] is defined
     in terms of [ultimately], and [limit _ _ p] means precisely that [p]
     maps [ultimately] to [ultimately]. *)
  introv [ c u ] hp.
  (* The multiplicative factor is unaffected by the transformation. *)
  exists c.
  (* The hypothesis [u] states that for large enough [j], [f j] is
     bounded by [c] times [g j]. The hypothesis [hp] states that
     [p i] becomes arbitrarily large as [i] becomes large enough.
     The result follows directly from the combination of these
     hypotheses. *)
  eapply filter_closed_under_inclusion.
    eapply hp. eexact u.
    simpl. eauto.
Qed.

(* Note: the conclusion of the above lemma could be rephrased as follows. *)

Goal 
  forall f g : J -> B,
  forall p : I -> J,
   dominated          ultimatelyI                           (fun i => f (p i)) (fun i => g (p i)) <->
  @dominated _ (image ultimatelyI p) (filter_image _ _) B _           f                  g.
Proof.
  intros. unfold dominated, image. tauto.
Qed.

End ParameterTransformation.

(* Close the section, so as to allow instantiating the previous lemma with new
   values of [I] and [J]. *)

Section ParameterManipulation.

Context {I} {ultimatelyI} `{Filter I ultimatelyI}.
Context {J} {ultimatelyJ} `{Filter J ultimatelyJ}.
Context {B} `{Norm B}.

(* This lemma allows getting rid of one parameter, by instantiating it
   with a function of the other parameters. *)

(* Let [f] and [g] be functions of two parameters [i] and [j] -- that is, more
   precisely, functions of a pair [(i, j)]. Assume [f] is dominated by [g], in
   the sense of the product filter. Now, let [p] be a function of [I] into
   [J], which defines [j] in terms of [i]. Assume that [p i] becomes
   arbitrarily large as [i] grows. Then, [fun i => f (i, p i)] is dominated by
   [fun i => g (i, p i)]. *)

Lemma parameter_instantiation:
  forall f g : I * J -> B,
  dominated _ f g ->
  forall p : I -> J,
  limit _ _ p ->
  dominated _
    (fun i => f (i, p i))
    (fun i => g (i, p i)).
Proof.
  intros.
  dominated_upto ltac:(fun _ => eapply @parameter_transformation with (p := (fun i => (i, p i))); eauto).
  (* Check that [(i, p i)] gets arbitrarily large as [i] grows. *)
  eauto using limit_pair, limit_id.
Qed.

(* This lemma allows introducing an (unnecessary) parameter. *)

Lemma parameter_weakening:
  forall f g : I -> B,
  dominated _ f g ->
  dominated _
    (fun ij : I * J => let (i, j) := ij in f i)
    (fun ij : I * J => let (i, j) := ij in g i).
Proof.
  intros.
  dominated_upto ltac:(fun _ => eapply @parameter_transformation with (p := fst); eauto).
  eauto using limit_fst.
Qed.

End ParameterManipulation.

