Require Import Coq.Logic.Classical_Pred_Type.
Require Import LibTactics.
Require Import LibNatExtra.
Require Import LibSet.
Require Import Filter.

(* If [ultimately] is a filter on [A], and if [P] is a subset of [A],
   then [within ultimately P] is also a filter on [A]. A formula [Q]
   is ultimately true within [P] if and only if [P -> Q] is ultimately
   true. *)

Definition within
  {A : Type} ultimately `{Filter A ultimately}
  (P : set A)
: filter A :=
  fun (Q : set A) => ultimately (fun x => P x -> Q x).

(* There is a sanity condition on [P], though. The formula [P] must not forbid
   `going to the limit' in the sense of [ultimately]. To take a concrete
   example, if our initial filter is [towards_infinity], then it would not
   make sense for [P] to be the property [fun n => n <= k]. If we did choose
   such a [P], then [within towards_infinity P] would be a filter that says
   `when [n] tends towards infinity while remaining under [k]'. This makes no
   sense.

   What is an appropriate condition on [P]? We could require [ultimately P],
   but that would be too strong; if ultimately [P] holds, then ultimately [P
   -> Q] is equivalent to ultimately [Q]. (Another way to see that this is
   wrong is to take an example where [P] is [fun (i, n) => i <= n]. This
   property does not hold ultimately, yet it does make sense.)

   An appropriate condition seems to be [~ ultimately ~P]. Indeed, if [P] is
   ultimately false, this means that [P] `forbids going to the limit'. We can
   see that if [P] is ultimately false, then [P -> Q] is ultimately true,
   regardless of [Q], and that is not a good thing, as we expect [ultimately
   (P -> Q)] to imply something about [Q]. Technically, the condition
   [~ ultimately ~P] seems to be exactly what is needed in order to prove that
   [within ultimately P] does not accept an empty [Q].

   In fact, the double negation is not necessary. One can reformulate this
   condition in a positive manner, by saying that, for every [Q] that holds
   ultimately, the intersection [P /\ Q] is nonempty. This is a positive
   way of saying that [P] does not forbid going to infinity -- in fact, [P]
   has elements that are as close to infinity as desired. This positive
   formulation appears in Coquelicot; see the lemma [subset_filter_proper]. *)

Goal
  forall {A : Type} ultimately `{Filter A ultimately},
  forall P,
  ~ ultimately (fun a => ~P a) <->
  forall Q, ultimately Q -> exists a, P a /\ Q a.
Proof.
  split.

  (* Left-to-right implication. *)
  (* Reductio by absurdum. If there did not exist [a] that satisfies [P /\ Q],
     then [~ (P /\ Q)] would hold everywhere. *)
  introv hnunP huQ. eapply not_all_not_ex; intro hnPQ.
  (* Thus, [~ (P /\ Q)] would hold ultimately. *)
  assert (hunPQ: ultimately (fun a => ~ (P a /\ Q a))).
    eapply filter_universe. eexact hnPQ.
  (* However, by hypothesis, [Q] holds ultimately. By combining these facts,
     we find that [~P] holds ultimately. *)
  assert (ultimately (fun a => ~ P a)).
    eapply filter_closed_under_intersection.
      eexact huQ.
      eexact hunPQ.
      eauto.
  (* This contradicts the hypothesis [~ ultimately ~P]. *)
  tauto.

  (* Right-to-left implication. *)
  intros hx hunP.
  destruct (hx _ hunP).
  tauto.

Qed.

(* Let us make this condition a type class. *)

Class NonBlocking
  {A : Type} ultimately `{Filter A ultimately}
  (P : set A)
:= {
  nonblocking:
    forall Q, ultimately Q -> exists a, P a /\ Q a
}.

(* Subject to the condition studied above, [within _ P] is a filter. *)

Instance filter_within
  {A : Type} ultimately `{Filter A ultimately}
  (P : set A)
  `{@NonBlocking _ _ _ P}
:
  Filter (within _ P).
Proof.
  unfold within. econstructor.
  (* 1. There exists an element in this filter, namely the universe. *)
  exists (fun a : A => True). eapply filter_universe. eauto.
  (* 2. Every element of this filter is nonempty. *)
  intros Q hQ. destruct (nonblocking _ hQ) as [ a [ ? ? ]]. eauto.
  (* 3. Closure by intersection and subset. *)
  introv h1 h2 ?.
  eapply filter_closed_under_intersection.
    eexact h1.
    eexact h2.
    eauto.
Qed.

