Set Implicit Arguments.
Generalizable All Variables.
Require Import LibTactics.
Require Import LibNatExtra.
Require Import Filter.

(* The filter [at_and_above m] intuitively represents validity at and
   above [m]. *)

Definition at_and_above (m : nat) (F : nat -> Prop) :=
  forall n, m <= n -> F n.

(* This instance declaration can be problematic, as it introduces a
   non-standard filter at type [nat]. This can create surprises in
   the automated search for instances. *)

Instance filter_at_and_above : forall m, Filter (at_and_above m).
Proof.
  unfold at_and_above. econstructor.
  (* There exists an element in this filter, namely the universe, [le 0]. *)
  exists (fun n => 0 <= n). eauto with omega.
  (* Every element of the filter contains [m], hence is nonempty. *)
  intros. exists m. eauto.
  (* Closure by intersection and subset. *)
  eauto with omega.
Qed.

(* Every subset of the form [le n], where [n <= m], is a member of this
   filter. *)

Lemma at_and_above_le:
  forall m n,
  n <= m ->
  at_and_above m (le n).
Proof.
  unfold at_and_above. eauto with omega.
Qed.

Hint Resolve at_and_above_le : filter.

