Set Implicit Arguments.
Generalizable All Variables.
Require Import LibTactics.
Require Import LibNatExtra.
Require Import Filter.
Require Import Norm.
Require Import Dominated.
Require Import FilterTowardsInfinity.

(* TEMPORARY prove these facts for the finest possible filter? *)
(* TEMPORARY try to prove these facts for an arbitrary parameter type [A]
             could be done by parameter transformation *)

(* Common domination facts. *)

(* n^k1 = O(n^k2), if k1 <= k2. *)

Lemma dominated_power_k1_k2:
  forall k1 k2,
  k1 <= k2 ->
  dominated _ (fun n => n^k1) (fun n => n^k2).
Proof.
  (* The constant is 1. *)
  exists 1. 
  (* The assertion is true for [n >= 1]. *)
  exists 1.
  (* Check that the inequality holds. *)
  intros. simpl. ring_simplify. eauto with monotonic.
Qed.

(* 1 = O(n). *)

Lemma dominated_one_n:
  dominated _ (fun n => 1) (fun n => n).
Proof.
  dominated_upto ltac:(fun _ => eapply dominated_power_k1_k2 with (k1 := 0) (k2 := 1)).
  eauto.
  simpl. ring_simplify. eauto.
Qed.

(* A constant function is dominated by any nonzero function. *)

Lemma dominated_constant_general:
  forall c g,
  towards_infinity (fun n => 0 < g n) ->
  dominated towards_infinity (fun _ => c) g.
Proof.
  introv [ m ? ].
  (* The constant is [c]. *)
  exists c.
  (* The assertion is true for [n >= m]. *)
  exists m.
  (* We must check that the inequality holds. *)
  intros. simpl. eauto using mult_magnifies_right.
Qed.

(* Every constant is O(1). *)

Lemma dominated_constant:
  forall c,
  dominated _ (fun a => c) (fun a => 1).
Proof.
  intros. eapply dominated_constant_general.
  exists 0. intros. omega.
Qed.

