Set Implicit Arguments.
Require Import Coq.ZArith.BinInt.

(* A norm is a function into [nat]. *)

Class Norm (B : Type) := {
  norm: B -> nat
}.

(* The identity function is a norm on [nat]. *)

Instance Norm_nat : Norm nat := {
  norm n := n
}.

(* The absolute value function is a norm on [Z]. *)

Instance Norm_Z : Norm Z := {
  norm := Zabs_nat
}.

