#!/bin/bash
set -euo pipefail

# This script creates a self-contained archive with TLC, CFML, UnionFind.

# A command to get the current git hash.
HASH="git log -1 --pretty=%H"
# A command to get the current date.
DATE="/bin/date +%Y%m%d"
# GNU cp supports -L.
# We need to follow symbolic links when copying TLC and CFML.
if command -v gcp >/dev/null ; then CP="gcp -L" ; else CP="cp -L" ; fi

# The name of the archive directory.
ARCHIVE=UnionFind
# The name of the main archive subdirectory.
MAIN=UnionFind

# Coq's user-contrib directory.
CONTRIB=`coqc -where`/user-contrib
# Where to find TLC.
TLC=$CONTRIB/TLC
# Where to find CFML.
CFML=$CONTRIB/CFML

# Wipe out a previous archive.
rm -rf $ARCHIVE $ARCHIVE.tar.gz

# Create the archive.
mkdir $ARCHIVE
$DATE > $ARCHIVE/DATE
$CP INSTALL README $ARCHIVE
$CP -r Makefile.export $ARCHIVE/Makefile

# Copy TLC into the archive.
echo "Copying TLC..."
mkdir $ARCHIVE/TLC
(cd $TLC && $HASH) > $ARCHIVE/TLC/HASH
$CP -r $TLC/{*.v,Makefile,Makefile.coq} $ARCHIVE/TLC

# Copy CFML into the archive.
echo "Copying CFML..."
$CP -r $CFML $ARCHIVE/
(cd $CFML && $HASH) > $ARCHIVE/CFML/HASH
make -C $ARCHIVE/CFML clean > /dev/null

# Copy a subset of our files into the main archive subdirectory.
echo "Copying UnionFind..."
mkdir $ARCHIVE/$MAIN
$HASH > $ARCHIVE/$MAIN/HASH
$CP -r Makefile math src proof $ARCHIVE/$MAIN
make -C $ARCHIVE/$MAIN clean > /dev/null

# Create a .tar file, filtering out the files that we do not wish or
# need to publish.
echo "Creating archive..."
tar \
  -X ../.gitignore \
  --exclude=.git \
  --exclude=.gitignore \
  --exclude=attic \
  --exclude=CFML/bin \
  --exclude=CFML/dev \
  --exclude=CFML/examples \
  --exclude=CFML/model \
  --exclude=TODO \
  --exclude=math/Big.v \
  --exclude=math/Dominated*.v \
  --exclude=math/FilterEverywhere.v \
  --exclude=math/FilterAtAndAbove.v \
  --exclude=math/FilterWithin.v \
  --exclude=math/Master.v \
  --exclude=math/Norm.v \
  --exclude=math/rename \
  --exclude=src/Main.ml \
  --exclude=src/Null.ml \
  --exclude=src/Store*.ml \
  --exclude=src/*.mli \
  --exclude=open.sh \
  -cvz -f $ARCHIVE.tar.gz $ARCHIVE

# Remove the archive directory, and re-create it from the .tar file.
rm -rf $ARCHIVE
tar xfz $ARCHIVE.tar.gz
# Remove our local installation of TLC and CFML, so as to make sure that
# they are not used!
echo "Uninstalling TLC and CFML..."
(cd $CONTRIB && mv TLC TLC.bak && mv CFML CFML.bak)
function finish {
  echo "Reinstalling TLC and CFML..."
  (cd $CONTRIB && mv TLC.bak TLC && mv CFML.bak CFML)
  echo "Done."
}
trap finish EXIT
# Try to compile.
echo "Testing installation..."
make -C $ARCHIVE -j16
