(* When OCaml's built-in store is used, no explicit store is needed. *)

type 'a store =
  unit

let new_store () =
  ()

(* A reference is a primitive reference. *)

type 'a rref =
  'a ref

let make () v =
  (), ref v

let get () x =
  (), !x

let set () x v =
  x := v

let eq () x y =
  (), x == y

