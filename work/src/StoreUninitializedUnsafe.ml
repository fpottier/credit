open Store

module Make (S : STORE) = struct

  include S

  exception Uninitialized

  let get s r =
    let _, ox as sox = S.get s r in
    if Null.is_null ox then
      raise Uninitialized
    else
      sox

  let make_uninitialized s =
    S.make s Null.null

end

