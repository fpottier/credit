open Store

(* This module offers an implementation of [STORE] based on mutable references. *)

include STORE
  with type 'a store = unit
   and type 'a rref = 'a ref
