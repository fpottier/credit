open Store

(* This functor offers an implementation of [STORE_UNINIT], which is unsafe in
   the sense that it uses a null pointer internally. Its use is safe unless
   one exploits the equality ['a store = 'a S.store] in a bad way --
   forgetting that the store can contain uninitialized references. *)

module Make (S : STORE) : STORE_UNINIT
  with type 'a store = 'a S.store
   and type 'a rref = 'a S.rref

