(* This module is unsafe. Do not use it. *)

val null: 'a
val is_null: 'a -> bool
