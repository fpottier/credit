open Store

(* This module offers an implementation of [STORE] based on immutable integer maps.
   The stores thus obtained are persistent. *)

module Make (IntMap : sig
  type 'a t
  val empty: 'a t
  val find: int -> 'a t -> 'a
  val add: int -> 'a -> 'a t -> 'a t
end) : STORE

(* TEMPORARY [include] not supported by CFML

(* The easiest way of instantiating the above functor is with integer maps found
   in the standard library. This is done here. *)

include STORE

*)

