open Store

(* This functor offers a safe implementation of [STORE_UNINIT]. *)

module Make (S : STORE) : STORE_UNINIT
  with type 'a store = 'a option S.store
   and type 'a rref = 'a option S.rref

