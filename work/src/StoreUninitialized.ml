open Store

module Make (S : STORE) = struct

  type 'a store =
      'a option S.store

  let new_store =
    S.new_store

  type 'a rref =
      'a option S.rref

  let make s x =
    S.make s (Some x)

  exception Uninitialized

  let get s r =
    let s, ox = S.get s r in
    match ox with
    | None ->
        raise Uninitialized
    | Some x ->
        s, x

  let set s r x =
    S.set s r (Some x)

  let eq =
    S.eq

  let make_uninitialized s =
    S.make s None

end

