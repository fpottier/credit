open Ocamlbuild_plugin
open Command

let () =
  dispatch (fun event ->
    match event with
    | After_rules ->
        (* Add the flag -S when compiling to native code. *)
        flag ["ocaml"; "compile"; "native"] (S [A "-S"]);
        (* Enable a lot of warnings. *)
        flag ["ocaml"; "compile"] (S [A "-w"; A "@1..3@8..12@14..21@23..40-41@43-33"])
    | _ ->
        ()
  )

