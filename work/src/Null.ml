(* We use a unique memory cell as a null pointer. *)
let null =
  ref ()
let null =
  Obj.magic null

let is_null x =
  null == x

