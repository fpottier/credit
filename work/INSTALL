These instructions assume that you are using a UNIX system.

A. You need OCaml. Any version of OCaml >= 4.01.0 should work.

   If you do not have OCaml, we suggest that you install it by installing its
   package manager, opam. The following commands might work:

     wget https://raw.github.com/ocaml/opam/master/shell/opam_installer.sh
     sudo /bin/sh ./opam_installer.sh /usr/local/bin

   If they don't, please consult https://opam.ocaml.org/doc/Install.html

B. You need the pprint library for OCaml.

     opam install pprint

C. You need Coq 8.5.x. If you do not have it, install it via OPAM:

     opam repo add coq-released https://coq.inria.fr/opam/released
     opam update
     opam install -j4 -v coq.8.5.3

   The flag -j4 is optional; it speeds things up. Use as many cores as you wish.

   (Newer versions of Coq might work too, but have not been tested.)

D. You should be good to go:

     make -j4

   This should compile TLC and CFML first,
   then check our proofs concerning UnionFind.
