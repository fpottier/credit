Set Implicit Arguments.
(* Load TLC. *)
Require Import LibTactics.
Require Import LibFunc LibSet LibRelation LibPer.
Require Import TLCBuffer.
(* Load extra math libraries. *)
Require Import LibNatExtra.
Require Import InverseAckermann.
(* Load the mathematical analysis of UnionFind. *)
Require Import UnionFind01Data.
Require Import UnionFind02EmptyCreate.
Require Import UnionFind03Link.
Require Import UnionFind04Compress.
Require Import UnionFind05IteratedCompression.
Require Import UnionFind06Join.
Require Import UnionFind11Rank.
Require Import UnionFind12RankEmptyCreate.
Require Import UnionFind13RankLink.
Require Import UnionFind14RankCompress.
Require Import UnionFind15RankJoin.
Require Import UnionFind41Potential.
Require Import UnionFind43PotentialAnalysis.
Require Import UnionFind44PotentialJoin.
(* Load the CFML library, with time credits. *)
Require Import CFLib.
Require Pervasives_ml.
Require Import Pervasives_proof.
Require Import CFLibCreditsNat.
(* Load the Coq version (produced by the CFML tool) of the OCaml code
   [UnionFind.ml]. Avoid using [Import] here, as it is preferable to
   avoid polluting our namespace. *)
Require UnionFind_ml.


(* -------------------------------------------------------------------------- *)

(* Open a section. This allows us to parameterize the whole file over certain
   parameters, such as the type [data] of the data stored at the roots.
   Furthermore, this makes all [Hints] local, which is good. *)

Section UnionFind.

(* The parameter [data] is the type of the data stored at the roots.
   It corresponds to the type variable ['a] in the OCaml code. *)

Variable data : Type.

(* For convenience, import types and data constructors into our namespace. *)

Notation "'rank'"    := (UnionFind_ml.rank_).
Notation "'elem'"    := (UnionFind_ml.elem_ data).
Notation "'content'" := (UnionFind_ml.content_ data).
Notation "'Link'"    := (UnionFind_ml.Link).
Notation "'Root'"    := (UnionFind_ml.Root).

(* Hints for the type-checker. *)

(* It seems important that [D], [R], and [V] which are the public parameters to
   [UF], mention [elem] in their types, as opposed to [loc]. Internally [elem]
   is equal to [loc]. Ultimately, however, the type [elem] is made opaque. *)

Implicit Types x y r : elem.
Implicit Types v : data.
Implicit Types D : set elem.         (* domain *)
Implicit Types F : binary elem.      (* edges *)
Implicit Types K : elem -> nat.      (* rank *)
Implicit Types R : elem -> elem.     (* functional view of [is_repr F] *)
Implicit Types V : elem -> data.     (* data stored at the roots *)
Implicit Types M : map elem content. (* CFML's view of the memory *)

(* TLC views the operation of reading a finite map, [M[x]], as total. This
   requires the codomain of the map to be inhabited. Thus, we must assume
   that the type [data] is inhabited, and prove that the type [content] is
   inhabited. *)

Variable Inhab_data : Inhab data.
Existing Instance Inhab_data.

Global Instance Inhab_content : Inhab content.
Proof.
  intros. applys (prove_Inhab (Root 0 arbitrary)).
Qed.

(* -------------------------------------------------------------------------- *)

(* Invariants and representation predicate. *)

(* The predicate [Mem ...] relates the mathematical graph encoded by [D/F/K/V]
   and the memory encoded by the finite map [M]. In short,
    1. the domain of [M] coincides with [D];
    2. the links in [M] coincide with the links in [F];
    3. the rank and data stored at a root in [M] agree with [K] and [V]. *)

Record Mem D F K V M : Prop := {
  Mem_dom :
    dom M = D;
  Mem_val :
    forall x, x \in D ->
    match M[x] with
    | Link y   => F x y
    | Root k v => is_root F x /\ k = K x /\ v = V x
    end
 }.

(* An alternate form, shown in the paper. *)

Goal
  forall D F K M V,
  Mem D F K V M <->
  dom M = D /\
  forall x, x \in D ->
  match M[x] with
  | Link y   => F x y
  | Root k v => is_root F x /\ k = K x /\ v = V x
  end.
Proof using.
  intros. split; inversion 1; econstructor; intuition eauto.
Qed.

(* The predicate [Inv ...] relates the mathematical graph encoded by [D/F/K/V]
   and the view that is exposed to the client, which is encoded by [D/R/V].
   In short,
   1. [D/F/K] form a ranked disjoint set forest, as per [is_rdsf];
   2. [R] is included in [is_repr F],
      which means that [R x = y] implies [is_repr F x y],
      or in other words, [R] agrees with [F];
   3. [V] agrees with [R]. *)


Record Inv D F K R V : Prop := {
  Inv_rdsf  : is_rdsf D F K;
  Inv_incl  : incl_fr R (is_repr F);
  Inv_data  : forall x, V x = V (R x)
}.

(* An alternate form, shown in the paper. *)

Goal
  forall D F K R V,
  Inv D F K R V <->
    is_rdsf D F K /\
    (forall x, is_repr F x (R x)) /\
    (forall x, V x = V (R x)).
Proof using.
  intros. split; inversion 1; econstructor; intuition eauto.
Qed.

(* Throughout, we instantiate the parameter [r] of Alstrup et al.'s proof
   with the value 1. Thus, we write just [Phi] for [Phi 1]. *)

Notation Phi := (Phi 1).

(* [UF D R V] is the representation predicate. It is an abstract predicate: the
   client uses this predicate, but does not know how it is defined. *)

(* The definition asserts the existence (and ownership) of a group of references,
   whose content is described by a finite map [M]. It asserts the two invariants
   above. Finally, it asserts the existence (and ownership) of [Phi] time credits,
   which have been set aside to help pay for future operations. The definition
   quantifies existentially over [F/K/M], which therefore are not exposed to the
   client. *)

Definition UF D R V : hprop :=
  Hexists F K M,
  Group Ref M \*
  \[ Inv D F K R V ] \*
  \[ Mem D F K V M ] \*
  \$ (Phi D F K).

(* -------------------------------------------------------------------------- *)

(* Private hints, for the proofs that follow. *)

(* The references that we read and write are part of a group. Use appropriate
   specifications for the primitive operations [ml_get] and [ml_set]. *)

Hint Extern 1 (RegisterSpec Pervasives_ml.infix_emark__) =>
  Provide (@Pervasives_proof.infix_emark_spec_group content).
Hint Extern 1 (RegisterSpec Pervasives_ml.infix_colon_eq__) =>
  Provide (@Pervasives_proof.infix_colon_eq_spec_group content).

Hint Resolve Inv_rdsf Inv_incl Inv_data.
Hint Resolve is_rdsf_is_dsf.

(* This is needed for [hsimpl] to work well on [UF]. *)

Ltac hsimpl_hook H ::=
  match H with
  | UF _ _ _ => hsimpl_try_same tt
  end.

(* The type [rank] is just an abbreviation for [int]. Unfold it when needed. *)

Ltac math_0 ::= unfolds rank.
Hint Extern 1 (_ = _ :> rank) => math.

(* Miscellaneous. *)

Hint Extern 1 (?x \in ?D) => congruence.
Hint Extern 1 (Inhab content) => apply Inhab_content.

Ltac reveal_loc :=
  unfold UnionFind_ml.elem_, Pervasives_ml.ref_ in *.

(* -------------------------------------------------------------------------- *)

(* The functions [V] and [R] are compatible with [R]. In other words, every
   member of an equivalence class must have the same image through [V] (see
   [Inv_data]) and through [R]. For this reason, when we wish to update one of
   these functions, we must update it not just at one point, but at a whole
   equivalence class. *)

(* The function [update1 R f x b] coincides with the function [f] everywhere,
   except on the equivalence class of [x], whose elements are mapped to [b]. *)

Definition update1 (B : Type) (f : elem -> B) R x (b : B) :=
  fcupdate f (fun z => R z = R x) b.

(* The function [update2 R f x y b] coincides with the function [f] everywhere,
   except on the equivalence classes of [x] and [y], whose elements are mapped
   to [b]. *)

Definition update2 (B : Type) (f : elem -> B) R x y b :=
  fcupdate f (fun z => R z = R x \/ R z = R y) b.

(* Updating [V] by mapping the equivalence classes of [x] and [x] to [V x]
   has no effect. *)

Lemma update2_V_self : forall D F K R V x,
  Inv D F K R V ->
  update2 V R x x (V x) = V.
Proof using.
  introv [ _ _ H ]. unfold update2.
  eapply fcupdate_self; intros.
  branches; rewrite H; congruence.
Qed.

(* If [x] is a root, then updating [R] by mapping the equivalence classes of [x]
   and [x] to [x] has no effect. *)

Lemma update2_R_self : forall R x,
  R x = x ->
  update2 R R x x x = R.
Proof using.
  intros. unfold update2.
  eapply fcupdate_self; intros.
  branches; congruence.
Qed.

(* Updating [f] at [R x] and [R y] is the same as updating [f] at [x] and [y]. *)

Lemma update2_root : forall (B : Type) (f : elem -> B) R x y b,
  idempotent R ->
  update2 f R (R x) (R y) b = update2 f R x y b.
Proof using.
  introv H. unfold update2. do 2 rewrite* H.
Qed.

(* Updating [V] at [x] and [y] is the same as updating [V] at [y] and [x]. *)

Lemma update2_sym : forall R V x y v,
  update2 V R x y v = update2 V R y x v.
Proof using.
  intros. extens. intros z. unfold update2.
  f_equal. clear z. extens; intros z. tauto.
Qed.

(* The function [link_by_rank_R], which describes an update of [R] during
   linking by rank, is in fact an instance of [update2]. *)

Goal
  forall R x y z,
  link_by_rank_R R x y z = update2 R R x y z.
Proof.
  reflexivity.
Qed.

(* -------------------------------------------------------------------------- *)

(* Exploitation and preservation lemmas for the invariant [Inv]. *)

(* If [x] is in the domain and [r] is its representative, then [r] is in the
   domain and [r] is its own representative. *)

Lemma Inv_root : forall D F K R V x r,
  Inv D F K R V ->
  x \in D ->
  r = R x ->
  r \in D /\ R r = r.
Proof using.
  intros; subst; split.
  applys* sticky_R.
  applys* idempotent_R.
Qed.

(* The invariant is preserved when a new element [r] is created and the
   function [V] is updated at [r] in an arbitrary manner. *)

Lemma Inv_make : forall D R F K V D' V' r v,
  Inv D F K R V ->
  r \notin D ->
  D' = D \u \{r} ->
  V' = update1 V R r v ->
  Inv D' F K R V'.
Proof using.
  introv HI Dr ED EV. subst D'. lets [Hrdsf Hroots Hdata]: HI.
  constructor.
  { eauto using is_rdsf_create. }
  { auto. }
  { subst V'. eapply fcupdate_absorbs; intros; eauto.
    rewrites* (>> idempotent_R R). }
Qed.

(* The invariant is preserved by updating [V] at one equivalence class. *)

Lemma Inv_update1 : forall D F K R V x v,
  Inv D F K R V ->
  Inv D F K R (update1 V R x v).
Proof using.
  introv [? ? ?]. constructors~; intros.
  eapply fcupdate_absorbs; intros; eauto.
  rewrites* (>> idempotent_R R).
Qed.

(* The invariant is preserved by a [link] operation. *)

(* The hypothesis is that the invariant holds and [x] and [y] are distinct roots
   in the domain. The conclusion is that the invariant holds again and the
   potential [Phi] grows by at most 2. The domain [D] is unchanged, while the
   other parameters [F'/K'/R'/V'/z] are described by five equations. *)

Lemma Inv_link: forall D R F K V F' K' V' x y z,
  (* Hypotheses: *)
  Inv D F K R V ->
  x <> y ->
  x \in D ->
  y \in D ->
  R x = x ->
  R y = y ->
  (* Five equations, describing the situation after the link: *)
  z = new_repr_by_rank K x y ->
  F' = link_by_rank_F F K x y ->
  K' = link_by_rank_K K x y ->
  V' = update2 V R x y (V z) ->
  let R' := link_by_rank_R R x y z in
  (* Conclusions: *)
  Inv D F' K' R' V' /\
  (Phi D F' K' <= Phi D F K + 2)%nat.
Proof using Inhab_data.
  clear Inhab_data.
  introv HI. intros. subst R' F' K' V' z. split. constructor.
  (* Preservation of [is_rdsf]. *)
  { eauto 8 using is_rdsf_link, R_self_is_root. }
  (* Preservation of the agreement between [R] and [F]. *)
  { eauto 8 using link_by_rank_R_link_by_rank_F_agree, R_self_is_root. }
  (* Preservation of the agreement between [V] and [R]. *)
  { intros w.
    unfold update2, fcupdate, link_by_rank_R. repeat case_if~.
    { false. forwards* HR: idempotent_R R. rewrite HR in *. tauto. }
    { rewrites~ <- (>> Inv_data HI). }
  }
  (* Change in potential. *)
  { eapply potential_increase_during_link; eauto using R_self_is_root. }
Qed.

(* This tactic helps apply the above lemma. The user is supposed to supply
   expressions for [F'/K'/V'/z], which we prove equal to what we expect.
   This allows some flexibility. *)

Ltac Inv_link F' K' V' x y z :=
  match goal with HInv: Inv ?D ?F ?K ?R ?V |- _ =>
    let Hpotential := fresh in
    (* Apply [Inv_link] to appropriate parameters. *)
    forwards* (?&Hpotential):
      (Inv_link
        (F' := F') (K' := K') (V' := V')
        (x := x) (y := y) (z := z) HInv
      );
    (* We expect to see some equations as sub-goals. We prove them by
       unfolding definitions and performing a simple case analysis. *)
    try solve [
       unfold new_repr_by_rank, link_by_rank_F, link_by_rank_K;
       case_if; solve [ eauto | math ]
    ];
    (* In the last subgoal, we have the conclusions of [Inv_link] at hand.
       In particular, the inequality [Hpotential] is a lower bound on the
       number of credits that remain. Exploit it and forget it. *)
    xgc_credit Hpotential;
    clear Hpotential
  end.

(* -------------------------------------------------------------------------- *)

(* Exploitation and preservation lemmas for the invariant [Mem]. *)

(* If [x] is in the domain and is a root, then [M] maps [x] to [Root]
   with rank [K x] and data [V x]. *)

Lemma Mem_root : forall D R F K M V x,
  Inv D F K R V ->
  Mem D F K V M ->
  x \in D ->
  R x = x ->
  M[x] = Root (K x) (V x).
Proof using.
  introv HI HM Dx Rxx.
  forwards~ HV: (Mem_val HM) x. destruct (M[x]).
    false* (>> R_self_is_root HI HV).
    destruct HV as (?&?&?). congruence.
Qed.

(* Conversely, if [M] maps [x] to [Root], then [x] is a root whose
   rank and data are as predicted by [M]. *)

Lemma Mem_root_inv : forall D F K M V x rx vx,
  Mem D F K V M ->
  x \in D ->
  M[x] = Root rx vx ->
  is_root F x /\ rx = K x /\ vx = V x.
Proof using.
  introv HM Dx HE. forwards~ HVx: (Mem_val HM) x.
  destruct (M[x]); tryfalse. inverts* HE.
Qed.

(* [Mem] is preserved when a new element [r] is created with rank 0 and the
   function [V] is updated at [r] in an arbitrary manner. *)

Lemma Mem_make : forall D R F K D' r M V v,
  Inv D F K R V ->
  Mem D F K V M ->
  is_rdsf D F K ->
  r \notin D ->
  D' = D \u \{r} ->
  Mem D' F K (update1 V R r v) (M[r:=Root 0 v]).
Proof using.
  introv HI [HD HV] HG Dr ED. constructors; subst D'.
  { rewrite~ dom_update; congruence. }
  { introv Dx.
    rewrite update_read_if. case_if. splits.
    { eauto using only_roots_outside_D with is_dsf. }
    { (* We use the fact that [K] is zero outside the domain. *)
      erewrite is_rdsf_zero_rank_outside_domain by eauto. math. }
    { unfold update1. rewrite fcupdate_hit; reflexivity. }
    { set_in; tryfalse.
      unfold update1. rewrite fcupdate_miss.
      { eapply HV; eauto. }
      { intro. forwards* Rr: R_is_identity_outside_D R r.
        forwards*: Inv_root x (R x). }}}
Qed.

(* [Mem] is preserved by installing a link from a root [x] to a root [y]
   without updating any rank. *)

Lemma Mem_link : forall D F R K F' M V V' x y,
  Inv D F K R V ->
  Mem D F K V M ->
  x \in D ->
  x = R x ->
  y = R y ->
  F' = link F x y ->
  V' = update2 V R x y (V y) ->
  Mem D F' K V' (M[x:=Link y]).
Proof using.
  introv HI [HD HR] Dx Rx Ry EG' HV. subst D F'. constructors; simpl.
  { rewrite~ dom_update_index. }
  intros a Da. specializes HR Da.
  rewrite update_read_if. case_if.
  { eauto using link_appears. }
  destruct (M[a]).
  { eauto using link_previous. }
  rewrite~ HV. destruct HR as (?&?&E). splits*.
  { applys* is_root_link. }
  rewrites (rm E).
  unfold update2, fcupdate. rewrites <- (rm Rx). rewrites <- (rm Ry).
  forwards* Ra: is_root_R_self a. rewrite Ra. case_if~.
  branches. false. congruence.
Qed.

(* [Mem] is preserved by installing a link from a root [x] to a root [y]
   and incrementing the rank of [y]. *)

Lemma Mem_link_incr : forall D F R K F' K' M V V' x y (rx : rank) v,
  Inv D F K R V ->
  Mem D F K V M ->
  x \in D ->
  y \in D ->
  x = R x ->
  y = R y ->
  x <> y ->
  rx = K x ->
  F' = link F x y ->
  K' = fupdate K y (1 + K x)%nat ->
  v = V y ->
  V' = update2 V R x y (V y) ->
  Mem D F' K' V' (M[x:=Link y][y:=Root (rx + 1) v]).
Proof using.
  introv HI HM Dx Dy Rx Ry HN Hy Kx HK. introv Hv HV'.
  forwards* HM': (>> Mem_link V V' x y HM).
  sets M': (M[x:=Link y]). clearbody M'.
  destruct HM' as [HD HV]; simpls. constructors; simpl.
  { rewrite~ dom_update_index. rewrite~ LibMap.index_def. }
  intros a Da. specializes HV Da. rewrite update_read_if. case_if. splits.
  { subst F'. applys* is_root_link. applys* R_self_is_root. }
  { subst K'. subst rx. unfold fupdate. case_if~. }
  { subst v. subst V'. unfold update2, fcupdate.
    rewrites <- (rm Rx). rewrites <- (rm Ry). case_if~. }
  destruct (M'[a]).
  { subst F'. auto. }
  { subst F' K'. destruct HV. split~. unfolds fupdate. case_if~. }
Qed.

(* [Mem] is preserved by installing a direct link from [x] to [y] during
   path compression. *)

Lemma Mem_compress : forall D F K F' M V x y,
  Mem D F K V M ->
  x \in D ->
  F' = compress F x y ->
  Mem D F' K V (M[x:=Link y]).
Proof using.
  introv [HD HR] Dx EG'. subst. constructors; simpl.
  { rewrite~ dom_update_index. }
  intros a Da. specializes HR Da.
  rewrite update_read_if. case_if.
  { eauto using compress_x_z. }
  destruct (M[a]).
  { eauto using compress_preserves_other_edges. }
  destruct HR. split*.
  eauto using compress_preserves_roots_other_than_x.
Qed.

(* [Mem] is preserved when [V] is updated at one equivalence class and [M]
   is updated at the representative element. *)

Lemma Mem_update1 : forall D R F K M V r x v,
  Inv D F K R V ->
  Mem D F K V M ->
  x \in D ->
  r = R x ->
  Mem D F K (update1 V R x v) (M[r := Root (K r) v]).
Proof using.
  introv HI [HM1 HM2] E Rx. forwards* (Dr&Rr): Inv_root x r.
  constructors~.
  { rewrite~ dom_update_index. rewrite index_def. subst~. }
  intros y Dy. specializes HM2 Dy. rewrite update_read_if. case_if. splits.
  { applys* R_self_is_root. congruence. }
  { subst~. }
  { unfold update1. rewrite fcupdate_hit. reflexivity. congruence. }
  { unfold update1, fcupdate. case_if~.
    destruct~ (M[y]). false. destruct HM2 as (H1&H2&H3).
    forwards*: is_root_R_self H1. congruence. }
Qed.

(* -------------------------------------------------------------------------- *)

(* Private lemmas about the representation predicate [UF]. *)

(* If [UF D R V] holds, then [Inv D F K R] holds for some [F] and [K]. *)

Lemma UF_Inv : forall D R V,
  UF D R V ==> UF D R V \* Hexists F K, \[Inv D F K R V].
Proof using.
  intros. unfold UF. xpull ;=> F K M HI HM. xsimpl*.
Qed.

(* Thus, any pure consequence of [Inv] is a consequence of [UF]. *)

Lemma UF_extract : forall D R V (P:Prop),
  (forall F K, Inv D F K R V -> P) ->
  UF D R V ==> UF D R V \* \[ P ].
Proof using.
  intros. unfold UF. xpull ;=> F K M HI HM. xsimpl*.
Qed.

(* -------------------------------------------------------------------------- *)

(* Public lemmas about the representation predicate. *)

(* If [UF D R V] holds, then [R] is idempotent. *)

Theorem UF_idempotent : forall D R V,
  UF D R V ==> UF D R V \* \[ idempotent R ].
Proof using.
  intros. applys UF_extract. eauto using idempotent_R.
Qed.

(* If [UF D R V] holds, then [R] preserves [D]. *)

Theorem UF_image : forall D R V,
  UF D R V ==> UF D R V \* \[ forall x, x \in D -> R x \in D ].
Proof using.
  intros. applys UF_extract. eauto using sticky_R.
Qed.

(* If [UF D R V] holds, then [R] is the identity outside of [D]. *)

Theorem UF_identity : forall D R V,
  UF D R V ==> UF D R V \* \[ forall x, x \notin D -> R x = x ].
Proof using.
  intros. applys UF_extract. eauto using R_is_identity_outside_D.
Qed.

(* If [UF D R V] holds, then [V] is compatible with [R]. *)

Theorem UF_compatible : forall D R V,
  UF D R V ==> UF D R V \* \[ forall x, x \in D -> V x = V (R x) ].
Proof using.
  intros. applys UF_extract. eauto.
Qed.

(* A summary of the above properties. Shown in the paper. *)

Theorem UF_properties : forall D R V,
  UF D R V ==> UF D R V \* \[
    (forall x, R (R x) = R x) /\
    (forall x, x \in D -> R x \in D) /\
    (forall x, x \notin D -> R x = x) /\
    (forall x, x \in D -> V x = V (R x))
  ].
Proof using.
  intros.
  xchange UF_idempotent; xpull; intros.
  xchange UF_image; xpull; intros.
  xchange UF_identity; xpull; intros.
  xchange UF_compatible; xpull; intros.
  xsimpl. splits; eauto.
Qed.

(* An empty instance of the Union-Find data structure can be created out of
   thin air. This is how the data structure is initialized. *)

Theorem UF_create : forall V,
  \[] ==> UF \{} id V.
Proof using.
  unfold UF. intro.
  hchange (>> Group_create (@Ref content)).
  hsimpl (\{} : set elem) (@empty elem) (fun (_ : elem) => arbitrary).
  { rewrite Phi_empty. rewrite~ credits_nat_zero_eq_prove. }
  { constructors.
    { rewrite~ dom_empty. }
    { intros. false. }
  }
  { constructors; simpl.
    { apply is_rdsf_empty. }
    { intro x. eapply is_repr_empty. }
    { eauto. }
  }
Qed.

Theorem UF_create_arbitrary :
  \[] ==> UF \{} id arbitrary.
Proof.
  eapply UF_create.
Qed.

(* Two separate instances of the UnionFind data structure can be merged, without
   actually doing anything at runtime. *)

(* In principle, we could also prove the converse property, [UF_split]. A UnionFind
   data structure can be split in two independent parts, provided the split respects
   the equivalence relation. Considering the amount of dreary work that went into
   establishing [UF_join], I leave this to future work. *)

Theorem UF_join : forall D1 R1 V1 D2 R2 V2,
  UF D1 R1 V1 \* UF D2 R2 V2 ==>
  UF
    (D1 \u D2)
    (fun x => If x \in D1 then R1 x else R2 x)
    (fun x => If x \in D1 then V1 x else V2 x)
  \* \[ D1 \# D2 ].
Proof using.
  unfold UF. intros. xpull ;=> F1 K1 M1 HI1 HM1 F2 K2 M2 HI2 HM2.
  sets D: (D1 \u D2).
  sets R: (fun x => If x \in D1 then R1 x else R2 x).
  sets V: (fun x => If x \in D1 then V1 x else V2 x).
  set (F := LibRelation.union F1 F2).
  set (K := fun x => If x \in D1 then K1 x else K2 x).
  set (M := LibBag.union M1 M2).

  (* Merge the group regions. We learn that their domains are disjoint. *)
  xchange~ Group_join. typeclass.
  xpull ;=> Hdom.
  rewrite~ LibMap.disjoint_def in Hdom.

  destruct HM1 as [ ? HM1 ].
  destruct HM2 as [ ? HM2 ].

  (* Disjointness lemmas. Trivial and painful. *)

  assert (forall x, x \in D1 -> x \in D2 -> False).
  { intros. subst D1 D2. set_norm. eauto. }

  assert (forall x, x \in D2 -> x \notin D1).
  { intros. intro. eauto. }

  assert (forall x y, F1 x y -> x \in D1).
  { eauto with confined is_dsf. }

  assert (forall x y, F2 x y -> x \in D2).
  { eauto with confined is_dsf. }

  assert (forall x, x \in D1 -> F1 x = F x).
  { intros. subst F. extens; intros y.
    unfold union; split; intros; try branches; eauto; false; eauto. }

  assert (forall x, x \in D2 -> F2 x = F x).
  { intros. subst F. extens; intros y.
    unfold union; split; intros; try branches; eauto; false; eauto. }

  assert (forall x, x \in D1 -> K1 x = K x).
  { intros. subst K. simpl. cases_if~. }

  assert (forall x, x \in D2 -> K2 x = K x).
  { intros. subst K. simpl. cases_if~. false; eauto. }

  assert (forall x, x \in D1 -> R1 x = R x).
  { intros. subst R. simpl. cases_if~. }

  assert (forall x, x \in D2 -> R2 x = R x).
  { intros. subst R. simpl. cases_if~. false; eauto. }

  assert (forall x, x \in D1 -> V1 x = V x).
  { intros. subst V. simpl. cases_if~. }

  assert (forall x, x \in D2 -> V2 x = V x).
  { intros. subst V. simpl. cases_if~. false; eauto. }

  assert (forall x, x \in D1 -> M1[x] = M[x]).
  { intros. subst D1 M. eauto using union_read_l. }

  assert (forall x, x \in D2 -> M2[x] = M[x]).
  { intros. subst D2 M. eauto using union_read_r. }

  (* Give the witnesses and decompose the goal. *)
  xsimpl F K M.

  (* Sum of the credits. *)
  { credits_join. subst D1 D2.
    rewrite (@Phi_join 1 _ D F K); eauto. }

  (* Disjointness of the domains. *)
  { subst D1 D2. eauto. }

  (* Preservation of [Mem]. *)
  { constructor.
    { subst M D D1 D2. eapply dom_union. }
    { intros x Dx. subst D. set_norm.
      unfold is_root in *.
      reveal_loc.
      destruct Dx as [ Dx | Dx ].
      { repeat match goal with h: forall_ x \in D1, _ |- _ =>
          specializes h Dx; try rewrite <- h
        end. exact HM1. }
      { repeat match goal with h: forall_ x \in D2, _ |- _ =>
          specializes h Dx; try rewrite <- h
        end. exact HM2. }
    }
  }

  (* Preservation of [Inv]. *)
  { destruct HI1, HI2. constructor.
    (* Preservation of [is_rdsf]. *)
    { subst D F K. eapply is_rdsf_join; eauto. subst D1 D2. eauto. }
    (* Preservation of the agreement between [R] and [F]. *)
    { intros x. subst F R. simpl. cases_if.
      (* Case: [x \in D1]. *)
      { eapply is_repr_join_direct_1; subst D1 D2; eauto. }
      (* Case: [x \notin D1]. *)
      { eapply is_repr_join_direct_2; subst D1 D2; eauto. }
    }
    (* Preservation of the compatibility of [V]. *)
    { intros x. subst V R. simpl. cases_if; [ | destruct (classic (x \in D2)) ].
      (* Case: [x \in D1]. *)
      { assert (R1 x \in D1). { eauto using sticky_R. }
        cases_if. intuition eauto. }
      (* Case: [x \in D2]. *)
      { assert (R2 x \in D2). { eauto using sticky_R. }
        cases_if. false; eauto. eauto. }
      (* Case: [x \notin D1 \u D2]. *)
      { assert (h: R2 x = x). { eapply R_is_identity_outside_D; eauto with is_dsf. }
        rewrite h. cases_if. reflexivity. }
    }
  }

Qed.

(* -------------------------------------------------------------------------- *)

(* Verification of [make]. *)

(* The function call [make v] requires [UF D R V] as well as O(1) time credits.
   It returns a new element [x], that is, [x] is not in [D]. It updates the
   data structure to [UF D' R' V'], where:
   1. [D'] is [D] extended with [x];
   2. [R'] is [R];
   3. [V'] is [V] extended with a mapping of [x] to [v]. *)

Theorem make_spec : forall D R V v,
  app UnionFind_ml.make [v]
    PRE  (UF D R V \* \$3)
    POST (fun x => Hexists D' V',
       UF D' R V' \* \[
         x \notin D /\
         D' = D \u \{x} /\
         R x = x /\
         V' = update1 V R x v
       ]).
Proof using.
  xcf. intros.
  xpay (2 + 1)%nat.
  unfold UF. xpull ;=> F K M HI HM. xapp. intros x.
  hchange (Group_add_fresh x). typeclass. typeclass. xpull ;=> Mx.
  asserts Dx: (x \notin D). rewrite~ <- (Mem_dom HM).
  forwards* EQ: Phi_extend. simpl in EQ. rewrite plus_comm in EQ.
  credits_join.
  rewrite EQ. xsimpl~.
  { splits; eauto using R_is_identity_outside_D. }
  { applys* Mem_make. }
  { applys* Inv_make. }
Qed.

(* The variant shown in the paper. *)

(* Note that, in this specific situation, [R x = x] holds, and [x]
   is the only inhabitant of its equivalence class, so the condition
   [R z = R x] is in fact equivalent to [z = x]. The function [V']
   maps [x] to [v] and elsewhere coincides with [V]. *)

Theorem make_spec_paper : forall D R V v,
  app UnionFind_ml.make [v]
    PRE  (UF D R V \* \$3)
    POST (fun x =>
     UF (D \u \{x}) R (fun z => If R z = R x then v else V z)
     \* \[ x \notin D ]).
Proof using.
  intros. xapp_spec make_spec.
  intros x. xpull; intros D' V' (?&?&?&?). subst D' V'. xsimpl~.
Qed.

(* -------------------------------------------------------------------------- *)

(* Verification of [find]. *)

(* Because [find] is a recursive function, we must begin with a specification
   that is amenable to an inductive proof. It states that [find] essentially
   implements the mathematical predicate [bw_ipc]. If [bw_ipc F x d F'] holds,
   which means that path compression at [x] requires [d] steps and changes the
   graph from [F] to [F'], then [find] requires [d+1] time credits and changes
   the memory from [M] to some [M'] that agrees with [F']. Furthermore, the
   value [r] returned by [find] is the representative of [x]. *)

Lemma find_spec_inductive: forall d D R F K F' M V x,
  Inv D F K R V ->
  Mem D F K V M ->
  x \in D ->
  bw_ipc F x d F' ->
  app UnionFind_ml.find [x]
   PRE  (Group Ref M \* \$ (d+1))
   POST (fun r' =>
     Hexists M', Group Ref M' \* \[ Mem D F' K V M' /\ r' = R x ]
    ).
Proof using.
  intros d. induction_wf IH: Wf_nat.lt_wf d. hide IH.
  introv HI HM Dx HC. xcf. xpay (d + 1)%nat.
  lets HMD: (Mem_dom HM).
  unfold UnionFind_ml.elem_ in *.
  xapps*. forwards* HV: (Mem_val HM) x.
  reveal_loc.
  xmatch; rename H0 into HK; rewrite <- HK in HV.
  (* Case: Root. *)
  { destruct HV as [HR HP]. inverts HC.
    { forwards*: is_root_R_self HR. xrets*. }
    { false* a_root_has_no_parent HR. }
  }
  (* Case: Link. *)
  { rename HV into HF. invert HC.
    { intros. subst F. subst. false* a_root_has_no_parent. }
    intros F'0 F'' x0 y0 z' d' HF' HR' HB' EF' Ex Ed EF''. subst F'' x0 d.
    lets* E: is_dsf_functional D HF HF'. subst y0. clear HF'.
    assert (y \in D). { eauto with confined is_dsf. }
    assert (z' = R y). { eapply is_repr_incl_R; eauto. } subst z'.
    xapp_spec~ IH (>> HI HM HB').
    replace (S d') with ((d'+1)%nat); [|omega]. chsimpl.
    intros M' (HM'&Er). lets HMD': (Mem_dom HM').
    reveal_loc.
    xapp~. xret. xsimpl. split~.
    { subst z. applys~ Mem_compress HM'. }
    assert (is_equiv F x y). { eauto using path_is_equiv with rtclosure. }
    assert (R x = R y). { eauto using is_equiv_incl_same_R. }
    congruence.
  }
Qed.

(* The function call [find x] requires [UF D R V] as well as O(alpha(D)) time
   credits. It preserves [UF D R V] and returns the representative of [x]. *)

Theorem find_spec : forall D R V x, x \in D ->
  app UnionFind_ml.find [x]
    PRE  (UF D R V \* \$(2 * alpha (card D) + 4))
    POST (fun y => UF D R V \* \[ R x = y ]).
Proof using.
  introv Dx. unfold UF. xpull ;=> F K M HI HM.
  forwards* (d&F'&HC&HP): amortized_cost_of_iterated_path_compression_simplified x.
  math_rewrite (2 * alpha (card D) + 4 = 2 * alpha (card D) + 3 + 1)%nat.
  xgc_credit HP. credits_split.
  xapply (>> find_spec_inductive HI HM Dx HC). chsimpl.
  intros z. xpull ;=> M' (HM'&Hz). subst z. xsimpl~.
  constructor; eauto using is_rdsf_bw_ipc, bw_ipc_preserves_RF_agreement.
Qed.

Hint Extern 1 (RegisterSpec UnionFind_ml.find) => Provide find_spec.

(* -------------------------------------------------------------------------- *)

(* Verification of [get]. *)

(* The function call [get x] requires [UF D R V] as well as O(alpha(D)) time
   credits. It preserves [UF D R V] and returns the data stored at [x]. *)

Theorem get_spec : forall D R V x, x \in D ->
  app UnionFind_ml.get [x]
    PRE  (UF D R V \* \$(2 * alpha (card D) + 5))
    POST (fun v => UF D R V \* \[ v = V x ]).
Proof using.
  introv Dx. xcf. xpay ((2 * alpha (card D) + 4) + 1)%nat.
  xapp~ as rx. intros Erx.
  unfold UF. xpull ;=> F K M HI HM.
  lets HMD: (Mem_dom HM). forwards* (Drx&Rrx): Inv_root x rx.
  xapps. subst~.
  forwards* EM: Mem_root rx.
  reveal_loc. rewrites (rm EM).
  (* later: rewrite* (>> Mem_root rx)   needs rewrite modulo conversion *)
  xmatch. xret. xsimpl~. rewrite <- Erx. rewrite~ <- (Inv_data HI).
Qed.

(* -------------------------------------------------------------------------- *)

(* Verification of [set]. *)

(* The function call [set x v] requires [UF D R V] as well as O(alpha(D)) time
   credits. It produces [UF D R V'], where [V'] is obtained from [V] by mapping
   the equivalence class of [x] to [v]. *)

Theorem set_spec : forall D R V x v,
  x \in D ->
  app UnionFind_ml.set [x v]
    PRE  (UF D R V \* \$(2 * alpha (card D) + 5))
    POST (# Hexists V', UF D R V' \* \[ V' = update1 V R x v ]).
Proof using.
  introv Dx. xcf. xpay ((2 * alpha (card D) + 4) + 1)%nat.
  xapp~ as rx. intros Erx.
  unfold UF. xpull ;=> F K M HI HM.
  lets HMD: (Mem_dom HM). forwards* (Drx&Rrx): Inv_root x rx.
  xapps. subst~.
  forwards* EM: Mem_root rx.
  reveal_loc. rewrites (rm EM).
  xmatch. xapps~. subst~. (* later: automate the subst~ here and elsewhere *)
  xsimpl~. applys* Mem_update1. applys~ Inv_update1.
Qed.

(* The variant shown in the paper. *)

Theorem set_spec_paper : forall D R V x v, x \in D ->
  app UnionFind_ml.set [x v]
    PRE  (UF D R V \* \$(2 * alpha (card D) + 5))
    POST (# UF D R (fun z => If R z = R x then v else V z)).
Proof using.
  intros. xapp_spec~ set_spec. intros _. xpull; intros V' ?. subst V'. xsimpl.
Qed.

(* -------------------------------------------------------------------------- *)

(* Verification of [eq]. *)

(* The function call [eq x y] requires [UF D R V] as well as O(alpha(D)) time
   credits. It preserves [UF D R V] and returns a Boolean outcome which
   indicates whether [x] and [y] are members of a common equivalence class. *)

Theorem eq_spec : forall D R V x y,
  x \in D -> y \in D ->
  app UnionFind_ml.eq [x y]
    PRE  (UF D R V \* \$(4 * alpha (card D) + 9))
    POST (fun b => UF D R V \* \[ b = isTrue (R x = R y) ]).
Proof using.
  introv Dx Dy. xcf.
  xpay (1 + (4 * alpha (card D) + 8))%nat.
  xcredit ((2 * alpha (card D) + 4) + (2 * alpha (card D) + 4))%nat.
  xapp* as x'. chsimpl. intros Rx.
  xapp* as y'. chsimpl. intros Ry.
  xapp. intros b. xsimpl. subst. rew_logic*.
Qed.

(* The variant shown in the paper. *)

Theorem eq_spec_paper : forall D R V x y, x \in D -> y \in D ->
  app UnionFind_ml.eq [x y]
    PRE  (UF D R V \* \$(4 * alpha (card D) + 9))
    POST (fun b => UF D R V \* \[ b = true <-> R x = R y ]).
Proof using.
  intros. xapp_spec~ eq_spec. intros b. xpull. intros ?. xsimpl. subst b.
  rewrite isTrue_eq_true. tauto.
Qed.

(* -------------------------------------------------------------------------- *)

(* Verification of [link]. (This is an internal function.) *)

(* The function call [link x y] requires [UF D R V] as well as O(1) time credits.
   It requires [x] and [y] to be roots. It chooses the new representative [z] to
   be one of [x] or [y], and returns [z], together with [UF D' R' V'], where:
   1. [D'] is [D], i.e., the domain is unchanged;
   2. [R'] is [R], where the equivalence classes of [x] and [y] are mapped to [z];
   3. [V'] is [V], where the equivalence classes of [x] and [y] are mapped to [V z].
*)

Lemma link_spec : forall D R V x y,
  x \in D ->
  y \in D ->
  R x = x ->
  R y = y ->
  app UnionFind_ml.link [x y]
    PRE  (UF D R V \* \$3)
    POST (fun z => UF D (update2 R R x y z) (update2 V R x y (V z)) \* \[ z = x \/ z = y ]
    ).
Proof using.
  introv Dx Dy Rx Ry. xcf.
  unfold UF. xpull ;=> F K M HI HM.
  xpay (2 + 1)%nat. xapps. xif.
  (* Case: [x == y]. *)
  { xret.
    erewrite update2_R_self by eauto.
    erewrite update2_V_self by eauto.
    xsimpl~. }
  (* Case: [x != y]. *)
  lets HMD: (Mem_dom HM).
  reveal_loc.
  xapps~. xapps~. xmatch.
  (* Case: [Root, Root]. *)
  { forwards* (HRx&Kx&Vx): Mem_root_inv x HM. substs.
    forwards* (HRy&Ky&Vy): Mem_root_inv y HM. substs.
    xif; [ | xif ].
    (* Sub-case: [K x < K y]. *)
    { Inv_link
      (* F' := *) (link F x y)
      (* K' := *) K
      (* V' := *) (update2 V R x y (V y))
                  x y
      (* z  := *) y.
      xapp*. xret. xsimpl*.
      applys~ Mem_link HI.
    }
    (* Sub-case: [K x > K y]. *)
    { Inv_link
      (* F' := *) (link F y x)
      (* K' := *) K
      (* V' := *) (update2 V R x y (V x))
                  x y
      (* z  := *) x.
      xapp*. xret. xsimpl*.
      applys~ Mem_link HI. applys update2_sym.
    }
    (* Sub-case: [K x = K y]. *)
    { assert (K x = K y). { math. }
      Inv_link
      (* F' := *) (link F y x)
      (* K' := *) (fupdate K x (1 + K x)%nat)
      (* V' := *) (update2 V R x y (V x))
                  x y
      (* z  := *) x.
      xapp~. xapp~.
      { apply~ LibMap.indom_update_already. }
      xret. chsimpl*. chsimpl.
      applys* Mem_link_incr HM. applys update2_sym.
    }
  }
  (* Case: non-[Root, Root]. This cannot happen. *)
  xfail.
  match goal with h: xnegpat _ |- _ => eapply h end.
  f_equal; applys* Mem_root.
Qed.

Hint Extern 1 (RegisterSpec UnionFind_ml.link) => Provide link_spec.

(* The variant shown in the paper. *)

Lemma link_spec_paper : forall D R V x y,
  x \in D -> y \in D -> R x = x -> R y = y ->
  app UnionFind_ml.link [x y]
    PRE  (UF D R V \* \$3)
    POST (fun z =>
          UF D (fun w => If R w = R x \/ R w = R y then z else R w)
               (fun w => If R w = R x \/ R w = R y then V z else V w)
          \* \[ z = x \/ z = y ]
    ).
Proof.
  exact link_spec.
Qed.

(* -------------------------------------------------------------------------- *)

(* Verification of [union]. *)

(* The function call [union x y] requires [UF D R V] as well as O(alpha(D)) time
   credits. It chooses the new representative [z] to be one of [x] or [y], and
   returns [z], together with [UF D' R' V'], where:
   1. [D'] is [D], i.e., the domain is unchanged;
   2. [R'] is [R], where the equivalence classes of [x] and [y] are mapped to [z];
   3. [V'] is [V], where the equivalence classes of [x] and [y] are mapped to [V z].
*)

Theorem union_spec : forall D R V x y,
  x \in D ->
  y \in D ->
  app UnionFind_ml.union [x y]
    PRE  (UF D R V \* \$(4 * alpha (card D) + 12))
    POST (fun z => Hexists R' V',
     UF D R' V' \* \[
       R' = update2 R R x y z /\
       V' = update2 V R x y (V z) /\
       (z = R x \/ z = R y)
     ]).
Proof using.
  introv Dx Dy. xcf.
  math_rewrite (4 * alpha (card D) + 12
  = 1 + 3 + (2 * alpha (card D) + 4) + (2 * alpha (card D) + 4))%nat.
  credits_split. xpay.
  xapp*. chsimpl. intros Rx.
  xapp*. chsimpl. intros Ry.
  subst.
  xchange UF_Inv. xpull ;=> F K HI. lets: sticky_R.
  forwards* HP: idempotent_R. hnf in HP.
  xapp*. intros r. xpull ;=> E.
  do 2 rewrites* (>> update2_root).
  xsimpl*.
Qed.

(* The variant shown in the paper. *)

Theorem union_spec_paper : forall D R V x y, x \in D -> y \in D ->
  app UnionFind_ml.union [x y]
    PRE  (UF D R V \* \$(4 * alpha (card D) + 12))
    POST (fun z =>
     UF D
       (fun w => If R w = R x \/ R w = R y then   z else R w)
       (fun w => If R w = R x \/ R w = R y then V z else V w)
     \* \[ z = R x \/ z = R y ]).
Proof using.
  intros. xapp_spec~ union_spec. intros z.
  xpull; intros R' V' (?&?&?). subst R' V'.
  xsimpl~.
Qed.

(* -------------------------------------------------------------------------- *)

End UnionFind.

(* -------------------------------------------------------------------------- *)

(* Make [elem] and [UF] abstract, as best we can. *)

Global Opaque UnionFind_ml.elem_ UF.

(* Register the public specifications, for use by clients. *)

Hint Extern 1 (RegisterSpec UnionFind_ml.make) => Provide make_spec.
Hint Extern 1 (RegisterSpec UnionFind_ml.find) => Provide find_spec.
Hint Extern 1 (RegisterSpec UnionFind_ml.get) => Provide get_spec.
Hint Extern 1 (RegisterSpec UnionFind_ml.set) => Provide set_spec.
Hint Extern 1 (RegisterSpec UnionFind_ml.eq) => Provide eq_spec.
Hint Extern 1 (RegisterSpec UnionFind_ml.union) => Provide union_spec.

(* -------------------------------------------------------------------------- *)

(* A TODO list for the future.

   Hide the inhabitation hypothesis [Inhab data], as much as possible.

   Introduce big-O's.

   Verify one or more algorithms which use UnionFind.

   Set up simplified API(s).
   1. an API where a PER B is used instead of D and R.
   2. an API where D disappears and R and V are finite maps.
   3. an API where V does not appear.

   E.g., for item 1 above: *)

Generalizable All Variables.

Definition UFB `{Inhab data}
  (B : binary (UnionFind_ml.elem_ data)) V
:=
  Hexists D R,
  UF _ D R V \*
  \[ forall x y, B x y = (x \in D /\ y \in D /\ R x = R y) ].

(* And, for item 2 above: *)

Definition UFM `{Inhab data}
  (R : map (UnionFind_ml.elem_ data) (UnionFind_ml.elem_ data))
  (V : map (UnionFind_ml.elem_ data) data)
:=
  Hexists D R' V',
  UF _ D R' V' \* \[
    D = dom R /\
    (forall x, x \in D -> R' x = R[x]) /\
    (forall x, x \in D -> V' x = V[x])
  ].

(* In order to check which axioms our proof uses, the following commands
   can be used. One should find:
   - a few logical axioms (propositional extensionality; functional
     extensionality; indefinite description; ...);
   - a fixed number of axioms posited in CFML's library;
   - for each OCaml function [f], a constant [f] of type [func] and a
     characteristic formula [f__cf].

Print Assumptions UF_idempotent.
Print Assumptions UF_image.
Print Assumptions UF_identity.
Print Assumptions UF_compatible.
Print Assumptions UF_properties.
Print Assumptions UF_create.
Print Assumptions UF_join.
Print Assumptions make_spec.
Print Assumptions find_spec.
Print Assumptions get_spec.
Print Assumptions set_spec.
Print Assumptions eq_spec.
Print Assumptions union_spec.

*)
