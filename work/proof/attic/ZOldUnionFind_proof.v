Set Implicit Arguments.
Require Import LibTactics.
Require Import CFHeaps CFLib.         (* why CFHeaps not part of CFLib? *)
Open Local Scope heap_scope_advanced. (* why not automatic? *)
Require Import UnionFind_ml.
Require Import LibSet LibRelation LibPer.
Require Import LibNatExtra.
Require Import InverseAckermann.
Require Import TLCBuffer CFMLBuffer.
Require Import UnionFind01Data.
Require Import UnionFind02EmptyCreate.
Require Import UnionFind04Compress.
Require Import UnionFind05Rank.
Require Import UnionFind06RankEmptyCreate.
Require Import UnionFind07RankLink.
Require Import UnionFind08RankCompress.
Require Import UnionFind09Potential.
Require Import UnionFind12Evolution.



(* -------------------------------------------------------------------------- *)

Section TypeParameter.
Parameter N : nat. (* The maximal number of vertices *)

Global Instance Inhab_content : Inhab content. (* important! *)
Proof. intros. applys (prove_Inhab (Root 0)). Qed.


(* -------------------------------------------------------------------------- *)

(* The abstract type [UFLoc] represents a vertex in some union-find data
   structure. *)

(* PUBLIC ABSTRACT *)
Definition UFLoc := loc.



Record st : Type := make_st {
  stD : set loc;     (* domain of the graph *)
  stF : binary loc;  (* edges of the graph *)
  stR : loc -> nat   (* ranks of the nodes *)
}.

Local Notation st_valid G :=
  (is_rdsf (stD G) (stF G) (stR G)).

Record st_compressed (G G':st) := {
  st_compressed_D : stD G' = stD G;
  st_compressed_F : is_repr (stF G') = is_repr (stF G);
  st_compressed_R : stR G' = stR G }.

Lemma st_compressed_domain:
  forall G G' x,
  st_compressed G G' ->
  x \in stD G ->
  x \in stD G'.
Proof using.
  inversion 1. congruence.
Qed.

Lemma st_compressed_trans : forall G2 G1 G3,
  st_compressed G1 G2 ->
  st_compressed G2 G3 ->
  st_compressed G1 G3.
Proof using.
  introv [D1 F1] [D2 F2]. constructors; congruence.
Qed.


Definition st_empty := 
  {|stD := \{} : set loc;
    stF := @empty loc; (* todo: use maximally-inserted argument A *)
    stR := (fun _ => 0%nat) (* todo: une définition existe ? *)
  |}.

(* The abstract predicate [UF B V] represents the ownership of one union-find
   data structure (as a whole). The parameter [B] is a partial equivalence
   relation (PER) on abstract locations. It encodes which abstract locations
   participate in the data structure and which are considered equivalent. The
   parameter [V] is a function of abstract locations to values, which respects
   [B]. It encodes the user-defined value that is associated with each
   equivalence class. *)

(* The existentially quantified [M], [D], [F], [R] can be viewed as auxiliary
   variables, or (semi-)phantom state, which the definition of [UF] connects
   with the parameters [B] and [V]. The map [M] describes the region of memory
   that we own; this region is populated with references to values of type
   [content A]. The parameters [D], [F], [R] represent the mathematical state
   of the union-find data structure, as in [UnionFind05Rank]. *)

(* The (purely logical) predicate [LUF B V M D F R] defines the relationships
   that must exist between these six parameters. *)


Record LUF
  (B : binary UFLoc)
  (M : map loc content)
  (G: st)
: Prop := {
  LUF_st :
    (* The mathematical data structure must be valid. *)
    st_valid G;
  LUF_per :
    (* The PER that is exposed to the client is the one that arises out of
       the mathematical data structure. *)
    B = dsf_per (stD G) (stF G);
  LUF_dom :
    (* The domain of the mathematical data structure is the footprint of
       the memory region. *)
    stD G = dom M;
  LUF_binds :
    (* The edges of the mathematical data structure are the pointers that
       exist in the memory region. *)
    stF G = (fun x y => binds M x (Link y));
  LUF_ranks :
    (* The ranks in the mathematical data structure agree with the ranks
       that are stored in memory (only at roots; no rank is stored at
       non-root vertices). (There is an implicit conversion; [r] has
       type [Z] whereas the rank of [x] has type [nat].) *)
    (forall x r, binds M x (Root r) -> r = (stR G) x)
 }.
  (* optional? assert that "dom G" contains nothing but keys that are roots *)


(* [UF B V] is now defined simply as a conjunction of 1- the ownership of
   the memory region described by [M] and 2- the purely logical predicate
   [LUF ...] and 3- an appropriate number of time credits. *)

Local Notation Phi_ G :=
  (Phi (stD G) (stF G) (stR G) N).

Definition UF (B : binary UFLoc) : hprop :=
  Hexists M G,
  Group (Ref Id) M \*
  [ LUF B M G ]    \*
  \$ (Phi_ G).

(* -------------------------------------------------------------------------- *)


(* This technical lemma helps establish a logical consequence of [UF B]. *)

(* If [UF B] holds, then [B] is a PER.*)

(* PUBLIC *)
Lemma UF_per:
  forall B,
  UF B ==>+ [ per B ].
Proof using.
  intros. unfold UF. hextract as M G Inv. hsimpl*.
  rewrite* (LUF_per Inv). applys* per_dsf_per.
  lets [? _]: (LUF_st Inv). eauto with is_dsf.
Qed.

Lemma LUF_per_dom : forall B M G,
  LUF B M G -> per_dom B = stD G.
Proof using.
  introv Inv. rewrite* (LUF_per Inv). 
  rewrite~ per_dom_dsf_per.
  lets [? _]: (LUF_st Inv). eauto with is_dsf.
Qed.

Lemma LUF_dom_M : forall B M G,
  LUF B M G -> dom M = per_dom B.
Proof using.
  introv Inv. rewrite (LUF_per_dom Inv). rewrite~ (LUF_dom Inv).
Qed.


(* -------------------------------------------------------------------------- *)
(* Hints *)

(* Use group specification for ml_get and ml_set *)
Hint Extern 1 (RegisterSpec ml_get) => Provide (@ml_get_spec_group content).
Hint Extern 1 (RegisterSpec ml_set) => Provide (@ml_set_spec_group content).


(* -------------------------------------------------------------------------- *)

(* Initialization. *)

(* An operation that does not involve any code, yet is part of the interface
   that we offer to the client, is the operation of creating a new (empty)
   instance of the union-find data structure. Because [UF] is an abstract
   predicate, without this operation, none of the other operations would be
   usable. *)

(* PUBLIC *)
Lemma UF_create :
  [] ==> UF (@empty _).
Proof using.
  unfold UF.
  hchange (>> Group_create (Ref (@Id content))).
  hsimpl \{} st_empty.
  simpl. rewrite Phi_empty. rewrite~ credits_zero.
  constructors; simpl.
    apply is_rdsf_empty.
    unfold dsf_per. rewrite~ confine_empty.
    rewrite~ dom_empty.
    binary_extensional.
    intros. false.
Qed.

Ltac luf :=
  match goal with h: LUF _ _ _ |- _ => inversion h; clear h end;
  simpl in *.

Lemma LUF_make : forall r B M G,
  LUF B M G ->
  r \notin (per_dom B) ->
  let D' := (stD G) \u \{r} in
  let G' := make_st D' (stF G) (stR G) in
  LUF (per_add_node B r) (M\(r:=Root 0)) G'.
Proof using.
  (* This proof is purely administrative. It is just a matter of
     converting assertions between different representations. *)
  intros r B M [ D F R ] ? ?. luf.
  assert (r \notin D).
  { subst B.
    rewrite per_dom_dsf_per in * by eauto with is_dsf.
    assumption. }
  constructor; simpl.
  { eauto using is_rdsf_create. }
  { rewrite dsf_per_create by eauto with is_dsf.
    subst B. reflexivity. }
  { subst D.
    rewrite dom_update_notin by assumption.
    reflexivity. }
  { subst D F. extens; intros x y.
    eauto using binds_update_neq_iff with typeclass_instances. }
  { intros z rz ?.
    forwards: binds_update_analysis. eauto. branches; unpack; [ eauto | ].
    subst. injections.
    erewrite is_rdsf_zero_rank_outside_domain by eauto.
    reflexivity. }
Qed.

Ltac st_compressed :=
  match goal with h: st_compressed _ _ |- _ => inversion h; clear h end;
  simpl in *; subst.

Lemma LUF_find : forall x y z B M M' G G',
  st_compressed G G' ->
  LUF B M G ->
  x \in stD G ->
  M\(x) = Link y ->
  LUF B M' G' ->
  is_repr (stF G') y z ->
  let F'' := compress (stF G') x z in
  let G'' := make_st (stD G') F'' (stR G') in
  LUF B (M'\(x:=Link z)) G''.
(* TEMPORARY the proposed statement could not be true: if M' matches G',
   then M'' matches some G'' which cannot be G'. Furthermore, even fixed
   like this, it remains problematic. I have already done some of the
   painful work by proving the equivalence between bw_ipc and fw_ipc,
   and I believe we should use that in the proof of find. *)
Proof using.
  intros. destruct G as [ D F R ]. destruct G' as [ D' F' R' ].
  st_compressed. do 2 luf.
  constructor; simpl.
  { constructor; simpl; subst F''.
    { eapply is_rdsf_compress. eauto. subst F'.
Admitted.

Lemma LUF_eq : forall B M G x y x' y',
  LUF B M G ->
  x \in per_dom B ->
  y \in per_dom B ->
  is_repr (stF G) x x' ->
  is_repr (stF G) y y' ->
  x' = y' <-> B x y.
Proof using.
  intros. destruct G as [ D F R ]. luf.
  subst B. rewrite per_dom_dsf_per in * by eauto with is_dsf.
  unfold dsf_per, confine. split.
  { intros. subst x'. splits; eauto with is_equiv. }
  { intros. unpack. eauto using is_repr_is_equiv_is_repr_bis with is_dsf. }
Qed.

Lemma LUF_root: forall x B M G,
  LUF B M G -> 
  x \in stD G ->
  is_root (stF G) x ->
  M\(x) = Root (stR G x).
Proof.
  intros. destruct G as [ D F R ]. luf.
  match goal with |- ?mx = _ => destruct mx eqn:? end.
  (* Case: [M(x)] is [Link _]. Impossible, since [x] is a root. *)
  { false.
    forwards: binds_prove. { subst D. eauto. } { eauto. }
    match goal with h: is_root _ _ |- _ => eapply h end.
    subst F. eassumption. }
  (* Case: [M(x)] is [Root _]. *)
  { forwards: binds_prove. { subst D. eauto. } { eauto. }
    f_equal. eauto. }
Qed.

Lemma LUF_is_repr_B : forall G B M x x',
  LUF B M G ->
  is_repr (stF G) x x' ->
  x \in (stD G) ->
  B x x'.
Proof.
  intros. destruct G as [ D F R ]. luf.
  assert (x' \in D).
  { eapply is_equiv_in_D_direct; eauto using is_repr_equiv_root with is_dsf. }
  subst B. unfold dsf_per, confine. splits; eauto using is_repr_equiv_root.
Qed.

(* Et ca c'est pour gérer link facilement dans la preuve du code *)

Definition LUF_link_hyp B M G x y rx ry :=
  LUF B M G /\
  x <> y /\
  x \in stD G /\
  y \in stD G /\
  is_root (stF G) x /\
  is_root (stF G) y /\
  M\(x) = Root rx /\
  M\(y) = Root ry.
  (* TODO ce serait bien si on pouvait toujours écrire "binds M x v"
     et jamais "M\(x) = v" car c'est pénible de convertir. En plus
     les deux ne sont équivalents que si x \in dom M, ce qui semble
     contre-intuitif. Les hypothèses Inhab sont étranges aussi. Un
     bon nettoyage de LibMap...? *)

Definition LUF_link_conc B G x y M' :=
  exists G',
  LUF (per_add_edge B x y) M' G' /\
  exists (n : nat), (Phi_ G + alpha N = Phi_ G' + n)%nat.

Lemma binds_discriminate:
  forall (M : map loc content) x rx y z,
  M\(x) = Root rx ->
  binds M y (Link z) ->
  y <> x.
Proof.
  introv h1 h2. intro. subst.
  erewrite binds_def in h2. destruct h2 as [ ? h2 ].
  rewrite h1 in h2. discriminate.
Qed.

Lemma binds_update_indom_iff_left:
  forall (M : map loc content) x rx y z,
  binds (M\(y:=Link z)) x (Root rx) ->
  binds M x (Root rx).
Proof.
  intros.
  rewrite <- binds_update_indom_iff in *. branches; unpack.
  assumption.
  congruence.
Qed.

Hint Rewrite my_Z_of_nat_def : rew_int_comp. (* TODO maybe add to LibInt? *)
Hint Rewrite <- Nat2Z.inj_lt : rew_int_comp. (* et hop *)

Ltac rank_arithmetic :=
  try subst;
  unfold rank in *;  (* the type [rank] expands to [Z] *)
  int_comp_to_zarith;
  try omega.

Lemma LUF_link_lt : forall B M G x y rx ry M',
  LUF_link_hyp B M G x y rx ry ->
  rx < ry ->
  M' = (M\(x:=Link y)) ->
  card (stD G) <= N ->
  LUF_link_conc B G x y M'.
Proof using.
  unfold LUF_link_hyp, LUF_link_conc.
  intros; unpack.
  destruct G as [ D F R ]. luf.
  assert (rx = R x). { subst D. eauto using binds_prove. }
  assert (ry = R y). { subst D. eauto using binds_prove. }
  exists (make_st D (link_by_rank_F F R x y) (link_by_rank_R R x y)).
  simpl. split.
  { constructor; simpl.
    { eauto using is_rdsf_link. }
    { subst B. rewrite dsf_per_add_edge_by_rank by eauto. reflexivity. }
    { eauto using dom_update_in_variant with typeclass_instances. }
    { unfold link_by_rank_F. by_cases_If.
      { unfold UnionFind03Link.link, union, per_single. subst F M'.
        extens. intros v w.
        rewrite <- binds_update_indom_iff.
        split; intros; branches; unpack; eauto using binds_discriminate with congruence. }
      { false. rank_arithmetic. }
    }
    { unfold link_by_rank_R. by_cases_If.
      { false. rank_arithmetic. }
      { subst M'. eauto using binds_update_indom_iff_left. }
    }
  }
  { eapply leq_to_eq_plus.
    eapply potential_increase_during_link; try reflexivity; eauto. }
Qed.

Check inj_lt.
SearchAbout Z_of_nat.

Axiom LUF_link_gt : forall B M G x y rx ry M',
  LUF_link_hyp B M G x y rx ry ->
  rx > ry ->
  M' = (M\(y:=Link x)) ->
  LUF_link_conc B G x y M'.

Axiom LUF_link_eq : forall B M G x y rx ry M',
  LUF_link_hyp B M G x y rx ry ->
  rx = ry ->
  M' = (M\(y:=Link x))\(x:=Root (rx+1)) ->
  LUF_link_conc B G x y M'.



(* These lemmas may be inlined if their proofs are trivial *)

Axiom is_repr_in_stD : forall G x y, (* st_valid G ->  if needed *) 
  is_repr (stF G) x y -> y \in (stD G).

Axiom is_repr_is_root : forall G x y, (* st_valid G ->  if needed *) 
  is_repr (stF G) x y -> is_root (stF G) y.

Axiom per_add_edge_equiv : forall (B:binary UFLoc) x' y' x y,
  B x x' -> B y y' ->
  per_add_edge B x y = per_add_edge B x' y'.



(* -------------------------------------------------------------------------- *)
(* Verification. *)


(* todo *)
Close Scope heap_scope_advanced. (* pour pas que hkeep s'affiche tt le temps *)


(* Creating a new vertex. *)

(* The specification below says:
   - the operation [make x] requires [UF B V],
                        and requires that [x] represent [X] at type [T];
   - it produces a new abstract location [r], not in the domain of [B];
   - it updates the data structure as follows:
       [r] is added to [B] as an isolated element, and
       a mapping of [r] to [X] is added to [V].
*)

Lemma make_spec :
  Spec make () |R>> forall B,
    R (\$ 1 \* UF B (* \* [card (per_dom B) < N] *))
        (fun r => [r \notin (per_dom B)] \*
                  UF (per_add_node B r)).
Proof using.
  xcf. intros. xpay.
  unfold UF. xextract as M G Inv.
  xapp. intros r. hchange (Group_add_fresh r).
  hextract as Mr.
  asserts Br: (r \notin per_dom B). rewrite~ <- (LUF_dom_M Inv). clear Mr.
  (* TODO: hsimpl picks the wrong G. *)
  (* need to use G' as in the conclusion of LUF_make,
     and apply Phi_extend to justify the potential *)
  (* hsimpl~. hsimpl. apply~ LUF_make. *)
Admitted.

Hint Extern 1 (RegisterSpec make) => Provide make_spec.



Lemma find_spec : 
  Spec find x |R>> forall B M G,
    LUF B M G -> x \in (stD G) -> 
    R (Group (Ref Id) M \* \$ alpha N)
        (fun (r:loc) => Hexists M' G', Group (Ref Id) M'  
           \* [LUF B M' G' /\ st_compressed G G' /\ is_repr (stF G') x r]).
Proof using.
  skip_goal IH. (* TODO: xinduction_heap *) hide IH. 
  xcf. introv Inv0 Bx. lets DM0: (LUF_dom Inv0).
  xpay_skip. (* todo: l'induction, c'est le seul truc difficile qui reste *) 
  xapp* as mx. congruence. intros Mx. xmatch.
  (* root *)
  xret. hsimpl* G. splits~. split~. 
    admit. (* francois easy: M(x)=Root _ -> is_repr x x *)
  (* link *)
  xapp_spec IH Inv0.
    admit. (* francois easy: l'image de M contient des valeurs dans dom M *)
  intros M' G' (Inv1&Fyz&FE).
  xapp. typeclass.
    lets DM1: (LUF_dom Inv1). rewrite (st_compressed_D Fyz) in DM1. congruence.
  xret. hsimpl G'.
  admit. (* TEMPORARY
  forwards* Inv2: LUF_find. splits~.
    lets: LUF_binds Inv2. admit.
    (* francois easy: link x y /\ is_repr y z -> is_repr x z *)
  *)
Qed.

Hint Extern 1 (RegisterSpec find) => Provide find_spec.

Lemma equiv_spec : 
  Spec UnionFind_ml.equiv (x : rref) (y : rref) |R>> forall B,
    x \in per_dom B -> y \in per_dom B ->
    R (UF B \* \$(1 + 2*(alpha N))) 
      (fun b => UF B \* [b = isTrue (B x y)]). 
Proof using.
  xcf. introv Dx Dy. rewrite two_times_unfold. credits_split. xpay.
  unfold UF at 1. xextract as M0 G0 Inv0.
  lets E: (LUF_per_dom Inv0). rewrite E in Dx,Dy.
  xapp* as x'. intros M1 G1 (Inv1&C1&R1).
  xapp* as y'. rewrite~ (st_compressed_D C1). intros M2 G2 (Inv2&C2&R2).
  forwards C: st_compressed_trans C1 C2.
  admit.
  (* TODO: use new spec of find, remove st_compressed_potential
  lets (n&EQ): st_compressed_potential C. rewrite EQ. credits_split.
  xapp. intros b. unfold UF. hsimpl~ M2 G2.
  subst. rew_logic. rewrite (st_compressed_F C2) in R2.
  applys* LUF_eq Inv1; rewrite E; assumption. *)
Qed.


Lemma link_spec :
  Spec UnionFind_ml.link x y |R>> forall B M G,
    LUF B M G ->
    x \in (stD G) -> y \in (stD G) -> (* needed? or derivable from is_root? *)
    is_root (stF G) x -> is_root (stF G) y ->
    card (stD G) <= N ->
    R (Group (Ref Id) M \* \$ (Phi_ G + alpha N + 1))
      (# Hexists M' G', Group (Ref Id) M' \* \$(Phi_ G')
          \* [LUF (per_add_edge B x y) M' G']). 
Proof using.
  xcf. introv Inv Dx Dy Rx Ry Card. sets_eq c: (Phi_ G + alpha N)%nat.
  credits_split. xpay. xapps. xif.
  (* case x != y *)
  lets E: (LUF_dom Inv). xapps. congruence. xapps. congruence. xmatch.
  (* case: x and y are roots *)
  xif.
  (* case rx < ry *)
  forwards* (G'&Inv'&n&EQ): (>> LUF_link_lt B M G x y rx ry). splits*.
  xapp. typeclass. congruence.
  intros _. subst c. rewrite EQ. credits_split. hsimpl~ G'. 
  xif.
  (* case rx > ry *)
  forwards* (G'&Inv'&n&EQ): (>> LUF_link_gt B M G x y rx ry). splits*.
  xapp. typeclass. congruence.
  intros _. subst c. rewrite EQ. credits_split. hsimpl~ G'. 
  (* case rx = ry *)
  forwards* (G'&Inv'&n&EQ): (>> LUF_link_eq B M G x y rx ry). splits*.
    rank_arithmetic.
  xapp. typeclass. congruence.
  xapp. typeclass. apply map_indom_update_already. typeclass. congruence.
  intros _. subst c. rewrite EQ. credits_split. hsimpl~ G'. 
  (* case: x or y not a root *)
  xfail ((rm C0) (stR G x) (stR G y)). fequals.
    applys* LUF_root Inv.
    applys* LUF_root Inv.
  (* case x == y *)
  subst c. credits_split. xret. hsimpl M G.
  admit. (* francois, rewrite sur: "per_add_edge B y y = B" *)
Qed.
  
Hint Extern 1 (RegisterSpec link) => Provide link_spec.


Lemma union_spec :
  Spec UnionFind_ml.union x y |R>> forall B,
    x \in (per_dom B) -> y \in (per_dom B) ->
    R (UF B \* \$(2+3*(alpha N))) (# UF (per_add_edge B x y)). 
Proof using.
  xcf. introv Dx Dy. rewrite three_times_unfold.
  math_rewrite (2 = 1+1)%nat. credits_split. xpay.
  unfold UF at 1. xextract as M G Inv0.
  lets E: (LUF_per_dom Inv0). rewrite E in Dx,Dy.
  xapp* as x'. intros M1 G1 (Inv1&C1&F1).
  xapp* as y'. rewrite~ (st_compressed_D C1). intros M2 G2 (Inv2&C2&F2).
  forwards C: st_compressed_trans C1 C2.
  admit.
  (* TODO: use new spec of find, remove st_compressed_potential
  lets (n&EQ): st_compressed_potential C. rewrite EQ. credits_split.
  xapp* Inv2.
    rewrite~ (st_compressed_D C2). applys* is_repr_in_stD. 
    applys* is_repr_in_stD.
    rewrite <- (st_compressed_F C2) in F1. applys* is_repr_is_root. 
    applys* is_repr_is_root. 
  credits_split. hsimpl. 
  hextract as M3 G3 Inv3. unfold UF. hsimpl M3 G3.
    rewrite* (per_add_edge_equiv B x' y').
      applys* LUF_is_repr_B Inv1. eauto using st_compressed_domain.
      applys* LUF_is_repr_B Inv2. eauto using st_compressed_domain. *)
Qed.

Hint Extern 1 (RegisterSpec union) => Provide union_spec.


End TypeParameter.

Global Opaque UFLoc UF.

