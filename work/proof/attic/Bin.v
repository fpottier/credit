

(* -------------------------------------------------------------------------- *)

(* We lift the representation predicate [T] through the type [content _].
   This definition is essentially boilerplate. It says, roughly, that a
   [Link] does not own its successor, and that a [Root] owns the value
   that is stored in it, to the extent prescribed by [T]. *)

Definition Content :  htype (content A) (content a) :=
  fun (cA : content A) (ca : content a) =>
  match cA, ca with
  | Link L, Link l =>
      [L = l]
  | Root R X, Root r x =>
      [R = r] \* T X x
  | _, _ =>
      [False]
  end.


Definition LUF
  (B : binary UFLoc) (V : UFLoc -> A)
  (M : map loc (content A))
  (D : set loc) (F : binary loc) (R : loc -> nat)
: Prop :=
  (* The mathematical data structure must be valid. *)
  is_rdsf D F R /\
  (* The PER that is exposed to the client is the one that arises out of
     the mathematical data structure. *)
  B = dsf_per D F /\
  (* The domain of the mathematical data structure is the footprint of
     the memory region. *)
  D = dom M /\
  (* The edges of the mathematical data structure are the pointers that
     exist in the memory region. *)
  F = (fun x y => binds M x (Link y)) /\
  (* The ranks in the mathematical data structure agree with the ranks
     that are stored in memory (only at roots; no rank is stored at
     non-root vertices). *)
  (forall x r v, binds M x (Root r v) -> r = R x) /\
  (* The function [V] that is exposed to the client agrees with the values
     stored in memory (only at roots; no value is stored at non-root
     vertices). *)
  (forall x r v, binds M x (Root r v) -> v = V x) /\
  (* The function [V] that is exposed to the client is compatible with
     the PER [B]. *)
  (forall x y, B x y -> V x = V y)
.
(* PUBLIC ABSTRACT *)
Definition UF
  (B : binary UFLoc) (V : UFLoc -> A)
: hprop :=
  Hexists M D F R,
  Group (Ref Content) M \* [ LUF B V M D F R ].



Lemma Content_Root : forall x X r,
  x ~> T X ==> (Root r x) ~> Content (Root r X).
Proof. intros. hunfolds~ Content. Qed.


(* PUBLIC *)
Lemma make_spec : (* changer le code ou la spec pour utiliser le meme nom de l'argument *)
  Spec make (x : a) |R>> forall B V (X : A),
    R (UF B V \* x ~> T X)
      (fun r => [r \notin (per_dom B)] \*
                UF (per_add_node B r) (fupdate V r X)).
Proof.
  xcf. intros. unfold UF. xextract as M D F R Inv.
  (* Allocate the new reference in the existing group region. *)
(*for debug, look at output: 
  xapp_show_types.
  xspec_show_types (@ml_ref_spec_group_general A a T).
 (* observe mismatch between a and content a *)
  lets H: (@ml_ref_spec_group_general _ _ Content).
  xspec_show_types H.
  lets HE: (spec_elim_1_1 H).
  try (xlet; xuntag). xapplys HE.
 (* this one is better *)
*)
xchange (@Content_Root v X).
xapp_spec (@ml_ref_spec_group_general _ _ Content).
hsimpl.
hsimpl.
skip.
skip.
Admitted.






(* This technical lemma helps establish a logical consequence of [UF B V]. *)

Lemma UF_LUF:
  forall (P : binary UFLoc -> (UFLoc -> A) -> Prop),
  forall B V,
  (forall M D F R, LUF B V M D F R -> P B V) ->
  UF B V ==>+ [ P B V ].
Proof.
  intros. unfold UF. hextract as M D F R. intros. hcancel; eauto.
Qed.

(* If [UF B V] holds, then [B] is a PER. *)

(* PUBLIC *)
Lemma UF_per:
  forall B V,
  UF B V ==>+ [ per B ].
Proof.
  intros. eapply UF_LUF with (P := fun B V => per B). intros.
  unfold LUF in *. unpack. subst B.
  eauto using per_dsf_per with is_dsf.
Qed.

(* If [UF B V] holds, then [V] respects [B]. *)

(* PUBLIC *)
Lemma UF_respects:
  forall B V,
  UF B V ==>+ [ forall x y, B x y -> V x = V y ].
Proof.
  intros. eapply UF_LUF with (P := fun B V => forall x y, B x y -> V x = V y ). intros.
  unfold LUF in *. unpack.
  eauto.
Qed.



(* -------------------------------------------------------------------------- *)

(* CFPrim *)
(* TEMPORARY
   does this look good? can we prove it based on [ml_ref_spec_group]? *)
Parameter ml_ref_spec_group_general : forall A a (T : htype A a),
  Spec ml_ref (v:a) |R>> forall (M:map loc A) (V : A),
    R (Group (Ref T) M \* v ~> T V) (fun (l:loc) => 
       Group (Ref T) (M\(l:=V)) \* [l \notindom M]).

(* lets [I1 I2 I3 I4 I5 I6 I7]: Inv. *)




Close Scope heap_scope_advanced. (* pour pas que hkeep s'affiche tt le temps *)




Lemma xok_post_lemma : forall B (Q1 Q2: B->hprop),
  Q1 = Q2 -> Q1 ===> Q2.
Proof using. intros. subst. intros x. hsimpl. Qed.

(*
Ltac xapp_final2 R HR :=
  eapply local_wframe; 
     [ xlocal
     | eapply HR
     | hsimpl
     | try clears R; try xok ].

Ltac xapp_inst args solver ::=
  let R := fresh "R" in let LR := fresh "L" R in 
  let KR := fresh "K" R in let IR := fresh "I" R in
  intros R LR KR; hnf in KR; (* lazy beta in *)
  let H := xapp_compact KR args in
  forwards_then H ltac:(fun HR => try xapp_final2 R HR);    
  try clears R; solver tt.
(* todo: should clear R in indirect subgoals *)
*)

(* utile : *)


(*Axiom ml_get_spec_group : forall a,
  Spec ml_get (l:loc) |R>> forall (M:map loc a), 
    forall `{Inhab a}, l \in (dom M: set _) ->
    keep R (Group (Ref Id) M) (fun x => [x = M\(l)]).
*)



Lemma pred_le_refl_eq : forall (A : Type) (P Q : A -> Prop), 
  P = Q -> P ==> Q.
Proof using. intros. subst. apply pred_le_refl. Qed. 
