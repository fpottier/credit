Set Implicit Arguments.
Require Import LibTactics.
Require Import CFHeaps CFLib.         (* why CFHeaps not part of CFLib? *)
Open Local Scope heap_scope_advanced. (* why not automatic? *)
Require Import UnionFind_ml.
Require Import LibSet LibRelation LibPer.
Require Import TLCBuffer CFMLBuffer.
Require Import UnionFind01Data.
Require Import UnionFind05Rank.
Require Import UnionFind06RankEmptyCreate.
Generalizable Variable a A.

(* todo: move? generate? *)
Global Instance Inhab_content : forall `{Inhab a}, Inhab `{content a}.
Proof. intros. applys (prove_Inhab (Root 0 arbitrary)). Qed.


Ltac xpay_start tt :=
  xuntag tag_pay; apply local_erase; esplit; split.

Ltac xpay_core tt :=
  xpay_start tt; [ unfold pay_one; hsimpl | ].

Axiom pay_one_fake : forall H, pay_one H H.
Ltac xpay_fake tt :=
  xpay_start tt; [ apply pay_one_fake  | ].


Tactic Notation "xpay" := xpay_core tt.
Tactic Notation "xpay_skip" := xpay_fake tt.



(* -------------------------------------------------------------------------- *)

Section TypeParameter.

Variable A : Type.

(* -------------------------------------------------------------------------- *)

(* The abstract type [UFLoc] represents a vertex in some union-find data
   structure. *)

(* PUBLIC ABSTRACT *)
Definition UFLoc := loc.

(* The abstract predicate [UF B V] represents the ownership of one union-find
   data structure (as a whole). The parameter [B] is a partial equivalence
   relation (PER) on abstract locations. It encodes which abstract locations
   participate in the data structure and which are considered equivalent. The
   parameter [V] is a function of abstract locations to values, which respects
   [B]. It encodes the user-defined value that is associated with each
   equivalence class. *)

(* The existentially quantified [M], [D], [F], [R] can be viewed as auxiliary
   variables, or (semi-)phantom state, which the definition of [UF] connects
   with the parameters [B] and [V]. The map [M] describes the region of memory
   that we own; this region is populated with references to values of type
   [content A]. The parameters [D], [F], [R] represent the mathematical state
   of the union-find data structure, as in [UnionFind05Rank]. *)

(* The (purely logical) predicate [LUF B V M D F R] defines the relationships
   that must exist between these six parameters. *)


(* TODO: bundle  D F R into a single variable *)

Record LUF
  (B : binary UFLoc) (V : UFLoc -> A)
  (M : map loc (content A))
  (D : set loc) (F : binary loc) (R : loc -> nat)
: Prop := {
  LUF_rdsf :
    (* The mathematical data structure must be valid. *)
    is_rdsf D F R;
  LUF_per :
    (* The PER that is exposed to the client is the one that arises out of
       the mathematical data structure. *)
    B = dsf_per D F;
  LUF_dom :
    (* The domain of the mathematical data structure is the footprint of
       the memory region. *)
    D = dom M;
  LUF_binds :
    (* The edges of the mathematical data structure are the pointers that
       exist in the memory region. *)
    F = (fun x y => binds M x (Link y));
  LUF_ranks :
    (* The ranks in the mathematical data structure agree with the ranks
       that are stored in memory (only at roots; no rank is stored at
       non-root vertices). *)
    (forall x r v, binds M x (Root r v) -> r = R x);
  LUF_roots :
    (* The function [V] that is exposed to the client agrees with the values
       stored in memory (only at roots; no value is stored at non-root
       vertices). *)
    (forall x r v, binds M x (Root r v) -> V x = v);
 LUF_descr :
    (* The function [V] that is exposed to the client is compatible with
       the PER [B]. *)
    (forall x y, B x y -> V x = V y)
 }.
  (* optional? assert that "dom G" contains nothing but keys that are roots *)


(* [UF B V] is now defined simply as a conjunction of 1- the ownership of
   the memory region described by [M] and 2- the purely logical predicate
   [LUF ...]. *)

Require Import UnionFind09Potential.

Parameter N : nat.

Definition UF
  (B : binary UFLoc) (V : UFLoc -> A)
: hprop :=
  Hexists M D F R, 
   Group (Ref Id) M \* [ LUF B V M D F R ] \* \$ (Phi D F R N).
 

(* temp *)
Definition UF_def B V M D F R :=
  Group (Ref Id) M \* [ LUF B V M D F R ] \* \$ (Phi D F R N).



(* -------------------------------------------------------------------------- *)


Definition respects (B : binary UFLoc) (V : UFLoc -> A) :=
  forall x y, B x y -> V x = V y.

(* This technical lemma helps establish a logical consequence of [UF B V]. *)

(* If [UF B V] holds, then [B] is a PER and  [V] respects [B].*)

(* PUBLIC *)
Lemma UF_per:
  forall B V,
  UF B V ==>+ [ per B /\ respects B V ].
Proof using.
  intros. unfold UF. hextract as M D F R H. hsimpl*.
  destruct H; subst B. eauto using per_dsf_per with is_dsf.
Qed.


Lemma LUF_per_dom : forall B V M D F R,
  LUF B V M D F R -> per_dom B = D. (* = dom M. *)
Proof using.  
  introv [H1 H2 H3 _ _ _ _].
  rewrite H2. rewrite~ per_dom_dsf_per.
  applys* is_rdsf_is_dsf.
Qed.



(* -------------------------------------------------------------------------- *)
(* Hints *)

(* Use group specification for ml_get and ml_set *)
Hint Extern 1 (RegisterSpec ml_get) => Provide (@ml_get_spec_group (content A)).
Hint Extern 1 (RegisterSpec ml_set) => Provide (@ml_set_spec_group (content A)).


(* -------------------------------------------------------------------------- *)

(* Initialization. *)

(* An operation that does not involve any code, yet is part of the interface
   that we offer to the client, is the operation of creating a new (empty)
   instance of the union-find data structure. Because [UF] is an abstract
   predicate, without this operation, none of the other operations would be
   usable. *)

Lemma pred_le_refl_eq : forall (A : Type) (P Q : A -> Prop), 
  P = Q -> P ==> Q.
Proof using. intros. subst. apply pred_le_refl. Qed. 

(* PUBLIC *)
Lemma initialization:
  forall V,
  [] ==> UF (@empty _) V.
Proof using.
  intro. unfold UF.
  hchange (>> Group_create (Ref (@Id (content A)))).
  hsimpl \{}. (* todo: mettre des relations vides explicitement ?
    ou alors résoudre l'autre but en premier
    (nouvelle tactique "rotate" pour échanger les buts) *)
  admit. (* todo: potentiel zero initialement *)
  constructors.
    eapply is_rdsf_empty.
    unfold dsf_per. rewrite confine_empty. reflexivity.
    rewrite dom_empty. reflexivity. 
    binary_extensional.
    intros. false. 
    intros. false. 
    intros. false.
Qed. 



(* -------------------------------------------------------------------------- *)
(* Verification. *)


(* todo *)
Close Scope heap_scope_advanced. (* pour pas que hkeep s'affiche tt le temps *)




(* Creating a new vertex. *)

(* The specification below says:
   - the operation [make x] requires [UF B V],
                        and requires that [x] represent [X] at type [T];
   - it produces a new abstract location [r], not in the domain of [B];
   - it updates the data structure as follows:
       [r] is added to [B] as an isolated element, and
       a mapping of [r] to [X] is added to [V].
*)

Lemma make_spec :
  Spec make (x : A) |Beh>> forall B V (X : A),
    Beh (\$ 1 \* UF B V)
        (fun r => [r \notin (per_dom B)] \*
                  UF (per_add_node B r) (fupdate V r X)).
Proof using.
  xcf. intros. xpay.
  unfold UF. xextract as M D F R Inv.
  xapp. intros r. hchange (Group_add_fresh r).
  hextract as Mr. hsimpl. hsimpl.
    admit. (* Francois! *)
    rewrite (LUF_per_dom Inv). rewrite~ (LUF_dom Inv). 
Admitted.

Hint Extern 1 (RegisterSpec make) => Provide make_spec.



Lemma find_spec : forall `{Inhab A},
  Spec find (x: rref A) |Beh>> forall B V M D F R,
    LUF B V M D F R -> x \in D -> 
    Beh (Group (Ref Id) M)
        (fun (r:loc) => Hexists M' F' R', Group (Ref Id) M'  
           \* [LUF B V M' D F' R'
               /\ is_repr F x r
               /\ is_repr F = is_repr F']).
Proof using.
  intros IA. skip_goal IH. (* TODO: xinduction_heap *)
  hide IH. 
  xcf. introv Inv0 Bx. lets DM0: (LUF_dom Inv0).
  xpay_skip. (* todo *) 
  xapp* as mx. intros Mx. xmatch.
  (* root *)
  xret. hsimpl*. admit. (* francois *)
  (* link *)
  xapp_spec IH Inv0.
    admit. (* francois: M contient des valeurs dans dom M *)
  intros M' F' R' (Inv1&Fyz&FE).
  xapp. typeclass. lets DM1: (LUF_dom Inv1). rewrite~ <- DM1.
  xret. hsimpl. splits. 
    admit. (* francois: transitivité de is_repr via link *)
    admit. (* francois: update de LUF avec un arc: dans un lemme *)
    apply FE.
Admitted.

Hint Extern 1 (RegisterSpec find) => Provide find_spec.



Lemma eq_spec : forall `{Inhab A},
  Spec UnionFind_ml.eq (x : rref A) (y : rref A) |Beh>> forall B V,
    x \in per_dom B -> y \in per_dom B ->
    keep Beh (UF B V) (\= isTrue (is_equiv B x y)). 
Proof using.
  intros IA. xcf. introv Dx Dy. xpay_skip.
  unfold UF at 1. xextract as M0 D F0 R0 Inv0.
  lets E: (LUF_per_dom Inv0). rewrite E in Dx,Dy.
  xapp* as x'. typeclass.
  intros M1 F1 R1 (Inv1&Fx&FE1).
  xapp* as y'. typeclass.
  intros M2 F2 R2 (Inv2&Fy&FE2).
  xapp. intros b. unfold UF.
  sets k: (\$Phi D F0 R0 N). hsimpl. 
    skip.
    eauto.
    subst. rew_logic. admit. (* francois! *)
Admitted.


Lemma link_spec :
  Spec UnionFind_ml.link x y |Beh>> forall B V M D F R,
    LUF B V M D F R ->
    x \in D -> y \in D ->  (* needed? or derivable from is_root? *)
    is_root F x -> is_root F y ->
    Beh (Group (Ref Id) M) 
      (# Hexists M' F' R', Group (Ref Id) M' 
          \* [LUF (per_add_edge B x y) V M' D F' R']). 
Proof using.

(* notes:a
let link (f : 'a -> 'a -> 'a) (x : 'a rref) (y : 'a rref) : unit =
  if x != y then
    match !x, !y with
    | Root (rx, vx), Root (ry, vy) ->
        let v = f vx vy in
        if rx < ry then begin
          x := Link y;
          y := Root (ry, v)
        end
        else begin
          y := Link x;
          let r = if ry < rx then rx else rx + 1 in
          x := Root (r, v)
        end
    | _, _ ->
        assert false

*)
Admitted.
  
Hint Extern 1 (RegisterSpec link) => Provide link_spec.


Lemma union_spec :
  Spec UnionFind_ml.union (x : rref A) (y : rref A) |R>> forall B V,
    x \in (per_dom B) -> y \in (per_dom B) ->
    R (UF B V) (# Hexists V', UF (per_add_edge B x y) V'). 
Proof using.
  xcf. introv Dx Dy. xpay_skip.
  (* TODO: factorize the code and/or the proof with eq_spec *)
  unfold UF at 1. xextract as M D F R Inv0.
  lets E: (LUF_per_dom Inv0). rewrite E in Dx,Dy.
  xapp* as x'. typeclass.
  intros M1 F1 R1 (Inv1&Fx&FE1).
  xapp* as y'. typeclass.
  intros M2 F2 R2 (Inv2&Fy&FE2).
  xapp*. 
    admit. (* francois *) 
    admit. (* francois *)
    admit. (* francois *) 
    admit. (* francois *)
  hextract as M3 F3 R3 Inv3. unfold UF.
  sets k: (\$Phi D F R N). hsimpl.
    skip.
    admit. (* francois: add edge on x-y equivalent to x'-y' *)
(* TODO is_equiv_per_add_edge:
  is_equiv link =
  per_add_edge (is_equiv F) x y. *)
Admitted.

Hint Extern 1 (RegisterSpec union) => Provide union_spec.


End TypeParameter.

Global Opaque UFLoc UF.




















